package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.DecrementInventoryState;
import com.ami.abc.ami_app.models.DecrementInventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DecrementInventoryRepo {
    private ApiClient mApiClient;

    //constructor
    public DecrementInventoryRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<DecrementInventoryState> decrementInventory(String accessToken, int inventoryId, int quantity) {

        MutableLiveData<DecrementInventoryState> decrementInventoryStateMutableLiveData = new MutableLiveData<>();
        Call<DecrementInventory> call = mApiClient.amiService().decrementInventory(accessToken,inventoryId,quantity, Constants.DEC_TYPE);
        call.enqueue(new Callback<DecrementInventory>() {
            @Override
            public void onResponse(Call<DecrementInventory> call, Response<DecrementInventory> response) {
                if (response.code() == 200) {
                    decrementInventoryStateMutableLiveData.setValue(new DecrementInventoryState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    decrementInventoryStateMutableLiveData.setValue(new DecrementInventoryState(loginError));
                }
                else {
                    decrementInventoryStateMutableLiveData.setValue(new DecrementInventoryState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<DecrementInventory> call, Throwable t) {
                decrementInventoryStateMutableLiveData.setValue(new DecrementInventoryState(t));
            }
        });

        return decrementInventoryStateMutableLiveData;

    }
}

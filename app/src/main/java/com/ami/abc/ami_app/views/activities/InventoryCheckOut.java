package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;

import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.FailError;

import com.ami.abc.ami_app.models.InventoryCheckOutReport;
import com.ami.abc.ami_app.models.InventoryCheckedOutPoint;

import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.CheckOutInventoryViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryCheckOut extends AppCompatActivity {

    @BindView(R.id.asset_id)
    EditText asset_code;


    @BindView(R.id.quantity)
    TextInputEditText quantity;

    private List<String> custodianName = new ArrayList<>();
    @BindView(R.id.custodian)
    AutoCompleteTextView custodian;
    @BindView(R.id.custodian_title)
    TextView enterCustodian;
    @BindView(R.id.input_custodian_layout)
    RelativeLayout inputCustodianLayout;
//    @BindView(R.id.custodian_drop_down)
//    ImageView custodian_drop_down;

    @BindView(R.id.check_out)
    Button check_out;

    CheckOutInventoryViewModel checkOutInventoryViewModel;

    String building, room, depart, assetId, custodian_tag, barCode, quantityOut;
    String accessToken;

    @BindView(R.id.holder_layout)
    LinearLayout holder_layout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_check_out);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.check_out));

        checkOutInventoryViewModel = ViewModelProviders.of(this).get(CheckOutInventoryViewModel.class);


        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");

        //specific for scan tag
        custodian_tag = scanIntents.getStringExtra("custodianTag");
        barCode = scanIntents.getStringExtra("barCode");
        quantityOut = scanIntents.getStringExtra("quantityOut");


        if (assetId != null) {
            asset_code.setText(assetId);
        } else {
            asset_code.setText(barCode);

        }

        custodian.setText(custodian_tag);
        quantity.setText(quantityOut);

        checkOutInventoryViewModel.getAllCustodiansResponse().observe(this, custodianState -> {
            assert custodianState != null;
            if (custodianState.getAllCustodians() != null) {
                handleCustodians(custodianState.getAllCustodians());
            }
            if (custodianState.getMessage() != null) {
                handleNetworkResponse(custodianState.getMessage());
            }
            if (custodianState.getErrorThrowable() != null) {
                handleError(custodianState.getErrorThrowable());
            }
            if (custodianState.getLoginError() != null) {
                handleUnAuthorized(custodianState.getLoginError());
            }
        });

        checkOutInventoryViewModel.checkOutInventoryResponse().observe(this, checkOutInventoryState -> {
            assert checkOutInventoryState != null;
            if (checkOutInventoryState.getInventoryCheckedOutPoint() != null) {
                handleInventoryCheckOut(checkOutInventoryState.getInventoryCheckedOutPoint());
            }

            if (checkOutInventoryState.getErrorThrowable() != null) {
                handleError(checkOutInventoryState.getErrorThrowable());

            }
            if (checkOutInventoryState.getMessage() != null) {
                handleNetworkResponse(checkOutInventoryState.getMessage());
            }
            if (checkOutInventoryState.getLoginError() != null) {
                handleUnAuthorized(checkOutInventoryState.getLoginError());
            }
            if (checkOutInventoryState.getFailError() != null) {
                handleFailError(checkOutInventoryState.getFailError());
            }
        });


        enterCustodian.setOnClickListener(v -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Scanner : ");
            // prevent cancel of AlertDialog on click of back button and outside touch
            alert.setCancelable(false);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                inputCustodianLayout.setVisibility(View.VISIBLE);
                inputCustodianLayout.requestFocus();
//                holder_layout.setAlpha(0.4f);
//                progressBar.setVisibility(View.VISIBLE);

            }));

            alert.setPositiveButton("Camera", (dialog, which) -> {
                String code = asset_code.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(this, "Enter Your Code First", Toast.LENGTH_SHORT).show();
                } else {
                    Intent scanTag = new Intent(this, ScanActivity.class);
                    scanTag.putExtra("TAG", "Z");
                    scanTag.putExtra("barCode", code);
                    scanTag.putExtra("quantityOut", quantity.getText().toString().trim());
                    startActivity(scanTag);

                    inputCustodianLayout.setVisibility(View.VISIBLE);
                }
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();

        });

        //set up custodian drop down
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, custodianName);
        custodian.setAdapter(adapter2);

//        custodian_drop_down.setOnClickListener(v -> custodian.showDropDown());
//        custodian.setOnClickListener(v -> custodian.showDropDown());


        check_out.setOnClickListener(v -> {
            String productCode = asset_code.getText().toString().trim();
            String cust = custodian.getText().toString().trim();
            int quant = Integer.parseInt(quantity.getText().toString());

            if (TextUtils.isEmpty(cust)) {
                Toast.makeText(this, "Please enter the custodian", Toast.LENGTH_SHORT).show();

            } else if (quant == 0) {
                Toast.makeText(this, "Please enter the current quantity you want to check out", Toast.LENGTH_SHORT).show();
            } else {

                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                checkOutInventoryViewModel.checkOutInventory("Bearer " + accessToken, productCode, cust, quant);
            }

        });

    }

    private void handleCustodians(AllCustodians allCustodians) {

        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        boolean status = allCustodians.getStatus();
//        if (status) {
//            List<Custodian> custodianList = allCustodians.getCustodians();
//
//            List<Integer> codes = new ArrayList<>();
//            for (Custodian custodian : custodianList) {
//                codes.add(custodian.getBarcode());
//            }
//            String code = custodian.getText().toString();
//            if (codes.contains(code)){
//
//            }
//        }
    }


    private void handleInventoryCheckOut(InventoryCheckedOutPoint inventoryCheckedOutPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = inventoryCheckedOutPoint.getStatus();
        if (status) {
            InventoryCheckOutReport inventoryReports = new InventoryCheckOutReport();
            inventoryReports.setId(inventoryCheckedOutPoint.getInventoryCheckedOut().getInventoryId());
            inventoryReports.setBarcode(assetId);
            inventoryReports.setName("inventory");
            inventoryReports.setCount(Integer.parseInt(inventoryCheckedOutPoint.getInventoryCheckedOut().getQuantity()));
            checkOutInventoryViewModel.saveScannedAsset(inventoryReports);

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(inventoryCheckedOutPoint.getMessage());
            alert.setCancelable(false);
            alert.setMessage("Do you want to scan continue checking out ?");

            alert.setNeutralButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNegativeButton("Scanner", (dialog, which) -> {
                        Intent scanAsset = new Intent(this, InventoryCheckOut.class);
                        scanAsset.putExtra("building", building);
                        scanAsset.putExtra("room", room);
                        scanAsset.putExtra("mDepartment", depart);
                        scanAsset.putExtra("TAG", "G");
                        startActivity(scanAsset);
                        dialog.dismiss();
                    }
            );
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "G");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        }
    }

    private void handleNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setNeutralButton("Edit Details", (dialog, which) -> {
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, InventoryCheckOut.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "G");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "G");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private void handleError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }

    }

    private void handleFailError(FailError failError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        //Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(failError.getMessage());

        alert.setNeutralButton("Edit Details", (dialog, which) -> {
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, InventoryCheckOut.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "G");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "G");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void handleUnAuthorized(LoginError loginError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }
}

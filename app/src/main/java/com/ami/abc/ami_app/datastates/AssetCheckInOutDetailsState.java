package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AssetCheckInOutDetails;

public class AssetCheckInOutDetailsState {
    private AssetCheckInOutDetails assetCheckInOutDetails;
    private String message;
    private Throwable errorThrowable;

    public AssetCheckInOutDetailsState(AssetCheckInOutDetails assetCheckInOutDetails) {
        this.assetCheckInOutDetails = assetCheckInOutDetails;
        this.message = null;
        this.errorThrowable = null;
    }

    public AssetCheckInOutDetailsState(String message) {
        this.message = message;
        this.assetCheckInOutDetails = null;
        this.errorThrowable = null;
    }

    public AssetCheckInOutDetailsState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.assetCheckInOutDetails = null;
    }

    public AssetCheckInOutDetails getAssetCheckInOutDetails() {
        return assetCheckInOutDetails;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

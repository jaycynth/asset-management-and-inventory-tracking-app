package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.InventoryCheckOutReportDao;
import com.ami.abc.ami_app.models.InventoryCheckOutReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class InventoryCheckOutReportsRepo {
    private InventoryCheckOutReportDao inventoryCheckOutReportDao;
    private Executor executor;

    public InventoryCheckOutReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        inventoryCheckOutReportDao = AppDatabase.getDatabase(AMI.context).inventoryCheckOutReportDao();

    }


    public void saveInventorys(InventoryCheckOutReport inventorys) {
        executor.execute(() -> inventoryCheckOutReportDao.saveInventory(inventorys));
    }


    public LiveData<List<InventoryCheckOutReport>> getInventorysReports() {
        return inventoryCheckOutReportDao.getInventorysReports();
    }



    public void deleteInventorys() {
        executor.execute(() -> inventoryCheckOutReportDao.deleteInventorys());
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryCheckPointState;
import com.ami.abc.ami_app.models.InventoryCheckPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryCheckRepo {
    private ApiClient mApiClient;
    //constructor
    public InventoryCheckRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<InventoryCheckPointState> inventoryCheck(String accessToken) {

        MutableLiveData<InventoryCheckPointState> inventoryCheckPointStateMutableLiveData = new MutableLiveData<>();
        Call<InventoryCheckPoint> call = mApiClient.amiService().inventoryCheck(accessToken);
        call.enqueue(new Callback<InventoryCheckPoint>() {
            @Override
            public void onResponse(Call<InventoryCheckPoint> call, Response<InventoryCheckPoint> response) {
                if (response.code() == 200) {
                    inventoryCheckPointStateMutableLiveData.setValue(new InventoryCheckPointState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryCheckPointStateMutableLiveData.setValue(new InventoryCheckPointState(loginError));
                }else {
                    inventoryCheckPointStateMutableLiveData.setValue(new InventoryCheckPointState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<InventoryCheckPoint> call, Throwable t) {
                inventoryCheckPointStateMutableLiveData.setValue(new InventoryCheckPointState(t));
            }
        });

        return inventoryCheckPointStateMutableLiveData;

    }
}

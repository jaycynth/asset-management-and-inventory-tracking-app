package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInventoryGone {
    @Expose
    private Boolean status;
    @SerializedName("inventoryout")
    @Expose
    private InventoryGone inventoryout;
    @SerializedName("remainingout")
    @Expose
    private Integer remainingout;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public InventoryGone getInventoryout() {
        return inventoryout;
    }

    public void setInventoryout(InventoryGone inventoryout) {
        this.inventoryout = inventoryout;
    }

    public Integer getRemainingout() {
        return remainingout;
    }

    public void setRemainingout(Integer remainingout) {
        this.remainingout = remainingout;
    }

}

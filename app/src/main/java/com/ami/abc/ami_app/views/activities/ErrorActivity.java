package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.ErrorViewModel;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ErrorActivity extends AppCompatActivity {

    @BindView(R.id.error_button)
    Button errorButton;

    String Tag;

    String token;

    ErrorViewModel errorViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        ButterKnife.bind(this);

        errorViewModel = ViewModelProviders.of(this).get(ErrorViewModel.class);
        token = SharedPreferenceManager.getInstance(this).getToken();

        Intent errorIntent = getIntent();
        Tag = errorIntent.getStringExtra("TAG");


        errorViewModel.getBuildingsResponse().observe(this, buildingsState -> {
            if (buildingsState.getAllBuildings() != null) {
                handleGetAllBuildings(buildingsState.getAllBuildings());
            }

            if (buildingsState.getMessage() != null) {
                handleNetworkResponse(buildingsState.getMessage());
            }

            if (buildingsState.getErrorThrowable() != null) {
                handleError(buildingsState.getErrorThrowable());
            }


        });

        errorButton.setOnClickListener(v->{
            if(Tag.equals("A")){

                errorViewModel.getAllBuildings("Bearer " + token);
                Toast.makeText(this, "This is from main activity", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void handleError(Throwable errorThrowable) {

        if (errorThrowable instanceof IOException) {
            Intent errorIntent = new Intent(this, ErrorActivity.class);
            errorIntent.putExtra("TAG", "A");
            errorIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(errorIntent);
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message){

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
    private void handleGetAllBuildings(AllBuildings allBuildings) {

        boolean status = allBuildings.getStatus();
        if (status) {
            Toast.makeText(this, "true", Toast.LENGTH_SHORT).show();
        }
    }

}

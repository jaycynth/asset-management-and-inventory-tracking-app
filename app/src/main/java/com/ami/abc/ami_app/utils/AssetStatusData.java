package com.ami.abc.ami_app.utils;


import java.util.ArrayList;
import java.util.List;

public class AssetStatusData {
    public static List<String> getAssetStatuses() {
        List<String> statusName = new ArrayList<>();
        statusName.add("Good");
        statusName.add("Fair");
        statusName.add("Needs Repair");
        statusName.add("New");
        statusName.add("Poor");
        return statusName;
    }

}

package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.BulkStockState;
import com.ami.abc.ami_app.datastates.InventoryScannedStrictState;
import com.ami.abc.ami_app.datastates.PostStockTakeState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.models.CountedItems;
import com.ami.abc.ami_app.models.StockTakeReport;
import com.ami.abc.ami_app.repos.BulkStockTakeRepo;
import com.ami.abc.ami_app.repos.CountedItemsRepo;
import com.ami.abc.ami_app.repos.InventoryScannedStrictRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;
import com.ami.abc.ami_app.repos.StockTakeReportRepo;

import java.util.List;

import okhttp3.RequestBody;

public class StockTakeViewModel extends AndroidViewModel {

    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;

    private MediatorLiveData<InventoryScannedStrictState> inventoryScannedStrictStateMediatorLiveData;
    private InventoryScannedStrictRepo inventoryScannedStrictRepo;

    private MediatorLiveData<PostStockTakeState> postStockTakeStateMediatorLiveData;
   // private PostStockRepo postStockRepo;

    // save reports for  stock take
    private StockTakeReportRepo stockTakeReportRepo;

    private CountedItemsRepo countedItemsRepo;

    private LiveData<List<CountedItems>> listLiveData;

    private MediatorLiveData<BulkStockState> bulkStockStateMediatorLiveData;
    private BulkStockTakeRepo bulkStockTakeRepo;

    private MediatorLiveData<StockTakeState> getInventoryScannedStateMediatorLiveData;



    public StockTakeViewModel(Application application){
        super(application);

        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        getInventoryScannedStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);

        inventoryScannedStrictStateMediatorLiveData = new MediatorLiveData<>();
        inventoryScannedStrictRepo  = new InventoryScannedStrictRepo(application);

        postStockTakeStateMediatorLiveData = new MediatorLiveData<>();
        //postStockRepo = new PostStockRepo();

        stockTakeReportRepo = new StockTakeReportRepo();

        countedItemsRepo = new CountedItemsRepo();

        listLiveData = countedItemsRepo.getCountedItems();

        bulkStockStateMediatorLiveData = new MediatorLiveData<>();
        bulkStockTakeRepo = new BulkStockTakeRepo(application);
    }

    public void deleteByItemId(int itemId){
        countedItemsRepo.deleteByItemId(itemId);
    }

    public void delete(CountedItems countedItems){
        countedItemsRepo.delete(countedItems);
    }

    public LiveData<InventoryScannedStrictState> inventoryScannedStrictResponse(){
        return inventoryScannedStrictStateMediatorLiveData;
    }

    public void inventoryScannedStrict(String code,String store, String accessToken){

        LiveData<InventoryScannedStrictState> inventoryScannedStrictStateLiveData = inventoryScannedStrictRepo.getInventoryScannedStrict(code,store, accessToken);
        inventoryScannedStrictStateMediatorLiveData.addSource(inventoryScannedStrictStateLiveData,
                inventoryScannedStrictStateMediatorLiveData-> {
                    if (this.inventoryScannedStrictStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryScannedStrictStateMediatorLiveData.removeSource(inventoryScannedStrictStateLiveData);
                    }
                    this.inventoryScannedStrictStateMediatorLiveData.setValue(inventoryScannedStrictStateMediatorLiveData);
                });

    }

    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }
    public LiveData<StockTakeState> getInventoryScannedResponse(){
        return getInventoryScannedStateMediatorLiveData;
    }

    public void inventoryScanned(String code,String accessToken){

        LiveData<StockTakeState> getInventoryScannedStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        getInventoryScannedStateMediatorLiveData.addSource(getInventoryScannedStateLiveData,
                getInventoryScannedStateMediatorLiveData-> {
                    if (this.getInventoryScannedStateMediatorLiveData.hasActiveObservers()){
                        this.getInventoryScannedStateMediatorLiveData.removeSource(getInventoryScannedStateLiveData );
                    }
                    this.getInventoryScannedStateMediatorLiveData.setValue(getInventoryScannedStateMediatorLiveData);
                });

    }


    public void saveScannedInventory(StockTakeReport stockTakeReport) {
        stockTakeReportRepo.saveInventorys(stockTakeReport);
    }

    public void deleteAll(){
        stockTakeReportRepo.deleteInventorys();
    }


    public void deleteCountedItems(){
        countedItemsRepo.deleteCountedItems();
    }

    public void saveCountedItems(CountedItems countedItems){
        countedItemsRepo.saveCountedItems(countedItems);
    }

    public LiveData<List<CountedItems>> getListLiveData() {
        return listLiveData;
    }


    public LiveData<BulkStockState> bulkStockTakeResponse() {
        return bulkStockStateMediatorLiveData;
    }


    public void bulkStockTake(String accessToken, RequestBody requestBody) {
        LiveData<BulkStockState> bulkStockStateLiveData = bulkStockTakeRepo.bulkStockTake(accessToken, requestBody);
        bulkStockStateMediatorLiveData.addSource(bulkStockStateLiveData,
                bulkStockStateMediatorLiveData -> {
                    if (this.bulkStockStateMediatorLiveData.hasActiveObservers()) {
                        this.bulkStockStateMediatorLiveData.removeSource(bulkStockStateLiveData);
                    }
                    this.bulkStockStateMediatorLiveData.setValue(bulkStockStateMediatorLiveData);
                });

    }




}

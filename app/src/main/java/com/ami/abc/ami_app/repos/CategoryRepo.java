package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.CategoryState;
import com.ami.abc.ami_app.models.AllCategories;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryRepo {

    private ApiClient mApiClient;
    //constructor
    public CategoryRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<CategoryState> getAllCategories(String accessToken) {

        MutableLiveData<CategoryState> categoryStateMutableLiveData = new MutableLiveData<>();
        Call<AllCategories> call = mApiClient.amiService().getAllCategories(accessToken);
        call.enqueue(new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                if (response.code() == 200) {
                    categoryStateMutableLiveData.setValue(new CategoryState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    categoryStateMutableLiveData.setValue(new CategoryState(loginError));
                }
                else {
                    categoryStateMutableLiveData.setValue(new CategoryState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                categoryStateMutableLiveData.setValue(new CategoryState(t));
            }
        });

        return categoryStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.database.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.AssetCheckOutReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssetCheckOutReportDao {

    @Insert(onConflict = REPLACE)
    void saveAsset(AssetCheckOutReport assetCheckOutReport);

    //observe the list in the room database
    @Query("SELECT * FROM  AssetCheckOutReport")
    LiveData<List<AssetCheckOutReport>> getAssetsReports();


    @Delete
    void delete(AssetCheckOutReport assetsReports);

    //delete all the assets in the room database upon sending information to the assets verified table
    @Query("DELETE FROM AssetCheckOutReport")
    void deleteAssets();
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AddAssetImage;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;

public class AddAssetImageState {
    private AddAssetImage addAssetImage;
    private Throwable errorThrowable;
    private String message;
    private FailError failError;
    private LoginError loginError;

    public AddAssetImageState(AddAssetImage addAssetImage) {
        this.addAssetImage = addAssetImage;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public AddAssetImageState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.failError = null;
        this.loginError = null;
        this.addAssetImage = null;
        this.message = null;
    }

    public AddAssetImageState(String message) {
        this.message = message;
        this.failError = null;
        this.loginError = null;
        this.addAssetImage = null;
        this.errorThrowable = null;

    }

    public AddAssetImageState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.loginError = null;
        this.addAssetImage = null;
        this.errorThrowable = null;
    }

    public AddAssetImageState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.failError = null;
        this.addAssetImage = null;
        this.errorThrowable = null;
    }

    public AddAssetImage getAddAssetImage() {
        return addAssetImage;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public String getMessage() {
        return message;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

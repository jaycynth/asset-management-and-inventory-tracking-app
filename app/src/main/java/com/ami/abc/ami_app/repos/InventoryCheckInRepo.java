package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryCheckedInPointState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryCheckInRepo {
    private ApiClient mApiClient;
    //constructor
    public InventoryCheckInRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<InventoryCheckedInPointState> inventoryCheckIn(String accessToken, String productCode, int quantity) {

        MutableLiveData<InventoryCheckedInPointState> inventoryCheckedInPointStateMutableLiveData = new MutableLiveData<>();
        Call<InventoryCheckedInPoint> call = mApiClient.amiService().checkInInventory(accessToken, productCode, quantity);
        call.enqueue(new Callback<InventoryCheckedInPoint>() {
            @Override
            public void onResponse(Call<InventoryCheckedInPoint> call, Response<InventoryCheckedInPoint> response) {
                if (response.code() == 200) {
                    inventoryCheckedInPointStateMutableLiveData.setValue(new InventoryCheckedInPointState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryCheckedInPointStateMutableLiveData.setValue(new InventoryCheckedInPointState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryCheckedInPointStateMutableLiveData.setValue(new InventoryCheckedInPointState(failError));
                }
            }

            @Override
            public void onFailure(Call<InventoryCheckedInPoint> call, Throwable t) {
                inventoryCheckedInPointStateMutableLiveData.setValue(new InventoryCheckedInPointState(t));
            }
        });

        return inventoryCheckedInPointStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AssetCheckedOutPoint;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;

public class AssetCheckedOutPointState {
    private AssetCheckedOutPoint assetCheckedOutPoint;
    private String  message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;

    public AssetCheckedOutPointState(AssetCheckedOutPoint assetCheckedOutPoint) {
        this.assetCheckedOutPoint = assetCheckedOutPoint;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public AssetCheckedOutPointState(String message) {
        this.message = message;
        this.assetCheckedOutPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;

    }

    public AssetCheckedOutPointState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.assetCheckedOutPoint = null;
        this.loginError = null;
        this.failError = null;

    }

    public AssetCheckedOutPointState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.assetCheckedOutPoint = null;
        this.failError = null;
    }

    public AssetCheckedOutPointState(FailError failError) {
        this.failError = failError;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.assetCheckedOutPoint = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AssetCheckedOutPoint getAssetCheckedOutPoint() {
        return assetCheckedOutPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

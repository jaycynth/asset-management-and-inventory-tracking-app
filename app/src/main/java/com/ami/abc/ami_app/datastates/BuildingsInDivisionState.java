package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetBuildingsInDivision;
import com.ami.abc.ami_app.models.LoginError;

public class BuildingsInDivisionState {
    private GetBuildingsInDivision getRoomInBuilding;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;

    public BuildingsInDivisionState(GetBuildingsInDivision getRoomInBuilding) {
        this.getRoomInBuilding = getRoomInBuilding;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public BuildingsInDivisionState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.getRoomInBuilding = null;
        this.loginError = null;
        this.failError = null;
    }

    public BuildingsInDivisionState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.getRoomInBuilding = null;
        this.loginError = null;
        this.failError = null;
    }

    public BuildingsInDivisionState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.getRoomInBuilding = null;
        this.failError = null;
    }

    public BuildingsInDivisionState(FailError failError) {
        this.failError = failError;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.getRoomInBuilding = null;
    }

    public GetBuildingsInDivision getGetRoomInBuilding() {
        return getRoomInBuilding;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public GetBuildingsInDivision getBuildingInDivision() {
        return getRoomInBuilding;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AddAssetImageState;
import com.ami.abc.ami_app.models.AddAssetImage;
import com.ami.abc.ami_app.utils.Constants;

import java.io.File;
import java.net.SocketTimeoutException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAssetImageRepo {
    private ApiClient mApiClient;

    public AddAssetImageRepo(Application application){
        mApiClient = new ApiClient(application);
    }

    public LiveData<AddAssetImageState> postAddAssetImage(String accessToken, File photo,int assetId ){
        MutableLiveData<AddAssetImageState> addAssetImageStateMutableLiveData = new MutableLiveData<>();
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), photo);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("photo", photo.getName(), mFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), photo.getName());

        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), Constants.TYPE);


        Call<AddAssetImage> call = mApiClient.amiService().addAssetImage(accessToken,type,fileToUpload, filename,assetId);
        call.enqueue(new Callback<AddAssetImage>() {
            @Override
            public void onResponse(Call<AddAssetImage> call, Response<AddAssetImage> response) {
                if (response.code() == 200 ){
                    addAssetImageStateMutableLiveData.setValue(new AddAssetImageState(response.body()));

                }else {

                    addAssetImageStateMutableLiveData.setValue(new AddAssetImageState(response.message()));
                }
            }
            @Override
            public void onFailure(Call<AddAssetImage> call, Throwable t) {
                addAssetImageStateMutableLiveData.setValue(new AddAssetImageState(t));
                if(t instanceof SocketTimeoutException){
                    Log.d("message", "Socket Time out. Please try again.");
                }
                Log.d("MainActivity",t.getMessage());



            }
        });

        return addAssetImageStateMutableLiveData;
    }

}
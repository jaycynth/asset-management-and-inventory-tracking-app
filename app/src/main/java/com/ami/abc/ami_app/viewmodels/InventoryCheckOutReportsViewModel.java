package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.InventoryCheckOutReport;
import com.ami.abc.ami_app.repos.InventoryCheckOutReportsRepo;

import java.util.List;

public class InventoryCheckOutReportsViewModel extends AndroidViewModel {
    private InventoryCheckOutReportsRepo inventoryCheckOutReportsRepo;

    private LiveData<List<InventoryCheckOutReport>> listLiveData;



    public InventoryCheckOutReportsViewModel(Application application) {
               super(application);
        inventoryCheckOutReportsRepo = new InventoryCheckOutReportsRepo();

        listLiveData = inventoryCheckOutReportsRepo.getInventorysReports();
    }

    public LiveData<List<InventoryCheckOutReport>> getListLiveData() {
        return listLiveData;
    }



    public void saveScannedInventory(InventoryCheckOutReport inventoryCheckOutReport) {
        inventoryCheckOutReportsRepo.saveInventorys(inventoryCheckOutReport);
    }

}

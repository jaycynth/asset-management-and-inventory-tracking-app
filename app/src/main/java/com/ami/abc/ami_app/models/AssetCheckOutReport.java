package com.ami.abc.ami_app.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class AssetCheckOutReport {
    @NonNull
    @PrimaryKey
    private int id;

    private String barcode;
    private String name;
    private String custodian;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustodian() {
        return custodian;
    }

    public void setCustodian(String location) {
        this.custodian = custodian;
    }
}

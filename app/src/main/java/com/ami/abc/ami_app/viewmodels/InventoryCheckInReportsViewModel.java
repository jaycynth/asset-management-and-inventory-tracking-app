package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.InventoryCheckInReport;
import com.ami.abc.ami_app.repos.InventoryCheckInReportsRepo;

import java.util.List;

public class InventoryCheckInReportsViewModel extends AndroidViewModel {
    private InventoryCheckInReportsRepo inventoryCheckInReportsRepo;

    private LiveData<List<InventoryCheckInReport>> listLiveData;



    public InventoryCheckInReportsViewModel(Application application) {
     super(application);
        inventoryCheckInReportsRepo = new InventoryCheckInReportsRepo();

        listLiveData = inventoryCheckInReportsRepo.getInventorysReports();
    }

    public LiveData<List<InventoryCheckInReport>> getListLiveData() {
        return listLiveData;
    }



    public void saveScannedInventory(InventoryCheckInReport inventoryCheckInReport) {
        inventoryCheckInReportsRepo.saveInventorys(inventoryCheckInReport);
    }

}

package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.AssetCheckReportsAdapter;

import com.ami.abc.ami_app.viewmodels.AssetsCheckReportsViewModel;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetReportsActivity extends AppCompatActivity {
    @BindView(R.id.assets_reports)
    RecyclerView assets_reports;

    static RecyclerView.LayoutManager layoutManager;

    AssetsCheckReportsViewModel assetsCheckReportsViewModel;

    @BindView(R.id.no_reports)
    TextView no_reports;

    AssetCheckReportsAdapter assetCheckReportsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_reports);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.assets_reports));


        assetsCheckReportsViewModel = ViewModelProviders.of(this).get(AssetsCheckReportsViewModel.class);

        assetsCheckReportsViewModel.getListLiveData().observe(this, assetCheckReports -> {
            assert assetCheckReports != null;
            if (!assetCheckReports.isEmpty()) {
                initView(assetCheckReports);

            } else {
                assets_reports.setVisibility(View.GONE);
                no_reports.setVisibility(View.VISIBLE);
            }
        });


    }

    private void initView(List<com.ami.abc.ami_app.models.AssetCheckReport> assetCheckReports) {
        assetCheckReportsAdapter = new AssetCheckReportsAdapter(assetCheckReports,this);
        layoutManager = new LinearLayoutManager(this);
        assets_reports.setLayoutManager(layoutManager);
        assets_reports.setItemAnimator(new DefaultItemAnimator());
        assets_reports.setAdapter(assetCheckReportsAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, ReportOptionsActivity.class);
        startActivity(loc);
    }
}

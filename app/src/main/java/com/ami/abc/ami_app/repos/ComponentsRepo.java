package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.ComponentsDao;
import com.ami.abc.ami_app.models.CacheComponents;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ComponentsRepo {
    private ComponentsDao componentsDao;
    private Executor executor;

    public ComponentsRepo() {
        executor = Executors.newSingleThreadExecutor();
        componentsDao = AppDatabase.getDatabase(AMI.context).componentsDao();
    }

    public void saveComponents(CacheComponents cacheComponents) {
        executor.execute(() -> componentsDao.saveComponent(cacheComponents));
    }

    public LiveData<List<CacheComponents>> getComponents() {
        return componentsDao.getComponents();
    }


    public void deleteAllComponents() {
        executor.execute(() -> componentsDao.deleteAllComponents());
    }

    public List<CacheComponents> getAll() {

        return componentsDao.getAll();

    }
}

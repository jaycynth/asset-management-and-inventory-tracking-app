package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryCheckedOutPoint {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("checked out")
    @Expose
    private InventoryCheckedOut inventoryCheckedOut;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public InventoryCheckedOut getInventoryCheckedOut() {
        return inventoryCheckedOut;
    }

    public void setInventoryCheckedOut(InventoryCheckedOut inventoryCheckedOut) {
        this.inventoryCheckedOut = inventoryCheckedOut;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddedAsset {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("building")
    @Expose
    private String building;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("supplier")
    @Expose
    private String supplier;
    @SerializedName("date_of_purchase")
    @Expose
    private String dateOfPurchase;
    @SerializedName("purchase_cost")
    @Expose
    private String purchaseCost;
    @SerializedName("est_end_of_life_cost")
    @Expose
    private String estEndOfLifeCost;
    @SerializedName("depart")
    @Expose
    private String depart;
    @SerializedName("cust")
    @Expose
    private String cust;
    @SerializedName("purchase_breakdown")
    @Expose
    private String purchaseBreakdown;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getPurchaseBreakdown() {
        return purchaseBreakdown;
    }

    public void setPurchaseBreakdown(String purchaseBreakdown) {
        this.purchaseBreakdown = purchaseBreakdown;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseCost(String purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public String getEstEndOfLifeCost() {
        return estEndOfLifeCost;
    }

    public void setEstEndOfLifeCost(String estEndOfLifeCost) {
        this.estEndOfLifeCost = estEndOfLifeCost;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}

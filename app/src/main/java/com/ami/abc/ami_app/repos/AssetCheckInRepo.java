package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetCheckedInPointState;
import com.ami.abc.ami_app.models.AssetCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetCheckInRepo {
    private ApiClient mApiClient;

    public AssetCheckInRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AssetCheckedInPointState> checkinAsset(String accessToken, String code, String mileage, String fuel) {
        MutableLiveData<AssetCheckedInPointState> assetCheckedInPointStateMutableLiveData = new MutableLiveData<>();
        Call<AssetCheckedInPoint> call = mApiClient.amiService().checkInAsset(accessToken, code, mileage, fuel);
        call.enqueue(new Callback<AssetCheckedInPoint>() {
            @Override
            public void onResponse(Call<AssetCheckedInPoint> call, Response<AssetCheckedInPoint> response) {
                if (response.code() == 200) {
                    assetCheckedInPointStateMutableLiveData.setValue(new AssetCheckedInPointState(response.body()));

                } else if (response.code() == 401 || response.code() == 500 || response.code() == 422) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    assetCheckedInPointStateMutableLiveData.setValue(new AssetCheckedInPointState(loginError));
                }
                else {
                    assetCheckedInPointStateMutableLiveData.setValue(new AssetCheckedInPointState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AssetCheckedInPoint> call, Throwable t) {
                assetCheckedInPointStateMutableLiveData.setValue(new AssetCheckedInPointState(t));
            }
        });

        return assetCheckedInPointStateMutableLiveData;


    }
}

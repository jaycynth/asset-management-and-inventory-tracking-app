package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AddInventoryImageState;
import com.ami.abc.ami_app.models.AddInventoryImage;
import com.ami.abc.ami_app.utils.Constants;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInventoryImageRepo {
    private ApiClient mApiClient;

    public AddInventoryImageRepo(Application application){
        mApiClient = new ApiClient(application);
    }

    public LiveData<AddInventoryImageState> postAddInventoryImage(String accessToken, File photo, int inventoryId ){
        MutableLiveData<AddInventoryImageState> addInventoryImageStateMutableLiveData = new MutableLiveData<>();
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), photo);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("photo", photo.getName(), mFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), photo.getName());

        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), Constants.TYPE);


        Call<AddInventoryImage> call = mApiClient.amiService().addInventoryImage(accessToken,type,fileToUpload, filename,inventoryId);
        call.enqueue(new Callback<AddInventoryImage>() {
            @Override
            public void onResponse(Call<AddInventoryImage> call, Response<AddInventoryImage> response) {
                if (response.code() == 200 ){
                    addInventoryImageStateMutableLiveData.setValue(new AddInventoryImageState(response.body()));

                }else {
                    addInventoryImageStateMutableLiveData.setValue(new AddInventoryImageState(response.message()));
                }
            }
            @Override
            public void onFailure(Call<AddInventoryImage> call, Throwable t) {
                addInventoryImageStateMutableLiveData.setValue(new AddInventoryImageState(t));
            }
        });

        return addInventoryImageStateMutableLiveData;
    }
}

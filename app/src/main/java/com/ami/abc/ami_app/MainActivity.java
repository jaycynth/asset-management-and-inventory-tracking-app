package com.ami.abc.ami_app;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.models.Divisions;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.views.activities.LoginActivity;
import com.ami.abc.ami_app.views.activities.MoreSummary;
import com.ami.abc.ami_app.views.activities.ReportOptionsActivity;
import com.ami.abc.ami_app.views.activities.ScanOptionsActivity;
import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.AllDepartments;
import com.ami.abc.ami_app.models.AllRooms;
import com.ami.abc.ami_app.models.Building;
import com.ami.abc.ami_app.models.Department;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Logout;
import com.ami.abc.ami_app.models.Role;
import com.ami.abc.ami_app.models.Room;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;
import com.ami.abc.ami_app.views.activities.TwoFactorVerificationActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.scan_card)
    CardView scan_card;

    @BindView(R.id.reports_card)
    CardView reports_card;

    @BindView(R.id.more_layout)
    RelativeLayout more_summary;

    MainViewModel mainViewModel;
    String accessToken;

    List<Building> buildingList = new ArrayList<>();

    @BindView(R.id.total_buildings)
    TextView total_buildings;

    @BindView(R.id.building_count)
    ProgressBar building_count;

    List<Divisions> divisionsList = new ArrayList<>();

    @BindView(R.id.total_divisions)
    TextView total_divisions;

    @BindView(R.id.division_count)
    ProgressBar division_count;


    List<Room> roomList = new ArrayList<>();

    @BindView(R.id.total_rooms)
    TextView total_rooms;

    @BindView(R.id.room_count)
    ProgressBar room_count;

    List<Department> departmentList = new ArrayList<>();

    @BindView(R.id.total_departments)
    TextView total_departments;

    @BindView(R.id.department_count)
    ProgressBar department_count;

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;

    @BindView(R.id.generate_2fa_code)
    CardView generate2faCode;


    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        if (!SharedPreferenceManager.getInstance(this).isLoggedIn()) {
            Intent homeIntent = new Intent(this, LoginActivity.class);
            startActivity(homeIntent);
        }


        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {

            progressBar.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);

            mainViewModel.getRole("Bearer " + accessToken);
            mainViewModel.getAllBuildings("Bearer " + accessToken);
            mainViewModel.getRooms("Bearer " + accessToken);
            mainViewModel.getDepartments("Bearer " + accessToken);
            mainViewModel.getDivisions("Bearer " + accessToken);

        }


        refresh.setColorSchemeColors(Color.BLUE);
        //setting a listener on the swipe view refresh listener
        refresh.setOnRefreshListener(this::onRefresh);


        mainViewModel.getRoleResponse().observe(this, getRoleState -> {
            if (getRoleState != null) {
                if (getRoleState.getGetRole() != null) {
                    handleRoles(getRoleState.getGetRole());
                }
                if (getRoleState.getMessage() != null) {
                    handleNetworkResponse(getRoleState.getMessage());
                }

                if (getRoleState.getErrorThrowable() != null) {
                    handleError(getRoleState.getErrorThrowable());
                }

                if (getRoleState.getLoginError() != null) {
                    handleUnauthorized(getRoleState.getLoginError());
                }
                if (getRoleState.getError() != null) {
                    handleNoToken(getRoleState.getError());
                }
            }
        });

        mainViewModel.getBuildingsResponse().observe(this, buildingsState -> {
            assert buildingsState != null;
            if (buildingsState.getAllBuildings() != null) {
                handleGetAllBuildings(buildingsState.getAllBuildings());
            }

            if (buildingsState.getMessage() != null) {
                handleNetworkResponse(buildingsState.getMessage());
            }

            if (buildingsState.getErrorThrowable() != null) {
                handleError(buildingsState.getErrorThrowable());
            }

            if (buildingsState.getLoginError() != null) {
                handleUnauthorized(buildingsState.getLoginError());
            }
            if (buildingsState.getError() != null) {
                handleNoToken(buildingsState.getError());
            }
        });

        mainViewModel.getDivisionResponse().observe(this, divisionState -> {
            assert divisionState != null;
            if (divisionState.getGetDivisions() != null) {
                handleGetAllDivisions(divisionState.getGetDivisions());
            }

            if (divisionState.getMessage() != null) {
                handleNetworkResponse(divisionState.getMessage());
            }

            if (divisionState.getErrorThrowable() != null) {
                handleError(divisionState.getErrorThrowable());
            }
            if (divisionState.getLoginError() != null) {
                handleUnauthorized(divisionState.getLoginError());
            }
            if (divisionState.getError() != null) {
                handleNoToken(divisionState.getError());
            }
        });


        mainViewModel.getRoomResponse().observe(this, roomState -> {
            assert roomState != null;
            if (roomState.getAllRooms() != null) {
                handleGetAllRooms(roomState.getAllRooms());
            }

            if (roomState.getMessage() != null) {
                handleNetworkResponse(roomState.getMessage());
            }

            if (roomState.getErrorThrowable() != null) {
                handleError(roomState.getErrorThrowable());
            }
            if (roomState.getLoginError() != null) {
                handleUnauthorized(roomState.getLoginError());
            }
            if (roomState.getError() != null) {
                handleNoToken(roomState.getError());
            }
        });

        mainViewModel.getDepartmentResponse().observe(this, departmentState -> {
            assert departmentState != null;
            if (departmentState.getAllDepartments() != null) {
                handleGetAllDepartments(departmentState.getAllDepartments());
            }

            if (departmentState.getMessage() != null) {
                handleNetworkResponse(departmentState.getMessage());
            }

            if (departmentState.getErrorThrowable() != null) {
                handleError(departmentState.getErrorThrowable());
            }
            if (departmentState.getLoginError() != null) {
                handleUnauthorized(departmentState.getLoginError());
            }
            if (departmentState.getError() != null) {
                handleNoToken(departmentState.getError());
            }
        });

        mainViewModel.getLogoutResponse().observe(this, logoutState -> {
            assert logoutState != null;
            if (logoutState.getLogout() != null) {
                handleLogout(logoutState.getLogout());
            }
            if (logoutState.getMessage() != null) {
                handleNetworkResponse(logoutState.getMessage());
            }
            if (logoutState.getErrorThrowable() != null) {
                handleError(logoutState.getErrorThrowable());
            }
            if (logoutState.getLoginError() != null) {
                handleUnauthorized(logoutState.getLoginError());
            }
            if (logoutState.getError() != null) {
                handleNoToken(logoutState.getError());
            }
        });


        more_summary.setOnClickListener(v -> {
            Intent more = new Intent(MainActivity.this, MoreSummary.class);
            startActivity(more);
        });

        generate2faCode.setOnClickListener(v -> {
            Intent homeIntent = new Intent(MainActivity.this, TwoFactorVerificationActivity.class);
            startActivity(homeIntent);
        });


    }


    private void handleRoles(GetRole getRole) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        final View.OnClickListener onClickListener = v -> Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();

        List<String> roleNames = new ArrayList<>();
        List<Role> roles = getRole.getRoles();
        if (getRole.getType().equals("Admin")) {
            scan_card.setOnClickListener(v -> {
                //start intent to scan page details of the assets
                Intent scan = new Intent(MainActivity.this, ScanOptionsActivity.class);
                startActivity(scan);

            });

            reports_card.setOnClickListener(v -> {
                //start intent to report page details of the assets
                Intent reports = new Intent(MainActivity.this, ReportOptionsActivity.class);
                startActivity(reports);

            });

        } else if (getRole.getManaging().equals("Asset") || getRole.getManaging().equals("Inventory")) {

            scan_card.setOnClickListener(v -> {
                //start intent to scan page details of the assets
                Intent scan = new Intent(MainActivity.this, ScanOptionsActivity.class);
                startActivity(scan);

            });

            reports_card.setOnClickListener(v -> {
                //start intent to report page details of the assets
                Intent reports = new Intent(MainActivity.this, ReportOptionsActivity.class);
                startActivity(reports);

            });
        } else {

            for (Role role : roles) {
                roleNames.add(role.getName());


            }
            //TODO misspelt on the backend, to change to Inventory on both ends
            if (roleNames.contains("Asset Mobile Report") || roleNames.contains("Iventory Mobile Report")) {
                //scan_card.setEnabled(false);
                //scan_card.setOnClickListener(onClickListener);

                reports_card.setOnClickListener(v -> {
                    //start intent to report page details of the assets
                    Intent reports = new Intent(MainActivity.this, ReportOptionsActivity.class);
                    startActivity(reports);

                });

            } else {
                reports_card.setOnClickListener(onClickListener);


            }


            scan_card.setOnClickListener(v -> {
                //start intent to scan page details of the assets
                Intent scan = new Intent(MainActivity.this, ScanOptionsActivity.class);
                startActivity(scan);

            });

        }

    }

    private void handleLogout(Logout logout) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        boolean status = logout.getStatus();
        if (status) {
            Toast.makeText(this, "Log Out : " + logout.getMessage(), Toast.LENGTH_SHORT).show();
            SharedPreferenceManager.getInstance(this).clear();
            Intent logOut = new Intent(MainActivity.this, LoginActivity.class);
            logOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(logOut);
            finish();
        }
    }


    private void handleGetAllBuildings(AllBuildings allBuildings) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        boolean status = allBuildings.getStatus();
        if (status) {
            buildingList = allBuildings.getBuildings();
            total_buildings.setText(String.format("%s Buildings", String.valueOf(buildingList.size())));
            building_count.setProgress(buildingList.size());
        }
    }

    private void handleGetAllDivisions(GetDivisions getDivisions) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        boolean status = getDivisions.getStatus();
        if (status) {
            divisionsList = getDivisions.getData();
            total_divisions.setText(String.format("%s Divisions", String.valueOf(divisionsList.size())));
            division_count.setProgress(divisionsList.size());
        }
    }

    private void handleGetAllDepartments(AllDepartments allDepartments) {

        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        boolean status = allDepartments.getStatus();
        if (status) {
            departmentList = allDepartments.getData();

            total_departments.setText(String.format("%s Departments", String.valueOf(departmentList.size())));
            department_count.setProgress(departmentList.size());
        }

    }

    private void handleGetAllRooms(AllRooms allRooms) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        boolean status = allRooms.getStatus();
        if (status) {
            roomList = allRooms.getRooms();

            total_rooms.setText(String.format("%s Rooms", String.valueOf(roomList.size())));
            room_count.setProgress(roomList.size());
        }

    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "You have been logged out", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnauthorized(LoginError loginError) {
        Toast.makeText(this, loginError.getMessage(), Toast.LENGTH_SHORT).show();
        Intent logOut = new Intent(this, LoginActivity.class);
        logOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logOut);
        finish();
    }

    private void handleNoToken(Error error) {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
//        Intent logOut = new Intent(this, LoginActivity.class);
//        logOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(logOut);
//        finish();
    }


    /*pull to refresh on swipe */
    private void onRefresh() {
        new Handler().postDelayed(() ->

                mainViewModel.getAllBuildings("Bearer " + accessToken), 50000);
        mainViewModel.getRole("Bearer " + accessToken);
        mainViewModel.getRooms("Bearer " + accessToken);
        mainViewModel.getDivisions("Bearer " + accessToken);
        mainViewModel.getDepartments("Bearer " + accessToken);
        progressBar.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        refresh.setRefreshing(false);
    }


    private void logout() {
        progressBar.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);
        mainViewModel.deleteAll();
        mainViewModel.deleteAllAssets();
        mainViewModel.logout("Bearer " + accessToken);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.logout) {
            logout();

        }
        return super.onOptionsItemSelected(item);
    }

    /* Create the specific menu in the activity */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onStart() {
        if (!SharedPreferenceManager.getInstance(this).isLoggedIn()) {
            Intent homeIntent = new Intent(this, LoginActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeIntent);
            finish();
        }

        super.onStart();
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            logout();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press Back again to exit", Toast.LENGTH_SHORT).show();
    }


}

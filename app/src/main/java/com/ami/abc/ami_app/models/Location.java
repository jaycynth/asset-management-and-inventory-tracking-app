package com.ami.abc.ami_app.models;


import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;


/*
This class is an entity for location whereby it stores all the buildingName; i.e. its list and its associated List of rooms
upon login
*/
@Entity
public class Location {
    @PrimaryKey
    @NonNull
    private String id;
    private String buildingName;
    private String roomName;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuilding() {
        return buildingName;
    }

    public void setBuilding(String building) {
        this.buildingName = building;
    }

    public  String getRoom() {
        return roomName;
    }

    public void setRoom( String room) {
        this.roomName = room;
    }


}

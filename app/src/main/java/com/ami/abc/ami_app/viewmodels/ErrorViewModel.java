package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AssetState;
import com.ami.abc.ami_app.datastates.BuildingsState;
import com.ami.abc.ami_app.datastates.DepartmentState;
import com.ami.abc.ami_app.datastates.GetAssetScannedDetailsState;
import com.ami.abc.ami_app.datastates.GetRoleState;
import com.ami.abc.ami_app.datastates.GetRoomInBuildingState;
import com.ami.abc.ami_app.datastates.LogoutState;
import com.ami.abc.ami_app.datastates.MoveAssetState;
import com.ami.abc.ami_app.datastates.RoomState;
import com.ami.abc.ami_app.repos.AssetRepo;
import com.ami.abc.ami_app.repos.BuildingsRepo;
import com.ami.abc.ami_app.repos.DepartmentRepo;
import com.ami.abc.ami_app.repos.GetAssetsScannedRepo;
import com.ami.abc.ami_app.repos.GetRoleRepo;
import com.ami.abc.ami_app.repos.GetRoomsInBuildingRepo;
import com.ami.abc.ami_app.repos.LogoutRepo;
import com.ami.abc.ami_app.repos.MoveAssetRepo;
import com.ami.abc.ami_app.repos.RoomRepo;

public class ErrorViewModel extends AndroidViewModel {
    private MediatorLiveData<RoomState> roomStateMediatorLiveData;
    private RoomRepo roomRepo;

    private MediatorLiveData<BuildingsState> buildingsStateMediatorLiveData;
    private BuildingsRepo buildingsRepo;

    private MediatorLiveData<DepartmentState> departmentStateMediatorLiveData;
    private DepartmentRepo departmentRepo;

    private MediatorLiveData<MoveAssetState> moveAssetStateMediatorLiveData;
    private MoveAssetRepo moveAssetRepo;

    private MediatorLiveData<AssetState> assetStateMediatorLiveData;
    private AssetRepo assetRepo;

    private MediatorLiveData<GetRoomInBuildingState> getRoomInBuildingStateMediatorLiveData;
    private GetRoomsInBuildingRepo getRoomsInBuildingRepo;

    private MediatorLiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateMediatorLiveData;
    private GetAssetsScannedRepo getAssetsScannedRepo;

    private MediatorLiveData<GetRoleState> getRoleStateMediatorLiveData;
    private GetRoleRepo getRoleRepo;

    private MediatorLiveData<LogoutState> logoutStateMediatorLiveData;
    private LogoutRepo logoutRepo;


    public ErrorViewModel(Application application){
        super(application);
        roomStateMediatorLiveData = new MediatorLiveData<>();
        roomRepo = new RoomRepo(application);

        buildingsStateMediatorLiveData = new MediatorLiveData<>();
        buildingsRepo = new BuildingsRepo(application);

        departmentStateMediatorLiveData = new MediatorLiveData<>();
        departmentRepo = new DepartmentRepo(application);

        assetStateMediatorLiveData = new MediatorLiveData<>();
        assetRepo = new AssetRepo(application);

        moveAssetStateMediatorLiveData = new MediatorLiveData<>();
        moveAssetRepo = new MoveAssetRepo(application);

        getRoomInBuildingStateMediatorLiveData = new MediatorLiveData<>();
        getRoomsInBuildingRepo = new GetRoomsInBuildingRepo(application);

        getAssetScannedDetailsStateMediatorLiveData = new MediatorLiveData<>();
        getAssetsScannedRepo = new GetAssetsScannedRepo(application);;

        logoutStateMediatorLiveData = new MediatorLiveData<>();
        logoutRepo = new LogoutRepo(application);

        getRoleStateMediatorLiveData = new MediatorLiveData<>();
        getRoleRepo = new GetRoleRepo(application);
    }


    public LiveData<DepartmentState> getDepartmentResponse(){
        return departmentStateMediatorLiveData;
    }

    public void getDepartments(String accessToken){

        LiveData<DepartmentState> departmentStateLiveData = departmentRepo.getAllDepartments(accessToken);
        departmentStateMediatorLiveData.addSource(departmentStateLiveData,
                departmentStateMediatorLiveData -> {
                    if (this.departmentStateMediatorLiveData.hasActiveObservers()){
                        this.departmentStateMediatorLiveData.removeSource(departmentStateLiveData);
                    }
                    this.departmentStateMediatorLiveData.setValue(departmentStateMediatorLiveData);
                });

    }


    public LiveData<RoomState> getRoomResponse(){
        return roomStateMediatorLiveData;
    }

    public void getRooms(String accessToken){

        LiveData<RoomState> roomStateLiveData = roomRepo.getAllRooms(accessToken);
        roomStateMediatorLiveData.addSource(roomStateLiveData,
                roomStateMediatorLiveData -> {
                    if (this.roomStateMediatorLiveData.hasActiveObservers()){
                        this.roomStateMediatorLiveData.removeSource(roomStateLiveData);
                    }
                    this.roomStateMediatorLiveData.setValue(roomStateMediatorLiveData);
                });

    }

    public LiveData<BuildingsState> getBuildingsResponse(){
        return buildingsStateMediatorLiveData;
    }

    public void getAllBuildings(String accessToken){

        LiveData<BuildingsState> buildingsStateLiveData = buildingsRepo.getAllBuildings(accessToken);
        buildingsStateMediatorLiveData.addSource(buildingsStateLiveData,
                buildingsStateMediatorLiveData -> {
                    if (this.buildingsStateMediatorLiveData.hasActiveObservers()){
                        this.buildingsStateMediatorLiveData.removeSource(buildingsStateLiveData);
                    }
                    this.buildingsStateMediatorLiveData.setValue(buildingsStateMediatorLiveData);
                });

    }


    public LiveData<MoveAssetState> getMoveAssetResponse(){
        return moveAssetStateMediatorLiveData;
    }

    public void moveAssets(String accessToken, int assetId, String building, String room, String department,String division, String cust){

        LiveData<MoveAssetState> moveAssetStateLiveData = moveAssetRepo.moveAsset(accessToken, assetId, building, room, department,division, cust);
        moveAssetStateMediatorLiveData.addSource(moveAssetStateLiveData,
                moveAssetStateMediatorLiveData -> {
                    if (this.moveAssetStateMediatorLiveData.hasActiveObservers()){
                        this.moveAssetStateMediatorLiveData.removeSource(moveAssetStateLiveData);
                    }
                    this.moveAssetStateMediatorLiveData.setValue(moveAssetStateMediatorLiveData);
                });

    }


    public LiveData<AssetState> getAllAssetResponse(){
        return assetStateMediatorLiveData;
    }

    public void getAllAssets(String accessToken){
        LiveData<AssetState> assetStateLiveData = assetRepo.getAllAssets(accessToken);
        assetStateMediatorLiveData.addSource(assetStateLiveData,
                assetStateMediatorLiveData -> {
                    if (this.assetStateMediatorLiveData.hasActiveObservers()){
                        this.assetStateMediatorLiveData.removeSource(assetStateLiveData);
                    }
                    this.assetStateMediatorLiveData.setValue(assetStateMediatorLiveData);
                });
    }

    public LiveData<GetRoomInBuildingState> getRoomInBuildingResponse(){
        return getRoomInBuildingStateMediatorLiveData;
    }

    public void getRoomsInBuilding(int building_id, String accessToken){
        LiveData<GetRoomInBuildingState> getRoomInBuildingStateLiveData = getRoomsInBuildingRepo.getRoomsInBuilding(building_id, accessToken);
        getRoomInBuildingStateMediatorLiveData.addSource(getRoomInBuildingStateLiveData,
                getRoomInBuildingStateMediatorLiveData -> {
                    if (this.getRoomInBuildingStateMediatorLiveData.hasActiveObservers()){
                        this.getRoomInBuildingStateMediatorLiveData.removeSource(getRoomInBuildingStateLiveData);
                    }
                    this.getRoomInBuildingStateMediatorLiveData.setValue(getRoomInBuildingStateMediatorLiveData);
                });
    }

    public LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsResponse() {
        return getAssetScannedDetailsStateMediatorLiveData;
    }

    public void getAssetScannedDetails(String code, String accessToken) {
        LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateLiveData = getAssetsScannedRepo.getAssetScanned(code, accessToken);
        getAssetScannedDetailsStateMediatorLiveData.addSource(getAssetScannedDetailsStateLiveData,
                getAssetScannedDetailsStateMediatorLiveData -> {
                    if (this.getAssetScannedDetailsStateMediatorLiveData.hasActiveObservers()) {
                        this.getAssetScannedDetailsStateMediatorLiveData.removeSource(getAssetScannedDetailsStateLiveData);
                    }
                    this.getAssetScannedDetailsStateMediatorLiveData.setValue(getAssetScannedDetailsStateMediatorLiveData);
                });

    }



    public LiveData<LogoutState> getLogoutResponse(){
        return logoutStateMediatorLiveData;
    }

    public void logout(String accessToken){

        LiveData<LogoutState> logoutStateLiveData = logoutRepo.logoutUser(accessToken);
        logoutStateMediatorLiveData.addSource(logoutStateLiveData,
                logoutStateMediatorLiveData -> {
                    if (this.logoutStateMediatorLiveData.hasActiveObservers()){
                        this.logoutStateMediatorLiveData.removeSource(logoutStateLiveData);
                    }
                    this.logoutStateMediatorLiveData.setValue(logoutStateMediatorLiveData);
                });

    }

    public LiveData<GetRoleState> getRoleResponse(){
        return getRoleStateMediatorLiveData;
    }

    public void getRole(String accessToken){

        LiveData<GetRoleState> getRoleStateLiveData = getRoleRepo.getGetRole(accessToken);
        getRoleStateMediatorLiveData.addSource(getRoleStateLiveData,
                getRoleStateMediatorLiveData -> {
                    if (this.getRoleStateMediatorLiveData.hasActiveObservers()){
                        this.getRoleStateMediatorLiveData.removeSource(getRoleStateLiveData);
                    }
                    this.getRoleStateMediatorLiveData.setValue(getRoleStateMediatorLiveData);
                });

    }

}

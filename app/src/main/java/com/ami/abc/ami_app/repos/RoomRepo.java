package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.RoomState;

import com.ami.abc.ami_app.models.AllRooms;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomRepo {
    private ApiClient mApiClient;

    //constructor
    public RoomRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<RoomState> getAllRooms(String accessToken) {

        MutableLiveData<RoomState> roomStateMutableLiveData = new MutableLiveData<>();
        Call<AllRooms> call = mApiClient.getAllRoomsService().getRooms(accessToken);
        call.enqueue(new Callback<AllRooms>() {
            @Override
            public void onResponse(Call<AllRooms> call, Response<AllRooms> response) {
                if (response.code() == 200) {
                    roomStateMutableLiveData.setValue(new RoomState(response.body()));

                } else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    roomStateMutableLiveData.setValue(new RoomState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    roomStateMutableLiveData.setValue(new RoomState(error));
                }else {
                    roomStateMutableLiveData.setValue(new RoomState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllRooms> call, Throwable t) {
                roomStateMutableLiveData.setValue(new RoomState(t));
            }
        });

        return roomStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.BuildingsState;

import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuildingsRepo {

    private ApiClient mApiClient;

    //constructor
    public BuildingsRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<BuildingsState> getAllBuildings(String accessToken) {

        MutableLiveData<BuildingsState> buildingsStateMutableLiveData = new MutableLiveData<>();
        Call<AllBuildings> call = mApiClient.getAllBuildingsService().getBuildings(accessToken);
        call.enqueue(new Callback<AllBuildings>() {
            @Override
            public void onResponse(Call<AllBuildings> call, Response<AllBuildings> response) {
                if (response.code() == 200) {
                    buildingsStateMutableLiveData.setValue(new BuildingsState(response.body()));

                } else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    buildingsStateMutableLiveData.setValue(new BuildingsState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    buildingsStateMutableLiveData.setValue(new BuildingsState(error));
                }
                else {
                    buildingsStateMutableLiveData.setValue(new BuildingsState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllBuildings> call, Throwable t) {
                buildingsStateMutableLiveData.setValue(new BuildingsState(t));
            }
        });

        return buildingsStateMutableLiveData;

    }
}

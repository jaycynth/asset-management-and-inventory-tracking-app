package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.DecrementInventoryState;
import com.ami.abc.ami_app.datastates.InventoryOutState;
import com.ami.abc.ami_app.datastates.InventoryState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.repos.DecrementInventoryRepo;
import com.ami.abc.ami_app.repos.InventoryOutRepo;
import com.ami.abc.ami_app.repos.InventoryRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;

public class InventoryOutViewModel extends AndroidViewModel {



    private MediatorLiveData<InventoryState> inventoryStateMediatorLiveData;
    private InventoryRepo inventoryRepo;

    private MediatorLiveData<DecrementInventoryState> decrementInventoryStateMediatorLiveData;
    private DecrementInventoryRepo decrementInventoryRepo;

    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;

    private MediatorLiveData<InventoryOutState> inventoryOutStateMediatorLiveData;
    private InventoryOutRepo inventoryOutRepo;


    public InventoryOutViewModel(Application application) {
              super(application);

        inventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryRepo = new InventoryRepo(application);

        decrementInventoryStateMediatorLiveData = new MediatorLiveData<>();
        decrementInventoryRepo = new DecrementInventoryRepo(application);

        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);

        inventoryOutStateMediatorLiveData = new MediatorLiveData<>();
        inventoryOutRepo = new InventoryOutRepo(application);
    }


    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }

    public LiveData<InventoryState> getAllInventoryResponse(){
        return inventoryStateMediatorLiveData;
    }

    public void getAllInventory(String accessToken){
        LiveData<InventoryState> inventoryStateLiveData = inventoryRepo.getAllInventory(accessToken);
        inventoryStateMediatorLiveData.addSource(inventoryStateLiveData,
                inventoryStateMediatorLiveData -> {
                    if (this.inventoryStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryStateMediatorLiveData.removeSource(inventoryStateLiveData);
                    }
                    this.inventoryStateMediatorLiveData.setValue(inventoryStateMediatorLiveData);
                });
    }

    public LiveData<DecrementInventoryState> decrementInventoryResponse(){
        return decrementInventoryStateMediatorLiveData;
    }

    public void decrementInventory(String accessToken,int inventoryId, int quantity){
        LiveData<DecrementInventoryState> decrementInventoryStateLiveData = decrementInventoryRepo.decrementInventory(accessToken, inventoryId, quantity);
        decrementInventoryStateMediatorLiveData.addSource(decrementInventoryStateLiveData,
                decrementInventoryStateMediatorLiveData-> {
                    if (this.decrementInventoryStateMediatorLiveData.hasActiveObservers()){
                        this.decrementInventoryStateMediatorLiveData.removeSource(decrementInventoryStateLiveData);
                    }
                    this.decrementInventoryStateMediatorLiveData.setValue(decrementInventoryStateMediatorLiveData);
                });
    }



    public LiveData<InventoryOutState> inventoryOutResponse(){
        return inventoryOutStateMediatorLiveData;
    }

    public void outInventory(int id, String accessToken, String customer, String invoiceNumber, int invoiceValue,int quantity){
        LiveData<InventoryOutState> inventoryOutStateLiveData = inventoryOutRepo.inventoryOut(id, accessToken, customer, invoiceNumber, invoiceValue, quantity);
        inventoryOutStateMediatorLiveData.addSource(inventoryOutStateLiveData,
                inventoryOutStateMediatorLiveData -> {
                    if (this.inventoryOutStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryOutStateMediatorLiveData.removeSource(inventoryOutStateLiveData);
                    }
                    this.inventoryOutStateMediatorLiveData.setValue(inventoryOutStateMediatorLiveData);
                });
    }

}

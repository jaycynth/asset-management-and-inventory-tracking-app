package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssetCheckedInPoint {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("checked_in")
    @Expose
    private AssetCheckedIn checkedIn;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public AssetCheckedIn getCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(AssetCheckedIn checkedIn) {
        this.checkedIn = checkedIn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

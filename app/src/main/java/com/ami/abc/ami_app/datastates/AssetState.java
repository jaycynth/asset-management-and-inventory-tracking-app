package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.LoginError;

public class AssetState {
    private AllAssets allAssets;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public AssetState(AllAssets allAssets) {
        this.allAssets = allAssets;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AssetState(String message) {
        this.message = message;
        this.allAssets = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AssetState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allAssets = null;

        this.loginError = null;
    }

    public AssetState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allAssets = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllAssets getAllAssets() {
        return allAssets;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

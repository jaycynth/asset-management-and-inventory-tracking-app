package com.ami.abc.ami_app.repos;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AddAssetState;

import com.ami.abc.ami_app.models.AddAsset;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetUnproccessableEntityErrors;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAssetRepo {
    private ApiClient mApiClient;

    //constructor
    public NewAssetRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AddAssetState> postNewAsset(String accessToken, String code, String name, String status, String serial, String model, String building, String room, String division, String supplier,
                                                String date_of_purchase, String purchase_cost, String est_end_of_life_cost, String depart, String cust, String asset_category, int sub_category_id,
                                                String fCost, String iDuties, String nRefundTax, String sCost, String dCost, String breakdownPurchaseCost, Boolean breakdown) {


        MutableLiveData<AddAssetState> addAssetMutableLiveData = new MutableLiveData<>();
        Call<AddAsset> call = mApiClient.postAssetService().postAsset(accessToken, code, name, status, serial, model, building, room, division,
                supplier, date_of_purchase, purchase_cost, est_end_of_life_cost, depart, cust,
                asset_category, sub_category_id, fCost, iDuties, nRefundTax, sCost, dCost, breakdownPurchaseCost, breakdown);
        call.enqueue(new Callback<AddAsset>() {
            @Override
            public void onResponse(Call<AddAsset> call, Response<AddAsset> response) {
                if (response.code() == 200) {
                    addAssetMutableLiveData.setValue(new AddAssetState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {
                    }.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(), type);
                    addAssetMutableLiveData.setValue(new AddAssetState(loginError));
                    //unprocessable
                } else if (response.code() == 422) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<GetUnproccessableEntityErrors>() {
                    }.getType();
                    GetUnproccessableEntityErrors errors = gson.fromJson(response.errorBody().charStream(), type);
                    addAssetMutableLiveData.setValue(new AddAssetState(errors));
                } else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {
                    }.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(), type);
                    addAssetMutableLiveData.setValue(new AddAssetState(failError));
                }
            }

            @Override
            public void onFailure(Call<AddAsset> call, Throwable t) {
                addAssetMutableLiveData.setValue(new AddAssetState(t));
            }
        });

        return addAssetMutableLiveData;

    }
}

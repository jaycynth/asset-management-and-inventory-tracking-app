package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.StoreState;
import com.ami.abc.ami_app.models.AllStores;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreRepo {
    private ApiClient mApiClient;

    public StoreRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<StoreState> getAllStores(String accessToken) {
        MutableLiveData<StoreState> storeStateMutableLiveData = new MutableLiveData<>();
        Call<AllStores> call = mApiClient.amiService().getStores(accessToken);
        call.enqueue(new Callback<AllStores>() {
            @Override
            public void onResponse(Call<AllStores> call, Response<AllStores> response) {
                if (response.code() == 200) {
                    storeStateMutableLiveData.setValue(new StoreState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    storeStateMutableLiveData.setValue(new StoreState(loginError));
                }
                else {
                    storeStateMutableLiveData.setValue(new StoreState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllStores> call, Throwable t) {
                storeStateMutableLiveData.setValue(new StoreState(t));
            }
        });

        return storeStateMutableLiveData;


    }

}

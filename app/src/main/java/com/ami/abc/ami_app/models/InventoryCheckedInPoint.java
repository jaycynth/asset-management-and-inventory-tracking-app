package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryCheckedInPoint {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("checked in")
    @Expose
    private InventoryCheckedIn checkedIn;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public InventoryCheckedIn getCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(InventoryCheckedIn checkedIn) {
        this.checkedIn = checkedIn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.BulkStockState;
import com.ami.abc.ami_app.models.BulkStockTakeEntry;
import com.ami.abc.ami_app.models.FailError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BulkStockTakeRepo {

    private ApiClient mApiClient;

    public BulkStockTakeRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<BulkStockState> bulkStockTake(String accessToken, RequestBody requestBody){
        MutableLiveData<BulkStockState> bulkStockTakeMutableLiveData = new MutableLiveData<>();
        Call<BulkStockTakeEntry> call = mApiClient.amiService().bulkStockEntry(accessToken, requestBody);
        call.enqueue(new Callback<BulkStockTakeEntry>() {
            @Override
            public void onResponse(Call<BulkStockTakeEntry> call, Response<BulkStockTakeEntry> response) {
                if (response.code() == 200 ){
                    bulkStockTakeMutableLiveData.setValue(new BulkStockState(response.body()));

                }else if(response.code() == 422){
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    bulkStockTakeMutableLiveData.setValue(new BulkStockState(failError));
                }
                else {
                    bulkStockTakeMutableLiveData.setValue(new BulkStockState(response.message()));
                }
            }
            @Override
            public void onFailure(Call<BulkStockTakeEntry> call, Throwable t) {
                bulkStockTakeMutableLiveData.setValue(new BulkStockState(t));
            }
        });

        return bulkStockTakeMutableLiveData;
    }

}

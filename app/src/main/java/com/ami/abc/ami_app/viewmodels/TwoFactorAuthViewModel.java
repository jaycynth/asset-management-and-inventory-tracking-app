package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.TwoFactorAuthState;
import com.ami.abc.ami_app.repos.TwoFactorAuthRepo;

public class TwoFactorAuthViewModel extends AndroidViewModel {

    private MediatorLiveData<TwoFactorAuthState> twoFactorAuthStateMediatorLiveData;
    private TwoFactorAuthRepo twoFactorAuthRepo;

    public TwoFactorAuthViewModel(Application application) {
        super(application);

        twoFactorAuthStateMediatorLiveData = new MediatorLiveData<>();
        twoFactorAuthRepo = new TwoFactorAuthRepo(application);
    }

    public LiveData<TwoFactorAuthState> twoFactorAuthResponse() {
        return twoFactorAuthStateMediatorLiveData;
    }

    public void twoFactorAuth(String accessToken, String code) {
        LiveData<TwoFactorAuthState> twoFactorAuthStateLiveData = twoFactorAuthRepo.getTwoFactorAuth(accessToken, code);
        twoFactorAuthStateMediatorLiveData.addSource(twoFactorAuthStateLiveData, twoFactorAuthStateMediatorLiveData -> {
            if (this.twoFactorAuthStateMediatorLiveData.hasActiveObservers()) {
                this.twoFactorAuthStateMediatorLiveData.removeSource(twoFactorAuthStateLiveData);
            }
            this.twoFactorAuthStateMediatorLiveData.setValue(twoFactorAuthStateMediatorLiveData);
        });

    }
}

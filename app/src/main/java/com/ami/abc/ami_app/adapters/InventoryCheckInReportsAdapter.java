package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.InventoryCheckInReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryCheckInReportsAdapter extends RecyclerView.Adapter<InventoryCheckInReportsAdapter.MyViewHolder> {
    private List<InventoryCheckInReport> inventoryCheckInReportList;
    private Context context;

    public InventoryCheckInReportsAdapter(List<InventoryCheckInReport> inventoryCheckInReportList, Context context) {
        this.inventoryCheckInReportList = inventoryCheckInReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.inventory_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.count)
        TextView count;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public InventoryCheckInReportsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_reports_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryCheckInReportsAdapter.MyViewHolder holder, int i) {
        InventoryCheckInReport assetScanned = inventoryCheckInReportList.get(i);
        holder.asset_code.setText(assetScanned.getBarcode());
        holder.name.setText("Product Name : " + assetScanned.getName());
        holder.count.setText("Count : "+ String.valueOf(assetScanned.getCount()));


    }

    @Override
    public int getItemCount() {
        return inventoryCheckInReportList.size();
    }


}


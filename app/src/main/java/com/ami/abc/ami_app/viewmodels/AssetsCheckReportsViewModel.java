package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.AssetCheckReport;
import com.ami.abc.ami_app.repos.AssetCheckReportsRepo;

import java.util.List;

public class AssetsCheckReportsViewModel extends AndroidViewModel {
    private AssetCheckReportsRepo assetCheckReportsRepo;

    private LiveData<List<AssetCheckReport>> listLiveData;



    public AssetsCheckReportsViewModel(Application application) {
           super(application);
        assetCheckReportsRepo = new AssetCheckReportsRepo();

        listLiveData = assetCheckReportsRepo.getAssetsReports();
    }

    public LiveData<List<AssetCheckReport>> getListLiveData() {
        return listLiveData;
    }



    //save individual assets scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(AssetCheckReport assetInReport) {
        assetCheckReportsRepo.saveAssets(assetInReport);
    }



}

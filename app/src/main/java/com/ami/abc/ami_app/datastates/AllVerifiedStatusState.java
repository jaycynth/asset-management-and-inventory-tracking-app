package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllVerifiedStatus;
import com.ami.abc.ami_app.models.LoginError;

public class AllVerifiedStatusState {
    private AllVerifiedStatus allVerifiedStatus;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public AllVerifiedStatusState(AllVerifiedStatus allVerifiedStatus) {
        this.allVerifiedStatus = allVerifiedStatus;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AllVerifiedStatusState(String message) {
        this.message = message;
        this.allVerifiedStatus = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AllVerifiedStatusState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allVerifiedStatus = null;
        this.loginError = null;
    }

    public AllVerifiedStatusState(LoginError loginError) {
        this.loginError = loginError;
        this.allVerifiedStatus = null;
        this.message = null;
        this.errorThrowable = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllVerifiedStatus getAllVerifiedStatus() {
        return allVerifiedStatus;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

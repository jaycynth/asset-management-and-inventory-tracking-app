package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;

import com.ami.abc.ami_app.models.Asset;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.AssetCheckReport;
import com.ami.abc.ami_app.models.AssetScanned;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.VerifyAsset;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.AssetViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AssetCheck extends AppCompatActivity {

    @BindView(R.id.asset_id)
    TextInputEditText asset_id;

    String building, room, depart;
    int assetId;
    AssetViewModel assetViewModel;

    @BindView(R.id.verify)
    Button verify;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.container_layout)
    RelativeLayout containerLayout;

    String accessToken;
    List<AssetScanned> assetScannedList = new ArrayList<>();

    @BindView(R.id.asset_name)
    TextView asset_name;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.at_cost)
    TextView purchase_cost;

    @BindView(R.id.custodian)
    TextView custodian;

    String code;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    RelativeLayout detailsLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_check);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.asset_check_title));

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        code = scanIntents.getStringExtra("scanResult");


        assetViewModel = ViewModelProviders.of(this).get(AssetViewModel.class);

        asset_id.requestFocus();

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        assert accessToken != null;


        if (code != null) {

            asset_id.setText(code);

            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);

            containerLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            assetViewModel.getAssetScannedDetails(code, "Bearer " + accessToken);
        }

        confirm.setOnClickListener(v -> {
            String code = asset_id.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                containerLayout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                assetViewModel.getAssetScannedDetails(code, "Bearer " + accessToken);
            }
        });

        assetViewModel.getAllAssetResponse().observe(this, assetState -> {
            assert assetState != null;
            if (assetState.getAllAssets() != null) {
                handleAssetResponse(assetState.getAllAssets());
            }

            if (assetState.getMessage() != null) {
                handleNetworkResponse(assetState.getMessage());
            }

            if (assetState.getErrorThrowable() != null) {
                handleError(assetState.getErrorThrowable());
            }

            if (assetState.getLoginError() != null) {
                handleUnauthorized(assetState.getLoginError());
            }
        });

        assetViewModel.verifyAssetsResponse().observe(this, verifyAssetState -> {
            assert verifyAssetState != null;
            if (verifyAssetState.getVerifyAsset() != null) {
                handleAssetVerification(verifyAssetState.getVerifyAsset());
            }

            if (verifyAssetState.getMessage() != null) {
                handleVerificationNetworkResponse(verifyAssetState.getMessage());
            }

            if (verifyAssetState.getErrorThrowable() != null) {
                handleVerificationError(verifyAssetState.getErrorThrowable());
            }
            if (verifyAssetState.getLoginError() != null) {
                handleUnauthorized(verifyAssetState.getLoginError());
            }
        });


        assetViewModel.getListLiveData().observe(this, assetScannedList1 -> {
            assetScannedList = assetScannedList1;
        });

        assetViewModel.getAssetScannedDetailsResponse().observe(this, getAssetScannedDetailsState -> {
            assert getAssetScannedDetailsState != null;
            if (getAssetScannedDetailsState.getGetAssetScannedDetails() != null) {
                handleScannedAssets(getAssetScannedDetailsState.getGetAssetScannedDetails());
            }
            if (getAssetScannedDetailsState.getMessage() != null) {
                handleScannedAssetsNetworkResponse(getAssetScannedDetailsState.getMessage());
            }

            if (getAssetScannedDetailsState.getErrorThrowable() != null) {
                handleScannedAssetsError(getAssetScannedDetailsState.getErrorThrowable());
            }
            if (getAssetScannedDetailsState.getLoginError() != null) {
                handleUnauthorized(getAssetScannedDetailsState.getLoginError());
            }
            if (getAssetScannedDetailsState.getFailError() != null) {
                handleFail(getAssetScannedDetailsState.getFailError());
            }
        });


        verify.setOnClickListener(v -> {
            containerLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);

            assetViewModel.getAllAssets("Bearer " + accessToken);
        });


    }

    private void handleFail(FailError failError) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        showAlertDialog(failError.getMessage() + " Do you want to continue ? ");
    }


    private void handleScannedAssets(GetAssetScannedDetails getAssetScannedDetails) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = getAssetScannedDetails.getStatus();
        if (status) {
            Asset asset = getAssetScannedDetails.getAsset();
            asset_name.setText(asset.getName());
            date.setText(asset.getDateOfPurchase());
            purchase_cost.setText(String.valueOf(asset.getPurchaseCost()));
            custodian.setText(asset.getCust());

            String buildingName = asset.getBuilding();
            String roomName = asset.getRoom();
            String departName = asset.getDepart();

            if (getAssetScannedDetails.getAsset().getDiscontinued() != null) {
                if (getAssetScannedDetails.getAsset().getDiscontinued().equalsIgnoreCase("disposed")) {
                    showAlertDialog("The asset you scanned was disposed");
                    return;
                }
            }

            if (getAssetScannedDetails.getAsset().getVerified() != null) {
                if (asset.getVerified() == 1) {
                    showAlertDialog("This asset has already been verified");
                    return;
                }
            }
            if (buildingName.equalsIgnoreCase(building) && roomName.equalsIgnoreCase(room) && departName.equalsIgnoreCase(depart)) {
                asset.setVerified(1);

                //store the value in room to send back to the database
                AssetScanned assetScanned = new AssetScanned();
                assetScanned.setId(asset.getId());
                assetScanned.setStatus("verified");
                assetViewModel.saveAssetReports(assetScanned);

                //store in room for reports
                AssetCheckReport assetsReports = new AssetCheckReport();
                assetsReports.setId(asset.getId());
                assetsReports.setBarcode(asset.getCode());
                assetsReports.setName(asset.getName());
                assetsReports.setStatus("verified");
                assetViewModel.saveAssetCheckReports(assetsReports);

                verificationDialog("verified");
            } else {
                //verify the asset by setting its status to be 1
                asset.setVerified(1);
                //store the value in room
                AssetScanned assetScanned = new AssetScanned();
                assetScanned.setId(asset.getId());
                assetScanned.setStatus("misplaced");
                assetViewModel.saveAssetReports(assetScanned);


                //store in room for reports
                AssetCheckReport assetsReports = new AssetCheckReport();
                assetsReports.setId(asset.getId());
                assetsReports.setBarcode(asset.getCode());
                assetsReports.setName(asset.getName());
                assetsReports.setStatus("misplaced");
                assetViewModel.saveAssetCheckReports(assetsReports);

                verificationDialog("misplaced");
            }


            if (buildingName.equalsIgnoreCase(building) && roomName.equalsIgnoreCase(room) && departName.equalsIgnoreCase(depart)) {
                asset.setVerified(1);

                //store the value in room to send back to the database
                AssetScanned assetScanned = new AssetScanned();
                assetScanned.setId(asset.getId());
                assetScanned.setStatus("verified");
                assetViewModel.saveAssetReports(assetScanned);

                //store in room for reports
                AssetCheckReport assetsReports = new AssetCheckReport();
                assetsReports.setId(asset.getId());
                assetsReports.setBarcode(asset.getCode());
                assetsReports.setName(asset.getName());
                assetsReports.setStatus("verified");
                assetViewModel.saveAssetCheckReports(assetsReports);

                verificationDialog("verified");
            } else {
                //verify the asset by setting its status to be 1
                asset.setVerified(1);
                //store the value in room
                AssetScanned assetScanned = new AssetScanned();
                assetScanned.setId(asset.getId());
                assetScanned.setStatus("misplaced");
                assetViewModel.saveAssetReports(assetScanned);


                //store in room for reports
                AssetCheckReport assetsReports = new AssetCheckReport();
                assetsReports.setId(asset.getId());
                assetsReports.setBarcode(asset.getCode());
                assetsReports.setName(asset.getName());
                assetsReports.setStatus("misplaced");
                assetViewModel.saveAssetCheckReports(assetsReports);

                verificationDialog("misplaced");
            }

        }

    }


    private void handleScannedAssetsNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        showAlertDialog(message);
    }

    private void handleScannedAssetsError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getCause().toString());
        }
    }

    //method that transforms response from Pojo to json to pass as parameter to the retrofit call as request body
    private String serialize(List<AssetScanned> assetScannedList, String arrayKey, String objectKey) {
        JsonObject json = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (AssetScanned assetScanned : assetScannedList) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(assetScanned);
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(objectKey, jsonElement);
            jsonArray.add(jsonElement);
        }
        json.add(arrayKey, jsonArray);

        return json.toString();
    }

    //uploading data you have already verified
    private void handleAssetVerification(VerifyAsset verifyAsset) {

        int status = verifyAsset.getStatus();
        if (status == 200) {
            Toast.makeText(this, verifyAsset.getMessage(), Toast.LENGTH_SHORT).show();
            /* delete all the assets stored in room that were verified , so that on the next verification
            it does not still upload the  previous scan */
            assetViewModel.deleteScannedAssets();
            onBackPressed();
        }

    }

    private void handleVerificationError(Throwable errorThrowable) {
        verify.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }


    private void handleVerificationNetworkResponse(String message) {
        //verify.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        verificationDialog(message);

        //Toast.makeText(this, "verify : " + message, Toast.LENGTH_SHORT).show();

    }

    //gets all the assets then  looks for the one with the barcode scanned then checks to see its verified status
    private void handleAssetResponse(AllAssets allAssets) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = allAssets.getStatus();
        if (status) {
            List<Asset> assetOneList = allAssets.getData();
            for (Asset asset : assetOneList) {

                if (building.equals(asset.getBuilding()) && depart.equals(asset.getDepart()) && room.equals(asset.getRoom())) {
                    //verify the asset by setting its status to be 1
                    asset.setVerified(1);

                    //store the value in room to send back to the database
                    AssetScanned assetScanned = new AssetScanned();
                    assetScanned.setId(asset.getId());
                    assetScanned.setStatus("verified");
                    assetViewModel.saveAssetReports(assetScanned);

                    //store in room for reports
                    AssetCheckReport assetsReports = new AssetCheckReport();
                    assetsReports.setId(asset.getId());
                    assetsReports.setBarcode(asset.getCode());
                    assetsReports.setName(asset.getName());
                    assetsReports.setStatus("verified ");
                    assetViewModel.saveAssetCheckReports(assetsReports);


                    verificationDialog("verified");

                    return;

                }
                //verify the asset by setting its status to be 1
                asset.setVerified(1);
                //store the value in room
                AssetScanned assetScanned = new AssetScanned();
                assetScanned.setId(asset.getId());
                assetScanned.setStatus("misplaced");
                assetViewModel.saveAssetReports(assetScanned);


                //store in room for reports
                AssetCheckReport assetsReports = new AssetCheckReport();
                assetsReports.setId(asset.getId());
                assetsReports.setBarcode(asset.getCode());
                assetsReports.setName(asset.getName());
                assetsReports.setStatus("misplaced");
                assetViewModel.saveAssetCheckReports(assetsReports);

                verificationDialog("misplaced");


            }
        }

    }

    private void handleNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        verificationDialog("Asset" + message + "Do you want to continue ? ");

        verify.setEnabled(false);
    }

    private void handleError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }

    }

    private void handleUnauthorized(LoginError loginError) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, loginError.getMessage(), Toast.LENGTH_SHORT).show();

    }


    private void showAlertDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Scan Results ");
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setNeutralButton("Camera", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "B");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );

        alert.setNegativeButton("Scanner", ((dialog, which) -> {
            Intent scanAsset = new Intent(this, AssetCheck.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "B");
            startActivity(scanAsset);
            dialog.dismiss();
        }));

        alert.setPositiveButton("Exit", (dialog, which) -> {
            onBackPressed();
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void verificationDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Scan Results ");
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setNeutralButton("Camera", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "B");
                    startActivity(scanAsset);
                    dialog.dismiss();


                }
        );
        alert.setNegativeButton("Scanner", ((dialog, which) -> {
            Intent scanAsset = new Intent(this, AssetCheck.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "B");
            startActivity(scanAsset);
            dialog.dismiss();
        }));
        alert.setPositiveButton("Verify", (dialog, which) -> {
            Toast.makeText(this, "Please wait as we upload your checks", Toast.LENGTH_SHORT).show();

            String assetList = serialize(assetScannedList, "data", "data");
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), assetList);
            assetViewModel.verifyAssets("Bearer " + accessToken, requestBody);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, AssetsOptions.class);
        startActivity(loc);

    }
}

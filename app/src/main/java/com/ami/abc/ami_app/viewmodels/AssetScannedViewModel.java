package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;

import com.ami.abc.ami_app.models.AssetScanned;
import com.ami.abc.ami_app.repos.AssetScannedRepo;

public class AssetScannedViewModel extends AndroidViewModel {

    private AssetScannedRepo assetScannedRepo;

    public AssetScannedViewModel(Application application) {
        super(application);
       assetScannedRepo = new AssetScannedRepo();
    }

    //save individual assets scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(AssetScanned assetScanned) {
        assetScannedRepo.saveAssetsScanned(assetScanned);
    }

    public void updateScannedAsset(AssetScanned assetScanned){
        assetScannedRepo.updateAssetsScanned(assetScanned);
    }
}

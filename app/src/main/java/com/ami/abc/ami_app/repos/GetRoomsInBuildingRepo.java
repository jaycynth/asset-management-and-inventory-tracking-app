package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.GetRoomInBuildingState;
import com.ami.abc.ami_app.models.GetRoomInBuilding;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRoomsInBuildingRepo {

    private ApiClient mApiClient;

    public GetRoomsInBuildingRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<GetRoomInBuildingState> getRoomsInBuilding(int building_id, String accessToken) {

        MutableLiveData<GetRoomInBuildingState> getRoomInBuildingStateMutableLiveData = new MutableLiveData<>();

        Call<GetRoomInBuilding> call = mApiClient.amiService().getRoomsInBuilding(building_id, accessToken);
        call.enqueue(new Callback<GetRoomInBuilding>() {
            @Override
            public void onResponse(Call<GetRoomInBuilding> call, Response<GetRoomInBuilding> response) {
                if (response.code() == 200) {
                    getRoomInBuildingStateMutableLiveData.setValue(new GetRoomInBuildingState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    getRoomInBuildingStateMutableLiveData.setValue(new GetRoomInBuildingState(loginError));
                }else {
                    getRoomInBuildingStateMutableLiveData.setValue(new GetRoomInBuildingState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetRoomInBuilding> call, Throwable t) {
                getRoomInBuildingStateMutableLiveData.setValue(new GetRoomInBuildingState(t));
            }
        });

        return getRoomInBuildingStateMutableLiveData;

    }
}
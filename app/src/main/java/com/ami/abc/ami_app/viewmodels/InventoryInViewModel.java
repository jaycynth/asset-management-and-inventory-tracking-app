package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AddInventoryImageState;
import com.ami.abc.ami_app.datastates.AddInventoryState;
import com.ami.abc.ami_app.datastates.AssetState;
import com.ami.abc.ami_app.datastates.IncrementInventoryState;
import com.ami.abc.ami_app.datastates.InventoryScannedStrictState;
import com.ami.abc.ami_app.datastates.InventoryState;
import com.ami.abc.ami_app.datastates.SupplierState;
import com.ami.abc.ami_app.models.InventoryInReport;
import com.ami.abc.ami_app.repos.AddInventoryImageRepo;
import com.ami.abc.ami_app.repos.AddInventoryRepo;
import com.ami.abc.ami_app.repos.AssetRepo;
import com.ami.abc.ami_app.repos.InventoryInReportsRepo;
import com.ami.abc.ami_app.repos.InventoryIncrementRepo;
import com.ami.abc.ami_app.repos.InventoryRepo;
import com.ami.abc.ami_app.repos.InventoryScannedStrictRepo;
import com.ami.abc.ami_app.repos.SuppliersRepo;

import java.io.File;

public class InventoryInViewModel extends AndroidViewModel {
    private MediatorLiveData<AssetState> assetStateMediatorLiveData;
    private AssetRepo assetRepo;

    private MediatorLiveData<SupplierState> supplierStateMediatorLiveData;
    private SuppliersRepo suppliersRepo;

    private MediatorLiveData<InventoryState> inventoryStateMediatorLiveData;
    private InventoryRepo inventoryRepo;

    private MediatorLiveData<IncrementInventoryState> incrementInventoryStateMediatorLiveData;
    private InventoryIncrementRepo inventoryIncrementRepo;

    private MediatorLiveData<AddInventoryState> addInventoryStateMediatorLiveData;
    private AddInventoryRepo addInventoryRepo;

    private MediatorLiveData<AddInventoryImageState> addInventoryImageStateMediatorLiveData;
    private AddInventoryImageRepo addInventoryImageRepo;

    private MediatorLiveData<InventoryScannedStrictState> inventoryScannedStrictStateMediatorLiveData;
    private InventoryScannedStrictRepo inventoryScannedStrictRepo;

    //saving reports to room
    private InventoryInReportsRepo inventorysInReportsRepo;


    public InventoryInViewModel(Application application) {
       super(application);
        assetStateMediatorLiveData = new MediatorLiveData<>();
        assetRepo = new AssetRepo(application);

        supplierStateMediatorLiveData = new MediatorLiveData<>();
        suppliersRepo = new SuppliersRepo(application);

        inventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryRepo = new InventoryRepo(application);

        incrementInventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryIncrementRepo = new InventoryIncrementRepo(application);

        addInventoryStateMediatorLiveData = new MediatorLiveData<>();
        addInventoryRepo = new AddInventoryRepo(application);

        addInventoryImageStateMediatorLiveData  = new MediatorLiveData<>();
        addInventoryImageRepo = new AddInventoryImageRepo(application);

        inventoryScannedStrictStateMediatorLiveData = new MediatorLiveData<>();
        inventoryScannedStrictRepo  = new InventoryScannedStrictRepo(application);

        //saving reports to room
        inventorysInReportsRepo = new InventoryInReportsRepo();
    }

    //save individual inventorys scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedInventory(InventoryInReport inventoryInReport) {
        inventorysInReportsRepo.saveInventorys(inventoryInReport);
    }

    public LiveData<InventoryScannedStrictState> inventoryScannedStrictResponse(){
        return inventoryScannedStrictStateMediatorLiveData;
    }

    public void inventoryScannedStrict(String code,String store, String accessToken){

        LiveData<InventoryScannedStrictState> inventoryScannedStrictStateLiveData = inventoryScannedStrictRepo.getInventoryScannedStrict(code,store, accessToken);
        inventoryScannedStrictStateMediatorLiveData.addSource(inventoryScannedStrictStateLiveData,
                inventoryScannedStrictStateMediatorLiveData-> {
                    if (this.inventoryScannedStrictStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryScannedStrictStateMediatorLiveData.removeSource(inventoryScannedStrictStateLiveData);
                    }
                    this.inventoryScannedStrictStateMediatorLiveData.setValue(inventoryScannedStrictStateMediatorLiveData);
                });

    }

    public LiveData<SupplierState> getAllSuppliersResponse(){
        return supplierStateMediatorLiveData;
    }

    public void getAllSuppliers(String accessToken){
        LiveData<SupplierState> supplierStateLiveData = suppliersRepo.getAllSuppliers(accessToken);
        supplierStateMediatorLiveData.addSource(supplierStateLiveData,
                supplierStateMediatorLiveData -> {
                    if (this.supplierStateMediatorLiveData.hasActiveObservers()){
                        this.supplierStateMediatorLiveData.removeSource(supplierStateLiveData);
                    }
                    this.supplierStateMediatorLiveData.setValue(supplierStateMediatorLiveData);
                });
    }

    public LiveData<InventoryState> getAllInventoryResponse(){
        return inventoryStateMediatorLiveData;
    }

    public void getAllInventory(String accessToken){
        LiveData<InventoryState> inventoryStateLiveData = inventoryRepo.getAllInventory(accessToken);
        inventoryStateMediatorLiveData.addSource(inventoryStateLiveData,
                inventoryStateMediatorLiveData -> {
                    if (this.inventoryStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryStateMediatorLiveData.removeSource(inventoryStateLiveData);
                    }
                    this.inventoryStateMediatorLiveData.setValue(inventoryStateMediatorLiveData);
                });
    }

    public LiveData<IncrementInventoryState> incrementInventoryResponse(){
        return incrementInventoryStateMediatorLiveData;
    }

    public void incrementInventory(String accessToken,int inventoryId, int quantity){
        LiveData<IncrementInventoryState> incrementInventoryStateLiveData = inventoryIncrementRepo.incrementInventory(accessToken, inventoryId, quantity);
        incrementInventoryStateMediatorLiveData.addSource(incrementInventoryStateLiveData,
                incrementInventoryStateMediatorLiveData -> {
                    if (this.incrementInventoryStateMediatorLiveData.hasActiveObservers()){
                        this.incrementInventoryStateMediatorLiveData.removeSource(incrementInventoryStateLiveData);
                    }
                    this.incrementInventoryStateMediatorLiveData.setValue(incrementInventoryStateMediatorLiveData);
                });
    }


    public LiveData<AddInventoryState> addInventoryResponse(){
        return addInventoryStateMediatorLiveData;
    }

    public void addInventory(String accessToken, String productName, String productCode, String building,
                             String room, String supplier,int quantity, int unitPrice, int reorderLevel){
        LiveData<AddInventoryState> addInventoryStateLiveData = addInventoryRepo.addInventory(accessToken, productName, productCode, building, room, supplier, quantity, unitPrice, reorderLevel);
        addInventoryStateMediatorLiveData.addSource(addInventoryStateLiveData,
                addInventoryStateMediatorLiveData -> {
                    if (this.addInventoryStateMediatorLiveData.hasActiveObservers()){
                        this.addInventoryStateMediatorLiveData.removeSource(addInventoryStateLiveData);
                    }
                    this.addInventoryStateMediatorLiveData.setValue(addInventoryStateMediatorLiveData);
                });
    }


    public LiveData<AssetState> getAllAssetResponse() {
        return assetStateMediatorLiveData;
    }

    public void getAllAssets(String accessToken) {
        LiveData<AssetState> assetStateLiveData = assetRepo.getAllAssets(accessToken);
        assetStateMediatorLiveData.addSource(assetStateLiveData,
                assetStateMediatorLiveData -> {
                    if (this.assetStateMediatorLiveData.hasActiveObservers()) {
                        this.assetStateMediatorLiveData.removeSource(assetStateLiveData);
                    }
                    this.assetStateMediatorLiveData.setValue(assetStateMediatorLiveData);
                });
    }


    public LiveData<AddInventoryImageState> addInventoryImage(){
        return addInventoryImageStateMediatorLiveData;
    }

    public void postAddInventoryImage(String accessToken, File photo, int inventoryId){
        LiveData<AddInventoryImageState> addInventoryImageStateLiveData = addInventoryImageRepo.postAddInventoryImage(accessToken, photo, inventoryId);
        addInventoryImageStateMediatorLiveData.addSource(addInventoryImageStateLiveData,
                addInventoryImageStateMediatorLiveData -> {
                    if (this.addInventoryImageStateMediatorLiveData.hasActiveObservers()){
                        this.addInventoryImageStateMediatorLiveData.removeSource(addInventoryImageStateLiveData);
                    }
                    this.addInventoryImageStateMediatorLiveData.setValue(addInventoryImageStateMediatorLiveData);
                });

    }

}

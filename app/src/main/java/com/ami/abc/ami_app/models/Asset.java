package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Asset {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image_id")
    @Expose
    private Object imageId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("building")
    @Expose
    private String building;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("depart")
    @Expose
    private String depart;
    @SerializedName("cust")
    @Expose
    private String cust;
    @SerializedName("supplier")
    @Expose
    private String supplier;
    @SerializedName("date_of_purchase")
    @Expose
    private String dateOfPurchase;
    @SerializedName("purchase_cost")
    @Expose
    private String purchaseCost;
    @SerializedName("est_end_of_life_cost")
    @Expose
    private String estEndOfLifeCost;
    @SerializedName("est_no_hours")
    @Expose
    private Object estNoHours;
    @SerializedName("no_hours_worked")
    @Expose
    private Object noHoursWorked;
    @SerializedName("verified")
    @Expose
    private Integer verified;
    @SerializedName("discontinued")
    @Expose
    private String discontinued;
    @SerializedName("division")
    @Expose
    private String division;

    @SerializedName("dynamicfields")
    @Expose
    private List<String> dynamicfields = null;
    @SerializedName("category")
    @Expose
    private Category category;


    @SerializedName("asset_category")
    @Expose
    private String assetCategory;
    @SerializedName("breakdown_purchase_cost")
    @Expose
    private String breakdownPurchaseCost;

    @SerializedName("dismantling_costs")
    @Expose
    private String dismantlingCosts;

    @SerializedName("freight_cost")
    @Expose
    private String freightCost;
    @SerializedName("import_duties")
    @Expose
    private String importDuties;

    @SerializedName("non_refundable_taxes")
    @Expose
    private String nonRefundableTaxes;


    @SerializedName("set_up_cost")
    @Expose
    private String setUpCost;

    @SerializedName("sub_category_id")
    @Expose
    private Object subCategoryId;


    public String getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(String assetCategory) {
        this.assetCategory = assetCategory;
    }

    public String getBreakdownPurchaseCost() {
        return breakdownPurchaseCost;
    }

    public void setBreakdownPurchaseCost(String breakdownPurchaseCost) {
        this.breakdownPurchaseCost = breakdownPurchaseCost;
    }


    public String getDismantlingCosts() {
        return dismantlingCosts;
    }

    public void setDismantlingCosts(String dismantlingCosts) {
        this.dismantlingCosts = dismantlingCosts;
    }


    public String getFreightCost() {
        return freightCost;
    }

    public void setFreightCost(String freightCost) {
        this.freightCost = freightCost;
    }

    public String getImportDuties() {
        return importDuties;
    }

    public void setImportDuties(String importDuties) {
        this.importDuties = importDuties;
    }


    public String getNonRefundableTaxes() {
        return nonRefundableTaxes;
    }

    public void setNonRefundableTaxes(String nonRefundableTaxes) {
        this.nonRefundableTaxes = nonRefundableTaxes;
    }


    public String getSetUpCost() {
        return setUpCost;
    }

    public void setSetUpCost(String setUpCost) {
        this.setUpCost = setUpCost;
    }


    public Object getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Object subCategoryId) {
        this.subCategoryId = subCategoryId;
    }


    public List<String> getDynamicfields() {
        return dynamicfields;
    }

    public void setDynamicfields(List<String> dynamicfields) {
        this.dynamicfields = dynamicfields;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getImageId() {
        return imageId;
    }

    public void setImageId(Object imageId) {
        this.imageId = imageId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseCost(String purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public String getEstEndOfLifeCost() {
        return estEndOfLifeCost;
    }

    public void setEstEndOfLifeCost(String estEndOfLifeCost) {
        this.estEndOfLifeCost = estEndOfLifeCost;
    }

    public Object getEstNoHours() {
        return estNoHours;
    }

    public void setEstNoHours(Object estNoHours) {
        this.estNoHours = estNoHours;
    }

    public Object getNoHoursWorked() {
        return noHoursWorked;
    }

    public void setNoHoursWorked(Object noHoursWorked) {
        this.noHoursWorked = noHoursWorked;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public String getDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(String discontinued) {
        this.discontinued = discontinued;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
}

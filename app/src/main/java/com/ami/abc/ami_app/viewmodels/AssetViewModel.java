package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AssetState;
import com.ami.abc.ami_app.datastates.GetAssetScannedDetailsState;
import com.ami.abc.ami_app.datastates.VerifyAssetState;
import com.ami.abc.ami_app.models.AssetCheckReport;
import com.ami.abc.ami_app.models.AssetScanned;
import com.ami.abc.ami_app.repos.AssetCheckReportsRepo;
import com.ami.abc.ami_app.repos.AssetRepo;
import com.ami.abc.ami_app.repos.AssetScannedRepo;
import com.ami.abc.ami_app.repos.GetAssetsScannedRepo;
import com.ami.abc.ami_app.repos.VerifyAssetRepo;

import java.util.List;

import okhttp3.RequestBody;

public class AssetViewModel extends AndroidViewModel {
    private MediatorLiveData<AssetState> assetStateMediatorLiveData;
    private AssetRepo assetRepo;

    private AssetScannedRepo assetScannedRepo;

    private MediatorLiveData<VerifyAssetState> verifyAssetStateMediatorLiveData;
    private VerifyAssetRepo verifyAssetRepo;

    private LiveData<List<AssetScanned>> listLiveData;

    private MediatorLiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateMediatorLiveData;
    private GetAssetsScannedRepo getAssetsScannedRepo;

    private AssetCheckReportsRepo assetsCheckReportsRepo;


    public AssetViewModel(Application application) {
        super(application);
        assetStateMediatorLiveData = new MediatorLiveData<>();
        assetRepo = new AssetRepo(application);

        assetScannedRepo = new AssetScannedRepo();
        listLiveData = assetScannedRepo.getAssetsScanned();

        verifyAssetStateMediatorLiveData = new MediatorLiveData<>();
        verifyAssetRepo = new VerifyAssetRepo(application);

        getAssetScannedDetailsStateMediatorLiveData = new MediatorLiveData<>();
        getAssetsScannedRepo = new GetAssetsScannedRepo(application);


        assetsCheckReportsRepo = new AssetCheckReportsRepo();


    }

    //saves asset check data to room
    public void saveAssetCheckReports(AssetCheckReport assetCheckReport) {
        assetsCheckReportsRepo.saveAssets(assetCheckReport);
    }

    //delete all assets checked stored in the asset check table
    public void deleteAssets() {
        assetsCheckReportsRepo.deleteAssets();
    }


    public LiveData<AssetState> getAllAssetResponse() {
        return assetStateMediatorLiveData;
    }

    public void getAllAssets(String accessToken) {
        LiveData<AssetState> assetStateLiveData = assetRepo.getAllAssets(accessToken);
        assetStateMediatorLiveData.addSource(assetStateLiveData,
                assetStateMediatorLiveData -> {
                    if (this.assetStateMediatorLiveData.hasActiveObservers()) {
                        this.assetStateMediatorLiveData.removeSource(assetStateLiveData);
                    }
                    this.assetStateMediatorLiveData.setValue(assetStateMediatorLiveData);
                });
    }


    //data we are posting to server
    public void saveAssetReports(AssetScanned assetScanned) {
        assetScannedRepo.saveAssetsScanned(assetScanned);
    }

    //emptying the table after we post to server
    public void deleteScannedAssets() {
        assetScannedRepo.deleteAssets();
    }


    public LiveData<VerifyAssetState> verifyAssetsResponse() {
        return verifyAssetStateMediatorLiveData;
    }


    public void verifyAssets(String accessToken, RequestBody requestBody) {
        LiveData<VerifyAssetState> verifyAssetStateLiveData = verifyAssetRepo.verifyAssets(accessToken, requestBody);
        verifyAssetStateMediatorLiveData.addSource(verifyAssetStateLiveData,
                verifyAssetStateMediatorLiveData -> {
                    if (this.verifyAssetStateMediatorLiveData.hasActiveObservers()) {
                        this.verifyAssetStateMediatorLiveData.removeSource(verifyAssetStateLiveData);
                    }
                    this.verifyAssetStateMediatorLiveData.setValue(verifyAssetStateMediatorLiveData);
                });

    }

    public LiveData<List<AssetScanned>> getListLiveData() {
        return listLiveData;
    }


    public LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsResponse() {
        return getAssetScannedDetailsStateMediatorLiveData;
    }

    public void getAssetScannedDetails(String code, String accessToken) {
        LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateLiveData = getAssetsScannedRepo.getAssetScanned(code, accessToken);
        getAssetScannedDetailsStateMediatorLiveData.addSource(getAssetScannedDetailsStateLiveData,
                getAssetScannedDetailsStateMediatorLiveData -> {
                    if (this.getAssetScannedDetailsStateMediatorLiveData.hasActiveObservers()) {
                        this.getAssetScannedDetailsStateMediatorLiveData.removeSource(getAssetScannedDetailsStateLiveData);
                    }
                    this.getAssetScannedDetailsStateMediatorLiveData.setValue(getAssetScannedDetailsStateMediatorLiveData);
                });

    }


}

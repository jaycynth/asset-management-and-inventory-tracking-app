package com.ami.abc.ami_app.database;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import android.content.Context;
import androidx.annotation.NonNull;

import com.ami.abc.ami_app.database.converters.DateTypeConverter;
import com.ami.abc.ami_app.database.dao.AssetCheckInReportDao;
import com.ami.abc.ami_app.database.dao.AssetCheckOutReportDao;
import com.ami.abc.ami_app.database.dao.AssetsCheckReportDao;
import com.ami.abc.ami_app.database.dao.AssetsDao;
import com.ami.abc.ami_app.database.dao.AssetsInReportDao;
import com.ami.abc.ami_app.database.dao.ComponentsDao;
import com.ami.abc.ami_app.database.dao.CountedItemsDao;
import com.ami.abc.ami_app.database.dao.InventoryCheckInReportDao;
import com.ami.abc.ami_app.database.dao.InventoryCheckOutReportDao;
import com.ami.abc.ami_app.database.dao.InventoryInReportDao;
import com.ami.abc.ami_app.database.dao.StockTakeReportDao;
import com.ami.abc.ami_app.models.AssetCheckInReport;
import com.ami.abc.ami_app.models.AssetCheckOutReport;
import com.ami.abc.ami_app.models.AssetCheckReport;
import com.ami.abc.ami_app.models.AssetInReport;
import com.ami.abc.ami_app.models.AssetScanned;
import com.ami.abc.ami_app.models.CacheComponents;
import com.ami.abc.ami_app.models.CountedItems;
import com.ami.abc.ami_app.models.InventoryCheckInReport;
import com.ami.abc.ami_app.models.InventoryCheckOutReport;
import com.ami.abc.ami_app.models.InventoryInReport;
import com.ami.abc.ami_app.models.StockTakeReport;

@Database(entities = {CountedItems.class, AssetScanned.class, AssetInReport.class, AssetCheckReport.class, AssetCheckInReport.class, AssetCheckOutReport.class, InventoryInReport.class, InventoryCheckInReport.class, InventoryCheckOutReport.class, StockTakeReport.class, CacheComponents.class}, version = 29, exportSchema = false)
@TypeConverters(DateTypeConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    //Singleton
    private static volatile AppDatabase INSTANCE;
    private static final Callback callback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

        }
    };

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "ami_db")
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }



    //all dao
    public abstract AssetsDao assetsDao();



    //for assets reports
    public abstract AssetsInReportDao assetsInReportDao();

    public abstract AssetsCheckReportDao assetCheckReport();

    public abstract AssetCheckInReportDao assetCheckInReportDao();

    public abstract AssetCheckOutReportDao assetCheckOutReportDao();


    //for inventory reports
    public abstract InventoryInReportDao inventoryInReportDao();

    public abstract InventoryCheckOutReportDao inventoryCheckOutReportDao();

    public abstract InventoryCheckInReportDao inventoryCheckInReportDao();

    public abstract StockTakeReportDao stockTakeReportDao();

    public abstract CountedItemsDao countedItemsDao();

    public abstract ComponentsDao componentsDao();


}
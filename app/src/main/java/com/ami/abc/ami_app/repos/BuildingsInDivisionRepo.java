package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.BuildingsInDivisionState;
import com.ami.abc.ami_app.models.GetBuildingsInDivision;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuildingsInDivisionRepo {
    private ApiClient mApiClient;

    //constructor
    public BuildingsInDivisionRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<BuildingsInDivisionState> getBuildingsInDivision(int divisionId,String accessToken) {

        MutableLiveData<BuildingsInDivisionState> buildingsInDivisionStateMutableLiveData = new MutableLiveData<>();
        Call<GetBuildingsInDivision> call = mApiClient.amiService().getBuildingsInDivision(divisionId,accessToken);
        call.enqueue(new Callback<GetBuildingsInDivision>() {
            @Override
            public void onResponse(Call<GetBuildingsInDivision> call, Response<GetBuildingsInDivision> response) {
                if (response.code() == 200) {
                    buildingsInDivisionStateMutableLiveData.setValue(new BuildingsInDivisionState(response.body()));

                } else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    buildingsInDivisionStateMutableLiveData.setValue(new BuildingsInDivisionState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    buildingsInDivisionStateMutableLiveData.setValue(new BuildingsInDivisionState(error));
                }
                else {
                    buildingsInDivisionStateMutableLiveData.setValue(new BuildingsInDivisionState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetBuildingsInDivision> call, Throwable t) {
                buildingsInDivisionStateMutableLiveData.setValue(new BuildingsInDivisionState(t));
            }
        });

        return buildingsInDivisionStateMutableLiveData;

    }
}

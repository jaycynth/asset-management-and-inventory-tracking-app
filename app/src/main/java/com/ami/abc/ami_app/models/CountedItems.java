package com.ami.abc.ami_app.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class CountedItems {

    @PrimaryKey
    private int id;
    
    private int quantity;
    private String product_code;
    private String shelf;
    private String store;


    public String getShelf() {
        return shelf;
    }

    public void setShelf(String shelf) {
        this.shelf = shelf;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int count) {
        this.quantity = count;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String inventoryCode) {
        this.product_code = inventoryCode;
    }
}

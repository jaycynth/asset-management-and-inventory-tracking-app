package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.VerifyAssetState;
import com.ami.abc.ami_app.repos.VerifyAssetRepo;

import okhttp3.RequestBody;

public class VerifyAssetViewModel extends AndroidViewModel {
    private MediatorLiveData<VerifyAssetState> verifyAssetStateMediatorLiveData;
    private VerifyAssetRepo verifyAssetRepo;


    public VerifyAssetViewModel(Application application) {
        super(application);
        verifyAssetStateMediatorLiveData = new MediatorLiveData<>();
        verifyAssetRepo = new VerifyAssetRepo(application);
    }

    public LiveData<VerifyAssetState> verifyAssetsResponse() {
        return verifyAssetStateMediatorLiveData;
    }

    public void verifyAssets(String accessToken, RequestBody requestBody) {
        LiveData<VerifyAssetState> verifyAssetStateLiveData = verifyAssetRepo.verifyAssets(accessToken, requestBody);
        verifyAssetStateMediatorLiveData.addSource(verifyAssetStateLiveData,
                verifyAssetStateMediatorLiveData -> {
                    if (this.verifyAssetStateMediatorLiveData.hasActiveObservers()) {
                        this.verifyAssetStateMediatorLiveData.removeSource(verifyAssetStateLiveData);
                    }
                    this.verifyAssetStateMediatorLiveData.setValue(verifyAssetStateMediatorLiveData);
                });

    }

}

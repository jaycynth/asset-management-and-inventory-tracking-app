package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.TwoFactorAuthState;
import com.ami.abc.ami_app.models.TwoFactorAuth;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TwoFactorAuthRepo {

    private ApiClient mApiClient;

    public TwoFactorAuthRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<TwoFactorAuthState> getTwoFactorAuth(String accessToken, String code) {
        MutableLiveData<TwoFactorAuthState> twoFactorAuthStateMutableLiveData = new MutableLiveData<>();
        Call<TwoFactorAuth> call = mApiClient.amiService().twoFactorAuth(accessToken, code);
        call.enqueue(new Callback<TwoFactorAuth>() {
            @Override
            public void onResponse(Call<TwoFactorAuth> call, Response<TwoFactorAuth> response) {
                if (response.code() == 200) {
                    twoFactorAuthStateMutableLiveData.setValue(new TwoFactorAuthState(response.body()));

                } else {
                    twoFactorAuthStateMutableLiveData.setValue(new TwoFactorAuthState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<TwoFactorAuth> call, Throwable t) {
                twoFactorAuthStateMutableLiveData.setValue(new TwoFactorAuthState(t));
            }
        });

        return twoFactorAuthStateMutableLiveData;

    }
}

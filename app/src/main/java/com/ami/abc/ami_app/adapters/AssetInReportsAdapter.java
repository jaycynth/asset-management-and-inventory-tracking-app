package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AssetInReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetInReportsAdapter extends RecyclerView.Adapter<AssetInReportsAdapter.MyViewHolder> {
    private List<AssetInReport> assetInReportList;
    private Context context;

    public AssetInReportsAdapter(List<AssetInReport> assetInReportList, Context context) {
        this.assetInReportList = assetInReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.location)
        TextView location;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public AssetInReportsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.assets_report_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetInReportsAdapter.MyViewHolder holder, int i) {
        AssetInReport assetScanned = assetInReportList.get(i);
        holder.asset_code.setText(assetScanned.getBarcode());
        holder.name.setText("Product Name : " + assetScanned.getName());
        holder.location.setText("Located at : "+ assetScanned.getLocation());


    }

    @Override
    public int getItemCount() {
        return assetInReportList.size();
    }

    public void setAssetInReportList(List<AssetInReport> assetInReportList){
        this.assetInReportList = assetInReportList;
        notifyDataSetChanged();
    }
}

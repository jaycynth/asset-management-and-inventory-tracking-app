package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.MoveAsset;

public class MoveAssetState {
    private MoveAsset moveAsset;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;


    public MoveAssetState(MoveAsset moveAsset) {
        this.moveAsset = moveAsset;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
    }

    public MoveAssetState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.moveAsset = null;
        this.loginError = null;
    }

    public MoveAssetState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.moveAsset = null;
        this.loginError = null;
    }

    public MoveAssetState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.moveAsset = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public MoveAsset getMoveAsset() {
        return moveAsset;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

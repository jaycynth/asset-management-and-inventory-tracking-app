package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllStores;
import com.ami.abc.ami_app.models.LoginError;

public class StoreState {
    private AllStores allStores;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public StoreState(AllStores allStores) {
        this.allStores = allStores;
        this.message = null;
        this.errorThrowable = null;
        this.loginError= null;
    }

    public StoreState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.loginError = null;
        this.allStores = null;
    }

    public StoreState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.loginError = null;
        this.allStores = null;
        this.message = null;
    }

    public StoreState(LoginError loginError) {
        this.loginError = loginError;
        this.allStores = null;
        this.errorThrowable = null;
        this.message = null;
    }

    public AllStores getAllStores() {
        return allStores;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

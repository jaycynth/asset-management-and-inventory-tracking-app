package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllCategories;
import com.ami.abc.ami_app.models.LoginError;

public class CategoryState {
    private AllCategories allCategories;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public CategoryState(AllCategories allCategories) {
        this.allCategories = allCategories;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public CategoryState(String message) {
        this.message = message;
        this.allCategories = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public CategoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allCategories = null;
        this.loginError = null;
    }

    public CategoryState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allCategories = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllCategories getAllCategories() {
        return allCategories;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.MoveAssetState;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.MoveAsset;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoveAssetRepo {
    private ApiClient mApiClient;

    //constructor
    public MoveAssetRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<MoveAssetState> moveAsset(String accessToken,int assetId, String building, String room, String department,String division,String cust){

        MutableLiveData<MoveAssetState> moveAssetStateMutableLiveData = new MutableLiveData<>();
        Call<MoveAsset> call = mApiClient.amiService().moveAsset(accessToken, assetId, building, room, department, division, cust);
        call.enqueue(new Callback<MoveAsset>() {
            @Override
            public void onResponse(Call<MoveAsset> call, Response<MoveAsset> response) {
                if (response.code() == 200) {
                    moveAssetStateMutableLiveData.setValue(new MoveAssetState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    moveAssetStateMutableLiveData.setValue(new MoveAssetState(loginError));
                }else {
                    moveAssetStateMutableLiveData.setValue(new MoveAssetState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<MoveAsset> call, Throwable t) {
                moveAssetStateMutableLiveData.setValue(new MoveAssetState(t));
            }
        });

        return moveAssetStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.PostStockTake;

public class PostStockTakeState {

    private PostStockTake stockTake;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public PostStockTakeState(PostStockTake stockTake) {
        this.stockTake = stockTake;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public PostStockTakeState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.stockTake = null;
        this.loginError = null;

    }

    public PostStockTakeState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.stockTake = null;
        this.message = null;
        this.loginError = null;
    }

    public PostStockTakeState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.stockTake = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public PostStockTake getStockTake() {
        return stockTake;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}



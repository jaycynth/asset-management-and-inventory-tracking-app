package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryState;
import com.ami.abc.ami_app.models.AllInventory;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryRepo {

    private ApiClient mApiClient;
    //constructor
    public InventoryRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<InventoryState> getAllInventory(String accessToken) {

        MutableLiveData<InventoryState> inventoryStateMutableLiveData = new MutableLiveData<>();
        Call<AllInventory> call = mApiClient.amiService().getAllInventory(accessToken);
        call.enqueue(new Callback<AllInventory>() {
            @Override
            public void onResponse(Call<AllInventory> call, Response<AllInventory> response) {
                if (response.code() == 200) {
                    inventoryStateMutableLiveData.setValue(new InventoryState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryStateMutableLiveData.setValue(new InventoryState(loginError));
                }else {
                    inventoryStateMutableLiveData.setValue(new InventoryState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllInventory> call, Throwable t) {
                inventoryStateMutableLiveData.setValue(new InventoryState(t));
            }
        });

        return inventoryStateMutableLiveData;

    }
}

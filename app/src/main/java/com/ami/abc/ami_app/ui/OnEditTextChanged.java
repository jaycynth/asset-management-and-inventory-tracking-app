package com.ami.abc.ami_app.ui;

public interface OnEditTextChanged {
    void onTextChanged(int pos, String name);
}

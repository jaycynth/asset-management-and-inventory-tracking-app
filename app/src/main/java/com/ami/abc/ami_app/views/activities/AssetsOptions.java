package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Role;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetsOptions extends AppCompatActivity {


    @BindView(R.id.asset_check_card)
    CardView asset_check_card;

    @BindView(R.id.asset_move_card)
    CardView asset_move_card;

    @BindView(R.id.asset_in_card)
    CardView asset_in_card;

    @BindView(R.id.search_card)
    CardView search_card;

    @BindView(R.id.check_in_card)
    CardView check_in_card;

    @BindView(R.id.check_out_card)
    CardView check_out_card;

    MainViewModel mainViewModel;
    String accessToken;

    @BindView(R.id.first_layout)
    LinearLayout firstLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    List<String> roleName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assets_options);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Assets");

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null) {
            firstLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            mainViewModel.getRole("Bearer " + accessToken);

        }


        mainViewModel.getRoleResponse().observe(this, getRoleState -> {
            assert getRoleState != null;
            if (getRoleState.getGetRole() != null) {
                handleRoles(getRoleState.getGetRole());
            }
            if (getRoleState.getMessage() != null) {
                handleNetworkResponse(getRoleState.getMessage());
            }

            if (getRoleState.getErrorThrowable() != null) {
                handleError(getRoleState.getErrorThrowable());
            }

            if (getRoleState.getLoginError() != null) {
                handleUnauthorized(getRoleState.getLoginError());
            }
        });


        asset_in_card.setOnClickListener(v -> {
            Intent scanAssets = new Intent(AssetsOptions.this, EnterLocation.class);
            scanAssets.putExtra("TAG", "A");
            startActivity(scanAssets);

        });


        asset_check_card.setOnClickListener(v -> {
            Intent scanAssets = new Intent(AssetsOptions.this, EnterLocation.class);
            scanAssets.putExtra("TAG", "B");
            startActivity(scanAssets);

        });

        asset_move_card.setOnClickListener(v -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Scanner : ");
            alert.setCancelable(true);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                Intent scanAssets = new Intent(AssetsOptions.this, AssetMove.class);
                scanAssets.putExtra("TAG", "C");
                startActivity(scanAssets);


            }));

            alert.setPositiveButton("Camera ", (dialog, which) -> {
                Intent scanAssets = new Intent(AssetsOptions.this, ScanActivity.class);
                scanAssets.putExtra("TAG", "C");
                startActivity(scanAssets);

                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();


        });

        search_card.setOnClickListener(v -> {
            Intent searchAssets = new Intent(AssetsOptions.this, SearchAssetsActivity.class);
            startActivity(searchAssets);

        });

        check_in_card.setOnClickListener(v -> {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Scanner : ");
            alert.setCancelable(true);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                Intent scanAssets = new Intent(AssetsOptions.this, AssetCheckInActivity.class);
                scanAssets.putExtra("TAG", "J");
                startActivity(scanAssets);


            }));

            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAssets = new Intent(AssetsOptions.this, ScanActivity.class);
                scanAssets.putExtra("TAG", "J");
                startActivity(scanAssets);

                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();


        });

        check_out_card.setOnClickListener(v -> {
            Intent scanAssets = new Intent(AssetsOptions.this, EnterLocation.class);
            scanAssets.putExtra("TAG", "I");
            startActivity(scanAssets);
        });
    }

    private void handleRoles(GetRole getRole) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        List<Role> roleList;
        roleList = getRole.getRoles();

        for (Role role : roleList) {
            roleName.add(role.getName());
        }

        final View.OnClickListener onClickListener = v ->
                Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();

        if (roleName.contains("Asset Check In") && roleName.contains("Asset Check Out") && roleName.contains("Asset Upload")) {


            asset_move_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Check Out") && roleName.contains("Asset Verification")) {

            asset_move_card.setOnClickListener(onClickListener);

            asset_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Check Out") && roleName.contains("Asset Move")) {

            asset_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Upload") && roleName.contains("Asset Move")) {

            check_out_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Upload") && roleName.contains("Asset Verification")) {

            check_out_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out") && roleName.contains("Asset Upload") && roleName.contains("Asset Move")) {

            check_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out") && roleName.contains("Asset Upload") && roleName.contains("Asset Verification")) {

            check_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Upload") && roleName.contains("Asset Move") && roleName.contains("Asset Verification")) {

            check_in_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Check Out")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Verification")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Upload")) {
            asset_check_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check In") && roleName.contains("Asset Move")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out") && roleName.contains("Asset Verification")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out") && roleName.contains("Asset Move")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out") && roleName.contains("Asset Upload")) {
            asset_check_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Upload") && roleName.contains("Asset Verification")) {
            asset_move_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Asset Upload") && roleName.contains("Asset Move")) {
            asset_check_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Asset Verification") && roleName.contains("Asset Move")) {
            asset_in_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Check Out")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Asset Check In")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Asset Verification")) {
            asset_in_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Asset Move")) {
            asset_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Asset Upload")) {
            asset_check_card.setOnClickListener(onClickListener);

            asset_move_card.setOnClickListener(onClickListener);

            check_out_card.setOnClickListener(onClickListener);

            check_in_card.setOnClickListener(onClickListener);

            asset_check_card.setOnClickListener(onClickListener);

        }
    }


    private void handleError(Throwable errorThrowable) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnauthorized(LoginError loginError) {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent unauth = new Intent(this, LoginActivity.class);
//        startActivity(unauth);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(AssetsOptions.this, ScanOptionsActivity.class);
        startActivity(back);
    }
}

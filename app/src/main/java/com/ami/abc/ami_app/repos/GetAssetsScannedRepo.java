package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.GetAssetScannedDetailsState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAssetsScannedRepo {
    private ApiClient mApiClient;
    //constructor
    public GetAssetsScannedRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<GetAssetScannedDetailsState> getAssetScanned(String code, String accessToken) {

        MutableLiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateMutableLiveData = new MutableLiveData<>();
        Call<GetAssetScannedDetails> call = mApiClient.amiService().getAssetsScanned(code, accessToken);
        call.enqueue(new Callback<GetAssetScannedDetails>() {
            @Override
            public void onResponse(Call<GetAssetScannedDetails> call, Response<GetAssetScannedDetails> response) {
                if (response.code() == 200) {
                    getAssetScannedDetailsStateMutableLiveData.setValue(new GetAssetScannedDetailsState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    getAssetScannedDetailsStateMutableLiveData.setValue(new GetAssetScannedDetailsState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    getAssetScannedDetailsStateMutableLiveData.setValue(new GetAssetScannedDetailsState(failError));
                }
            }

            @Override
            public void onFailure(Call<GetAssetScannedDetails> call, Throwable t) {
                getAssetScannedDetailsStateMutableLiveData.setValue(new GetAssetScannedDetailsState(t));
            }
        });

        return getAssetScannedDetailsStateMutableLiveData;

    }
}

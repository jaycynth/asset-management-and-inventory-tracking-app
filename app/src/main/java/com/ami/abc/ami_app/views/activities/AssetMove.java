package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.AllDepartments;

import com.ami.abc.ami_app.models.Asset;

import com.ami.abc.ami_app.models.Building;
import com.ami.abc.ami_app.models.Custodian;
import com.ami.abc.ami_app.models.Department;
import com.ami.abc.ami_app.models.Divisions;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.GetBuildingsInDivision;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.models.GetRoomInBuilding;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.MoveAsset;
import com.ami.abc.ami_app.models.Room;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetMove extends AppCompatActivity {

    @BindView(R.id.asset_id)
    EditText asset_id;
    @BindView(R.id.asset_name)
    EditText asset_name;


    @BindView(R.id.from_building)
    EditText fromBuilding;
    @BindView(R.id.from_room)
    EditText fromRoom;
    @BindView(R.id.from_department)
    EditText fromDepartment;
    @BindView(R.id.from_division)
    EditText fromDivision;
    @BindView(R.id.from_custodian)
    EditText fromCustodian;


    @BindView(R.id.division)
    AutoCompleteTextView division;
    @BindView(R.id.drop_down_division)
    ImageView drop_down_division;

    @BindView(R.id.building)
    AutoCompleteTextView mBuilding;
    @BindView(R.id.drop_down_building)
    ImageView drop_down_building;

    @BindView(R.id.room)
    AutoCompleteTextView mRoom;
    @BindView(R.id.drop_down_room)
    ImageView drop_down_room;

    @BindView(R.id.department)
    AutoCompleteTextView department;
    @BindView(R.id.drop_down_department)
    ImageView drop_down_department;

    @BindView(R.id.custodian)
    AutoCompleteTextView custodian;
    @BindView(R.id.drop_down_custodian)
    ImageView drop_down_custodian;

    private List<Building> buildingList = new ArrayList<>();
    private List<String> buildingName = new ArrayList<>();

    private List<String> roomName = new ArrayList<>();

    private List<String> departmentName = new ArrayList<>();

    private List<String> divisionName = new ArrayList<>();


    @BindView(R.id.change)
    Button change;

    String building, room, mDepartment, mDivision, assetId;

    MainViewModel mainViewModel;
    String accessToken;

    @BindView(R.id.main_spin_kit)
    ProgressBar main_spin_kit;

    @BindView(R.id.layout_location_details)
    LinearLayout layout_location_details;

    List<Asset> assetList = new ArrayList<>();
    int id;

    @BindView(R.id.spin_kit)
    ProgressBar change_progress;

    int buildingId, divisionId;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    LinearLayout detailsLayout;


    String name, tag;
    String assetName;
    String fromDiv, fromBuild, fromRom, fromDepart;

    List<String> custodianName = new ArrayList<>();
    List<Custodian> custodians = new ArrayList<>();

    List<Divisions> divisionsList = new ArrayList<>();

    String custodianCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_move);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.asset_move_title));

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);


        Intent scanIntents = getIntent();
        assetId = scanIntents.getStringExtra("scanResult");
        tag = scanIntents.getStringExtra("TAG");
        fromDiv = scanIntents.getStringExtra("fromDivision");
        fromBuild = scanIntents.getStringExtra("fromBuilding");
        fromRom = scanIntents.getStringExtra("fromRoom");
        fromDepart = scanIntents.getStringExtra("fromDepart");


        asset_id.requestFocus();


        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            if (assetId != null) {

                asset_id.setText(assetId);

                confirm.setVisibility(View.GONE);
                detailsLayout.setVisibility(View.VISIBLE);

                main_spin_kit.setVisibility(View.VISIBLE);
                layout_location_details.setAlpha(0.3f);
                mainViewModel.getDivisions("Bearer " + accessToken);
               // mainViewModel.getAllBuildings("Bearer " + accessToken);
                mainViewModel.getDepartments("Bearer " + accessToken);
                mainViewModel.getAllAssets("Bearer " + accessToken);
                mainViewModel.getAllCustodians("Bearer " + accessToken);
                mainViewModel.getAssetScannedDetails(assetId, "Bearer " + accessToken);

            }
        }


        confirm.setOnClickListener(v -> {
            String code = asset_id.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                main_spin_kit.setVisibility(View.VISIBLE);
                layout_location_details.setAlpha(0.3f);
                mainViewModel.getDivisions("Bearer " + accessToken);
               // mainViewModel.getAllBuildings("Bearer " + accessToken);
                mainViewModel.getDepartments("Bearer " + accessToken);
                mainViewModel.getAllCustodians("Bearer " + accessToken);
                mainViewModel.getAllAssets("Bearer " + accessToken);
                mainViewModel.getAssetScannedDetails(code, "Bearer " + accessToken);

            }
        });

        mainViewModel.getAllCustodiansResponse().observe(this, custodianState -> {
            assert custodianState != null;
            if (custodianState.getAllCustodians() != null) {
                handleCustodians(custodianState.getAllCustodians());
            }
            if (custodianState.getMessage() != null) {
                handleNetworkResponse(custodianState.getMessage());
            }
            if (custodianState.getErrorThrowable() != null) {
                handleError(custodianState.getErrorThrowable());
            }
            if (custodianState.getLoginError() != null) {
                handleUnAuthorized(custodianState.getLoginError());
            }
        });

        mainViewModel.getDivisionResponse().observe(this, divisionState -> {
            assert divisionState != null;
            if (divisionState.getGetDivisions() != null) {
                handleGetAllDivisions(divisionState.getGetDivisions());
            }

            if (divisionState.getMessage() != null) {
                handleNetworkResponse(divisionState.getMessage());
            }

            if (divisionState.getErrorThrowable() != null) {
                handleError(divisionState.getErrorThrowable());
            }

        });

        mainViewModel.getBuildingsResponse().observe(this, buildingsState -> {
            assert buildingsState != null;
            if (buildingsState.getAllBuildings() != null) {
                handleGetAllBuildings(buildingsState.getAllBuildings());
            }

            if (buildingsState.getMessage() != null) {
                handleNetworkResponse(buildingsState.getMessage());
            }

            if (buildingsState.getErrorThrowable() != null) {
                handleError(buildingsState.getErrorThrowable());
            }
        });

        mainViewModel.getRoomInBuildingResponse().observe(this, getRoomInBuildingState -> {
            assert getRoomInBuildingState != null;
            if (getRoomInBuildingState.getGetRoomInBuilding() != null) {
                handleGetAllRoomsInBuilding(getRoomInBuildingState.getGetRoomInBuilding());
            }

            if (getRoomInBuildingState.getMessage() != null) {
                handleNetworkResponse(getRoomInBuildingState.getMessage());
            }

            if (getRoomInBuildingState.getErrorThrowable() != null) {
                handleError(getRoomInBuildingState.getErrorThrowable());
            }
            if (getRoomInBuildingState.getLoginError() != null) {
                handleUnAuthorized(getRoomInBuildingState.getLoginError());
            }
        });

        mainViewModel.getMoveAssetResponse().observe(this, moveAssetState -> {
            assert moveAssetState != null;
            if (moveAssetState.getMoveAsset() != null) {
                handleMoveAsset(moveAssetState.getMoveAsset());
            }

            if (moveAssetState.getMessage() != null) {
                handleMoveNetworkResponse(moveAssetState.getMessage());
            }

            if (moveAssetState.getErrorThrowable() != null) {
                handleMoveError(moveAssetState.getErrorThrowable());
            }
            if (moveAssetState.getLoginError() != null) {
                handleUnAuthorized(moveAssetState.getLoginError());
            }
        });

        mainViewModel.getDepartmentResponse().observe(this, departmentState -> {
            assert departmentState != null;
            if (departmentState.getAllDepartments() != null) {
                handleGetAllDepartments(departmentState.getAllDepartments());
            }

            if (departmentState.getMessage() != null) {
                handleNetworkResponse(departmentState.getMessage());
            }

            if (departmentState.getErrorThrowable() != null) {
                handleError(departmentState.getErrorThrowable());
            }
            if (departmentState.getLoginError() != null) {
                handleUnAuthorized(departmentState.getLoginError());
            }
        });

        mainViewModel.getAllAssetResponse().observe(this, assetState -> {
            assert assetState != null;
            if (assetState.getAllAssets() != null) {
                handleAssetResponse(assetState.getAllAssets());
            }

            if (assetState.getMessage() != null) {
                handleNetworkResponse(assetState.getMessage());
            }

            if (assetState.getErrorThrowable() != null) {
                handleError(assetState.getErrorThrowable());
            }
            if (assetState.getLoginError() != null) {
                handleUnAuthorized(assetState.getLoginError());
            }
        });

        mainViewModel.getAssetScannedDetailsResponse().observe(this, getAssetScannedDetailsState -> {
            assert getAssetScannedDetailsState != null;
            if (getAssetScannedDetailsState.getGetAssetScannedDetails() != null) {
                handleAssetDetailResponse(getAssetScannedDetailsState.getGetAssetScannedDetails());
            }

            if (getAssetScannedDetailsState.getMessage() != null) {
                handleNetworkResponse(getAssetScannedDetailsState.getMessage());
            }

            if (getAssetScannedDetailsState.getErrorThrowable() != null) {
                handleError(getAssetScannedDetailsState.getErrorThrowable());
            }
            if (getAssetScannedDetailsState.getLoginError() != null) {
                handleUnAuthorized(getAssetScannedDetailsState.getLoginError());
            }
            if (getAssetScannedDetailsState.getFailError() != null) {
                handleFail(getAssetScannedDetailsState.getFailError());
            }
        });


        mainViewModel.getBuildingsInDivisionResponse().observe(this, buildingsInDivisionState -> {
            if (buildingsInDivisionState.getBuildingInDivision() != null) {
                handleGetBuildingsInDivision(buildingsInDivisionState.getBuildingInDivision());
            }

            if (buildingsInDivisionState.getMessage() != null) {
                handleNetworkResponse(buildingsInDivisionState.getMessage());
            }

            if (buildingsInDivisionState.getErrorThrowable() != null) {
                handleError(buildingsInDivisionState.getErrorThrowable());
            }
            if (buildingsInDivisionState.getLoginError() != null) {
                handleUnAuthorized(buildingsInDivisionState.getLoginError());
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, buildingName);
        mBuilding.setAdapter(adapter);

        mBuilding.setOnItemClickListener((parent, view, position, id) -> {
            String mBuildingName = (String) parent.getItemAtPosition(position);
            if (buildingList.get(position).getName().equalsIgnoreCase(mBuildingName)) {
                buildingId = buildingList.get(position).getId();
                if (buildingId == 0) {
                    roomName.add("None");
                } else {
                    if (!roomName.isEmpty()) {

                        roomName.clear();
                    }
                    main_spin_kit.setVisibility(View.VISIBLE);
                    layout_location_details.setAlpha(0.3f);
                    mainViewModel.getRoomsInBuilding(buildingId, "Bearer " + accessToken);
                }
            }
        });
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, roomName);
        mRoom.setAdapter(adapter1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, departmentName);
        department.setAdapter(adapter2);

        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, divisionName);
        division.setAdapter(adapter5);

        division.setOnItemClickListener((adapterView, view, i, l) -> {
            String mDivisionName = (String) adapterView.getItemAtPosition(i);
            if (divisionsList.get(i).getName().equalsIgnoreCase(mDivisionName)) {
                divisionId = divisionsList.get(i).getId();
                if (divisionId == 0) {


                    buildingName.add("None");

                } else {

                    main_spin_kit.setVisibility(View.VISIBLE);
                    layout_location_details.setAlpha(0.3f);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (!buildingName.isEmpty()) {

                        buildingName.clear();
                    }

                    mainViewModel.getBuildingsInDivision(divisionId, "Bearer " + accessToken);
                }

            }
        });

        drop_down_division.setOnClickListener(v -> division.showDropDown());
        division.setOnClickListener(v -> division.showDropDown());

        drop_down_building.setOnClickListener(v -> mBuilding.showDropDown());
        mBuilding.setOnClickListener(v -> mBuilding.showDropDown());

        drop_down_room.setOnClickListener(v -> mRoom.showDropDown());
        mRoom.setOnClickListener(v -> mRoom.showDropDown());

        drop_down_department.setOnClickListener(v -> department.showDropDown());
        department.setOnClickListener(v -> department.showDropDown());


        //set up custodian drop down
        ArrayAdapter<String> adapter8 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, custodianName);
        custodian.setAdapter(adapter8);

        custodian.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String mDivisionName = (String) parent.getItemAtPosition(position);
                if (custodians.get(position).getName().equalsIgnoreCase(mDivisionName)) {
                    custodianCode = String.valueOf(custodians.get(position).getBarcode());
                }
            }
        });


        drop_down_custodian.setOnClickListener(v -> custodian.showDropDown());
        custodian.setOnClickListener(v -> custodian.showDropDown());


        change.setOnClickListener(v -> {
            String newBuilding = mBuilding.getText().toString().trim();
            String newRoom = mRoom.getText().toString().trim();
            String newDept = department.getText().toString().trim();
            String newDiv = division.getText().toString().trim();
            String cust = custodian.getText().toString().trim();

            if (TextUtils.isEmpty(newBuilding)) {
                mBuilding.requestFocus();
                Toast.makeText(this, "Enter your new Building", Toast.LENGTH_SHORT).show();

            } else if (TextUtils.isEmpty(newRoom)) {
                mRoom.requestFocus();
                Toast.makeText(this, "Enter your new room", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(newDept)) {
                department.requestFocus();
                Toast.makeText(this, "Enter your new department", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(newDiv)) {
                division.requestFocus();
                Toast.makeText(this, "Enter your new division", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(cust)) {
                Toast.makeText(this, "Enter your custodian first", Toast.LENGTH_SHORT).show();
            } else {
                change.setVisibility(View.INVISIBLE);
                change_progress.setVisibility(View.VISIBLE);



                mainViewModel.moveAssets("Bearer " + accessToken, id, newBuilding, newRoom, newDept, newDiv, custodianCode);
            }
        });


    }

    private void handleGetBuildingsInDivision(GetBuildingsInDivision buildingInDivision) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        buildingList = buildingInDivision.getBuildings();
        //  buildingName.add("None");

        if (buildingList.isEmpty()) {
            buildingName.add("None");
        } else {
            for (Building building : buildingList) {

                String bName = building.getName();
                buildingName.add(bName);
            }
        }
    }

    private void handleCustodians(AllCustodians allCustodians) {
        boolean status = allCustodians.getStatus();
        if (status) {
            custodians = allCustodians.getCustodians();
            for (Custodian custodian : custodians) {
                custodianName.add(custodian.getName());

            }
        }
    }

    private void handleGetAllDivisions(GetDivisions getDivisions) {

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        Divisions none = new Divisions(0, "None");
        divisionsList = getDivisions.getData();
        divisionsList.add(none);
        for (Divisions building : divisionsList) {
            String bName = building.getName();
            divisionName.add(bName);
        }
    }


    private void handleAssetDetailResponse(GetAssetScannedDetails getAssetScannedDetails) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = getAssetScannedDetails.getStatus();
        if (status) {
            Asset asset = getAssetScannedDetails.getAsset();
            assetName = asset.getName();
            asset_name.setText(asset.getName());
            fromBuilding.setText(asset.getBuilding());
            fromRoom.setText(asset.getRoom());
            fromDepartment.setText(asset.getDepart());
            fromDivision.setText(asset.getDivision());
            fromCustodian.setText(asset.getCust());
        }


    }


    private void handleAssetResponse(AllAssets allAssets) {
        boolean status = allAssets.getStatus();
        if (status) {
            assetList = allAssets.getData();
            for (Asset asset : assetList) {
                String code = asset_id.getText().toString().trim();
                if (code.equals(asset.getCode())) {
                    id = asset.getId();
                }
            }
        }
    }

    /* Handle movement of assets */

    private void handleMoveAsset(MoveAsset moveAsset) {
        change_progress.setVisibility(View.GONE);
        change.setVisibility(View.VISIBLE);

        int status = moveAsset.getStatus();

        if (status == 200) {
            showDialog(moveAsset.getMessage());

        }
    }

    private void handleMoveError(Throwable errorThrowable) {
        change_progress.setVisibility(View.GONE);
        change.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleMoveNetworkResponse(String message) {
        change_progress.setVisibility(View.GONE);
        change.setVisibility(View.VISIBLE);

        showDialog(message);
    }


    /* Getting the location details for chenging to when moving the assets */

    private void handleError(Throwable errorThrowable) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        showDialog(message);
    }

    private void handleUnAuthorized(LoginError loginError) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }

    private void handleFail(FailError failError) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        showDialog(failError.getMessage());
    }

    private void handleGetAllBuildings(AllBuildings allBuildings) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        Building none = new Building(0, "None");
        buildingList = allBuildings.getBuildings();
        buildingList.add(none);
        for (Building building : buildingList) {
            String bName = building.getName();
            buildingName.add(bName);
        }

    }

    private void handleGetAllRoomsInBuilding(GetRoomInBuilding getRoomInBuilding) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        List<Room> roomList = getRoomInBuilding.getRooms();
        for (Room room : roomList) {
            String rName = room.getName();
            roomName.add(rName);
        }
    }

    private void handleGetAllDepartments(AllDepartments allDepartments) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        List<Department> departmentList = allDepartments.getData();
        Department none = new Department(0, "None");
        departmentList.add(none);
        for (Department department : departmentList) {
            String dName = department.getName();
            departmentName.add(dName);
        }
    }


    private void showDialog(String message) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Scan Results ");
        alert.setCancelable(false);
        alert.setMessage(message + " Do you want to continue ? ");

        alert.setPositiveButton("Camera", (dialog, which) -> {

                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", mDepartment);
                    scanAsset.putExtra("TAG", "C");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );

        alert.setNeutralButton("Scanner", ((dialog, which) -> {
            Intent scanAsset = new Intent(this, AssetMove.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", mDepartment);
            scanAsset.putExtra("TAG", "C");
            startActivity(scanAsset);
            dialog.dismiss();
        }));

        alert.setNegativeButton("Exit", (dialog, which) -> {
            onBackPressed();
            dialog.dismiss();

        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, AssetsOptions.class);
        startActivity(loc);
    }
}

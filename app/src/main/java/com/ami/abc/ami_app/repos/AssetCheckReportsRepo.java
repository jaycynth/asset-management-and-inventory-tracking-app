package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.AssetsCheckReportDao;
import com.ami.abc.ami_app.models.AssetCheckReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AssetCheckReportsRepo {

    private AssetsCheckReportDao assetsInReportDao;
    private Executor executor;

    public AssetCheckReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        assetsInReportDao = AppDatabase.getDatabase(AMI.context).assetCheckReport();

    }


    public void saveAssets(AssetCheckReport assetCheckReport) {
        executor.execute(() -> assetsInReportDao.saveAsset(assetCheckReport));
    }


    public LiveData<List<AssetCheckReport>> getAssetsReports() {
        return assetsInReportDao.getAssetsReports();
    }



    public void deleteAssets() {
        executor.execute(() -> assetsInReportDao.deleteAssets());
    }
}

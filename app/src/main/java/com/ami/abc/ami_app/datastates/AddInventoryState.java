package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AddInventory;
import com.ami.abc.ami_app.models.LoginError;

public class AddInventoryState {
    private AddInventory addInventory;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public AddInventoryState(AddInventory addInventory) {
        this.addInventory = addInventory;
        this.message= null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AddInventoryState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.addInventory = null;
        this.loginError = null;
    }

    public AddInventoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.addInventory = null;
        this.loginError = null;

    }

    public AddInventoryState(LoginError loginError) {
        this.loginError = loginError;
        this.addInventory = null;
        this.message = null;
        this.errorThrowable = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AddInventory getAddInventory() {
        return addInventory;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

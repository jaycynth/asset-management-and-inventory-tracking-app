package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.TwoFactorAuth;

public class TwoFactorAuthState {
    private TwoFactorAuth twoFactorAuth;
    private String message;
    private Throwable errorThrowable;

    public TwoFactorAuthState(TwoFactorAuth twoFactorAuth) {
        this.twoFactorAuth = twoFactorAuth;
        this.message = null;
        this.errorThrowable = null;
    }

    public TwoFactorAuthState(String message) {
        this.message = message;
        this.twoFactorAuth = null;
        this.errorThrowable = null;
    }

    public TwoFactorAuthState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.twoFactorAuth = null;
        this.message = null;
    }

    public TwoFactorAuth getTwoFactorAuth() {
        return twoFactorAuth;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}




package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetState;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetRepo {
    private ApiClient mApiClient;

    public AssetRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AssetState> getAllAssets(String accessToken) {
        MutableLiveData<AssetState> assetStateMutableLiveData = new MutableLiveData<>();
        Call<AllAssets> call = mApiClient.getAllAssetService().getAssets(accessToken);
        call.enqueue(new Callback<AllAssets>() {
            @Override
            public void onResponse(Call<AllAssets> call, Response<AllAssets> response) {
                if (response.code() == 200) {
                    assetStateMutableLiveData.setValue(new AssetState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    assetStateMutableLiveData.setValue(new AssetState(loginError));
                }
                else {
                    assetStateMutableLiveData.setValue(new AssetState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllAssets> call, Throwable t) {
                assetStateMutableLiveData.setValue(new AssetState(t));
            }
        });

        return assetStateMutableLiveData;


    }

}

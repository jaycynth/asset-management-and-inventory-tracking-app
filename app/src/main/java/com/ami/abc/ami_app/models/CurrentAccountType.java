package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentAccountType {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("modules")
    @Expose
    private String modules;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("users")
    @Expose
    private Integer users;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("discontinue")
    @Expose
    private Integer discontinue;
    @SerializedName("inactive")
    @Expose
    private Integer inactive;
    @SerializedName("end_fin_year")
    @Expose
    private String endFinYear;
    @SerializedName("invoice_number")
    @Expose
    private Object invoiceNumber;
    @SerializedName("amt_paid")
    @Expose
    private Object amtPaid;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("affiliate_id")
    @Expose
    private Object affiliateId;
    @SerializedName("refcode")
    @Expose
    private Object refcode;
    @SerializedName("reason")
    @Expose
    private Object reason;
    @SerializedName("backup")
    @Expose
    private Integer backup;
    @SerializedName("verificationcode")
    @Expose
    private Object verificationcode;
    @SerializedName("date_verified")
    @Expose
    private Object dateVerified;
    @SerializedName("pending_activation")
    @Expose
    private Integer pendingActivation;
    @SerializedName("checkout_limit")
    @Expose
    private Integer checkoutLimit;
    @SerializedName("prefix")
    @Expose
    private String prefix;
    @SerializedName("format")
    @Expose
    private Integer format;
    @SerializedName("starting_code")
    @Expose
    private Integer startingCode;
    @SerializedName("automatic")
    @Expose
    private Integer automatic;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getUsers() {
        return users;
    }

    public void setUsers(Integer users) {
        this.users = users;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Integer getDiscontinue() {
        return discontinue;
    }

    public void setDiscontinue(Integer discontinue) {
        this.discontinue = discontinue;
    }

    public Integer getInactive() {
        return inactive;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public String getEndFinYear() {
        return endFinYear;
    }

    public void setEndFinYear(String endFinYear) {
        this.endFinYear = endFinYear;
    }

    public Object getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Object invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Object getAmtPaid() {
        return amtPaid;
    }

    public void setAmtPaid(Object amtPaid) {
        this.amtPaid = amtPaid;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(Object affiliateId) {
        this.affiliateId = affiliateId;
    }

    public Object getRefcode() {
        return refcode;
    }

    public void setRefcode(Object refcode) {
        this.refcode = refcode;
    }

    public Object getReason() {
        return reason;
    }

    public void setReason(Object reason) {
        this.reason = reason;
    }

    public Integer getBackup() {
        return backup;
    }

    public void setBackup(Integer backup) {
        this.backup = backup;
    }

    public Object getVerificationcode() {
        return verificationcode;
    }

    public void setVerificationcode(Object verificationcode) {
        this.verificationcode = verificationcode;
    }

    public Object getDateVerified() {
        return dateVerified;
    }

    public void setDateVerified(Object dateVerified) {
        this.dateVerified = dateVerified;
    }

    public Integer getPendingActivation() {
        return pendingActivation;
    }

    public void setPendingActivation(Integer pendingActivation) {
        this.pendingActivation = pendingActivation;
    }

    public Integer getCheckoutLimit() {
        return checkoutLimit;
    }

    public void setCheckoutLimit(Integer checkoutLimit) {
        this.checkoutLimit = checkoutLimit;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getFormat() {
        return format;
    }

    public void setFormat(Integer format) {
        this.format = format;
    }

    public Integer getStartingCode() {
        return startingCode;
    }

    public void setStartingCode(Integer startingCode) {
        this.startingCode = startingCode;
    }

    public Integer getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Integer automatic) {
        this.automatic = automatic;
    }

}

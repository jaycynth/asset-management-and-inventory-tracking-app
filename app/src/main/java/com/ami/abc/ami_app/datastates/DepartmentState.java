package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllDepartments;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.LoginError;

public class DepartmentState {
    private AllDepartments allDepartments;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private Error error;

    public DepartmentState(AllDepartments allDepartments) {
        this.allDepartments = allDepartments;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;
    }

    public DepartmentState(String message) {
        this.message = message;
        this.allDepartments = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;
    }

    public DepartmentState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.allDepartments = null;
        this.message = null;
        this.loginError = null;
        this.error = null;
    }

    public DepartmentState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allDepartments = null;
        this.error = null;
    }

    public DepartmentState(Error error) {
        this.error = error;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.allDepartments = null;
    }

    public Error getError() {
        return error;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllDepartments getAllDepartments() {
        return allDepartments;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoveAsset {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("assetmoved")
    @Expose
    private AssetMoved assetmoved;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public AssetMoved getAssetmoved() {
        return assetmoved;
    }

    public void setAssetmoved(AssetMoved assetmoved) {
        this.assetmoved = assetmoved;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.ShelvesState;
import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShelvesRepo {
    private ApiClient mApiClient;

    public ShelvesRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<ShelvesState> getAllShelvess(String accessToken) {
        MutableLiveData<ShelvesState> shelvesStateMutableLiveData = new MutableLiveData<>();
        Call<AllShelves> call = mApiClient.amiService().getAllShelves(accessToken);
        call.enqueue(new Callback<AllShelves>() {
            @Override
            public void onResponse(Call<AllShelves> call, Response<AllShelves> response) {
                if (response.code() == 200) {
                    shelvesStateMutableLiveData.setValue(new ShelvesState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    shelvesStateMutableLiveData.setValue(new ShelvesState(loginError));
                }
                else {
                    shelvesStateMutableLiveData.setValue(new ShelvesState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllShelves> call, Throwable t) {
                shelvesStateMutableLiveData.setValue(new ShelvesState(t));
            }
        });

        return shelvesStateMutableLiveData;


    }
}

package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.LoginState;
import com.ami.abc.ami_app.repos.LoginRepo;

public class LoginViewModel extends AndroidViewModel {
    private MediatorLiveData<LoginState> loginStateMediatorLiveData;
    private LoginRepo loginRepo;

    public LoginViewModel(Application application){
        super(application);
        loginStateMediatorLiveData = new MediatorLiveData<>();
        loginRepo = new LoginRepo(application);
    }

    public LiveData<LoginState> getLoginResponse(){
        return loginStateMediatorLiveData;
    }

    public void userLogin(String email, String password, Boolean withLogout){

        LiveData<LoginState> loginStateLiveData = loginRepo.loginUser(email, password, withLogout);
        loginStateMediatorLiveData.addSource(loginStateLiveData,
                loginStateMediatorLiveData -> {
                    if (this.loginStateMediatorLiveData.hasActiveObservers()){
                        this.loginStateMediatorLiveData.removeSource(loginStateLiveData);
                    }
                    this.loginStateMediatorLiveData.setValue(loginStateMediatorLiveData);
                });

    }
}

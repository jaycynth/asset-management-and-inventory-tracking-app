package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AllVerifiedStatusState;
import com.ami.abc.ami_app.models.AllVerifiedStatus;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllVerifiedAssetsRepo {
    private ApiClient mApiClient;

    public AllVerifiedAssetsRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AllVerifiedStatusState> getVerifiedAssets(String accessToken) {

        MutableLiveData<AllVerifiedStatusState> allVerifiedStatusStateMutableLiveData = new MutableLiveData<>();
        Call<AllVerifiedStatus> call = mApiClient.amiService().getAssetsVerified(accessToken);
        call.enqueue(new Callback<AllVerifiedStatus>() {
            @Override
            public void onResponse(Call<AllVerifiedStatus> call, Response<AllVerifiedStatus> response) {
                if (response.code() == 200) {
                    allVerifiedStatusStateMutableLiveData.setValue(new AllVerifiedStatusState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    allVerifiedStatusStateMutableLiveData.setValue(new AllVerifiedStatusState(loginError));
                }
                else {
                    allVerifiedStatusStateMutableLiveData.setValue(new AllVerifiedStatusState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllVerifiedStatus> call, Throwable t) {
                allVerifiedStatusStateMutableLiveData.setValue(new AllVerifiedStatusState(t));
            }
        });

        return allVerifiedStatusStateMutableLiveData;

    }
}

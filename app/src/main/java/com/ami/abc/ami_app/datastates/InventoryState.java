package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllInventory;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryState {

    private AllInventory allInventory;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public InventoryState(AllInventory allInventory) {
        this.allInventory = allInventory;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public InventoryState(String message) {
        this.message = message;
        this.allInventory = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public InventoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allInventory = null;
        this.loginError = null;
    }

    public InventoryState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allInventory = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllInventory getAllInventory() {
        return allInventory;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

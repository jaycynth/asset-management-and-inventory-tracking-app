package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssetCheckPoint {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("assetcheck")
    @Expose
    private List<AssetCheck> assetcheck = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AssetCheck> getAssetcheck() {
        return assetcheck;
    }

    public void setAssetcheck(List<AssetCheck> assetcheck) {
        this.assetcheck = assetcheck;
    }
}

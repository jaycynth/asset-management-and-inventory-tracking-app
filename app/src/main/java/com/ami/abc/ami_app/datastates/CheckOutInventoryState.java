package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryCheckedOutPoint;
import com.ami.abc.ami_app.models.LoginError;

public class CheckOutInventoryState {
    private InventoryCheckedOutPoint inventoryCheckedOutPoint;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;

    public CheckOutInventoryState(InventoryCheckedOutPoint inventoryCheckedOutPoint) {
        this.inventoryCheckedOutPoint = inventoryCheckedOutPoint;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public CheckOutInventoryState(String message) {
        this.message = message;
        this.inventoryCheckedOutPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public CheckOutInventoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.inventoryCheckedOutPoint = null;
        this.loginError = null;
        this.failError = null;
    }

    public CheckOutInventoryState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.inventoryCheckedOutPoint = null;
        this.failError = null;
    }

    public CheckOutInventoryState(FailError failError) {
        this.failError = failError;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.inventoryCheckedOutPoint = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public InventoryCheckedOutPoint getInventoryCheckedOutPoint() {
        return inventoryCheckedOutPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetCheckInOutDetailsState;
import com.ami.abc.ami_app.models.AssetCheckInOutDetails;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetCheckInOutDetailsRepo {
    private ApiClient mApiClient;
    //constructor
    public AssetCheckInOutDetailsRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AssetCheckInOutDetailsState> assetCheckInOutDetails(String accessToken, String code) {

        MutableLiveData<AssetCheckInOutDetailsState> assetCheckInOutDetailsStateMutableLiveData = new MutableLiveData<>();

        Call<AssetCheckInOutDetails> call = mApiClient.amiService().getAssetCheckInOutDetails(accessToken, code);
        call.enqueue(new Callback<AssetCheckInOutDetails>() {
            @Override
            public void onResponse(Call<AssetCheckInOutDetails> call, Response<AssetCheckInOutDetails> response) {
                if (response.code() == 200) {
                    assetCheckInOutDetailsStateMutableLiveData.setValue(new AssetCheckInOutDetailsState(response.body()));

                }
                else {
                    assetCheckInOutDetailsStateMutableLiveData.setValue(new AssetCheckInOutDetailsState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AssetCheckInOutDetails> call, Throwable t) {
                assetCheckInOutDetailsStateMutableLiveData.setValue(new AssetCheckInOutDetailsState(t));
            }
        });

        return assetCheckInOutDetailsStateMutableLiveData;

    }
}

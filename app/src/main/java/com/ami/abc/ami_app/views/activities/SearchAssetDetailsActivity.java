package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.AssetViewModel;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAssetDetailsActivity extends AppCompatActivity {

    @BindView(R.id.asset_id)
    TextView assetId;
    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.supplier)
    TextView supplier;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.at_cost)
    TextView purchaseCost;
    @BindView(R.id.custodian)
    TextView custodian;
    @BindView(R.id.location)
    TextView location;

    AssetViewModel assetViewModel;

    @BindView(R.id.container_layout)
    LinearLayout containerLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    String assetCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_asset_details);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Details");

        Intent details = getIntent();
        assetCode = details.getStringExtra("assetCode");
        assetViewModel = ViewModelProviders.of(this).get(AssetViewModel.class);

        String accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            containerLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            assetViewModel.getAssetScannedDetails(assetCode, "Bearer " + accessToken);
        }

        assetViewModel.getAssetScannedDetailsResponse().observe(this, getAssetScannedDetailsState -> {
            assert getAssetScannedDetailsState != null;
            if (getAssetScannedDetailsState.getGetAssetScannedDetails() != null) {
                handleScannedAssets(getAssetScannedDetailsState.getGetAssetScannedDetails());
            }
            if (getAssetScannedDetailsState.getMessage() != null) {
                handleScannedAssetsNetworkResponse(getAssetScannedDetailsState.getMessage());
            }

            if (getAssetScannedDetailsState.getErrorThrowable() != null) {
                handleScannedAssetsError(getAssetScannedDetailsState.getErrorThrowable());
            }
            if (getAssetScannedDetailsState.getLoginError() != null) {
                handleUnAuthorized(getAssetScannedDetailsState.getLoginError());
            }
        });


    }

    private void handleScannedAssets(GetAssetScannedDetails getAssetScannedDetails) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = getAssetScannedDetails.getStatus();
        if (status) {
            Asset asset = getAssetScannedDetails.getAsset();

            assetId.setText(assetCode);
            assetName.setText(asset.getName());
            supplier.setText(asset.getSupplier());
            date.setText(asset.getDateOfPurchase());
            purchaseCost.setText(String.valueOf(asset.getPurchaseCost()));
            if (asset.getVerified() == 0) {
                custodian.setText(getString(R.string.asset_not_verified));
            } else {
                custodian.setText(asset.getCust());
            }
            location.setText(String.format("%s, %s, %s", asset.getBuilding(), asset.getRoom(), asset.getDepart()));


        }


    }

    private void handleUnAuthorized(LoginError loginError) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Intent auth = new Intent(this, LoginActivity.class);
        startActivity(auth);
    }


    private void handleScannedAssetsNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void handleScannedAssetsError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getCause().toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(this, SearchAssetsActivity.class);
        startActivity(back);
    }
}

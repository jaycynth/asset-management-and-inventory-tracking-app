package com.ami.abc.ami_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.CacheComponents;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddedComponentsAdapter extends RecyclerView.Adapter<AddedComponentsAdapter.MyViewHolder> {

    private List<CacheComponents> cacheComponentsList;
    private Context context;


    public AddedComponentsAdapter(List<CacheComponents> cacheComponentsList, Context context) {
        this.cacheComponentsList = cacheComponentsList;
        this.context = context;

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.component_code)
        TextView componentCode;

        @BindView(R.id.component_name)
        TextView componentName;

        @BindView(R.id.component_lifespan)
        TextView componentLifespan;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
        CacheComponents cacheComponents = cacheComponentsList.get(pos);
        holder.componentCode.setText(String.format("Code: %s", cacheComponents.getCode()));
        holder.componentName.setText(String.format("Name: %s", cacheComponents.getName()));
        holder.componentLifespan.setText(String.format("Lifespan: %s", cacheComponents.getLifespan()));
    }

    @Override
    public int getItemCount() {
        return cacheComponentsList.size();
    }

}

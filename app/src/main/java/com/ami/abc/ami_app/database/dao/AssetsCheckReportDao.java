package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.AssetCheckReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssetsCheckReportDao {

    @Insert(onConflict = REPLACE)
    void saveAsset(AssetCheckReport assetCheckReport);
    //observe the list in the room database

    @Query("SELECT * FROM  AssetCheckReport")
    LiveData<List<AssetCheckReport>> getAssetsReports();

    @Query("SELECT * FROM AssetCheckReport")
    List<AssetCheckReport> getAll();

    @Delete
    void delete(AssetCheckReport assetsReports);

    //delete all the assets in the room database upon sending information to the assets verified table
    @Query("DELETE FROM AssetCheckReport")
    void deleteAssets();
}

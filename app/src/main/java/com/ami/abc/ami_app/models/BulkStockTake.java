package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BulkStockTake {
    @SerializedName("product_code")
    @Expose
    private Integer productCode;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    public Integer getProductCode() {
        return productCode;
    }

    public void setProductCode(Integer productCode) {
        this.productCode = productCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}

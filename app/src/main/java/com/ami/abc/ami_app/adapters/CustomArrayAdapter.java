package com.ami.abc.ami_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Building;
import com.ami.abc.ami_app.models.Divisions;

import java.util.ArrayList;
import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<Divisions> {
    private List<Divisions> divisionsListFull;
    private List<Divisions> filteredDivisionsList = new ArrayList<>();


    public CustomArrayAdapter(@NonNull Context context, @NonNull List<Divisions> divisionsList) {
        super(context, 0, divisionsList);
        this.divisionsListFull = divisionsList;
    }

    @Override
    public int getCount() {
        return filteredDivisionsList.size();
    }

    @Override
    public Divisions getItem(int position) {
        return filteredDivisionsList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return divisionsFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_array_layout, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.value);

        Divisions divisions = getItem(position);

        if (divisions != null) {
            textViewName.setText(divisions.getName());
        }

        return convertView;
    }

    private Filter divisionsFilter = new Filter() {
        List<Divisions> suggestions = new ArrayList<>();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(divisionsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Divisions item : divisionsListFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredDivisionsList.clear();
            if (results.count > 0) {
                filteredDivisionsList.addAll((List) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }


        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Divisions) resultValue).getName();
        }
    };
}

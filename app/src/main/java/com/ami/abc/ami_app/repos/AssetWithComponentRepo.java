package com.ami.abc.ami_app.repos;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetWithComponentsState;
import com.ami.abc.ami_app.models.AddAsset;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetWithComponentRepo {

    private ApiClient apiClient;

    public AssetWithComponentRepo(Application application) {
        apiClient = new ApiClient(application);
    }


    public LiveData<AssetWithComponentsState> postAssetWithComponents(String accessToken, RequestBody requestBody) {
        MutableLiveData<AssetWithComponentsState> addAssetMutableLiveData = new MutableLiveData<>();
        Call<AddAsset> call = apiClient.amiService().postAssetWithComponents(accessToken, requestBody);
        call.enqueue(new Callback<AddAsset>() {
            @Override
            public void onResponse(Call<AddAsset> call, Response<AddAsset> response) {
                if (response.code() == 200) {
                    addAssetMutableLiveData.setValue(new AssetWithComponentsState(response.body()));

                } else {
                    addAssetMutableLiveData.setValue(new AssetWithComponentsState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AddAsset> call, Throwable t) {
                addAssetMutableLiveData.setValue(new AssetWithComponentsState(t));
            }
        });

        return addAssetMutableLiveData;

    }
}
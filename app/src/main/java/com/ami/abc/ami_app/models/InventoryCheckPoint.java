package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InventoryCheckPoint {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("inventorycheck")
    @Expose
    private List<InventoryCheck> inventorycheck = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<InventoryCheck> getInventorycheck() {
        return inventorycheck;
    }

    public void setInventorycheck(List<InventoryCheck> inventorycheck) {
        this.inventorycheck = inventorycheck;
    }
}

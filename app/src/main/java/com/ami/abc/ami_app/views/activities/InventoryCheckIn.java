package com.ami.abc.ami_app.views.activities;


import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.FailError;

import com.ami.abc.ami_app.models.InventoryCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.CheckInInventoryViewModel;

import java.io.IOException;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryCheckIn extends AppCompatActivity {

    @BindView(R.id.asset_id)
    EditText asset_code;


    @BindView(R.id.quantity)
    TextInputEditText quantity;
    @BindView(R.id.inventory_name)
    TextInputEditText inventoryName;

    String building, room, depart, assetId;
    String accessToken;

    @BindView(R.id.holder_layout)
    LinearLayout holder_layout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    CheckInInventoryViewModel checkInInventoryViewModel;

    @BindView(R.id.check_in)
    Button check_in;

    String name;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    LinearLayout detailsLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_check_in);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.check_in));


        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");


        checkInInventoryViewModel = ViewModelProviders.of(this).get(CheckInInventoryViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null && assetId != null) {
            asset_code.setText(assetId);

            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);

            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            checkInInventoryViewModel.stockTake(assetId, "Bearer " + accessToken);
        }

        confirm.setOnClickListener(v->{
            String code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                checkInInventoryViewModel.stockTake(code, "Bearer " + accessToken);

            }
        });

        checkInInventoryViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleInventoryScanned(stockTakeState.getStockTake());
            }

            if (stockTakeState.getErrorThrowable() != null) {
                handleError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getMessage() != null) {
                handleNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getLoginError() != null) {
                handleUnAuthorized(stockTakeState.getLoginError());
            }
        });

        checkInInventoryViewModel.checkInInventoryResponse().observe(this, inventoryCheckedInPointState -> {
            assert inventoryCheckedInPointState != null;
            if (inventoryCheckedInPointState.getCheckedInPoint() != null) {
                handleInventoryCheckIn(inventoryCheckedInPointState.getCheckedInPoint());
            }

            if (inventoryCheckedInPointState.getErrorThrowable() != null) {
                handleError(inventoryCheckedInPointState.getErrorThrowable());
            }
            if (inventoryCheckedInPointState.getMessage() != null) {
                handleNetworkResponse(inventoryCheckedInPointState.getMessage());
            }

            if (inventoryCheckedInPointState.getFailError() != null) {
                handleFailError(inventoryCheckedInPointState.getFailError());
            }

            if (inventoryCheckedInPointState.getLoginError() != null) {
                handleUnAuthorized(inventoryCheckedInPointState.getLoginError());
            }
        });

        check_in.setOnClickListener(v -> {
            String productCode = asset_code.getText().toString().trim();
            String quantText = quantity.getText().toString().trim();
            if (TextUtils.isEmpty(quantText)) {
                Toast.makeText(this, "Please specify your quantity", Toast.LENGTH_SHORT).show();
            } else {
                int quant = Integer.parseInt(quantText);

                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                checkInInventoryViewModel.checkInInventory("Bearer " + accessToken, productCode, quant);
            }
        });

    }


    private void handleFailError(FailError failError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();

    }

    private void handleInventoryScanned(StockTake stockTake) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = stockTake.getStatus();
        if (status) {
            //quantity.setText(String.valueOf(getInventoryScanned.getInventory().getQuantity()));
            inventoryName.setText(stockTake.getInventory().getProductName());
            name = stockTake.getInventory().getProductName();
        }

    }

    private void handleInventoryCheckIn(InventoryCheckedInPoint checkedInPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        boolean status = checkedInPoint.getStatus();
        if (status) {
//            InventoryCheckInReport inventoryReports = new InventoryCheckInReport();
//            inventoryReports.setId(checkedInPoint.getCheckedIn().getInventoryId());
//            inventoryReports.setBarcode(assetId);
//            inventoryReports.setName(name);
//            inventoryReports.setCount(checkedInPoint.getCheckedIn().getQuantity());
//            checkInInventoryViewModel.saveScannedAsset(inventoryReports);

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(checkedInPoint.getMessage());
            alert.setCancelable(false);
            alert.setMessage("Do You want to continue checking in ? ");

            alert.setNegativeButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNeutralButton("Scanner", (dialog, which) -> {
                Intent scanAsset = new Intent(this, InventoryCheckIn.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "H");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "H");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        }
    }


    private void handleError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    /* handle all unauthorized issues for all endpoints in this activity */

    private void handleUnAuthorized(LoginError loginError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }
}

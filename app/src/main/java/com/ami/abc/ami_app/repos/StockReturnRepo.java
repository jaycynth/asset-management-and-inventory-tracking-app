package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.StockReturnState;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockReturn;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockReturnRepo {
    private ApiClient mApiClient;

    //constructor
    public StockReturnRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<StockReturnState> getStockReturn(String accessToken, int inventoryId, String invoiceNumber, int quantity) {

        MutableLiveData<StockReturnState> stockReturnStateMutableLiveData = new MutableLiveData<>();

        Call<StockReturn> call = mApiClient.amiService().stockReturn(accessToken, inventoryId, invoiceNumber, quantity);
        call.enqueue(new Callback<StockReturn>() {
            @Override
            public void onResponse(Call<StockReturn> call, Response<StockReturn> response) {
                if (response.code() == 200) {
                    stockReturnStateMutableLiveData.setValue(new StockReturnState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    stockReturnStateMutableLiveData.setValue(new StockReturnState(loginError));
                }else {
                    stockReturnStateMutableLiveData.setValue(new StockReturnState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<StockReturn> call, Throwable t) {
                stockReturnStateMutableLiveData.setValue(new StockReturnState(t));
            }
        });

        return stockReturnStateMutableLiveData;

    }
}

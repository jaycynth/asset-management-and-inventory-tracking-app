package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.AssetCheckInReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssetCheckInReportDao {

    @Insert(onConflict = REPLACE)
    void saveAsset(AssetCheckInReport assetCheckInReport);
    //observe the list in the room database

    @Query("SELECT * FROM  AssetCheckInReport")
    LiveData<List<AssetCheckInReport>> getAssetsReports();

    @Query("SELECT * FROM AssetCheckInReport")
    List<AssetCheckInReport> getAll();

    @Delete
    void delete(AssetCheckInReport assetsReports);

    //delete all the assets in the room database upon sending information to the assets verified table
    @Query("DELETE FROM AssetCheckInReport")
    void deleteAssets();
}

package com.ami.abc.ami_app.viewmodels;


import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.BuildingsState;
import com.ami.abc.ami_app.repos.BuildingsRepo;

public class BuildingViewModel extends AndroidViewModel {
    private MediatorLiveData<BuildingsState> buildingsStateMediatorLiveData;
    private BuildingsRepo buildingsRepo;

    public BuildingViewModel(Application application){
        super(application);
        buildingsStateMediatorLiveData = new MediatorLiveData<>();
        buildingsRepo = new BuildingsRepo(application);
    }

    public LiveData<BuildingsState> getBuildingsResponse(){
        return buildingsStateMediatorLiveData;
    }

    public void getAllBuildings(String accessToken){

        LiveData<BuildingsState> buildingsStateLiveData = buildingsRepo.getAllBuildings(accessToken);
        buildingsStateMediatorLiveData.addSource(buildingsStateLiveData,
                buildingsStateMediatorLiveData -> {
                    if (this.buildingsStateMediatorLiveData.hasActiveObservers()){
                        this.buildingsStateMediatorLiveData.removeSource(buildingsStateLiveData);
                    }
                    this.buildingsStateMediatorLiveData.setValue(buildingsStateMediatorLiveData);
                });

    }
}

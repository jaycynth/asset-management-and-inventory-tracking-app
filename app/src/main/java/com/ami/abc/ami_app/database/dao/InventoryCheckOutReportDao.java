package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.InventoryCheckOutReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface InventoryCheckOutReportDao {
    @Insert(onConflict = REPLACE)
    void saveInventory(InventoryCheckOutReport inventoryReports);
    //observe the list in the room database

    @Query("SELECT * FROM  InventoryCheckOutReport")
    LiveData<List<InventoryCheckOutReport>> getInventorysReports();

    @Query("SELECT * FROM InventoryCheckOutReport")
    List<InventoryCheckOutReport> getAll();

    @Delete
    void delete(InventoryCheckOutReport inventoryReports);

    //delete all the inventory in the room database upon sending information to the inventory verified table
    @Query("DELETE FROM InventoryCheckOutReport")
    void deleteInventorys();

}

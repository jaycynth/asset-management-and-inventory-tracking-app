package com.ami.abc.ami_app.views.activities;

import static android.Manifest.permission.CAMERA;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllVerifiedStatus;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.ScanActivityViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.Result;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {


    private static final int REQUEST_CAMERA = 1;
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private boolean mAutoFocus = true;


    private ZXingScannerView scannerView;

    String building, room, department, division, Tag, invoice, barCode, quantityOut, name, iSerial, iModel;
    String fromDivision, fromBuilding, fromRoom, fromDepart;
    String from;
    int count = 0;

    ScanActivityViewModel scanActivityViewModel;
    ProgressBar progressBar;
    String myResult;


    List<Inventory> toSend = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mAutoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);

        } else {
            mAutoFocus = true;

        }

        try {
            scannerView = new ZXingScannerView(this);
            setContentView(scannerView);
            scannerView.setResultHandler(this);
            scannerView.startCamera();
            // scannerView.setFlash(!scannerView.getFlash());
        } catch (Exception e) {
            Log.d("error", Objects.requireNonNull(e.getMessage()));
        }


        Intent scanIntents = getIntent();

        building = scanIntents.getStringExtra("building");
        division = scanIntents.getStringExtra("division");
        room = scanIntents.getStringExtra("room");
        department = scanIntents.getStringExtra("mDepartment");
        Tag = scanIntents.getStringExtra("TAG");
        from  = scanIntents.getStringExtra("from");
        invoice = scanIntents.getStringExtra("invoiceNumber");

        barCode = scanIntents.getStringExtra("barCode");

        //for inventory check out
        quantityOut = scanIntents.getStringExtra("quantityOut");

        //for asset check out
        name = scanIntents.getStringExtra("assetName");
        iSerial = scanIntents.getStringExtra("serial");
        iModel = scanIntents.getStringExtra("model");

        //for asset move
        fromDivision = scanIntents.getStringExtra("fromDivision");
        fromBuilding = scanIntents.getStringExtra("fromBuilding");
        fromRoom = scanIntents.getStringExtra("fromRoom");
        fromDepart = scanIntents.getStringExtra("fromDepart");


        scanActivityViewModel = ViewModelProviders.of(this).get(ScanActivityViewModel.class);

        scanActivityViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleStockTake(stockTakeState.getStockTake());
            }
            if (stockTakeState.getErrorThrowable() != null) {
                handleError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getMessage() != null) {
                handleNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getFailError() != null) {
                handleFailError(stockTakeState.getFailError());
            }
            if (stockTakeState.getLoginError() != null) {
                handleUnAuthorized(stockTakeState.getLoginError());
            }
        });


        scanActivityViewModel.getAllVerifiedAssetsResponse().observe(this, allVerifiedStatusState -> {
            assert allVerifiedStatusState != null;
            if (allVerifiedStatusState.getAllVerifiedStatus() != null) {
                handleAllVerifiedAssets(allVerifiedStatusState.getAllVerifiedStatus());
            }
            if (allVerifiedStatusState.getMessage() != null) {
                handleNetworkResponse(allVerifiedStatusState.getMessage());
            }

            if (allVerifiedStatusState.getErrorThrowable() != null) {
                handleError(allVerifiedStatusState.getErrorThrowable());
            }

            if (allVerifiedStatusState.getLoginError() != null) {
                handleUnAuthorized(allVerifiedStatusState.getLoginError());
            }
        });

    }


    private void handleAllVerifiedAssets(AllVerifiedStatus allVerifiedStatus) {
        progressBar.setVisibility(View.GONE);
        boolean status = allVerifiedStatus.getStatus();
        if (status) {
            List<Asset> allVerifiedAssets = allVerifiedStatus.getAssets();
            for (Asset asset : allVerifiedAssets) {
                if (myResult.equals(asset.getCode())) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Scan Results ");
                    alert.setCancelable(false);
                    alert.setMessage("Asset already scanned");

                    alert.setNegativeButton("Exit", (dialog, which) -> {
                                onBackPressed();
                                dialog.dismiss();
                            }
                    );
                    alert.setPositiveButton("Continue Scanning", (dialog, which) -> {
                        Intent scanAsset = new Intent(this, ScanActivity.class);
                        scanAsset.putExtra("building", building);
                        scanAsset.putExtra("room", room);
                        scanAsset.putExtra("mDepartment", department);
                        scanAsset.putExtra("TAG", "B");
                        startActivity(scanAsset);
                        dialog.dismiss();
                    });
                    AlertDialog dialog = alert.create();
                    dialog.show();

                    return;
                }
            }

            Intent scanResult = new Intent(this, AssetCheck.class);
            scanResult.putExtra("scanResult", myResult);
            scanResult.putExtra("building", building);
            scanResult.putExtra("room", room);
            scanResult.putExtra("mDepartment", department);
            scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(scanResult);
            finish();


        }
    }


    //for stock take
    private void handleStockTake(StockTake stockTake) {
        progressBar.setVisibility(View.GONE);

        boolean status = stockTake.getStatus();
        if (status) {
            /* if the location details inputted matches that of the product details then,proceed scanning as normal
            but if the location details don't match then prompt the user to go on with the scanning or go back to the
            my location activity to change the location
             */

            if (building.equalsIgnoreCase(stockTake.getInventory().getStore()) && room.equalsIgnoreCase(stockTake.getInventory().getShelf())) {

                Inventory inventory = stockTake.getInventory();
                toSend.add(inventory);
                stockTake.setCounted(count++);

                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Item Name: " + stockTake.getInventory().getProductName());
                alert.setCancelable(false);
                alert.setMessage("Do you want to continue scanning ?");

                alert.setNegativeButton("Go to Details", (dialog, which) -> {
                            goToDetails();
                            dialog.dismiss();
                        }
                );
                alert.setPositiveButton("Yes", (dialog, which) -> {
                    onResume();
                    dialog.dismiss();
                });
                AlertDialog dialog = alert.create();
                dialog.show();
            } else {
                Toast.makeText(this, "Inventory not found", Toast.LENGTH_SHORT).show();
            }


        }
    }


    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, "Network Failure", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Conversion Problems", Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);

        Toast.makeText(this, "Inventory " + message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Intent auth = new Intent(this, LoginActivity.class);
        startActivity(auth);
        Log.d("UnauthorizedError", loginError.getMessage());

    }

    private void handleFailError(FailError failError) {
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(failError.getMessage());
        alert.setCancelable(false);
        alert.setMessage("Do you want to scan again ?");

        alert.setNegativeButton("Go to Details", (dialog, which) -> {
                    goToDetails();
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Yes", (dialog, which) -> {
            onResume();
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length > 0) {

                boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraAccepted) {
                    Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(CAMERA)) {
                            showMessageOKCancel(
                                    (dialog, which) -> {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            requestPermissions(new String[]{CAMERA},
                                                    REQUEST_CAMERA);
                                        }
                                    });
                        }
                    }
                }
            }
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new Builder(ScanActivity.this)
                .setMessage("You need to allow access to both the permissions")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void goToDetails() {
        Intent scanResult = new Intent(this, StockTakeActivity.class);

        Gson gson = new Gson();
        Type type = new TypeToken<List<Inventory>>() {
        }.getType();
        String json = gson.toJson(toSend, type);

        scanResult.putExtra("scanResult", json);

        scanResult.putExtra("building", building);
        scanResult.putExtra("room", room);
        scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(scanResult);
        finish();
    }

    @Override
    public void handleResult(Result result) {

        try {
            ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            tg.startTone(ToneGenerator.TONE_PROP_BEEP);

            myResult = result.getText();
            Log.d("QRCodeScanner", result.getText());
            Log.d("QRCodeScanner", result.getBarcodeFormat().toString());

            if(from.equals("SCAN_ROOM")) {
                Intent scanResult = new Intent(this, EnterLocation.class);
                scanResult.putExtra("scanResult", myResult);
                scanResult.putExtra("building", building);
                scanResult.putExtra("division", division);
                scanResult.putExtra("TAG", Tag);
                scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(scanResult);
                finish();
            }

            //Intent to the appropriate activity that sent the scan request
            switch (Tag) {

                case "A": {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(getString(R.string.choose_upload_type));
                    alert.setCancelable(false);
                    alert.setPositiveButton("Asset Upload", (dialog, which1) -> {

                                Intent scan = new Intent(this, AssetIn.class);
                                scan.putExtra("building", building);
                                scan.putExtra("room", room);
                                scan.putExtra("mDepartment", department);
                                scan.putExtra("division", division);
                                scan.putExtra("TAG", Tag);
                                scan.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(scan);

                                dialog.dismiss();

                            }
                    );

                    alert.setNegativeButton("Asset Upload With Components", (dialog, which1) -> {
                        Intent scan = new Intent(this, AssetInComponents.class);
                        scan.putExtra("building", building);
                        scan.putExtra("room", room);
                        scan.putExtra("mDepartment", department);
                        scan.putExtra("division", division);
                        scan.putExtra("TAG", Tag);
                        scan.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(scan);
                        dialog.dismiss();
                    });
                    AlertDialog dialog = alert.create();
                    dialog.show();

                    finish();
                    break;
                }
                case "B": {

                    progressBar = new ProgressBar(ScanActivity.this, null, android.R.attr.progressBarStyleLarge);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
                    params.addRule(RelativeLayout.CENTER_IN_PARENT);
                    scannerView.addView(progressBar, params);
                    progressBar.setVisibility(View.VISIBLE);
                    String accessToken = SharedPreferenceManager.getInstance(this).getToken();
                    scanActivityViewModel.getAllVerifiedAssets("Bearer " + accessToken);


                    break;
                }
                case "C": {
                    Intent scanResult = new Intent(this, AssetMove.class);
                    scanResult.putExtra("scanResult", myResult);

                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "D": {
                    Intent scanResult = new Intent(this, InventoryIn.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "E": {
                    Intent scanResult = new Intent(this, InventoryOut.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "F": {

                    progressBar = new ProgressBar(ScanActivity.this, null, android.R.attr.progressBarStyleLarge);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
                    params.addRule(RelativeLayout.CENTER_HORIZONTAL);
                    scannerView.addView(progressBar, params);
                    progressBar.setVisibility(View.VISIBLE);


                    String accessToken = SharedPreferenceManager.getInstance(this).getToken();
                    scanActivityViewModel.stockTake(myResult, "Bearer " + accessToken);


                    break;
                }
                case "G": {
                    Intent scanResult = new Intent(this, InventoryCheckOut.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "H": {
                    Intent scanResult = new Intent(this, InventoryCheckIn.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "I": {
                    Intent scanResult = new Intent(this, AssetCheckOutActivity.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("TAG", "I");
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                case "J": {
                    Intent scanResult = new Intent(this, AssetCheckInActivity.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("building", building);
                    scanResult.putExtra("room", room);
                    scanResult.putExtra("mDepartment", department);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }

                case "K": {
                    Intent scanResult = new Intent(this, StockReturnActivity.class);
                    scanResult.putExtra("scanResult", myResult);
                    scanResult.putExtra("invoiceNumber", invoice);
                    scanResult.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(scanResult);
                    finish();

                    break;
                }
                //scanning customer tag and send back to inventory checkOut activity
                case "Z": {
                    Intent scanResult = new Intent(this, InventoryCheckOut.class);
                    scanResult.putExtra("custodianTag", myResult);
                    scanResult.putExtra("quantityOut", quantityOut);
                    scanResult.putExtra("barCode", barCode);
                    startActivity(scanResult);
                    finish();

                    break;
                }

                //scanning custodian tag and send back to  asset check out activity
                case "Y": {
                    Intent scanResult = new Intent(this, AssetCheckOutActivity.class);
                    scanResult.putExtra("custodianTag", myResult);
                    scanResult.putExtra("assetName", name);
                    scanResult.putExtra("barCode", barCode);
                    scanResult.putExtra("serial", iSerial);
                    scanResult.putExtra("TAG", "Y");
                    scanResult.putExtra("model", iModel);
                    startActivity(scanResult);
                    finish();

                    break;
                }

                //scanning custodian tag and send back to  asset move activity
                case "X": {
                    Intent scanResult = new Intent(this, AssetMove.class);
                    scanResult.putExtra("custodianTag", myResult);
                    scanResult.putExtra("assetName", name);
                    scanResult.putExtra("barCode", barCode);
                    scanResult.putExtra("fromDivision", fromDivision);
                    scanResult.putExtra("fromBuilding", fromBuilding);
                    scanResult.putExtra("fromRoom", fromRoom);
                    scanResult.putExtra("fromDepart", fromDepart);
                    scanResult.putExtra("TAG", "X");
                    startActivity(scanResult);
                    finish();

                    break;
                }
                default: {
                    break;
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
                scannerView.setAutoFocus(mAutoFocus);

            } else {
                requestPermission();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCameraPreview();
        scannerView.stopCamera();
    }


    @Override
    protected void onPause() {
        scannerView.stopCameraPreview();
        scannerView.stopCamera();

        scannerView.setAutoFocus(false);
        super.onPause();
    }


}

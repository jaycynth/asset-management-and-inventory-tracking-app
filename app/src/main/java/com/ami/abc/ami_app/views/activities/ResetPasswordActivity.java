package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.ResetPassword;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.ResetPasswordViewModel;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordActivity extends AppCompatActivity {

    @BindView(R.id.email)
    TextInputEditText email;

    @BindView(R.id.submit)
    Button submit;

    String accessToken;
    ResetPasswordViewModel resetPasswordViewModel;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;
    @BindView(R.id.holder_layout)
    LinearLayout holderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.password_reset_title));

        resetPasswordViewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        resetPasswordViewModel.resetPasswordResponse().observe(this, resetPasswordState -> {
            assert resetPasswordState != null;
            if(resetPasswordState.getResetPassword() != null){
                handleResetPassword(resetPasswordState.getResetPassword());
            }

            if (resetPasswordState.getMessage() != null) {
                handleNetworkResponse(resetPasswordState.getMessage());
            }

            if (resetPasswordState.getErrorThrowable() != null) {
                handleError(resetPasswordState.getErrorThrowable());
            }
        });

        submit.setOnClickListener(v ->{
            String nEmail = email.getText().toString().trim();
            if(TextUtils.isEmpty(nEmail)){
                Toast.makeText(this, "Enter your email", Toast.LENGTH_SHORT).show();
            }else {
                holderLayout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                resetPasswordViewModel.resetPassword("Bearer " + accessToken, nEmail);
            }
        });


    }

    private void handleResetPassword(ResetPassword resetPassword) {
        holderLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        Toast.makeText(this, resetPassword.getMessage(), Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void handleError(Throwable errorThrowable) {
        holderLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        holderLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == android.R.id.home) {
            startActivity(new Intent(this, LoginActivity.class));

        }
        return super.onOptionsItemSelected(item);
    }

}

package com.ami.abc.ami_app.views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.ami.abc.ami_app.MainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Intent launch = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(launch);
        finish();
    }
}

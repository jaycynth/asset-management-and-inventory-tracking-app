package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.InventoryCheckInReportDao;
import com.ami.abc.ami_app.models.InventoryCheckInReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class InventoryCheckInReportsRepo {
    private InventoryCheckInReportDao inventoryCheckInReportDao;
    private Executor executor;

    public InventoryCheckInReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        inventoryCheckInReportDao = AppDatabase.getDatabase(AMI.context).inventoryCheckInReportDao();

    }


    public void saveInventorys(InventoryCheckInReport inventorys) {
        executor.execute(() -> inventoryCheckInReportDao.saveInventory(inventorys));
    }


    public LiveData<List<InventoryCheckInReport>> getInventorysReports() {
        return inventoryCheckInReportDao.getInventorysReports();
    }



    public void deleteInventorys() {
        executor.execute(() -> inventoryCheckInReportDao.deleteInventorys());
    }
}

package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.StockTakeReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface StockTakeReportDao {

    @Insert(onConflict = REPLACE)
    void saveInventory(StockTakeReport inventoryReports);
    //observe the list in the room database

    @Query("SELECT * FROM  StockTakeReport")
    LiveData<List<StockTakeReport>> getInventorysReports();

    @Query("SELECT * FROM StockTakeReport")
    List<StockTakeReport> getAll();

    @Delete
    void delete(StockTakeReport inventoryReports);



    //delete all the inventory in the room database upon sending information to the inventory verified table
    @Query("DELETE FROM StockTakeReport")
    void deleteInventorys();

}

package com.ami.abc.ami_app.views.activities;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ami.abc.ami_app.R;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.InventoryOutViewModel;

import java.io.IOException;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryOut extends AppCompatActivity {
    String building, room, depart, assetId;

    @BindView(R.id.asset_id)
    EditText asset_code;


    @BindView(R.id.decrement)
    Button decrement;

    @BindView(R.id.quantity)
    TextInputEditText new_quantity;

    @BindView(R.id.customer)
    TextInputEditText mCustomer;

    @BindView(R.id.invoice_number)
    TextInputEditText invoiceNumber;

    @BindView(R.id.invoice_value)
    TextInputEditText invoiceValue;


    InventoryOutViewModel inventoryOutViewModel;
    String accessToken;
    int inventoryId;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;
    @BindView(R.id.holder_layout)
    RelativeLayout holder_layout;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    RelativeLayout detailsLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_out);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.stock_out_title));

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");

        if (assetId == null) {
            asset_code.setText(" ");
        } else {
            asset_code.setText(assetId);
        }


        inventoryOutViewModel = ViewModelProviders.of(this).get(InventoryOutViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null && assetId != null) {
            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            confirm.setVisibility(View.GONE);
            inventoryOutViewModel.stockTake(assetId, "Bearer " + accessToken);
        }

        confirm.setOnClickListener(v -> {
            String code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Your Code", Toast.LENGTH_SHORT).show();

            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                inventoryOutViewModel.stockTake(code, "Bearer " + accessToken);
            }
        });

        inventoryOutViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleInventoryDetail(stockTakeState.getStockTake());
            }
            if (stockTakeState.getMessage() != null) {
                handleInventoryDetailNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getErrorThrowable() != null) {
                handleInventoryDetailError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getLoginError() != null) {
                handleInventoryDetailUnauthorized(stockTakeState.getLoginError());
            }
            if(stockTakeState.getFailError() != null){
                handleInventoryDetailFailError(stockTakeState.getFailError());
            }
        });

        inventoryOutViewModel.inventoryOutResponse().observe(this, inventoryOutState -> {
            assert inventoryOutState != null;
            if (inventoryOutState.getInventoryOut() != null) {
                handleOutInventory(inventoryOutState.getInventoryOut());
            }
            if (inventoryOutState.getMessage() != null) {
                handleDecrementNetworkResponse(inventoryOutState.getMessage());
            }
            if (inventoryOutState.getErrorThrowable() != null) {
                handleDecrementError(inventoryOutState.getErrorThrowable());
            }
            if (inventoryOutState.getLoginError() != null) {
                handleInventoryDetailUnauthorized(inventoryOutState.getLoginError());
            }
            if (inventoryOutState.getFailError() != null) {
                handleFail(inventoryOutState.getFailError());
            }
        });


        decrement.setOnClickListener(v -> {
            String quantityText = new_quantity.getText().toString();
            String customer = mCustomer.getText().toString().trim();
            String invoiceN = invoiceNumber.getText().toString().trim();
            String invoiceVText = invoiceValue.getText().toString().trim();

            if (TextUtils.isEmpty(quantityText)) {
                Toast.makeText(this, "Enter Quantity", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(customer)) {
                Toast.makeText(this, "Enter Customer Name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(invoiceN)) {
                Toast.makeText(this, "Enter Invoice Number", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(invoiceVText)) {
                Toast.makeText(this, "Enter Invoice Value", Toast.LENGTH_SHORT).show();
            } else {
                int quantity = Integer.parseInt(quantityText);
                int invoiceV = Integer.parseInt(invoiceVText);

                //post the new decremented current_quantity
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                inventoryOutViewModel.outInventory(inventoryId, "Bearer " + accessToken, customer, invoiceN, invoiceV, quantity);
            }

        });
    }



    private void handleFail(FailError failError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();
    }


    private void handleOutInventory(com.ami.abc.ami_app.models.InventoryOut inventoryOut) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        boolean status = inventoryOut.getStatus();
        if (status) {
            Toast.makeText(this, inventoryOut.getMessage(), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }

    }


    private void handleDecrementError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleDecrementNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }


    /* Gets the details of the inventory */
    private void handleInventoryDetail(StockTake stockTake) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        boolean status = stockTake.getStatus();
        if (status) {
            Inventory inventory = stockTake.getInventory();
            inventoryId = inventory.getId();
            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);
            int price = (int) Double.parseDouble(inventory.getUnitPrice());

            new_quantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.length() > 0) {
                        String quantityDrawn = new_quantity.getText().toString();

                        if (!TextUtils.isEmpty(quantityDrawn)) {
                            int qd = Integer.parseInt(quantityDrawn);
                            invoiceValue.setText(String.valueOf(qd * price));
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }


    }

    private void handleInventoryDetailError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleInventoryDetailNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleInventoryDetailUnauthorized(LoginError loginError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }
    private void handleInventoryDetailFailError(FailError failError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        asset_code.setText(" ");
        asset_code.requestFocus();
        Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }
}

package com.ami.abc.ami_app.views.activities;

import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerifyAsset extends AppCompatActivity {
    @BindView(R.id.edit_text_layout)
    LinearLayout linearLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.asset_id)
    TextInputEditText assetId;
    @BindView(R.id.verify)
    Button verify;

    String mBuilding, mRoom , mDepartment,Tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_asset);
        ButterKnife.bind(this);

        Intent send = getIntent();
        mBuilding = send.getStringExtra("building");
        mRoom = send.getStringExtra("room");
        mDepartment = send.getStringExtra("mDepartment");
        Tag = send.getStringExtra("TAG");

        verify.setOnClickListener(v->{
            String assetCode = assetId.getText().toString().trim();

            if(TextUtils.isEmpty(assetCode)){
                Toast.makeText(this, "Enter code to verify", Toast.LENGTH_SHORT).show();
            }else{
                linearLayout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                Intent scan = new Intent(VerifyAsset.this, AssetCheck.class);
                scan.putExtra("building", mBuilding);
                scan.putExtra("room", mRoom);
                scan.putExtra("mDepartment", mDepartment);
                scan.putExtra("TAG", Tag);
                scan.putExtra("scanResult",assetCode);
                startActivity(scan);
            }
        });
    }
}

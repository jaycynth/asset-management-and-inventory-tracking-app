package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.InventoryAdapter;
import com.ami.abc.ami_app.models.AllInventory;

import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.ui.ItemClickListener;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;

import com.ami.abc.ami_app.viewmodels.InventoryViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchInventoryActivity extends AppCompatActivity {

    @BindView(R.id.search)
    SearchView searchView;

    @BindView(R.id.all_verified_assets)
    RecyclerView all_verified_inventorys;

    static RecyclerView.LayoutManager layoutManager;

    InventoryAdapter inventoryAdapter;

    List<Inventory> allVerifiedInventorys = new ArrayList<>();
    List<Inventory> newList = new ArrayList<>();

    InventoryViewModel inventoryViewModel;

    String accessToken;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.no_results_layout)
    RelativeLayout noResultsLayout;

    @BindView(R.id.results_layout)
    RelativeLayout resultsLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_inventory);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.search_inventory_title));


        inventoryViewModel = ViewModelProviders.of(this).get(InventoryViewModel.class);

        inventoryViewModel.getAllInventoryResponse().observe(this,inventoryState -> {
            assert inventoryState != null;
            if(inventoryState.getAllInventory() != null){
                handleInventory(inventoryState.getAllInventory());
            }

            if (inventoryState.getMessage() != null) {
                handleNetworkResponse(inventoryState.getMessage());
            }

            if (inventoryState.getErrorThrowable() != null) {
                handleError(inventoryState.getErrorThrowable());
            }
            if(inventoryState.getLoginError() != null){
                handleUnAuthorized(inventoryState.getLoginError());
            }

        });

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            all_verified_inventorys.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            inventoryViewModel.getAllInventory("Bearer "+ accessToken);
        }

        searchView.setQueryHint("Name or Barcode");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String userInput = newText.toLowerCase();
                newList = new ArrayList<>();
                if(!TextUtils.isEmpty(userInput)) {

                    resultsLayout.setVisibility(View.VISIBLE);
                    noResultsLayout.setVisibility(View.GONE);

                    if(!allVerifiedInventorys.isEmpty()) {

                        for (Inventory inventory : allVerifiedInventorys) {
                            if (inventory.getProductName().toLowerCase().contains(userInput) || inventory.getProductCode().contains(userInput)) {

                                newList.add(inventory);
                            }
                        }

                        if (newList.isEmpty()) {
                            resultsLayout.setVisibility(View.GONE);
                            noResultsLayout.setVisibility(View.VISIBLE);
                        } else {
                            initView(newList);
                        }
                    }
                }else{
                    all_verified_inventorys.setAdapter(null);
                }
                return true;
            }
        });
    }

    private void handleUnAuthorized(LoginError loginError) {
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }


    private void handleInventory(AllInventory allInventory) {

        progressBar.setVisibility(View.GONE);
        all_verified_inventorys.setVisibility(View.VISIBLE);

        boolean status = allInventory.getStatus();
        if (status) {
            allVerifiedInventorys = allInventory.getInventory();

        }

    }

    private void initView(List<Inventory> inventoryList) {
        inventoryAdapter = new InventoryAdapter(inventoryList, this);
        layoutManager = new LinearLayoutManager(this);
        all_verified_inventorys.setLayoutManager(layoutManager);
        all_verified_inventorys.setItemAnimator(new DefaultItemAnimator());
        all_verified_inventorys.setAdapter(inventoryAdapter);
        all_verified_inventorys.setNestedScrollingEnabled(false);
        all_verified_inventorys.addOnItemTouchListener(new ItemClickListener(this, all_verified_inventorys, new ItemClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent details = new Intent(SearchInventoryActivity.this, SearchInventoryDetailsActivity.class);
                details.putExtra("inventoryCode", inventoryList.get(position).getProductCode());
                startActivity(details);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);
        all_verified_inventorys.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);
        all_verified_inventorys.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent scanOptions = new Intent(SearchInventoryActivity.this, ScanOptionsActivity.class);
        startActivity(scanOptions);
    }

}

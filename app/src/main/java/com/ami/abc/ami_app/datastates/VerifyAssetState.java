package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.VerifyAsset;

public class VerifyAssetState {
    private VerifyAsset verifyAsset;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public VerifyAssetState(VerifyAsset verifyAsset) {
        this.verifyAsset = verifyAsset;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public VerifyAssetState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.verifyAsset = null;
        this.loginError = null;
    }

    public VerifyAssetState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.verifyAsset = null;
        this.loginError = null;
    }

    public VerifyAssetState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.verifyAsset = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public VerifyAsset getVerifyAsset() {
        return verifyAsset;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

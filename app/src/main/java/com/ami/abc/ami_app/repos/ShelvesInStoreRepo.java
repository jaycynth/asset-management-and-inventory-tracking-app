package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.ShelvesInStoreState;
import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShelvesInStoreRepo {
    private ApiClient mApiClient;

    public ShelvesInStoreRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<ShelvesInStoreState> getShelvesIStore(String accessToken, int storeId) {

        MutableLiveData<ShelvesInStoreState> shelvesInStoreStateMutableLiveData = new MutableLiveData<>();

        Call<AllShelves> call = mApiClient.amiService().getShelvesInStore(accessToken, storeId);
        call.enqueue(new Callback<AllShelves>() {
            @Override
            public void onResponse(Call<AllShelves> call, Response<AllShelves> response) {
                if (response.code() == 200) {
                    shelvesInStoreStateMutableLiveData.setValue(new ShelvesInStoreState(response.body()));

                }
                else if (response.code() == 401) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    shelvesInStoreStateMutableLiveData.setValue(new ShelvesInStoreState(loginError));
                }else {
                    shelvesInStoreStateMutableLiveData.setValue(new ShelvesInStoreState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllShelves> call, Throwable t) {
                shelvesInStoreStateMutableLiveData.setValue(new ShelvesInStoreState(t));
            }
        });

        return shelvesInStoreStateMutableLiveData;

    }
}

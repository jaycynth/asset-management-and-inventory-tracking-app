package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AllVerifiedStatusState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.repos.AllVerifiedAssetsRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;

public class ScanActivityViewModel extends AndroidViewModel {
    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;

    private MediatorLiveData<AllVerifiedStatusState> allVerifiedStatusStateMediatorLiveData;
    private AllVerifiedAssetsRepo allVerifiedAssetsRepo;


    public ScanActivityViewModel(Application application) {
        super(application);
        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);

        allVerifiedStatusStateMediatorLiveData = new MediatorLiveData<>();
        allVerifiedAssetsRepo = new AllVerifiedAssetsRepo(application);

    }


    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }

    public LiveData<AllVerifiedStatusState> getAllVerifiedAssetsResponse(){
        return allVerifiedStatusStateMediatorLiveData;
    }

    public void getAllVerifiedAssets(String accessToken){

        LiveData<AllVerifiedStatusState> allVerifiedStatusStateLiveData = allVerifiedAssetsRepo.getVerifiedAssets(accessToken);
        allVerifiedStatusStateMediatorLiveData.addSource(allVerifiedStatusStateLiveData,
                allVerifiedStatusStateMediatorLiveData -> {
                    if (this.allVerifiedStatusStateMediatorLiveData.hasActiveObservers()){
                        this.allVerifiedStatusStateMediatorLiveData.removeSource(allVerifiedStatusStateLiveData);
                    }
                    this.allVerifiedStatusStateMediatorLiveData.setValue(allVerifiedStatusStateMediatorLiveData);
                });

    }

}

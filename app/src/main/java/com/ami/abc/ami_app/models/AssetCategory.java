package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssetCategory {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("depclass")
    @Expose
    private String depclass;
    @SerializedName("method")
    @Expose
    private Integer method;
    @SerializedName("count_type")
    @Expose
    private String countType;
    @SerializedName("dynamicfields")
    @Expose
    private List<Dynamicfield> dynamicfields = null;
    @SerializedName("sub_categories")
    @Expose
    private List<SubCategory> subCategories = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDepclass() {
        return depclass;
    }

    public void setDepclass(String depclass) {
        this.depclass = depclass;
    }

    public Integer getMethod() {
        return method;
    }

    public void setMethod(Integer method) {
        this.method = method;
    }

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public List<Dynamicfield> getDynamicfields() {
        return dynamicfields;
    }

    public void setDynamicfields(List<Dynamicfield> dynamicfields) {
        this.dynamicfields = dynamicfields;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }
}

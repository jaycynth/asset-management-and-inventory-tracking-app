package com.ami.abc.ami_app.views.activities;

import android.Manifest;
import android.app.AlertDialog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AddInventory;
import com.ami.abc.ami_app.models.AddInventoryImage;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.AllInventory;
import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.IncrementInventory;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.InventoryInReport;
import com.ami.abc.ami_app.models.InventoryScannedStrict;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Supplier;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.InventoryInViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryIn extends AppCompatActivity {

    private static final int REQUEST_PICTURE_CAPTURE = 2;
    String building, room, depart, assetId;

    @BindView(R.id.asset_id)
    EditText asset_code;

    InventoryInViewModel inventoryInViewModel;
    String accessToken;

    @BindView(R.id.quantity_increment)
    LinearLayout quantity_increment;
    @BindView(R.id.quantity)
    TextInputEditText currentQuantity;
    @BindView(R.id.update)
    Button update;
    @BindView(R.id.update_spin_kit)
    ProgressBar update_spin_kit;
    int inventoryId;
    int oldQuantity;
    @BindView(R.id.inventory_name1)
    TextInputEditText inventoryName;


    @BindView(R.id.inventory_add)
    LinearLayout inventory_add;
    @BindView(R.id.asset_name)
    TextInputEditText asset_name;
    @BindView(R.id.unit)
    TextInputEditText unit;
    @BindView(R.id.new_quantity)
    TextInputEditText new_quantity;

    private List<String> supplierName = new ArrayList<>();
    @BindView(R.id.supplier)
    AutoCompleteTextView nSupplier;
    @BindView(R.id.supplier_layout)
    TextInputLayout supplier_layout;
    @BindView(R.id.supplier_drop_down)
    ImageView supplier_drop_down;
    @BindView(R.id.reorder_level)
    TextInputEditText reorder_level;
    @BindView(R.id.add)
    Button add;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    List<Inventory> inventoryList = new ArrayList<>();


    @BindView(R.id.main_layout)
    RelativeLayout main_layout;
    @BindView(R.id.main_spin_kit)
    ProgressBar main_spin_kit;

    @BindView(R.id.confirm)
    Button confirm;


    List<Asset> assetList = new ArrayList<>();

    String code;
    String pictureFilePath;

    private static final int PERMISSION_REQUEST_CODE = 2;


    int id;
    File imgFile;

    boolean webUpload = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_in);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Inventory In");

        inventoryInViewModel = ViewModelProviders.of(this).get(InventoryInViewModel.class);


        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");


        if (assetId == null) {
            asset_code.setText(" ");
        } else {
            asset_code.setText(assetId);
        }

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null && assetId != null) {
            main_layout.setAlpha(0.4f);
            main_spin_kit.setVisibility(View.VISIBLE);

            confirm.setVisibility(View.GONE);
            inventoryInViewModel.getAllAssets("Bearer " + accessToken);


        }

        confirm.setOnClickListener(v -> {
            String code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                main_layout.setAlpha(0.4f);
                main_spin_kit.setVisibility(View.VISIBLE);

                inventoryInViewModel.getAllAssets("Bearer " + accessToken);
                //inventoryInViewModel.inventoryScannedStrict(code, building,"Bearer " + accessToken);


            }
        });

        inventoryInViewModel.inventoryScannedStrictResponse().observe(this, inventoryScannedStrictState -> {
            if (inventoryScannedStrictState.getAllInventory() != null) {
                handleInventoryScannedStrict(inventoryScannedStrictState.getAllInventory());
            }
            if (inventoryScannedStrictState.getErrorThrowable() != null) {
                handleError(inventoryScannedStrictState.getErrorThrowable());
            }

            if (inventoryScannedStrictState.getMessage() != null) {
                handleInventoryNetworkResponse(inventoryScannedStrictState.getMessage());
            }
            if (inventoryScannedStrictState.getFailError() != null) {
                handleInventoryFailError(inventoryScannedStrictState.getFailError());
            }
            if (inventoryScannedStrictState.getLoginError() != null) {
                handleUnAuthorized(inventoryScannedStrictState.getLoginError());
            }
        });


        inventoryInViewModel.addInventoryImage().observe(this, addInventoryImageState -> {
            assert addInventoryImageState != null;
            if (addInventoryImageState.getAddInventoryImage() != null) {
                handleAddInventoryImage(addInventoryImageState.getAddInventoryImage());
            }
            if (addInventoryImageState.getMessage() != null) {
                handleInventoryImageNetworkResponse(addInventoryImageState.getMessage());
            }
            if (addInventoryImageState.getErrorThrowable() != null) {
                handleInventoryImageError(addInventoryImageState.getErrorThrowable());
            }
            if (addInventoryImageState.getLoginError() != null) {
                handleUnAuthorized(addInventoryImageState.getLoginError());
            }
        });

        inventoryInViewModel.getAllAssetResponse().observe(this, assetState -> {
            assert assetState != null;
            if (assetState.getAllAssets() != null) {
                handleAllAssets(assetState.getAllAssets());
            }
            if (assetState.getMessage() != null) {
                handleNetworkResponse(assetState.getMessage());
            }
            if (assetState.getErrorThrowable() != null) {
                handleError(assetState.getErrorThrowable());
            }
            if (assetState.getLoginError() != null) {
                handleUnAuthorized(assetState.getLoginError());
            }
        });

        inventoryInViewModel.getAllInventoryResponse().observe(this, inventoryState -> {
            assert inventoryState != null;
            if (inventoryState.getAllInventory() != null) {
                handleInventory(inventoryState.getAllInventory());
            }
            if (inventoryState.getMessage() != null) {
                handleNetworkResponse(inventoryState.getMessage());
            }
            if (inventoryState.getErrorThrowable() != null) {
                handleError(inventoryState.getErrorThrowable());
            }
            if (inventoryState.getLoginError() != null) {
                handleUnAuthorized(inventoryState.getLoginError());
            }
        });

        inventoryInViewModel.incrementInventoryResponse().observe(this, incrementInventoryState -> {
            assert incrementInventoryState != null;
            if (incrementInventoryState.getIncrementInventory() != null) {
                handleIncrementInventory(incrementInventoryState.getIncrementInventory());
            }
            if (incrementInventoryState.getMessage() != null) {
                handleIncrementNetworkResponse(incrementInventoryState.getMessage());
            }
            if (incrementInventoryState.getErrorThrowable() != null) {
                handleIncrementError(incrementInventoryState.getErrorThrowable());
            }
            if (incrementInventoryState.getLoginError() != null) {
                handleUnAuthorized(incrementInventoryState.getLoginError());
            }
        });

        inventoryInViewModel.addInventoryResponse().observe(this, addInventoryState -> {
            assert addInventoryState != null;
            if (addInventoryState.getAddInventory() != null) {
                handleAddInventory(addInventoryState.getAddInventory());
            }
            if (addInventoryState.getMessage() != null) {
                handleAddNetworkResponse(addInventoryState.getMessage());
            }
            if (addInventoryState.getErrorThrowable() != null) {
                handleAddError(addInventoryState.getErrorThrowable());
            }

            if (addInventoryState.getLoginError() != null) {
                handleUnAuthorized(addInventoryState.getLoginError());
            }
        });


        inventoryInViewModel.getAllSuppliersResponse().observe(this, supplierState -> {
            assert supplierState != null;
            if (supplierState.getAllSuppliers() != null) {
                handleSuppliers(supplierState.getAllSuppliers());
            }
            if (supplierState.getMessage() != null) {
                handleSupplierNetworkResponse(supplierState.getMessage());
            }
            if (supplierState.getErrorThrowable() != null) {
                handleSupplierError(supplierState.getErrorThrowable());
            }
            if (supplierState.getLoginError() != null) {
                handleUnAuthorized(supplierState.getLoginError());
            }
        });

        //set up supplier drop down
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, supplierName);
        nSupplier.setAdapter(adapter);

        supplier_drop_down.setOnClickListener(v -> nSupplier.showDropDown());
        nSupplier.setOnClickListener(v -> nSupplier.showDropDown());

        update.setOnClickListener(v -> {
            String quant = currentQuantity.getText().toString().trim();
            //int usingQuantity = quantity - oldQuantity;
            if (TextUtils.isEmpty(quant)) {
                Toast.makeText(this, "enter quantity", Toast.LENGTH_SHORT).show();
            } else {
                update.setVisibility(View.INVISIBLE);
                update_spin_kit.setVisibility(View.VISIBLE);
                double dQuantity = Double.parseDouble(quant);
                int quantity = (int) dQuantity;

                inventoryInViewModel.incrementInventory("Bearer " + accessToken, inventoryId, quantity);
            }
        });

        add.setOnClickListener(v -> {
            String productCode = asset_code.getText().toString();
            String productName = asset_name.getText().toString();
            String supplier = nSupplier.getText().toString();
            String unitPriceText = unit.getText().toString();
            String quantityText = new_quantity.getText().toString();
            String reorderLeverText = reorder_level.getText().toString();

            if (TextUtils.isEmpty(productCode)) {
                Toast.makeText(this, "Enter Product Code", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(productName)) {
                Toast.makeText(this, "Enter product name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(supplier)) {
                Toast.makeText(this, "Enter supplier name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(unitPriceText)) {
                Toast.makeText(this, "Enter unit price", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(quantityText)) {
                Toast.makeText(this, "Enter quantity", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(reorderLeverText)) {
                Toast.makeText(this, "Enter the reorder level", Toast.LENGTH_SHORT).show();
            } else {

                add.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                double dUnitPrice = Double.parseDouble(unitPriceText);
                int unitPrice = (int) dUnitPrice;
                double dQuantity = Double.parseDouble(quantityText);
                int quantity = (int) dQuantity;
                int reorderLever = Integer.parseInt(reorderLeverText);
                inventoryInViewModel.addInventory("Bearer " + accessToken, productName, productCode, building, room, supplier, quantity, unitPrice, reorderLever);

            }
        });


    }

    private void handleInventoryFailError(FailError failError) {
        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);
        //Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
        alert1.setCancelable(false);
        alert1.setMessage(failError.getMessage() + ". Continue scanning ? ");

        alert1.setNegativeButton("Exit", (dialog1, which1) -> {
                    onBackPressed();
                    dialog1.dismiss();
                }
        );
        alert1.setNeutralButton("Scanner", ((dialog1, which1) -> {
            Intent scanAsset = new Intent(this, InventoryIn.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "D");
            startActivity(scanAsset);
            dialog1.dismiss();
        }));
        alert1.setPositiveButton("Camera", (dialog1, which1) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "D");
            startActivity(scanAsset);
            dialog1.dismiss();
        });
        AlertDialog dialog1 = alert1.create();
        dialog1.show();

    }

    private void handleInventoryNetworkResponse(String message) {
        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);
        //Toast.makeText(this, "jhssa"+ message, Toast.LENGTH_SHORT).show();

        //inventory does not exist so add it a fresh
        inventory_add.setVisibility(View.VISIBLE);
        quantity_increment.setVisibility(View.GONE);
        ///new_quantity.setText();
        Log.d("NetworkResponse",message);
    }


    private void handleInventoryScannedStrict(InventoryScannedStrict allInventory) {
        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);
        boolean status = allInventory.getStatus();

        if (status) {


            Inventory inventory = allInventory.getInventory();

            InventoryInReport assetsReports = new InventoryInReport();
            assetsReports.setId(inventory.getId());
            assetsReports.setBarcode(inventory.getProductCode());
            assetsReports.setName(inventory.getProductName());
            assetsReports.setCount(inventory.getQuantity());
            inventoryInViewModel.saveScannedInventory(assetsReports);


            quantity_increment.setVisibility(View.VISIBLE);
            inventory_add.setVisibility(View.GONE);

            inventoryName.setText(inventory.getProductName());
            oldQuantity = inventory.getQuantity();
            //currentQuantity.setText(String.valueOf(inventory.getQuantity() + 1));
            currentQuantity.setText(" ");
            Toast.makeText(this, inventory.getProductName() + " Added", Toast.LENGTH_SHORT).show();
            inventoryId = inventory.getId();


        }
    }

    private void handleInventoryImageError(Throwable errorThrowable) {
        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);

        if (errorThrowable instanceof IOException) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(getString(R.string.network_failure));
            alert.setCancelable(false);
            alert.setMessage("Do you want to retry..? ");

            alert.setNegativeButton("No", ((dialog, which) -> {

                //inner alert dialog
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                alert1.setCancelable(false);
                alert1.setMessage("Continue scanning ? ");

                alert1.setNeutralButton("Exit", (dialog1, which1) -> {
                            onBackPressed();
                            dialog1.dismiss();
                        }
                );
                alert1.setNegativeButton("Scanner", ((dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, InventoryIn.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                }));
                alert1.setPositiveButton("Camera", (dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                });
                AlertDialog dialog1 = alert1.create();
                dialog1.show();

                dialog.dismiss();

            }));

            alert.setPositiveButton("Yes", (dialog, which) -> {
                inventory_add.setAlpha(0.4f);
                main_spin_kit.setVisibility(View.VISIBLE);

                inventoryInViewModel.postAddInventoryImage("Bearer " + accessToken, imgFile, id);
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        } else {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(getString(R.string.error_occured));
            alert.setCancelable(false);
            alert.setMessage("Do you want to retry..? ");

            alert.setNegativeButton("No", ((dialog, which) -> {

                //inner alert dialog
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                alert1.setCancelable(false);
                alert1.setMessage("Continue scanning ? ");

                alert1.setNegativeButton("Exit", (dialog1, which1) -> {
                            onBackPressed();
                            dialog1.dismiss();
                        }
                );
                alert1.setNeutralButton("Scanner", ((dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, InventoryIn.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                }));
                alert1.setPositiveButton("Camera", (dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                });
                AlertDialog dialog1 = alert1.create();
                dialog1.show();

                dialog.dismiss();

            }));

            alert.setPositiveButton("Yes", (dialog, which) -> {
                inventory_add.setAlpha(0.4f);
                main_spin_kit.setVisibility(View.VISIBLE);

                inventoryInViewModel.postAddInventoryImage("Bearer " + accessToken, imgFile, id);
            });
            AlertDialog dialog = alert.create();
            dialog.show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleInventoryImageNetworkResponse(String message) {
        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);

        showAlertDialog();

        if (webUpload) {
            showAlertDialogContinue();
            return;
        }

        this.webUpload = true;
        showAlertDialog();

        Log.d("ImageNetworkResponse", message);
    }


    private void handleAddInventoryImage(AddInventoryImage addInventoryImage) {
        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);
        boolean status = addInventoryImage.getStatus();
        if (status) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setCancelable(false);
            alert.setMessage(addInventoryImage.getMessage() + " Do you want to continue adding items ? ");

            alert.setPositiveButton("Camera", (dialog, which) -> {
                        Intent scanAsset = new Intent(this, ScanActivity.class);
                        scanAsset.putExtra("building", building);
                        scanAsset.putExtra("room", room);
                        scanAsset.putExtra("mDepartment", depart);
                        scanAsset.putExtra("TAG", "D");
                        startActivity(scanAsset);
                        dialog.dismiss();

                    }
            );
            alert.setNeutralButton("Scanner", ((dialog, which) -> {
                Intent scanAsset = new Intent(this, InventoryIn.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
                dialog.dismiss();
            }));

            alert.setNegativeButton("Exit", (dialog, which) -> {
                onBackPressed();
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                takePhoto();

            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
                imgFile = new File(pictureFilePath);
                if (imgFile.exists()) {
                    Toast.makeText(this, "Image taken successfully", Toast.LENGTH_SHORT).show();
                    inventory_add.setAlpha(0.4f);
                    main_spin_kit.setVisibility(View.VISIBLE);

                    inventoryInViewModel.postAddInventoryImage("Bearer " + accessToken, imgFile, id);


                }
            }
        }

    }

    private File getPictureFile() {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "AMI_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(pictureFile, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert image != null;
        pictureFilePath = image.getAbsolutePath();
        return image;
    }


    /* Handles adding a new inventory and details and posting them to the server for the first time*/
    private void handleAddInventory(AddInventory addInventory) {
        progressBar.setVisibility(View.INVISIBLE);
        add.setVisibility(View.VISIBLE);

        boolean status = addInventory.getStatus();
        if (status) {
            id = addInventory.getAddedAsset().getId();

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(addInventory.getMessage());
            // prevent cancel of AlertDialog on click of back button and outside touch
            alert.setCancelable(false);
            alert.setMessage("Do you want to take item photo ? ");

            alert.setNegativeButton("No", ((dialog, which) -> {

                //inner alert dialog
                AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
                alert1.setCancelable(false);
                alert1.setMessage("Continue scanning ? ");

                alert1.setNegativeButton("Exit", (dialog1, which1) -> {
                            onBackPressed();
                            dialog1.dismiss();
                        }
                );
                alert1.setNeutralButton("Scanner", ((dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, InventoryIn.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                }));
                alert1.setPositiveButton("Camera", (dialog1, which1) -> {
                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "D");
                    startActivity(scanAsset);
                    dialog1.dismiss();
                });
                AlertDialog dialog1 = alert1.create();
                dialog1.show();

                dialog.dismiss();

            }));

            alert.setPositiveButton("Yes", (dialog, which) -> {
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        takePhoto();
                    } else {
                        Toast.makeText(this, "You have no permissions set", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_PICTURE_CAPTURE);
                    }


                } else {
                    takePhoto();
                }
            });
            AlertDialog dialog = alert.create();
            dialog.show();

        }
    }

    private void handleAddError(Throwable errorThrowable) {
        progressBar.setVisibility(View.INVISIBLE);
        add.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleAddNetworkResponse(String message) {
        progressBar.setVisibility(View.INVISIBLE);
        add.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    /* if you are only incrementing the quantity of an inventory when adding then the quantity is the
      only one posted back to the server */
    private void handleIncrementInventory(IncrementInventory incrementInventory) {
        update.setVisibility(View.VISIBLE);
        update_spin_kit.setVisibility(View.INVISIBLE);
        boolean status = incrementInventory.getStatus();
        if (status) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(incrementInventory.getMessage() + " successfully");
            alert.setCancelable(false);
            alert.setMessage("Continue scanning ?");

            alert.setNegativeButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNeutralButton("Scanner", ((dialog, which) -> {
                Intent scanAsset = new Intent(this, InventoryIn.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
                dialog.dismiss();
            }));
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        }


    }

    private void handleIncrementError(Throwable errorThrowable) {
        update.setVisibility(View.VISIBLE);
        update_spin_kit.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleIncrementNetworkResponse(String message) {
        update.setVisibility(View.VISIBLE);
        update_spin_kit.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleAllAssets(AllAssets allAssets) {
        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);


        boolean status = allAssets.getStatus();
        if (status) {
            assetList = allAssets.getData();
            for (Asset asset : assetList) {
                code = asset_code.getText().toString().trim();
                if (code.equals(asset.getCode())) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("The barcode you scanned was entered as an asset");
                    // prevent cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(false);
                    alert.setMessage("Do You wish to continue ?");


                    alert.setPositiveButton("Yes", ((dialog, which) -> {

                        main_layout.setAlpha(0.4f);
                        main_spin_kit.setVisibility(View.VISIBLE);

                        confirm.setVisibility(View.GONE);


                        // inventoryInViewModel.getAllInventory("Bearer " + accessToken);
                        inventoryInViewModel.inventoryScannedStrict(code, building, "Bearer " + accessToken);
                        inventoryInViewModel.getAllSuppliers("Bearer " + accessToken);

                        dialog.dismiss();
                    }));
                    alert.setNegativeButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    });
                    AlertDialog dialog = alert.create();
                    dialog.show();

                    return;
                }
            }

            main_layout.setAlpha(0.4f);
            main_spin_kit.setVisibility(View.VISIBLE);

            confirm.setVisibility(View.GONE);


            //inventoryInViewModel.getAllInventory("Bearer " + accessToken);
            inventoryInViewModel.inventoryScannedStrict(code, building, "Bearer " + accessToken);

            inventoryInViewModel.getAllSuppliers("Bearer " + accessToken);
        }
    }

    /* if you are adding inventory for the first time , it gives you a form to fill up all the details of the new
      inventory you are adding
      If the inventory you are adding already exists then it just gives you an option to increment the quantity that is already there
    */
    private void handleInventory(AllInventory allInventory) {
        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);

        confirm.setVisibility(View.GONE);

        boolean status = allInventory.getStatus();
        if (status) {
            inventoryList = allInventory.getInventory();

            for (Inventory inventory : inventoryList) {
                code = asset_code.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(this, "Enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    if (code.equals(inventory.getProductCode()) && building.equals(inventory.getStore())) {
                        //inventory exists so just increment the current_quantity

                        InventoryInReport assetsReports = new InventoryInReport();
                        assetsReports.setId(inventory.getId());
                        assetsReports.setBarcode(inventory.getProductCode());
                        assetsReports.setName(inventory.getProductName());
                        assetsReports.setCount(inventory.getQuantity());
                        inventoryInViewModel.saveScannedInventory(assetsReports);

                        quantity_increment.setVisibility(View.VISIBLE);
                        inventory_add.setVisibility(View.GONE);

                        inventoryName.setText(inventory.getProductName());
                        oldQuantity = inventory.getQuantity();
                        //currentQuantity.setText(String.valueOf(inventory.getQuantity() + 1));
                        currentQuantity.setText(" ");
                        Toast.makeText(this, inventory.getProductName() + " Added", Toast.LENGTH_SHORT).show();
                        inventoryId = inventory.getId();

                        return;
                    }

                }
            }

            //inventory does not exist so add it a fresh
            inventory_add.setVisibility(View.VISIBLE);
            quantity_increment.setVisibility(View.GONE);
            // new_quantity.setText(" ");

        }


    }

    private void handleError(Throwable errorThrowable) {
        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);

        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleNetworkResponse(String message) {
        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);

        main_layout.setAlpha(1);
        main_spin_kit.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    /* handle supplier response for the drop down */

    private void handleSuppliers(AllSuppliers allSuppliers) {
        boolean status = allSuppliers.getStatus();
        if (status) {
            List<Supplier> supplierList = allSuppliers.getSuppliers();
            for (Supplier supplier : supplierList) {
                supplierName.add(supplier.getName());
            }
        }
    }

    private void handleSupplierError(Throwable errorThrowable) {

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleSupplierNetworkResponse(String message) {
        Toast.makeText(this, "Supplier" + message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {

        inventory_add.setAlpha(1);
        main_spin_kit.setVisibility(View.GONE);

        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Log.d("UnauthorizedErrorLog", loginError.getMessage());

//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }

    private void takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File pictureFile = getPictureFile();
            Uri photoURI;
            if ((Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)) {
                photoURI = FileProvider.getUriForFile(this, getPackageName() + ".provider", pictureFile);
            } else {
                photoURI = Uri.fromFile(pictureFile);
            }
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);

        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Failure uploading to the server");
        alert.setCancelable(false);
        alert.setMessage("Please Upload using the web application.. ");


        alert.setPositiveButton("Continue", (dialog, which) -> {
            dialog.dismiss();
            //inner alert dialog
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setCancelable(false);
            alert1.setMessage("Continue scanning ? ");

            alert1.setNegativeButton("Exit", (dialog1, which1) -> {
                        dialog1.dismiss();
                        onBackPressed();

                    }
            );
            alert1.setNeutralButton("Scanner", ((dialog1, which1) -> {
                dialog1.dismiss();

                Intent scanAsset = new Intent(this, InventoryIn.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);

            }));
            alert1.setPositiveButton("Camera", (dialog1, which1) -> {
                dialog1.dismiss();

                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
            });
            AlertDialog dialog1 = alert1.create();
            dialog1.show();

        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private void showAlertDialogContinue() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Upload not successful");
        alert.setCancelable(false);

        alert.setNegativeButton("Continue", ((dialog, which) -> {

            //inner alert dialog
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setCancelable(false);
            alert1.setMessage("Continue scanning ? ");

            alert1.setNegativeButton("Exit", (dialog1, which1) -> {
                        onBackPressed();
                        dialog1.dismiss();
                    }
            );
            alert1.setNeutralButton("Scanner", ((dialog1, which1) -> {
                Intent scanAsset = new Intent(this, InventoryIn.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
                dialog1.dismiss();
            }));
            alert1.setPositiveButton("Camera", (dialog1, which1) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "D");
                startActivity(scanAsset);
                dialog1.dismiss();
            });
            AlertDialog dialog1 = alert1.create();
            dialog1.show();

            dialog.dismiss();

        }));

        alert.setPositiveButton("Yes", (dialog, which) -> {
            inventory_add.setAlpha(0.4f);
            main_spin_kit.setVisibility(View.VISIBLE);

            inventoryInViewModel.postAddInventoryImage("Bearer " + accessToken, imgFile, id);
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }

}
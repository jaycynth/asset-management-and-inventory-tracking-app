package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.InventoryState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.repos.InventoryRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;

public class InventoryViewModel extends AndroidViewModel {
    private MediatorLiveData<InventoryState> inventoryStateMediatorLiveData;
    private InventoryRepo inventoryRepo;

    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;


    public InventoryViewModel(Application application) {
        super(application);
        inventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryRepo = new InventoryRepo(application);

        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);
    }

    public LiveData<InventoryState> getAllInventoryResponse(){
        return inventoryStateMediatorLiveData;
    }

    public void getAllInventory(String accessToken){
        LiveData<InventoryState> inventoryStateLiveData = inventoryRepo.getAllInventory(accessToken);
        inventoryStateMediatorLiveData.addSource(inventoryStateLiveData,
                inventoryStateMediatorLiveData -> {
                    if (this.inventoryStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryStateMediatorLiveData.removeSource(inventoryStateLiveData);
                    }
                    this.inventoryStateMediatorLiveData.setValue(inventoryStateMediatorLiveData);
                });
    }
    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }


}

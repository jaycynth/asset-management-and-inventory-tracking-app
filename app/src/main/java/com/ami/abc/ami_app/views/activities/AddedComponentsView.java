package com.ami.abc.ami_app.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.AddedComponentsAdapter;
import com.ami.abc.ami_app.models.CacheComponents;
import com.ami.abc.ami_app.viewmodels.NewAssetViewModel;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddedComponentsView extends AppCompatActivity {

    @BindView(R.id.added_components_rv)
    RecyclerView addedComponents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_added_components_view);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Added Components");

        ButterKnife.bind(this);

        NewAssetViewModel newAssetViewModel = ViewModelProviders.of(this).get(NewAssetViewModel.class);

        newAssetViewModel.getComponentsListLiveData().observe(this, cacheComponents -> {
            if (cacheComponents != null && !cacheComponents.isEmpty()) {
                initView(cacheComponents);
            } else {
                Toast.makeText(this, "no components added yet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(List<CacheComponents> cacheComponentsList) {
        AddedComponentsAdapter addedComponentsAdapter = new AddedComponentsAdapter(cacheComponentsList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        addedComponents.setLayoutManager(layoutManager);
        addedComponents.setItemAnimator(new DefaultItemAnimator());
        addedComponents.setAdapter(addedComponentsAdapter);
        addedComponents.setNestedScrollingEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
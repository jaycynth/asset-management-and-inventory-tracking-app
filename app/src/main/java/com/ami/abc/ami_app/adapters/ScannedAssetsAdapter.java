package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AssetScanned;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScannedAssetsAdapter extends RecyclerView.Adapter<ScannedAssetsAdapter.MyViewHolder> {
    private List<AssetScanned> assetScannedList;
    private Context context;

    public ScannedAssetsAdapter(List<AssetScanned> assetScannedList, Context context) {
        this.assetScannedList = assetScannedList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public ScannedAssetsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.verified_asset_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ScannedAssetsAdapter.MyViewHolder holder, int i) {
        AssetScanned assetScanned = assetScannedList.get(i);
        holder.status.setText(assetScanned.getStatus());
        

    }

    @Override
    public int getItemCount() {
        return assetScannedList.size();
    }
}

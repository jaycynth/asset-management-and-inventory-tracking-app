package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.SupplierState;
import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuppliersRepo {

    private ApiClient mApiClient;

    //constructor
    public SuppliersRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<SupplierState> getAllSuppliers(String accessToken) {

        MutableLiveData<SupplierState> supplierStateMutableLiveData = new MutableLiveData<>();
        Call<AllSuppliers> call = mApiClient.amiService().getAllSuppliers(accessToken);
        call.enqueue(new Callback<AllSuppliers>() {
            @Override
            public void onResponse(Call<AllSuppliers> call, Response<AllSuppliers> response) {
                if (response.code() == 200) {
                    supplierStateMutableLiveData.setValue(new SupplierState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    supplierStateMutableLiveData.setValue(new SupplierState(loginError));
                }else {
                    supplierStateMutableLiveData.setValue(new SupplierState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllSuppliers> call, Throwable t) {
                supplierStateMutableLiveData.setValue(new SupplierState(t));
            }
        });

        return supplierStateMutableLiveData;

    }
}

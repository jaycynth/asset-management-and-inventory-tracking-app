package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.DepartmentState;
import com.ami.abc.ami_app.models.AllDepartments;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DepartmentRepo {

    private ApiClient mApiClient;
    //constructor
    public DepartmentRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<DepartmentState> getAllDepartments(String accessToken) {

        MutableLiveData<DepartmentState> departmentStateMutableLiveData = new MutableLiveData<>();
        Call<AllDepartments> call = mApiClient.getAllDepartmentsService().getDepartments(accessToken);
        call.enqueue(new Callback<AllDepartments>() {
            @Override
            public void onResponse(Call<AllDepartments> call, Response<AllDepartments> response) {
                if (response.code() == 200) {
                    departmentStateMutableLiveData.setValue(new DepartmentState(response.body()));

                } else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    departmentStateMutableLiveData.setValue(new DepartmentState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    departmentStateMutableLiveData.setValue(new DepartmentState(error));
                }
                else {
                    departmentStateMutableLiveData.setValue(new DepartmentState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllDepartments> call, Throwable t) {
                departmentStateMutableLiveData.setValue(new DepartmentState(t));
            }
        });

        return departmentStateMutableLiveData;

    }
}

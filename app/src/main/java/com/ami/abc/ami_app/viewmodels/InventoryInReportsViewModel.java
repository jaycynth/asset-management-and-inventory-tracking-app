package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.InventoryInReport;
import com.ami.abc.ami_app.repos.InventoryInReportsRepo;

import java.util.List;

public class InventoryInReportsViewModel extends AndroidViewModel {
    private InventoryInReportsRepo inventorysInReportsRepo;

    private LiveData<List<InventoryInReport>> listLiveData;



    public InventoryInReportsViewModel(Application application) {
      super(application);
        inventorysInReportsRepo = new InventoryInReportsRepo();

        listLiveData = inventorysInReportsRepo.getInventorysReports();
    }

    public LiveData<List<InventoryInReport>> getListLiveData() {
        return listLiveData;
    }


    public void saveScannedInventory(InventoryInReport inventoryInReport) {
        inventorysInReportsRepo.saveInventorys(inventoryInReport);
    }

    public void deleteAll(){
        inventorysInReportsRepo.deleteInventorys();
    }

}

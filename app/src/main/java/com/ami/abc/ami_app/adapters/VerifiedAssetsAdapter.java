package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Asset;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerifiedAssetsAdapter extends RecyclerView.Adapter<VerifiedAssetsAdapter.MyViewHolder> {

    private List<Asset> assetList;
    private Context context;

    public VerifiedAssetsAdapter(List<Asset> assetList, Context context) {
        this.assetList = assetList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.status)
        TextView status;

        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }


    }

    @NonNull
    @Override
    public VerifiedAssetsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.verified_asset_layout, parent, false);


        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull VerifiedAssetsAdapter.MyViewHolder holder, int i) {
        Asset asset = assetList.get(i);
        holder.asset_code.setText(asset.getCode());
        holder.name.setText(asset.getName());
        if (asset.getVerified() == 1) {
            holder.status.setText("(Asset verified)");
            holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));
        }else{
            holder.status.setText("Status : Asset Not verified");
        }


    }

    @Override
    public int getItemCount() {
        return assetList.size();
    }

    public void updateList(List<Asset> newList){
        //reinitialie the list
        assetList = new ArrayList<>();
        assetList.addAll(newList);
        notifyDataSetChanged();
    }

}

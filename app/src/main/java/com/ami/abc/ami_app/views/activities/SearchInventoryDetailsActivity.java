package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.InventoryViewModel;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchInventoryDetailsActivity extends AppCompatActivity {
    @BindView(R.id.asset_id)
    TextView assetId;
    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.supplier)
    TextView supplier;

    @BindView(R.id.purchase_cost)
    TextView purchaseCost;
    @BindView(R.id.custodian)
    TextView custodian;
    @BindView(R.id.location)
    TextView location;

    InventoryViewModel inventoryViewModel;

    @BindView(R.id.container_layout)
    LinearLayout containerLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    String assetCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_inventory_details);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Details");

        Intent details = getIntent();
        assetCode = details.getStringExtra("inventoryCode");


        inventoryViewModel = ViewModelProviders.of(this).get(InventoryViewModel.class);

        String accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null && assetCode != null) {
            containerLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
           inventoryViewModel.stockTake(assetCode,"Bearer "+accessToken);
        }

        inventoryViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if(stockTakeState.getStockTake()!= null){
                handleInventoryDetails(stockTakeState.getStockTake());
            }

            if(stockTakeState.getErrorThrowable() != null){
                handleError(stockTakeState.getErrorThrowable());
            }
            if(stockTakeState.getMessage() != null){
                handleNetworkResponse(stockTakeState.getMessage());
            }
        });
    }

    private void handleInventoryDetails(StockTake stockTake) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = stockTake.getStatus();
        if(status){
            Inventory inventory = stockTake.getInventory();
            assetId.setText(assetCode);
            assetName.setText(inventory.getProductName());
            supplier.setText(inventory.getSupplier());
            custodian.setText(String.valueOf(inventory.getQuantity()));
            location.setText(String.format("%s, %s", inventory.getShelf(), inventory.getStore()));
            purchaseCost.setText(inventory.getUnitPrice());

        }
    }

    private void handleUnAuthorized(LoginError loginError) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Intent auth = new Intent(this, LoginActivity.class);
        startActivity(auth);
    }


    private void handleNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void handleError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getCause().toString());
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(this, SearchInventoryActivity.class);
        startActivity(back);
    }
}

package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.MainActivity;
import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Login;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.UniqueLoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.LoginViewModel;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_username)
    EditText username;

    @BindView(R.id.login_password)
    EditText password;

    @BindView(R.id.login_button)
    Button login;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.forgot_password)
    TextView forgotPassword;


    LoginViewModel loginViewModel;

    String name, mPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        login.setOnClickListener(v -> {
            name = username.getText().toString().trim();
            mPassword = password.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                username.requestFocus();
                Toast.makeText(this, "Enter your username", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mPassword)) {
                password.requestFocus();
                Toast.makeText(this, "Enter Your password", Toast.LENGTH_SHORT).show();
            } else {
                loginViewModel.userLogin(name, mPassword, false);
                login.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        loginViewModel.getLoginResponse().observe(this, loginState -> {
            assert loginState != null;
            if (loginState.getAllLogins() != null) {
                handleLogin(loginState.getAllLogins());
            }
            if (loginState.getErrorThrowable() != null) {
                handleError(loginState.getErrorThrowable());
            }
            if (loginState.getMessage() != null) {
                handleNetworkResponse(loginState.getMessage());
            }
            if (loginState.getLoginError() != null) {
                handleLoginError(loginState.getLoginError());
            }
            if (loginState.getUniqueLoginError() != null) {
                handleFailError(loginState.getUniqueLoginError());
            }
        });

        forgotPassword.setOnClickListener(v -> {
            startActivity(new Intent(this, ResetPasswordActivity.class));
        });


    }

    private void handleFailError(UniqueLoginError uniqueLoginError) {
        progressBar.setVisibility(View.INVISIBLE);
        login.setVisibility(View.VISIBLE);


        if (!uniqueLoginError.getStatus() && uniqueLoginError.getLoggedin()) {
            loginDialog(uniqueLoginError.getMessage());
        }else if(!uniqueLoginError.getStatus() && !uniqueLoginError.getLoggedin()){
            Toast.makeText(this, uniqueLoginError.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void handleLoginError(LoginError loginError) {
        progressBar.setVisibility(View.INVISIBLE);
        login.setVisibility(View.VISIBLE);

        Toast.makeText(this, loginError.getMessage(), Toast.LENGTH_SHORT).show();
    }


    private void handleLogin(Login allLogins) {
        progressBar.setVisibility(View.INVISIBLE);

        boolean status = allLogins.getStatus();
        if (status) {
            //save token in shared preference
            String accessToken = allLogins.getAccessToken();
            SharedPreferenceManager.getInstance(this).saveToken(accessToken);

            //send user to main activity
            loginUser();
        }
    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.INVISIBLE);
        login.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.INVISIBLE);
        login.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void loginUser() {
        Intent homeIntent = new Intent(LoginActivity.this, MainActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
        finish();

    }

    private void loginDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setPositiveButton("Continue", ((dialog, which) -> {
            loginViewModel.userLogin(name, mPassword, true);
            login.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            dialog.dismiss();
        }));
        alert.setNegativeButton("Cancel", (dialog, which) -> {
            progressBar.setVisibility(View.INVISIBLE);
            login.setVisibility(View.VISIBLE);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }


}

package com.ami.abc.ami_app.datastates;


import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryScannedStrict;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryScannedStrictState {
    private InventoryScannedStrict allInventory;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;

    public InventoryScannedStrictState(InventoryScannedStrict allInventory) {
        this.allInventory = allInventory;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public InventoryScannedStrictState(String message) {
        this.message = message;
        this.allInventory = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public InventoryScannedStrictState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allInventory = null;
        this.loginError = null;
        this.failError = null;
    }

    public InventoryScannedStrictState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allInventory = null;
        this.failError = null;
    }

    public InventoryScannedStrictState(FailError failError) {
        this.failError = failError;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.allInventory = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public InventoryScannedStrict getAllInventory() {
        return allInventory;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.CountedItemsDao;
import com.ami.abc.ami_app.models.CountedItems;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CountedItemsRepo {
    private CountedItemsDao countedItemsDao;
    private Executor executor;

    public CountedItemsRepo() {
        executor = Executors.newSingleThreadExecutor();
        countedItemsDao = AppDatabase.getDatabase(AMI.context).countedItemsDao();
    }


    public void deleteByItemId(int itemId) {
        executor.execute(() -> {
            countedItemsDao.deleteItemById(itemId);
        });
    }
    public void saveCountedItems(CountedItems countedItems) {
        executor.execute(() -> countedItemsDao.saveCountedItems(countedItems));
    }


    public LiveData<List<CountedItems>> getCountedItems() {
        return countedItemsDao.getCountedItems();
    }


    public void deleteCountedItems() {
        executor.execute(() -> countedItemsDao.deleteCountedItems());
    }

    public void delete(CountedItems countedItems){
        executor.execute(() -> countedItemsDao.delete(countedItems));
    }
}

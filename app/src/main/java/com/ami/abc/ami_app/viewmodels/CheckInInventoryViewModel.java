package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.InventoryCheckPointState;
import com.ami.abc.ami_app.datastates.InventoryCheckedInPointState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.models.InventoryCheckInReport;
import com.ami.abc.ami_app.repos.InventoryCheckInRepo;
import com.ami.abc.ami_app.repos.InventoryCheckInReportsRepo;
import com.ami.abc.ami_app.repos.InventoryCheckRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;


public class CheckInInventoryViewModel extends AndroidViewModel {

    private MediatorLiveData<InventoryCheckPointState> inventoryCheckPointStateMediatorLiveData;
    private InventoryCheckRepo inventoryCheckRepo;

    private MediatorLiveData<InventoryCheckedInPointState> inventoryCheckedInPointStateMediatorLiveData;
    private InventoryCheckInRepo inventoryCheckInRepo;

    private InventoryCheckInReportsRepo inventoryReportsRepo;

    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;

    public CheckInInventoryViewModel(Application application) {
        super(application);
        inventoryCheckPointStateMediatorLiveData = new MediatorLiveData<>();
        inventoryCheckRepo = new InventoryCheckRepo(application);

        inventoryCheckedInPointStateMediatorLiveData = new MediatorLiveData<>();
        inventoryCheckInRepo = new InventoryCheckInRepo(application);

        inventoryReportsRepo = new InventoryCheckInReportsRepo();

        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);


    }

    /* this is for getting the specific details of the asset that is scanned */
    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }

    //save individual inventory scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(InventoryCheckInReport assetScanned) {
        inventoryReportsRepo.saveInventorys(assetScanned);
    }

    public LiveData<InventoryCheckPointState> checkInventoryResponse(){
        return inventoryCheckPointStateMediatorLiveData;
    }

    public void checkInventory(String accessToken){
        LiveData<InventoryCheckPointState> inventoryCheckPointStateLiveData = inventoryCheckRepo.inventoryCheck(accessToken);
        inventoryCheckPointStateMediatorLiveData.addSource(inventoryCheckPointStateLiveData,
                inventoryCheckPointStateMediatorLiveData -> {
                    if (this.inventoryCheckPointStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryCheckPointStateMediatorLiveData.removeSource(inventoryCheckPointStateLiveData);
                    }
                    this.inventoryCheckPointStateMediatorLiveData.setValue(inventoryCheckPointStateMediatorLiveData);
                });
    }


    public LiveData<InventoryCheckedInPointState> checkInInventoryResponse(){
        return inventoryCheckedInPointStateMediatorLiveData;
    }

    public void checkInInventory(String accessToken, String productCode, int quantity){
        LiveData<InventoryCheckedInPointState> inventoryCheckedInPointStateLiveData = inventoryCheckInRepo.inventoryCheckIn(accessToken, productCode, quantity);
        inventoryCheckedInPointStateMediatorLiveData.addSource(inventoryCheckedInPointStateLiveData,
                inventoryCheckedInPointStateMediatorLiveData -> {
                    if (this.inventoryCheckedInPointStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryCheckedInPointStateMediatorLiveData.removeSource(inventoryCheckedInPointStateLiveData);
                    }
                    this.inventoryCheckedInPointStateMediatorLiveData.setValue(inventoryCheckedInPointStateMediatorLiveData);
                });
    }


}

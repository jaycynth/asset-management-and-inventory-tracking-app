package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryCheckedInPointState {
    private InventoryCheckedInPoint checkedInPoint;
    private String message;
    private Throwable errorThrowable;
    private FailError failError;
    private LoginError loginError;

    public InventoryCheckedInPointState(InventoryCheckedInPoint checkedInPoint) {
        this.checkedInPoint = checkedInPoint;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryCheckedInPointState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.checkedInPoint = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryCheckedInPointState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.checkedInPoint = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryCheckedInPointState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.checkedInPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public InventoryCheckedInPointState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.checkedInPoint = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public InventoryCheckedInPoint getCheckedInPoint() {
        return checkedInPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public FailError getFailError() {
        return failError;
    }
}

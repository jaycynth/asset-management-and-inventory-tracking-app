package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.AssetsInReportDao;
import com.ami.abc.ami_app.models.AssetInReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AssetsInReportsRepo {
    private AssetsInReportDao assetsInReportDao;
    private Executor executor;

    public AssetsInReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        assetsInReportDao = AppDatabase.getDatabase(AMI.context).assetsInReportDao();

    }


    public void saveAssets(AssetInReport assets) {
        executor.execute(() -> assetsInReportDao.saveAsset(assets));
    }


    public LiveData<List<AssetInReport>> getAssetsReports() {
        return assetsInReportDao.getAssetsReports();
    }



    public void deleteAssets() {
        executor.execute(() -> assetsInReportDao.deleteAssets());
    }
}

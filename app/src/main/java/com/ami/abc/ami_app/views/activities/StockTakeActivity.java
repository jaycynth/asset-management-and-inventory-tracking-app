package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.ResultsAdapter;

import com.ami.abc.ami_app.models.BulkStockTakeEntry;
import com.ami.abc.ami_app.models.CountedItems;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.InventoryScannedStrict;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.models.StockTakeReport;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.StockTakeViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.view.View.GONE;

public class StockTakeActivity extends AppCompatActivity {
    String assetId;
    String store, shelf;

    @BindView(R.id.asset_id)
    EditText asset_code;
    @BindView(R.id.description)

    TextInputEditText description;
    @BindView(R.id.unit_price)
    TextInputEditText unit_price;
    @BindView(R.id.new_quantity)
    TextInputEditText newQuantity;

    @BindView(R.id.done)
    Button done;

    String accessToken;
    StockTakeViewModel stockTakeViewModel;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar1;
    @BindView(R.id.container_layout)
    RelativeLayout containerLayout;


    int count;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    RelativeLayout detailsLayout;

    String barcode;
    List<Inventory> barcodes = new ArrayList<>();

    ResultsAdapter resultsAdapter;

    @BindView(R.id.list_details_layout)
    RelativeLayout listDetailsLayout;

    @BindView(R.id.scanned_list_rv)
    RecyclerView list;

    static RecyclerView.LayoutManager layoutManager;

    @BindView(R.id.barcode_layout)
    LinearLayout barcodeLayout;

    @BindView(R.id.asset_id_title)
    TextView assetIdTitle;

    List<CountedItems> sorted = new ArrayList<>();
    CountedItems countedItems;

    @BindView(R.id.done_rv)
    Button doneRv;
    @BindView(R.id.spin_kit_rv)
    ProgressBar spinKitRv;

    @BindView(R.id.clear)
    Button clear;

    List<CountedItems> inventoryCountedList = new ArrayList<>();

    StockTakeReport stockTakeReport;

    @BindView(R.id.no_data)
    TextView noData;

    String code;

    List<Inventory> toSend = new ArrayList<>();

    Boolean detailsTag;


    @BindView(R.id.btn_layout)
    LinearLayout btnLayout;

    boolean visible;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_take);
        ButterKnife.bind(this);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.stock_take_title));

        stockTakeViewModel = ViewModelProviders.of(this).get(StockTakeViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();


        Intent scanIntents = getIntent();

        store = scanIntents.getStringExtra("building");
        shelf = scanIntents.getStringExtra("room");
        barcode = scanIntents.getStringExtra("scanResult");


        initSwipe();

        Gson gson = new Gson();
        Type type = new TypeToken<List<Inventory>>() {
        }.getType();
        barcodes = gson.fromJson(barcode, type);

        asset_code.requestFocus();

        if (barcodes != null) {
            if (!barcodes.isEmpty()) {

                barcodeLayout.setVisibility(GONE);
                visible = false;

                assetIdTitle.setVisibility(GONE);

                listDetailsLayout.setVisibility(View.VISIBLE);

                Map<String, Integer> frequencyMap = new HashMap<>();

                List<String> codes = new ArrayList<>();

                for (Inventory i : barcodes) {
                    codes.add(i.getProductCode());
                }

                for (String i : codes) {
                    Integer j = frequencyMap.get(i);
                    frequencyMap.put(i, (j == null) ? 1 : j + 1);
                }

                int count = 0;

                for (Map.Entry<String, Integer> entry : frequencyMap.entrySet()) {
                    countedItems = new CountedItems();
                    countedItems.setId(count++);
                    countedItems.setProduct_code(entry.getKey());
                    countedItems.setQuantity(entry.getValue());
                    countedItems.setStore(store);
                    countedItems.setShelf(shelf);
                    stockTakeViewModel.saveCountedItems(countedItems);

                    stockTakeReport = new StockTakeReport();
                    stockTakeReport.setId(count++);
                    stockTakeReport.setBarcode(entry.getKey());

                    stockTakeReport.setCount(entry.getValue());
                    stockTakeViewModel.saveScannedInventory(stockTakeReport);


                }

                stockTakeViewModel.getListLiveData().observe(this, countedItems1 -> {
                    if (countedItems1 != null) {
                        if (!countedItems1.isEmpty()) {
                            inventoryCountedList = countedItems1;
                            initView(inventoryCountedList);
                        } else {
                            Toast.makeText(this, "stock take to upload", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } else {
                barcodeLayout.setVisibility(GONE);
                visible = false;
                assetIdTitle.setVisibility(GONE);

                listDetailsLayout.setVisibility(View.VISIBLE);

                showDialog("You did not scan any barcode successfully");
                noData.setVisibility(View.VISIBLE);
                doneRv.setVisibility(GONE);
            }


        } else {

            barcodeLayout.setVisibility(GONE);

            visible = false;
            assetIdTitle.setVisibility(GONE);

            detailsDialog("Choose action");

        }


        stockTakeViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleStockTake(stockTakeState.getStockTake());
            }
            if (stockTakeState.getErrorThrowable() != null) {
                handleError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getMessage() != null) {
                handleNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getFailError() != null) {
                handleFailError(stockTakeState.getFailError());
            }
            if (stockTakeState.getLoginError() != null) {
                handleUnAuthorized(stockTakeState.getLoginError());
            }
        });

        stockTakeViewModel.getInventoryScannedResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleInventoryScanned(stockTakeState.getStockTake());
            }
            if (stockTakeState.getErrorThrowable() != null) {
                handleError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getMessage() != null) {
                handleInventoryNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getFailError() != null) {
                handleInventoryFailError(stockTakeState.getFailError());
            }
            if (stockTakeState.getLoginError() != null) {
                handleUnAuthorized(stockTakeState.getLoginError());
            }
        });

        stockTakeViewModel.inventoryScannedStrictResponse().observe(this, inventoryScannedStrictState -> {
            if (inventoryScannedStrictState.getAllInventory() != null) {
                handleInventoryScannedStrict(inventoryScannedStrictState.getAllInventory());
            }
            if (inventoryScannedStrictState.getErrorThrowable() != null) {
                handleError(inventoryScannedStrictState.getErrorThrowable());
            }
            if (inventoryScannedStrictState.getMessage() != null) {
                handleInventoryNetworkResponse(inventoryScannedStrictState.getMessage());
            }
            if (inventoryScannedStrictState.getFailError() != null) {
                handleInventoryFailError(inventoryScannedStrictState.getFailError());
            }
            if (inventoryScannedStrictState.getLoginError() != null) {
                handleUnAuthorized(inventoryScannedStrictState.getLoginError());
            }
        });

        stockTakeViewModel.bulkStockTakeResponse().observe(this, bulkStockState -> {
            assert bulkStockState != null;
            if (bulkStockState.getBulkStockTakeEntry() != null) {
                handleBulkStockTake(bulkStockState.getBulkStockTakeEntry());
            }
            if (bulkStockState.getErrorThrowable() != null) {
                handleBulkStockTakeError(bulkStockState.getErrorThrowable());
            }
            if (bulkStockState.getMessage() != null) {
                handleBulkStockTakeNetworkResponse(bulkStockState.getMessage());
            }
            if (bulkStockState.getFailError() != null) {
                handleBulkStockTakeFailError(bulkStockState.getFailError());
            }

        });

        done.setOnClickListener(v -> {


            String barCode = asset_code.getText().toString().trim();
            String unitPriceText = unit_price.getText().toString().trim();
            String quantityText = newQuantity.getText().toString().trim();

            if (TextUtils.isEmpty(barCode)) {
                Toast.makeText(this, "Barcode field is empty", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(quantityText)) {
                Toast.makeText(this, "Quantity field is empty", Toast.LENGTH_SHORT).show();
            } else {
                containerLayout.setAlpha(0.4f);
                progressBar1.setVisibility(View.VISIBLE);


                double quantity = Double.parseDouble(quantityText);


                //save for upload
                countedItems = new CountedItems();
                countedItems.setProduct_code(barCode);
                countedItems.setQuantity((int) quantity);
                countedItems.setStore(store);
                countedItems.setShelf(shelf);
                stockTakeViewModel.saveCountedItems(countedItems);

                //save for reports
                stockTakeReport = new StockTakeReport();
                stockTakeReport.setBarcode(barCode);
                stockTakeReport.setCount((int) quantity);
                stockTakeViewModel.saveScannedInventory(stockTakeReport);


                inventoryCountedList.add(countedItems);

                done.setVisibility(View.INVISIBLE);
                progressBar1.setVisibility(View.VISIBLE);

                String assetList = serialize(inventoryCountedList, "data", "data");
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), assetList);
                stockTakeViewModel.bulkStockTake("Bearer " + accessToken, requestBody);
            }

        });

        doneRv.setOnClickListener(v -> {

            if (inventoryCountedList.isEmpty()) {

                Toast.makeText(getApplicationContext(), "go back to stock take before upload", Toast.LENGTH_SHORT).show();

            } else {

                doneRv.setVisibility(View.INVISIBLE);
                btnLayout.setVisibility(View.INVISIBLE);
                spinKitRv.setVisibility(View.VISIBLE);
                list.setAlpha(0.4f);
                String assetList = serialize(inventoryCountedList, "data", "data");
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), assetList);
                stockTakeViewModel.bulkStockTake("Bearer " + accessToken, requestBody);

            }
        });

        clear.setOnClickListener(v -> {
            asset_code.setText(" ");
            asset_code.requestFocus();
            asset_code.findFocus();
            detailsLayout.setVisibility(GONE);
            barcodeLayout.setVisibility(View.VISIBLE);
            visible= true;
            assetIdTitle.setVisibility(View.VISIBLE);

            confirm.setVisibility(View.VISIBLE);

        });
    }

    /* swipe to delete for counted items */

    private void initSwipe() {

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                final int position = viewHolder.getAdapterPosition();
                CountedItems countedItem = resultsAdapter.getCountedItemAtPosition(position);

                Toast.makeText(StockTakeActivity.this, "Deleted Item " + countedItem.getProduct_code(), Toast.LENGTH_LONG).show();

                // Delete the counted item
                stockTakeViewModel.deleteByItemId(position);


                inventoryCountedList.remove(position);
                resultsAdapter.notifyItemRemoved(position);


                stockTakeViewModel.getListLiveData().observe(StockTakeActivity.this, countedItems1 -> {
                    if (countedItems1 != null) {
                        if (!countedItems1.isEmpty()) {
                            inventoryCountedList = countedItems1;
                            initView(inventoryCountedList);
                        }
                    }
                });


            }
        });

        helper.attachToRecyclerView(list);
    }


    private String serialize(List<CountedItems> inventoryCountedList, String arrayKey, String objectKey) {
        JsonObject json = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (CountedItems countedItems : inventoryCountedList) {
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(countedItems);
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(objectKey, jsonElement);
            jsonArray.add(jsonElement);
        }
        json.add(arrayKey, jsonArray);

        return json.toString();
    }

    private void handleBulkStockTake(BulkStockTakeEntry bulkStockTakeEntry) {
        list.setAlpha(1);

        doneRv.setVisibility(View.VISIBLE);
        spinKitRv.setVisibility(GONE);

        done.setVisibility(View.VISIBLE);
        progressBar1.setVisibility(GONE);

        boolean status = bulkStockTakeEntry.getStatus();
        if (status) {


            showDialog(bulkStockTakeEntry.getMessage());
            stockTakeViewModel.deleteCountedItems();

        }
    }

    private void handleBulkStockTakeFailError(FailError failError) {
        list.setAlpha(1);

        doneRv.setVisibility(View.VISIBLE);
        spinKitRv.setVisibility(GONE);

        done.setVisibility(View.VISIBLE);
        progressBar1.setVisibility(GONE);

        showDialog(failError.getMessage());
    }

    private void handleBulkStockTakeNetworkResponse(String message) {
        list.setAlpha(1);

        doneRv.setVisibility(View.VISIBLE);
        spinKitRv.setVisibility(GONE);


        done.setVisibility(View.VISIBLE);
        progressBar1.setVisibility(GONE);

        showDialog(message);

    }


    private void handleBulkStockTakeError(Throwable errorThrowable) {
        list.setAlpha(1);

        doneRv.setVisibility(View.VISIBLE);
        spinKitRv.setVisibility(GONE);

        done.setVisibility(View.VISIBLE);
        progressBar1.setVisibility(GONE);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleInventoryScannedStrict(InventoryScannedStrict allInventory) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        boolean status = allInventory.getStatus();
        Inventory inventory = allInventory.getInventory();
        if (status) {
            if (detailsTag) {
                confirm.setVisibility(GONE);
                detailsLayout.setVisibility(View.VISIBLE);

                description.setText(inventory.getProductName());
                unit_price.setText(String.valueOf(inventory.getUnitPrice()));
                newQuantity.setText(" ");
                newQuantity.requestFocus();


                if (inventory.getDiscontinued() != null) {
                    if (inventory.getDiscontinued().equals("disposed")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setCancelable(false);
                        alert.setTitle("This inventory was disposed");
                        alert.setMessage("Do you want to continue.?");

                        alert.setNegativeButton("Exit", (dialog, which) -> {
                                    dialog.dismiss();
                                }
                        );
                        alert.setPositiveButton("Scanner", (dialog, which) -> {
                                    Intent scanAsset = new Intent(this, StockTakeActivity.class);
                                    scanAsset.putExtra("building", store);
                                    scanAsset.putExtra("room", shelf);
                                    scanAsset.putExtra("TAG", "F");
                                    startActivity(scanAsset);
                                    dialog.dismiss();
                                }
                        );

                        AlertDialog dialog = alert.create();
                        dialog.show();


                    }

                }
            } else {
                toSend.add(inventory);
                allInventory.setCounted(count++);

                Toast.makeText(this, inventory.getProductName(), Toast.LENGTH_SHORT).show();

                //empty the edit text to continue scanning for continous scanning
                asset_code.setText(" ");
                asset_code.requestFocus();
                confirm.setOnClickListener(v -> {
                    String inputtedCode = asset_code.getText().toString().trim();
                    if (TextUtils.isEmpty(inputtedCode)) {
                        Toast.makeText(this, "Please enter barcode", Toast.LENGTH_SHORT).show();
                    } else {
                        containerLayout.setAlpha(0.4f);
                        progressBar1.setVisibility(View.VISIBLE);
                        stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                    }
                });

            }
        }
    }

    private void handleInventoryScanned(StockTake stockTake) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        boolean status = stockTake.getStatus();
        if (status) {
            Inventory inventory = stockTake.getInventory();

            if (inventory.getStore().equalsIgnoreCase(store)) {
                toSend.add(inventory);
                stockTake.setCounted(count++);

                Toast.makeText(this, inventory.getProductName(), Toast.LENGTH_SHORT).show();

                //empty the edit text to continue scanning for continous scanning
                asset_code.setText(" ");
                asset_code.requestFocus();
                confirm.setOnClickListener(v -> {
                    String inputtedCode = asset_code.getText().toString().trim();
                    if (TextUtils.isEmpty(inputtedCode)) {
                        Toast.makeText(this, "Please enter barcode", Toast.LENGTH_SHORT).show();
                    } else {
                        containerLayout.setAlpha(0.4f);
                        progressBar1.setVisibility(View.VISIBLE);
                        stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                    }
                });

            } else {
                Toast.makeText(this, "Item is misplaced, not counted", Toast.LENGTH_SHORT).show();
                asset_code.setText(" ");
                asset_code.requestFocus();
            }

        }
    }

    private void handleInventoryNetworkResponse(String message) {

        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(message);
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue scanning ?");

        alert.setNegativeButton("Exit", (dialog, which) -> {
                    Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
                    startActivity(loc);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Continue", (dialog, which) -> {
            barcodeLayout.setVisibility(View.VISIBLE);
            visible= true;
            assetIdTitle.setVisibility(View.VISIBLE);

            asset_code.setText(" ");
            asset_code.requestFocus();
            confirm.setOnClickListener(v -> {

                String inputtedCode = asset_code.getText().toString();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }

            });
            dialog.dismiss();

        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void handleInventoryFailError(FailError failError) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(failError.getMessage());
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue scanning ?");

        alert.setNegativeButton("Exit", (dialog, which) -> {
                    Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
                    startActivity(loc);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Continue", (dialog, which) -> {
            barcodeLayout.setVisibility(View.VISIBLE);

            visible=true;
            assetIdTitle.setVisibility(View.VISIBLE);
            asset_code.setText(" ");
            asset_code.requestFocus();

            confirm.setOnClickListener(v -> {

                String inputtedCode = asset_code.getText().toString();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a code", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }

            });
            dialog.dismiss();

        });
        AlertDialog dialog = alert.create();
        dialog.show();


    }


    /* Getting details of the inventory*/
    private void handleStockTake(StockTake stockTake) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        boolean status = stockTake.getStatus();
        if (status) {


            Inventory inventory = stockTake.getInventory();

            if (inventory.getStore().equalsIgnoreCase(store)) {
                confirm.setVisibility(GONE);
                detailsLayout.setVisibility(View.VISIBLE);

                description.setText(inventory.getProductName());
                unit_price.setText(String.valueOf(inventory.getUnitPrice()));
                newQuantity.setText(" ");
                newQuantity.requestFocus();


                if (inventory.getDiscontinued() != null) {
                    if (inventory.getDiscontinued().equals("disposed")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setCancelable(false);
                        alert.setTitle("This inventory was disposed");
                        alert.setMessage("Do you want to continue.?");

                        alert.setNegativeButton("Exit", (dialog, which) -> {
                                    dialog.dismiss();
                                }
                        );
                        alert.setPositiveButton("Scanner", (dialog, which) -> {
                                    Intent scanAsset = new Intent(this, StockTakeActivity.class);
                                    scanAsset.putExtra("building", store);
                                    scanAsset.putExtra("room", shelf);
                                    scanAsset.putExtra("TAG", "F");
                                    startActivity(scanAsset);
                                    dialog.dismiss();
                                }
                        );

                        AlertDialog dialog = alert.create();
                        dialog.show();


                    }

                }
            } else {
                Toast.makeText(this, "Item is misplaced,not counted", Toast.LENGTH_SHORT).show();
                asset_code.setText(" ");
                asset_code.requestFocus();

            }

        }
    }

    private void handleError(Throwable errorThrowable) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(message);
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue scanning ?");

        alert.setNegativeButton("Exit", (dialog, which) -> {
                    Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
                    startActivity(loc);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Continue", (dialog, which) -> {
            barcodeLayout.setVisibility(View.VISIBLE);
            visible = true;
            assetIdTitle.setVisibility(View.VISIBLE);
            asset_code.setText(" ");
            asset_code.requestFocus();

            confirm.setOnClickListener(v -> {
                String inputtedCode = asset_code.getText().toString().trim();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    detailsTag = true;
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }

            });
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void handleFailError(FailError failError) {
        progressBar1.setVisibility(View.INVISIBLE);
        containerLayout.setAlpha(1);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(failError.getMessage());
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue scanning ?");

        alert.setNegativeButton("Exit", (dialog, which) -> {
                    Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
                    startActivity(loc);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Continue", (dialog, which) -> {
            barcodeLayout.setVisibility(View.VISIBLE);
            visible = true;
            assetIdTitle.setVisibility(View.VISIBLE);
            asset_code.setText(" ");
            asset_code.requestFocus();

            confirm.setOnClickListener(v -> {
                String inputtedCode = asset_code.getText().toString().trim();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    detailsTag = true;
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }

            });
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void handleUnAuthorized(LoginError loginError) {
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }

    private void showDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setNeutralButton("Exit", (dialog, which) -> {
                    stockTakeViewModel.deleteCountedItems();
                    Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
                    startActivity(loc);
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, StockTakeActivity.class);
                    scanAsset.putExtra("building", store);
                    scanAsset.putExtra("room", shelf);
                    scanAsset.putExtra("TAG", "F");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", store);
            scanAsset.putExtra("room", shelf);
            scanAsset.putExtra("TAG", "F");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void detailsDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(message);
        alert.setCancelable(false);

        alert.setNegativeButton("Batch", (dialog, which) -> {
            detailsTag = true;


            asset_code.requestFocus();
            asset_code.findFocus();
            barcodeLayout.setVisibility(View.VISIBLE);
            visible = true;
            assetIdTitle.setVisibility(View.VISIBLE);

            confirm.setOnClickListener(v -> {

                String inputtedCode = asset_code.getText().toString();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }

            });
            dialog.dismiss();
        });

        alert.setNeutralButton("Exit", (dialog, which) -> {
                    dialog.dismiss();
                }
        );

        alert.setPositiveButton("Continous", (dialog, which) -> {
            detailsTag = false;

            asset_code.requestFocus();
            asset_code.findFocus();
            barcodeLayout.setVisibility(View.VISIBLE);
            visible = true;
            assetIdTitle.setVisibility(View.VISIBLE);

            confirm.setOnClickListener(v -> {
                String inputtedCode = asset_code.getText().toString();
                if (TextUtils.isEmpty(inputtedCode)) {
                    Toast.makeText(this, "Please enter a barcode", Toast.LENGTH_SHORT).show();
                } else {
                    containerLayout.setAlpha(0.4f);
                    progressBar1.setVisibility(View.VISIBLE);
                    stockTakeViewModel.inventoryScannedStrict(inputtedCode, store, "Bearer " + accessToken);
                }
            });
            dialog.dismiss();

        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void initView(List<CountedItems> sorted) {


        resultsAdapter = new ResultsAdapter(sorted, this);
        layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setItemAnimator(new DefaultItemAnimator());
        list.setAdapter(resultsAdapter);
        list.setNestedScrollingEnabled(false);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.stock_take_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }


        if (id == R.id.goToDetails) {

                goToDetails();

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {


        if(!visible) {


            barcodeLayout.setVisibility(View.VISIBLE);
            visible = true;
            assetIdTitle.setVisibility(View.VISIBLE);

            listDetailsLayout.setVisibility(GONE);
        }else{
            Intent loc = new Intent(StockTakeActivity.this, InventoryOptions.class);
            startActivity(loc);
        }

    }


    private void goToDetails() {

        barcodeLayout.setVisibility(GONE);
        visible = false;
        assetIdTitle.setVisibility(GONE);

        listDetailsLayout.setVisibility(View.VISIBLE);

        Map<String, Integer> frequencyMap = new HashMap<>();

        List<String> codes = new ArrayList<>();

        for (Inventory i : toSend) {
            codes.add(i.getProductCode());
        }

        for (String i : codes) {
            Integer j = frequencyMap.get(i);
            frequencyMap.put(i, (j == null) ? 1 : j + 1);
        }

        int count = 0;

        for (Map.Entry<String, Integer> entry : frequencyMap.entrySet()) {
            countedItems = new CountedItems();
            countedItems.setId(count++);
            countedItems.setProduct_code(entry.getKey());
            countedItems.setQuantity(entry.getValue());
            countedItems.setStore(store);
            countedItems.setShelf(shelf);
            stockTakeViewModel.saveCountedItems(countedItems);

            stockTakeReport = new StockTakeReport();
            stockTakeReport.setId(count++);
            stockTakeReport.setBarcode(entry.getKey());

            stockTakeReport.setCount(entry.getValue());
            stockTakeViewModel.saveScannedInventory(stockTakeReport);


        }

        stockTakeViewModel.getListLiveData().observe(this, countedItems1 -> {
            if (countedItems1 != null) {
                if (!countedItems1.isEmpty()) {
                    inventoryCountedList = countedItems1;
                    initView(inventoryCountedList);
                }
            }
        });
    }
}

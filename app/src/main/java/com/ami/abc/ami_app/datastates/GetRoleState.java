package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;

public class GetRoleState {

    private GetRole getRole;
    private String message;
    private Throwable errorThrowable;
    private FailError failError;
    private LoginError loginError;
    private Error error;

    public GetRoleState(GetRole getRole) {
        this.getRole = getRole;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
        this.error = null;
    }

    public GetRoleState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
        this.getRole = null;
        this.error = null;
    }

    public GetRoleState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.failError = null;
        this.loginError = null;
        this.getRole = null;
        this.message = null;
        this.error = null;
    }

    public GetRoleState(FailError failError) {
        this.failError = failError;
        this.loginError = null;
        this.getRole = null;
        this.message = null;
        this.errorThrowable = null;
        this.error = null;
    }

    public GetRoleState(LoginError loginError) {
        this.loginError = loginError;
        this.getRole = null;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.error = null;
    }

    public GetRoleState(Error error) {
        this.error = error;
        this.loginError = null;
        this.getRole = null;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public GetRole getGetRole() {
        return getRole;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.CheckOutInventoryState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryCheckedOutPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryCheckOutRepo {

    private ApiClient mApiClient;

    //constructor
    public InventoryCheckOutRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<CheckOutInventoryState> checkOutInventory(String accessToken, String productCode, String custodian, int quantity) {

        MutableLiveData<CheckOutInventoryState> checkOutInventoryStateMutableLiveData = new MutableLiveData<>();
        Call<InventoryCheckedOutPoint> call = mApiClient.amiService().checkOutInventory(accessToken, productCode, custodian, quantity);
        call.enqueue(new Callback<InventoryCheckedOutPoint>() {
            @Override
            public void onResponse(Call<InventoryCheckedOutPoint> call, Response<InventoryCheckedOutPoint> response) {
                if (response.code() == 200) {
                    checkOutInventoryStateMutableLiveData.setValue(new CheckOutInventoryState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    checkOutInventoryStateMutableLiveData.setValue(new CheckOutInventoryState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    checkOutInventoryStateMutableLiveData.setValue(new CheckOutInventoryState(failError));
                }
            }

            @Override
            public void onFailure(Call<InventoryCheckedOutPoint> call, Throwable t) {
                checkOutInventoryStateMutableLiveData.setValue(new CheckOutInventoryState(t));
            }
        });

        return checkOutInventoryStateMutableLiveData;

    }
}

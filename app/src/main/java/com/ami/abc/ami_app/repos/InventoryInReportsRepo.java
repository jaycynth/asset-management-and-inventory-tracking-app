package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.InventoryInReportDao;
import com.ami.abc.ami_app.models.InventoryInReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class InventoryInReportsRepo {
    private InventoryInReportDao inventoryInReportDao;
    private Executor executor;

    public InventoryInReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        inventoryInReportDao = AppDatabase.getDatabase(AMI.context).inventoryInReportDao();

    }


    public void saveInventorys(InventoryInReport inventorys) {
        executor.execute(() -> inventoryInReportDao.saveInventory(inventorys));
    }


    public LiveData<List<InventoryInReport>> getInventorysReports() {
        return inventoryInReportDao.getInventorysReports();
    }



    public void deleteInventorys() {
        executor.execute(() -> inventoryInReportDao.deleteInventorys());
    }
}

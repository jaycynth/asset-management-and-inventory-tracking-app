package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.VerifyAssetState;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.VerifyAsset;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyAssetRepo {
    private ApiClient mApiClient;

    public VerifyAssetRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<VerifyAssetState> verifyAssets(String accessToken, RequestBody requestBody){
        MutableLiveData<VerifyAssetState> verifyAssetStateMutableLiveData = new MutableLiveData<>();
        Call<VerifyAsset> call = mApiClient.amiService().verifyAssets(accessToken, requestBody);
        call.enqueue(new Callback<VerifyAsset>() {
            @Override
            public void onResponse(Call<VerifyAsset> call, Response<VerifyAsset> response) {
                if (response.code() == 200 ){
                    verifyAssetStateMutableLiveData.setValue(new VerifyAssetState(response.body()));

                }else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    verifyAssetStateMutableLiveData.setValue(new VerifyAssetState(loginError));
                }else {
                    verifyAssetStateMutableLiveData.setValue(new VerifyAssetState(response.message()));
                }
            }
            @Override
            public void onFailure(Call<VerifyAsset> call, Throwable t) {
                verifyAssetStateMutableLiveData.setValue(new VerifyAssetState(t));
            }
        });

        return verifyAssetStateMutableLiveData;
    }
}

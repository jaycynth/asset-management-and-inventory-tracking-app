package com.ami.abc.ami_app.database.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.AssetInReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssetsInReportDao {

    @Insert(onConflict = REPLACE)
    void saveAsset(AssetInReport assetsReports);
    //observe the list in the room database

    @Query("SELECT * FROM  AssetInReport")
    LiveData<List<AssetInReport>> getAssetsReports();

    @Query("SELECT * FROM AssetInReport")
    List<AssetInReport> getAll();

    @Delete
    void delete(AssetInReport assetsReports);

    //delete all the assets in the room database upon sending information to the assets verified table
    @Query("DELETE FROM AssetInReport")
    void deleteAssets();



}

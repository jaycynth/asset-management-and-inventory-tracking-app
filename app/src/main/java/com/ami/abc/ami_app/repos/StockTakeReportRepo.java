package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.StockTakeReportDao;
import com.ami.abc.ami_app.models.StockTakeReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class StockTakeReportRepo {
    private StockTakeReportDao stockTakeReportDao;
    private Executor executor;

    public StockTakeReportRepo() {
        executor = Executors.newSingleThreadExecutor();
        stockTakeReportDao = AppDatabase.getDatabase(AMI.context).stockTakeReportDao();

    }


    public void saveInventorys(StockTakeReport inventorys) {
        executor.execute(() -> stockTakeReportDao.saveInventory(inventorys));
    }


    public LiveData<List<StockTakeReport>> getInventorysReports() {
        return stockTakeReportDao.getInventorysReports();
    }



    public void deleteInventorys() {
        executor.execute(() -> stockTakeReportDao.deleteInventorys());
    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.LoginError;

public class CustodianState {
    private AllCustodians allCustodians;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;


    public CustodianState(AllCustodians allCustodians) {
        this.allCustodians = allCustodians;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
    }

    public CustodianState(String message) {
        this.message = message;
        this.allCustodians = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public CustodianState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allCustodians = null;
        this.loginError = null;
    }

    public CustodianState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allCustodians = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllCustodians getAllCustodians() {
        return allCustodians;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

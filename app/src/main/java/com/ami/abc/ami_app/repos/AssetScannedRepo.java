package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.AssetsDao;
import com.ami.abc.ami_app.models.AssetScanned;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AssetScannedRepo {
    private AssetsDao assetsDao;
    private Executor executor;

    public AssetScannedRepo() {
        executor= Executors.newSingleThreadExecutor();
        assetsDao= AppDatabase.getDatabase(AMI.context).assetsDao();

    }


    public void saveAssetsScanned(AssetScanned assets){
        executor.execute(()-> assetsDao.saveAssetScanned(assets));
    }

    public void updateAssetsScanned(AssetScanned assets) {
        executor.execute(() -> assetsDao.updateAssetScanned(assets));
    }

    public LiveData<List<AssetScanned>> getAssetsScanned() {
        return assetsDao.getAssetsScanned();
    }

    public void deleteAssets() {
        executor.execute(() -> assetsDao.deleteAssets());
    }

}

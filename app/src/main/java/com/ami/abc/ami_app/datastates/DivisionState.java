package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.models.LoginError;

public class DivisionState {
    private GetDivisions allDivisions;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private Error error;

    public DivisionState(GetDivisions allDivisions) {
        this.allDivisions = allDivisions;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;
    }

    public DivisionState(String message) {
        this.message = message;
        this.allDivisions = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;
    }

    public DivisionState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.allDivisions = null;
        this.message = null;
        this.loginError = null;
        this.error = null;
    }

    public DivisionState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allDivisions = null;
        this.error = null;
    }

    public DivisionState(Error error) {
        this.error = error;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.allDivisions = null;
    }

    public Error getError() {
        return error;
    }

    public GetDivisions getAllDivisions() {
        return allDivisions;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public GetDivisions getGetDivisions() {
        return allDivisions;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TwoFactorAuth {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("2fa")
    @Expose
    private String _2fa;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String get2fa() {
        return _2fa;
    }

    public void set2fa(String _2fa) {
        this._2fa = _2fa;
    }
}



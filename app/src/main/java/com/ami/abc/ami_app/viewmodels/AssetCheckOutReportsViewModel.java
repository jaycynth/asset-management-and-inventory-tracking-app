package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.AssetCheckOutReport;
import com.ami.abc.ami_app.repos.AssetCheckOutReportsRepo;

import java.util.List;

public class AssetCheckOutReportsViewModel extends AndroidViewModel {
    private AssetCheckOutReportsRepo assetCheckOutReportsRepo;

    private LiveData<List<AssetCheckOutReport>> listLiveData;



    public AssetCheckOutReportsViewModel(Application application) {
        super(application);

        assetCheckOutReportsRepo = new AssetCheckOutReportsRepo();

        listLiveData = assetCheckOutReportsRepo.getAssetsReports();
    }

    public LiveData<List<AssetCheckOutReport>> getListLiveData() {
        return listLiveData;
    }



    //save individual assets scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(AssetCheckOutReport assetInReport) {
        assetCheckOutReportsRepo.saveAssets(assetInReport);
    }

}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Logout;

public class LogoutState {
    private Logout logout;
    private Throwable errorThrowable;
    private String message;
    private LoginError loginError;
    private Error error;


    public LogoutState(Logout logout) {
        this.logout = logout;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
        this.error = null;
    }

    public LogoutState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.logout = null;
        this.message = null;
        this.loginError = null;
        this.error = null;
    }

    public LogoutState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.logout = null;
        this.loginError = null;
        this.error = null;
    }

    public LogoutState(LoginError loginError){
        this.message = null;
        this.errorThrowable = null;
        this.logout = null;
        this.loginError = loginError;
        this.error = null;
    }

    public LogoutState(Error error) {
        this.error = error;
        this.message = null;
        this.errorThrowable = null;
        this.logout = null;
        this.loginError = null;
    }


    public Error getError() {
        return error;
    }

    public Logout getLogout() {
        return logout;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public String getMessage() {
        return message;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

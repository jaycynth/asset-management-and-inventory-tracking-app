package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.ResetPassword;

public class ResetPasswordState {
    private ResetPassword resetPassword;
    private String message;
    private Throwable errorThrowable;

    public ResetPasswordState(ResetPassword resetPassword) {
        this.resetPassword = resetPassword;
        this.message = null;
        this.errorThrowable = null;
    }

    public ResetPasswordState(String message) {
        this.message = message;
        this.resetPassword = null;
        this.errorThrowable = null;
    }

    public ResetPasswordState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.resetPassword = null;

    }

    public ResetPassword getResetPassword() {
        return resetPassword;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

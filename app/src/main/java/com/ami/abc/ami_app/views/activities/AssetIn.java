package com.ami.abc.ami_app.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import com.ami.abc.ami_app.models.CurrentAccountType;
import com.ami.abc.ami_app.models.Errors;
import com.ami.abc.ami_app.models.GetSubCategory;
import com.ami.abc.ami_app.models.GetUnproccessableEntityErrors;
import com.ami.abc.ami_app.models.SubCategory;
import com.ami.abc.ami_app.utils.AssetStatusData;
import com.ami.abc.ami_app.views.fragments.BreakDownCostDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AddAsset;
import com.ami.abc.ami_app.models.AddAssetImage;
import com.ami.abc.ami_app.models.AddedAsset;
import com.ami.abc.ami_app.models.AllCategories;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.AssetCategory;
import com.ami.abc.ami_app.models.AssetInReport;
import com.ami.abc.ami_app.models.Custodian;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Supplier;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.NewAssetViewModel;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ami.abc.ami_app.utils.Constants.REQUEST_PICTURE_CAPTURE;

public class AssetIn extends AppCompatActivity {

    @BindView(R.id.asset_code_layout)
    TextInputLayout asset_code_layout;
    @BindView(R.id.asset_id)
    TextInputEditText asset_code;

    @BindView(R.id.asset_name)
    TextInputEditText asset_name;
    @BindView(R.id.asset_name_layout)
    TextInputLayout asset_name_layout;

    @BindView(R.id.status)
    AutoCompleteTextView nStatus;
    @BindView(R.id.status_layout)
    TextInputLayout status_layout;
    @BindView(R.id.status_drop_down)
    ImageView status_drop_down;

    @BindView(R.id.date)
    TextInputEditText mDate;
    @BindView(R.id.date_layout)
    TextInputLayout mDateLayout;

    @BindView(R.id.serial_number)
    TextInputEditText serial_number;
    @BindView(R.id.serial_number_layout)
    TextInputLayout serial_number_layout;

    @BindView(R.id.model)
    TextInputEditText nModel;
    @BindView(R.id.model_layout)
    TextInputLayout model_layout;

    @BindView(R.id.at_cost)
    TextInputEditText at_cost;
    @BindView(R.id.at_cost_layout)
    TextInputLayout at_cost_layout;

    @BindView(R.id.end_estimated_cost)
    TextInputEditText end_estimated_cost;
    @BindView(R.id.end_estimated_cost_layout)
    TextInputLayout end_estimated_cost_layout;

    List<AssetCategory> categoryList = new ArrayList<>();
    private List<String> categoryName = new ArrayList<>();
    @BindView(R.id.asset_category)
    AutoCompleteTextView category;
    @BindView(R.id.category_layout)
    TextInputLayout category_layout;
    @BindView(R.id.category_drop_down)
    ImageView category_drop_down;

    List<SubCategory> subCategoryList = new ArrayList<>();
    List<String> subCategoryName = new ArrayList<>();
    @BindView(R.id.sub_asset_category)
    AutoCompleteTextView subCategory;
    @BindView(R.id.sub_category_layout)
    TextInputLayout sub_category_layout;
    @BindView(R.id.sub_category_drop_down)
    ImageView sub_category_drop_down;

    private List<String> custodianName = new ArrayList<>();
    @BindView(R.id.custodian)
    AutoCompleteTextView custodian;
    @BindView(R.id.custodian_layout)
    TextInputLayout custodian_layout;
    @BindView(R.id.custodian_drop_down)
    ImageView custodian_drop_down;


    private List<String> supplierName = new ArrayList<>();
    @BindView(R.id.supplier)
    AutoCompleteTextView nSupplier;
    @BindView(R.id.supplier_layout)
    TextInputLayout supplier_layout;
    @BindView(R.id.supplier_drop_down)
    ImageView supplier_drop_down;


    @BindView(R.id.add)
    Button update;

    String building, room, depart, assetId, division;


    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    NewAssetViewModel newAssetViewModel;
    String accessToken;

    @BindView(R.id.container_layout)
    LinearLayout containerLayout;
    @BindView(R.id.take_photo_spin_kit)
    ProgressBar takePhotoSpin;


    String pictureFilePath;

    private static final int PERMISSION_REQUEST_CODE = 67;

    File imgFile;
    int id;

    boolean webUpload = false;
    int categoryId, subCategoryId;

    //fields for the additional breakdown cost
    String freightCost, importDuties, nonRefundableTax, setUpCost, dismantlingCost, breakdownPurchaseCost;
    Boolean breakdown = false;

    //track if asset is already uploaded per transaction or not
    Boolean assetUpdateStatus = false;

    @BindView(R.id.break_down_switch)
    Switch breakDownSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assets_in);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_asset));


        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        division = scanIntents.getStringExtra("division");
        assetId = scanIntents.getStringExtra("scanResult");


        asset_code.setText(assetId);
        asset_code.requestFocus();


        final Calendar myCalendar = Calendar.getInstance();


        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            mDate.setText(sdf.format(myCalendar.getTime()));
        };

        mDate.setOnClickListener(v -> new DatePickerDialog(AssetIn.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());


        newAssetViewModel = ViewModelProviders.of(this).get(NewAssetViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null) {
            newAssetViewModel.getCurrentAccountType("Bearer " + accessToken);
            newAssetViewModel.getAllSupliers("Bearer " + accessToken);
            newAssetViewModel.getAllCategories("Bearer " + accessToken);
            newAssetViewModel.getAllCustodians("Bearer " + accessToken);
        }


        newAssetViewModel.getPostNewAssetResponse().observe(this, addAssetState -> {
            assert addAssetState != null;
            if (addAssetState.getAddAsset() != null) {
                handlePost(addAssetState.getAddAsset());
            }
            if (addAssetState.getMessage() != null) {
                handleNetworkResponse(addAssetState.getMessage());
            }
            if (addAssetState.getErrorThrowable() != null) {
                handleError(addAssetState.getErrorThrowable());
            }
            if (addAssetState.getLoginError() != null) {
                handleUnAuthorized(addAssetState.getLoginError());
            }

            if (addAssetState.getFailError() != null) {
                handleFailError(addAssetState.getFailError());
            }

            if (addAssetState.getError() != null) {
                handleUnprocessedEntityError(addAssetState.getError());
            }
        });

        newAssetViewModel.getAllSuppliersResponse().observe(this, supplierState -> {
            assert supplierState != null;
            if (supplierState.getAllSuppliers() != null) {
                handleSuppliers(supplierState.getAllSuppliers());
            }
            if (supplierState.getMessage() != null) {
                handleNetworkResponse(supplierState.getMessage());
            }
            if (supplierState.getErrorThrowable() != null) {
                handleError(supplierState.getErrorThrowable());
            }
            if (supplierState.getLoginError() != null) {
                handleUnAuthorized(supplierState.getLoginError());
            }
        });


        newAssetViewModel.getAllCategoryResponse().observe(this, categoryState -> {
            assert categoryState != null;
            if (categoryState.getAllCategories() != null) {
                handleCategories(categoryState.getAllCategories());
            }
            if (categoryState.getMessage() != null) {
                handleNetworkResponse(categoryState.getMessage());
            }
            if (categoryState.getErrorThrowable() != null) {
                handleError(categoryState.getErrorThrowable());
            }
            if (categoryState.getLoginError() != null) {
                handleUnAuthorized(categoryState.getLoginError());
            }
        });


        newAssetViewModel.subCategoriesResponse().observe(this, subCategoriesState -> {
            if (subCategoriesState.getGetSubCategory() != null) {
                handleSubCategories(subCategoriesState.getGetSubCategory());
            }
            if (subCategoriesState.getMessage() != null) {
                handleSubCategoriesNetworkResponse(subCategoriesState.getMessage());
            }
            if (subCategoriesState.getThrowable() != null) {
                handleSubCategoriesError(subCategoriesState.getThrowable());
            }
        });


        newAssetViewModel.getAllCustodiansResponse().observe(this, custodianState -> {
            assert custodianState != null;
            if (custodianState.getAllCustodians() != null) {
                handleCustodians(custodianState.getAllCustodians());
            }
            if (custodianState.getMessage() != null) {
                handleNetworkResponse(custodianState.getMessage());
            }
            if (custodianState.getErrorThrowable() != null) {
                handleError(custodianState.getErrorThrowable());
            }
            if (custodianState.getLoginError() != null) {
                handleUnAuthorized(custodianState.getLoginError());
            }
        });


        newAssetViewModel.addAssetImage().observe(this, addAssetImageState -> {
            assert addAssetImageState != null;
            if (addAssetImageState.getAddAssetImage() != null) {
                handleAddAssetImage(addAssetImageState.getAddAssetImage());
            }
            if (addAssetImageState.getMessage() != null) {
                handleAssetImageNetworkResponse(addAssetImageState.getMessage());
            }
            if (addAssetImageState.getErrorThrowable() != null) {
                handleAssetImageError(addAssetImageState.getErrorThrowable());
            }

        });

        newAssetViewModel.getAccountCurrentType().observe(this, currentAccountTypeState -> {
            if (currentAccountTypeState.getCurrentAccountType() != null) {
                getCurrentAccountType(currentAccountTypeState.getCurrentAccountType());
            }
            if (currentAccountTypeState.getMessage() != null) {
                currentAccountTypeMessage(currentAccountTypeState.getMessage());
            }
            if (currentAccountTypeState.getThrowable() != null) {
                currentAccountTypeThrowable(currentAccountTypeState.getThrowable());
            }
            if (currentAccountTypeState.getLoginError() != null) {
                currentAccountTypeLoginError(currentAccountTypeState.getLoginError());
            }
        });


        //purchase cost to trigger additional fields using a pop up
        purchaseCostBreakdown();


        //set up drop down for status;
        List<String> statusName = AssetStatusData.getAssetStatuses();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, statusName);
        nStatus.setAdapter(adapter1);

        status_drop_down.setOnClickListener(v -> nStatus.showDropDown());

        nStatus.setOnClickListener(v -> nStatus.showDropDown());


        //set up custodian drop down
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, custodianName);
        custodian.setAdapter(adapter2);

        custodian_drop_down.setOnClickListener(v -> custodian.showDropDown());
        custodian.setOnClickListener(v -> custodian.showDropDown());


        //set up category drop down
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, categoryName);
        category.setAdapter(adapter3);

        category_drop_down.setOnClickListener(v -> category.showDropDown());
        category.setOnClickListener(v -> category.showDropDown());

        category.setOnItemClickListener((parent, view, position, id) -> {
            subCategoryName.clear();
            String categoryName = (String) parent.getItemAtPosition(position);

            if (categoryList.get(position).getType().equalsIgnoreCase(categoryName)) {
                categoryId = categoryList.get(position).getId();
                if (categoryId == 0) {


                    subCategoryName.add("None");

                } else {

                    containerLayout.setAlpha(0.3f);
                    Toast.makeText(this, "Loading sub categories........", Toast.LENGTH_SHORT).show();
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    if (!subCategoryName.isEmpty()) {
                        subCategoryName.clear();
                    }

                    newAssetViewModel.getAllSubCategories("Bearer " + accessToken);
                }

            }

        });


        //set up sub category drop down linked to category drop down
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, subCategoryName);
        subCategory.setAdapter(adapter5);

        sub_category_drop_down.setOnClickListener(v -> subCategory.showDropDown());
        subCategory.setOnClickListener(v -> subCategory.showDropDown());

        subCategory.setOnItemClickListener((parent, view, position, id) -> subCategoryId = subCategoryList.get(position).getId());


        //set up supplier drop down
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, supplierName);
        nSupplier.setAdapter(adapter4);

        supplier_drop_down.setOnClickListener(v -> nSupplier.showDropDown());
        nSupplier.setOnClickListener(v -> nSupplier.showDropDown());


        update.setOnClickListener(v -> {
            //update status to true showing that the set of values has already gone
            assetUpdateStatus = false;

            String code = asset_code.getText().toString().trim();
            String name = asset_name.getText().toString().trim();
            String status = nStatus.getText().toString().trim();
            String date_of_purchase = mDate.getText().toString().trim();
            String serial = serial_number.getText().toString().trim();
            String model = nModel.getText().toString().trim();
            String at_cost = this.at_cost.getText().toString().trim();
            String est_end_of_life_cost = end_estimated_cost.getText().toString().trim();
            String asset_category = category.getText().toString().trim();
            String cust = custodian.getText().toString().trim();
            String supplier = nSupplier.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                asset_name_layout.setError(getResources().getString(R.string.asset_name_error));
                asset_name.requestFocus();
                asset_name.findFocus();
            } else if (TextUtils.isEmpty(date_of_purchase)) {
                mDateLayout.setError(getResources().getString(R.string.date_error));
                mDate.requestFocus();
                mDate.findFocus();
            } else if (TextUtils.isEmpty(at_cost)) {
                at_cost_layout.setError(getResources().getString(R.string.purchase_cost_error));
                this.at_cost.requestFocus();
                this.at_cost.findFocus();
            } else if (TextUtils.isEmpty(est_end_of_life_cost)) {
                end_estimated_cost_layout.setError(getResources().getString(R.string.end_cost_error));
                end_estimated_cost.requestFocus();
                end_estimated_cost.findFocus();
            } else if (TextUtils.isEmpty(asset_category)) {
                category_layout.setError(getResources().getString(R.string.category_error));
                category.requestFocus();
                category.findFocus();
            } else {
                update.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                String accessToken = SharedPreferenceManager.getInstance(this).getToken();
                newAssetViewModel.postNewAsset("Bearer " + accessToken, code, name, status, serial, model,
                        building, room, division, supplier, date_of_purchase, at_cost,
                        est_end_of_life_cost, depart, cust, asset_category, subCategoryId,
                        freightCost, importDuties, nonRefundableTax, setUpCost, dismantlingCost, breakdownPurchaseCost, breakdown);
            }
        });


    }


    private void purchaseCostBreakdown() {
        //listen for changes on the switch and click on the purchase cost field

        breakDownSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //hide keyboard and show breakdown dialog
                toggleKeyboard(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();

            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });

        at_cost.setOnClickListener(v -> {
            if (breakDownSwitch.isChecked()) {
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();

            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);

            }
        });
    }

    private void toggleKeyboard(int showImplicit, int i) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(showImplicit, i);
        }
    }

    /*opens a dialog that shows additional fields of breakdown of purchase cost*/
    private void breakDownCostDialog() {
        BreakDownCostDialogFragment breakDownCostDialogFragment = new BreakDownCostDialogFragment();
        breakDownCostDialogFragment.setListener((fCost, iDuties, nRefundTax, sCost, dCost, bPurchaseCost) -> {

            assetUpdateStatus = true;
            int totalPurchase = Integer.parseInt(fCost) + Integer.parseInt(iDuties) +
                    Integer.parseInt(nRefundTax) + Integer.parseInt(sCost) + Integer.parseInt(dCost) +
                    Integer.parseInt(bPurchaseCost);

            at_cost.setText(String.valueOf(totalPurchase));

            //enable breakdown when values are returned
            breakdown = true;

            freightCost = fCost;
            importDuties = iDuties;
            nonRefundableTax = nRefundTax;
            setUpCost = sCost;
            dismantlingCost = dCost;
            breakdownPurchaseCost = bPurchaseCost;
        });
        Bundle bundle = new Bundle();
        bundle.putBoolean("assetPostStatus", assetUpdateStatus);
        bundle.putString("freightCost", freightCost);
        bundle.putString("importDuties", importDuties);
        bundle.putString("nonRefundableTax", nonRefundableTax);
        bundle.putString("setupCost", setUpCost);
        bundle.putString("dismantlingCost", dismantlingCost);
        bundle.putString("breakdownPurchaseCost", breakdownPurchaseCost);
        breakDownCostDialogFragment.setArguments(bundle);
        breakDownCostDialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    /* handle account type for barcode generation if automatic */

    private void getCurrentAccountType(CurrentAccountType currentAccountType) {
        int status = currentAccountType.getAutomatic();

        if (status == 1) {
            asset_code.setFocusable(false);
            asset_code.setClickable(false);
            asset_code.setText(getString(R.string.automatic_text));
            asset_code.setTextColor(getResources().getColor(R.color.colorAccent));
        }

    }

    private void currentAccountTypeMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void currentAccountTypeThrowable(Throwable throwable) {
        throwableError(throwable, "You are currently Offline", "An Error Occured", "CONVERSION ERRORS");
    }


    private void currentAccountTypeLoginError(LoginError loginError) {
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Log.d("Unauthorized Error", loginError.getMessage());
    }


    private void throwableError(Throwable throwable, String s, String s2, String s3) {
        if (throwable instanceof IOException) {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, s2, Toast.LENGTH_SHORT).show();
            Log.d(s3, Objects.requireNonNull(throwable.getMessage()));
        }
    }


    /* posting new asset image for the first time */
    private void handleAssetImageError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);

        if (errorThrowable instanceof IOException) {
            // Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();

            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setTitle(getString(R.string.network_failure));
            alert1.setCancelable(false);
            alert1.setMessage("Do you want to retry..?");

            alert1.setPositiveButton("Yes", (dialog1, which1) -> {

                containerLayout.setAlpha(0.4f);
                takePhotoSpin.setVisibility(View.VISIBLE);
                Toast.makeText(this, "Uploading photo...", Toast.LENGTH_SHORT).show();
                newAssetViewModel.postAddAssetImage("Bearer " + accessToken, imgFile, id);

                dialog1.dismiss();

            });


            alert1.setNegativeButton("No", (dialog1, which1) -> {
                continueScanningDialog("Continue Scanning ?");
                dialog1.dismiss();
            });
            AlertDialog dialog = alert1.create();
            dialog.show();
        } else {
            //Handles conversion errors
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setTitle(getString(R.string.error_occured));
            alert1.setCancelable(false);
            alert1.setMessage("Do you want to retry..?");

            alert1.setPositiveButton("Yes", (dialog1, which1) -> {

                        containerLayout.setAlpha(0.4f);
                        takePhotoSpin.setVisibility(View.VISIBLE);
                        newAssetViewModel.postAddAssetImage("Bearer " + accessToken, imgFile, id);

                        dialog1.dismiss();

                    }
            );


            alert1.setNegativeButton("No", (dialog1, which1) -> {
                continueScanningDialog("Continue Scanning ?");
                dialog1.dismiss();
            });
            AlertDialog dialog = alert1.create();
            dialog.show();
        }
    }

    private void handleAssetImageNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);
        showAlertDialog();

        if (webUpload) {
            showAlertDialogContinue();
            return;
        }

        this.webUpload = true;
        showAlertDialog();
        Log.d("ImageNetworkResponse", message);

    }

    private void handleAddAssetImage(AddAssetImage addAssetImage) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);
        boolean status = addAssetImage.getStatus();
        if (status) {
            continueScanningDialog(addAssetImage.getMessage() + " Do you want to continue adding items ? ");

        }
    }

    //Handles permissions of camera for taking the photo
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                takePhoto();


            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(AssetIn.this, new String[]{Manifest.permission.CAMERA}, REQUEST_PICTURE_CAPTURE);

            }
        }
    }


    //.....handles drop downs for custodians,categories and suppliers.

    private void handleCustodians(AllCustodians allCustodians) {
        boolean status = allCustodians.getStatus();
        if (status) {
            List<Custodian> custodianList = allCustodians.getCustodians();
            for (Custodian custodian : custodianList) {
                custodianName.add(custodian.getName());
            }
        }
    }

    private void handleCategories(AllCategories allCategories) {
        boolean status = allCategories.getStatus();
        if (status) {
            categoryList = allCategories.getAssetcategories();
            for (AssetCategory assetCategory : categoryList) {
                categoryName.add(assetCategory.getType());
            }
        }
    }


    private void handleSuppliers(AllSuppliers allSuppliers) {
        boolean status = allSuppliers.getStatus();
        if (status) {
            List<Supplier> supplierList = allSuppliers.getSuppliers();
            for (Supplier supplier : supplierList) {
                supplierName.add(supplier.getName());
            }
        }
    }

    //.............method to handle sub categories
    private void handleSubCategories(GetSubCategory getSubCategory) {
        containerLayout.setAlpha(1);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


        subCategoryList = getSubCategory.getSubCategories();

        if (subCategoryList.isEmpty()) {
            subCategoryName.add("None");
        } else {
            for (SubCategory subCategory : subCategoryList) {
                if (subCategory.getItemtypeId() == categoryId) {

                    String mSubCategoryName = subCategory.getName();
                    subCategoryName.add(mSubCategoryName);
                } else {
                    subCategoryName.add("None");
                }
            }
        }


    }

    private void handleSubCategoriesError(Throwable throwable) {
        containerLayout.setAlpha(1);
        throwableError(throwable, "You are Currently Offline", "An Error Occured", "CONVERSION ERRORS");

    }

    private void handleSubCategoriesNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }


    //..............Handles successful adding of new asset/................................
    private void handlePost(AddAsset addAsset) {
        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);

        AddedAsset addedAsset = addAsset.getAddedAsset();

        id = addedAsset.getId();

        AssetInReport assetsReports = new AssetInReport();
        assetsReports.setId(addedAsset.getId());
        assetsReports.setBarcode(addedAsset.getCode());
        assetsReports.setName(addedAsset.getName());
        assetsReports.setLocation(building + " , " + room);
        newAssetViewModel.saveScannedAsset(assetsReports);

        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
        alert1.setTitle("Asset Added Successfully ");
        alert1.setCancelable(false);
        alert1.setMessage("Do you want to add image photo");

        alert1.setPositiveButton("Yes", (dialog1, which1) -> {

            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    takePhoto();
                } else {
                    Toast.makeText(this, "You have no permissions set", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_PICTURE_CAPTURE);
                }

            } else {

                takePhoto();
            }

            //dialog1.dismiss();

        });


        alert1.setNegativeButton("No", (dialog1, which1) -> {
            continueScanningDialog(addAsset.getMessage());

            dialog1.dismiss();
        });
        AlertDialog dialog = alert1.create();
        dialog.show();

    }

    private void handleError(Throwable errorThrowable) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);

        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);

        throwableError(errorThrowable, getString(R.string.network_failure), getString(R.string.error_occured1), "conversion");
    }

    private void handleNetworkResponse(String message) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);

        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    // error 401 or 500, authentication errors
    private void handleUnAuthorized(LoginError loginError) {
        containerLayout.setAlpha(1);
        takePhotoSpin.setVisibility(View.GONE);

        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Log.d("Unauthorized Error", loginError.getMessage());
    }


    // error 404, future date selected
    private void handleFailError(FailError failError) {
        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);
        Toast.makeText(this, failError.getMessage(), Toast.LENGTH_LONG).show();
    }

    //error 422, same barCode used
    private void handleUnprocessedEntityError(GetUnproccessableEntityErrors error) {
        progressBar.setVisibility(View.GONE);
        update.setVisibility(View.VISIBLE);

        Errors errors = error.getErrors();
        List<String> allErrors = errors.getCode();

        Toast.makeText(this, allErrors.toString(), Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {

            if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
                imgFile = new File(pictureFilePath);
                if (imgFile.exists()) {
                    Toast.makeText(this, "Image taken successfully", Toast.LENGTH_SHORT).show();
                    containerLayout.setAlpha(0.4f);
                    takePhotoSpin.setVisibility(View.VISIBLE);
                    newAssetViewModel.postAddAssetImage("Bearer " + accessToken, imgFile, id);
                }
            }
        }

    }

    private File getPictureFile() {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "AMI_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(pictureFile, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert image != null;
        pictureFilePath = image.getAbsolutePath();
        return image;
    }


    private void takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File pictureFile = getPictureFile();
            Uri photoURI;
            if ((Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)) {
                photoURI = FileProvider.getUriForFile(this, getPackageName() + ".provider", pictureFile);
            } else {
                photoURI = Uri.fromFile(pictureFile);
            }
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);

        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
        alert1.setTitle("Failure uploading to the server");
        alert1.setCancelable(false);
        alert1.setMessage("Do you want to retry..?");

        alert1.setPositiveButton("Yes", (dialog, which1) -> {
                    dialog.dismiss();

                    containerLayout.setAlpha(0.4f);
                    takePhotoSpin.setVisibility(View.VISIBLE);
                    newAssetViewModel.postAddAssetImage("Bearer " + accessToken, imgFile, id);

                });

        alert1.setNegativeButton("No", (dialog, which1) -> {
            dialog.dismiss();

            continueScanningDialog("Continue scanning ?");
        });
        AlertDialog dialog = alert1.create();
        dialog.show();


    }

    private void showAlertDialogContinue() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Upload not successful");
        alert.setCancelable(false);

        alert.setPositiveButton("Continue", ((dialog, which) -> {
            dialog.dismiss();
            continueScanningDialog("Continue scanning ? ");

        }));

    }

    private void continueScanningDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(message);

        alert.setPositiveButton("Camera", (dialog, which) -> {
                    dialog.dismiss();

                    Intent scanAsset = new Intent(this, ScanActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("division", division);
                    scanAsset.putExtra("TAG", "A");
                    startActivity(scanAsset);

                });

        alert.setNeutralButton("Scanner", ((dialog, which) -> {
            dialog.dismiss();

            Intent scanAsset = new Intent(this, AssetIn.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("division", division);
            scanAsset.putExtra("TAG", "A");
            startActivity(scanAsset);
        }));

        alert.setNegativeButton("Exit", (dialog, which) -> {
            dialog.dismiss();
            onBackPressed();

        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, AssetsOptions.class);
        startActivity(loc);
    }
}

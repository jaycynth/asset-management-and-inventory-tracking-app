package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockReturn;

public class StockReturnState {
    private StockReturn stockReturn;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public StockReturnState(StockReturn stockReturn) {
        this.stockReturn = stockReturn;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public StockReturnState(String message) {
        this.message = message;
        this.stockReturn = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public StockReturnState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.stockReturn = null;
        this.message = null;
        this.loginError = null;
    }

    public StockReturnState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.stockReturn = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public StockReturn getStockReturn() {
        return stockReturn;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

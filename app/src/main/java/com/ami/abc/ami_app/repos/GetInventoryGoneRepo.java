package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryGoneState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetInventoryGone;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetInventoryGoneRepo {
    private ApiClient mApiClient;
    //constructor
    public GetInventoryGoneRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<InventoryGoneState> getInventoryGone( String accessToken, String code, String invoiceNumber) {

        MutableLiveData<InventoryGoneState> inventoryGoneStateMutableLiveData = new MutableLiveData<>();
        Call<GetInventoryGone> call = mApiClient.amiService().inventoryGone(accessToken, code, invoiceNumber);
        call.enqueue(new Callback<GetInventoryGone>() {
            @Override
            public void onResponse(Call<GetInventoryGone> call, Response<GetInventoryGone> response) {
                if (response.code() == 200) {
                    inventoryGoneStateMutableLiveData.setValue(new InventoryGoneState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryGoneStateMutableLiveData.setValue(new InventoryGoneState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);

                    inventoryGoneStateMutableLiveData.setValue(new InventoryGoneState(failError));
                }
            }

            @Override
            public void onFailure(Call<GetInventoryGone> call, Throwable t) {
                inventoryGoneStateMutableLiveData.setValue(new InventoryGoneState(t));
            }
        });

        return inventoryGoneStateMutableLiveData;

    }
}

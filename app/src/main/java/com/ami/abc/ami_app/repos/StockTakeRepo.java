package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockTakeRepo {
    private ApiClient mApiClient;

    //constructor
    public StockTakeRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<StockTakeState> getStockTake(String code, String accessToken) {

        MutableLiveData<StockTakeState> stockTakeStateMutableLiveData = new MutableLiveData<>();

        Call<StockTake> call = mApiClient.amiService().getInventoryScanned(code, accessToken);
        call.enqueue(new Callback<StockTake>() {
            @Override
            public void onResponse(Call<StockTake> call, Response<StockTake> response) {
                if (response.code() == 200) {
                    stockTakeStateMutableLiveData.setValue(new StockTakeState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    assert response.errorBody() != null;
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    stockTakeStateMutableLiveData.setValue(new StockTakeState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    assert response.errorBody() != null;
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    stockTakeStateMutableLiveData.setValue(new StockTakeState(failError));
                }
            }

            @Override
            public void onFailure(Call<StockTake> call, Throwable t) {
                stockTakeStateMutableLiveData.setValue(new StockTakeState(t));
            }
        });

        return stockTakeStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.IncrementInventory;
import com.ami.abc.ami_app.models.LoginError;

public class IncrementInventoryState {
    private IncrementInventory incrementInventory;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public IncrementInventoryState(IncrementInventory incrementInventory) {
        this.incrementInventory = incrementInventory;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public IncrementInventoryState(String message) {
        this.message = message;
        this.incrementInventory = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public IncrementInventoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.incrementInventory = null;
        this.message = null;
        this.loginError = null;
    }

    public IncrementInventoryState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.incrementInventory = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public IncrementInventory getIncrementInventory() {
        return incrementInventory;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

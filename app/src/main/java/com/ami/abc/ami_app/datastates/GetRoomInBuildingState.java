package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.GetRoomInBuilding;
import com.ami.abc.ami_app.models.LoginError;

public class GetRoomInBuildingState {
    private GetRoomInBuilding getRoomInBuilding;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public GetRoomInBuildingState(GetRoomInBuilding getRoomInBuilding) {
        this.getRoomInBuilding = getRoomInBuilding;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public GetRoomInBuildingState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.getRoomInBuilding = null;
        this.loginError = null;
    }

    public GetRoomInBuildingState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.getRoomInBuilding = null;
        this.loginError = null;
    }

    public GetRoomInBuildingState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.getRoomInBuilding = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public GetRoomInBuilding getGetRoomInBuilding() {
        return getRoomInBuilding;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

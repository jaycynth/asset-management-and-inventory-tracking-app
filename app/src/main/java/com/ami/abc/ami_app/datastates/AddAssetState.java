package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AddAsset;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetUnproccessableEntityErrors;
import com.ami.abc.ami_app.models.LoginError;

public class AddAssetState {
    private AddAsset addAsset;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;
    private GetUnproccessableEntityErrors error;

    public AddAssetState(AddAsset addAsset) {
        this.addAsset = addAsset;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
        this.error = null;
    }

    public AddAssetState(String message) {
        this.message = message;
        this.addAsset = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
        this.error = null;

    }

    public AddAssetState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.addAsset = null;
        this.loginError = null;
        this.failError = null;
        this.error = null;

    }

    public AddAssetState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.addAsset = null;
        this.errorThrowable = null;
        this.failError = null;
        this.error = null;

    }

    public AddAssetState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.addAsset = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;

    }

    public AddAssetState(GetUnproccessableEntityErrors error) {
        this.error = error;
        this.message = null;
        this.addAsset = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public GetUnproccessableEntityErrors getError() {
        return error;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AddAsset getAddAsset() {
        return addAsset;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

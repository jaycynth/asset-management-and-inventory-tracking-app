package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AddInventoryState;
import com.ami.abc.ami_app.models.AddInventory;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInventoryRepo {
    private ApiClient mApiClient;

    public AddInventoryRepo(Application application) {
        mApiClient = new ApiClient(application);
    }


    public LiveData<AddInventoryState> addInventory(String accessToken, String productName, String productCode, String building,
                                                    String room, String supplier,int quantity, int unitPrice, int reorderLevel) {

        MutableLiveData<AddInventoryState> addInventoryStateMutableLiveData = new MutableLiveData<>();
        Call<AddInventory> call = mApiClient.amiService().addInventory(accessToken, productName, productCode, building, room, supplier,
                quantity, unitPrice, reorderLevel);
        call.enqueue(new Callback<AddInventory>() {
            @Override
            public void onResponse(Call<AddInventory> call, Response<AddInventory> response) {
                if (response.code() == 200) {
                    addInventoryStateMutableLiveData.setValue(new AddInventoryState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    addInventoryStateMutableLiveData.setValue(new AddInventoryState(loginError));
                }
                else {
                    addInventoryStateMutableLiveData.setValue(new AddInventoryState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AddInventory> call, Throwable t) {
                addInventoryStateMutableLiveData.setValue(new AddInventoryState(t));
            }
        });

        return addInventoryStateMutableLiveData;

    }
}

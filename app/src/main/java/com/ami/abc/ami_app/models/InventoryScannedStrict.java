package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryScannedStrict {

        @SerializedName("status")
        @Expose
        private Boolean status;
        @SerializedName("inventory")
        @Expose
        private Inventory inventory;
        @SerializedName("counted")
        @Expose
        private Integer counted;

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Inventory getInventory() {
            return inventory;
        }

        public void setInventory(Inventory inventory) {
            this.inventory = inventory;
        }

        public Integer getCounted() {
            return counted;
        }

        public void setCounted(Integer counted) {
            this.counted = counted;
        }


}

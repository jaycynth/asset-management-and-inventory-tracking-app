package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetInventoryGone;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockReturn;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.StockReturnViewModel;

import java.io.IOException;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockReturnActivity extends AppCompatActivity {

    String building, room, depart, assetId;

    @BindView(R.id.asset_id)
    EditText asset_code;

    @BindView(R.id.inventory_name)
    TextInputEditText inventoryName;

    @BindView(R.id.returnn)
    Button returnn;

    @BindView(R.id.quantity)
    TextInputEditText currentQuantity;
    @BindView(R.id.new_quantity)
    TextInputEditText newQuantity;
    @BindView(R.id.customer)
    TextInputEditText mCustomer;
    @BindView(R.id.invoice_number)
    TextInputEditText invoiceNumber;
    @BindView(R.id.invoice_value)
    TextInputEditText invoiceValue;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;
    @BindView(R.id.holder_layout)
    RelativeLayout holder_layout;

    String accessToken;
    StockReturnViewModel stockReturnViewModel;
    int inventoryId;

    @BindView(R.id.details_layout)
    LinearLayout detailsLayout;
    @BindView(R.id.submit)
    Button submit;

    int inventoryOut;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.invoice_number_layout)
    LinearLayout invoiceNumberLayout;

    String code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_return);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Stock Return");

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");


        stockReturnViewModel = ViewModelProviders.of(this).get(StockReturnViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null && assetId != null) {

            asset_code.setText(assetId);

            confirm.setVisibility(View.GONE);
            invoiceNumberLayout.setVisibility(View.VISIBLE);

            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            stockReturnViewModel.stockTake(assetId, "Bearer " + accessToken);

        }

        confirm.setOnClickListener(v -> {
            code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                stockReturnViewModel.stockTake(code, "Bearer " + accessToken);

            }
        });


        submit.setOnClickListener(v -> {
            String invoice = invoiceNumber.getText().toString().trim();

            if (TextUtils.isEmpty(invoice)) {
                Toast.makeText(this, "Enter the invoice number", Toast.LENGTH_SHORT).show();
            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                if (assetId != null) {
                    stockReturnViewModel.inventoryGone("Bearer " + accessToken, assetId, invoice);
                } else {
                    stockReturnViewModel.inventoryGone("Bearer " + accessToken, code, invoice);
                }
            }

        });

        stockReturnViewModel.stockTakeResponse().observe(this, stockTakeState -> {
            assert stockTakeState != null;
            if (stockTakeState.getStockTake() != null) {
                handleStockDetails(stockTakeState.getStockTake());
            }
            if (stockTakeState.getErrorThrowable() != null) {
                handleError(stockTakeState.getErrorThrowable());
            }
            if (stockTakeState.getMessage() != null) {
                handleNetworkResponse(stockTakeState.getMessage());
            }
            if (stockTakeState.getFailError() != null) {
                handleFailError(stockTakeState.getFailError());
            }
            if (stockTakeState.getLoginError() != null) {
                handleUnAuthorized(stockTakeState.getLoginError());
            }
        });


        stockReturnViewModel.inventoryGoneResponse().observe(this, inventoryGoneState -> {
            assert inventoryGoneState != null;
            if (inventoryGoneState.getGetInventoryGone() != null) {
                handleInventoryOut(inventoryGoneState.getGetInventoryGone());
            }
            if (inventoryGoneState.getErrorThrowable() != null) {
                handleError(inventoryGoneState.getErrorThrowable());
            }
            if (inventoryGoneState.getMessage() != null) {
                handleNetworkResponse(inventoryGoneState.getMessage());
            }
            if (inventoryGoneState.getFailError() != null) {
                handleFailError(inventoryGoneState.getFailError());
            }
            if (inventoryGoneState.getLoginError() != null) {
                handleUnAuthorized(inventoryGoneState.getLoginError());
            }
        });

        stockReturnViewModel.stockReturnResponse().observe(this, stockReturnState -> {
            assert stockReturnState != null;
            if (stockReturnState.getStockReturn() != null) {
                handleStockReturn(stockReturnState.getStockReturn());
            }
            if (stockReturnState.getErrorThrowable() != null) {
                handleError(stockReturnState.getErrorThrowable());
            }
            if (stockReturnState.getMessage() != null) {
                handleNetworkResponse(stockReturnState.getMessage());
            }
            if (stockReturnState.getLoginError() != null) {
                handleUnAuthorized(stockReturnState.getLoginError());
            }
        });

        returnn.setOnClickListener(v -> {

            String quantText = newQuantity.getText().toString().trim();
            String invoice = invoiceNumber.getText().toString().trim();
            if (TextUtils.isEmpty(quantText)) {
                Toast.makeText(this, "Enter your quantity", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(invoice)) {
                Toast.makeText(this, "Enter your requisition number", Toast.LENGTH_SHORT).show();
            } else {

                int quant = Integer.parseInt(quantText);
                if (quant > inventoryOut) {
                    Toast.makeText(this, "Quantity being returned should be : " + inventoryOut, Toast.LENGTH_SHORT).show();
                } else {

                    holder_layout.setAlpha(0.4f);
                    progressBar.setVisibility(View.VISIBLE);
                    stockReturnViewModel.stockReturn("Bearer " + accessToken, inventoryId, invoice, quant);
                }

            }
        });


    }

    private void handleStockDetails(StockTake stockTake) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);

        confirm.setVisibility(View.GONE);
        invoiceNumberLayout.setVisibility(View.VISIBLE);

        boolean status = stockTake.getStatus();
        if (status) {
            inventoryName.setText(stockTake.getInventory().getProductName());
        }


    }

    private void handleStockReturn(StockReturn stockReturn) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);
        boolean status = stockReturn.getStatus();
        if (status) {
            Toast.makeText(this, stockReturn.getMessage(), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    private void handleFailError(FailError failError) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);
        // Toast.makeText(this, failError.getMessage(), Toast.LENGTH_SHORT).show();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // prevent cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setMessage(failError.getMessage() + " Do you want to continue ? ");

        alert.setNeutralButton("Amend", (dialog, which) -> {
                   // onBackPressed();
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, StockReturnActivity.class);
                    scanAsset.putExtra("TAG", "K");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("TAG", "K");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private void handleInventoryOut(GetInventoryGone getInventoryGone) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);
        boolean status = getInventoryGone.getStatus();
        if (status) {
            submit.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);
            currentQuantity.setText(String.valueOf(getInventoryGone.getInventoryout().getInventory().getQuantity()));

            newQuantity.setText(String.valueOf(getInventoryGone.getInventoryout().getQuantity()));
            mCustomer.setText(getInventoryGone.getInventoryout().getCustomer());
            invoiceValue.setText(String.valueOf(getInventoryGone.getInventoryout().getInvoiceValue()));

            inventoryId = getInventoryGone.getInventoryout().getInventory().getId();
            inventoryOut = getInventoryGone.getRemainingout();

        }
    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {
        progressBar.setVisibility(View.INVISIBLE);
        holder_layout.setAlpha(1);

        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }
}

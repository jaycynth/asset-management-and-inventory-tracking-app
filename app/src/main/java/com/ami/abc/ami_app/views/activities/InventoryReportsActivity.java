package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.StockTakeReportAdapter;
import com.ami.abc.ami_app.models.StockTakeReport;
import com.ami.abc.ami_app.viewmodels.StockTakeReportViewModel;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InventoryReportsActivity extends AppCompatActivity {
    @BindView(R.id.inventory_reports)
    RecyclerView inventory_reports;

    static RecyclerView.LayoutManager layoutManager;
    StockTakeReportViewModel stockTakeReportsViewModel;
    @BindView(R.id.no_reports)
    TextView no_reports;

    StockTakeReportAdapter stockTakeReportsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_reports);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.inventory_reports));


        stockTakeReportsViewModel = ViewModelProviders.of(this).get(StockTakeReportViewModel.class);
        stockTakeReportsViewModel.getListLiveData().observe(this, stockTakeReports -> {
            assert stockTakeReports != null;
            if (!stockTakeReports.isEmpty()) {
                initView(stockTakeReports);

            } else {
                inventory_reports.setVisibility(View.GONE);
                no_reports.setVisibility(View.VISIBLE);
            }
        });

    }

    private void initView(List<StockTakeReport> stockTakeReports) {
        stockTakeReportsAdapter = new StockTakeReportAdapter(stockTakeReports, this);
        layoutManager = new LinearLayoutManager(this);
        inventory_reports.setLayoutManager(layoutManager);
        inventory_reports.setItemAnimator(new DefaultItemAnimator());
        inventory_reports.setAdapter(stockTakeReportsAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }


        return super.onOptionsItemSelected(item);    }





    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, ReportOptionsActivity.class);
        startActivity(loc);
    }
}

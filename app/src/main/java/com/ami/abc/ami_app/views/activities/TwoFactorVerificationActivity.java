package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ami.abc.ami_app.MainActivity;
import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.TwoFactorAuth;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.TwoFactorAuthViewModel;
import com.google.zxing.Result;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class TwoFactorVerificationActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private boolean mAutoFocus = true;


    private ZXingScannerView scannerView;


    String myResult;

    TwoFactorAuthViewModel twoFactorAuthViewModel;

    String token;

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mAutoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);

        } else {
            mAutoFocus = true;

        }
        scannerView = new ZXingScannerView(this);
        if (scannerView != null) {
            setContentView(scannerView);
            scannerView.setResultHandler(this);
            scannerView.startCamera();


        }

        twoFactorAuthViewModel = ViewModelProviders.of(this).get(TwoFactorAuthViewModel.class);

        token = SharedPreferenceManager.getInstance(this).getToken();


        twoFactorAuthViewModel.twoFactorAuthResponse().observe(this,twoFactorAuthState -> {
            if(twoFactorAuthState.getTwoFactorAuth() != null){
                handleTwoFactorAuth(twoFactorAuthState.getTwoFactorAuth());
            }

            if(twoFactorAuthState.getErrorThrowable() != null){
                handleError(twoFactorAuthState.getErrorThrowable());
            }
            if(twoFactorAuthState.getMessage() != null){
                handleNetworkResponse(twoFactorAuthState.getMessage());
            }
        });


    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this,  message, Toast.LENGTH_SHORT).show();

    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleTwoFactorAuth(TwoFactorAuth twoFactorAuth) {
        progressBar.setVisibility(View.GONE);

        boolean status = twoFactorAuth.getStatus();
        if(status){

            LayoutInflater mInflater = getLayoutInflater();
            View alertLayout = mInflater.inflate(R.layout.activity_code_verification, null);
            final EditText code = alertLayout.findViewById(R.id.code);
            code.setText(twoFactorAuth.get2fa());
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Verification Code");
            alert.setView(alertLayout);
            alert.setCancelable(false);

            AlertDialog dialog = alert.create();
            dialog.show();

            Button btn = (Button) alertLayout.findViewById(R.id.id2);
            btn.setOnClickListener(v -> {
                if (dialog != null) {
                    startActivity(new Intent(this, MainActivity.class));
                    dialog.cancel();
                }
            });

        }

    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel(
                                        (dialog, which) -> {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{CAMERA},
                                                        REQUEST_CAMERA);
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(TwoFactorVerificationActivity.this)
                .setMessage("You need to allow access to both the permissions")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
                scannerView.setAutoFocus(mAutoFocus);

            } else {
                requestPermission();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCameraPreview();
        scannerView.stopCamera();
    }


    @Override
    protected void onPause() {
        scannerView.stopCameraPreview();
        scannerView.stopCamera();

        scannerView.setAutoFocus(false);
        super.onPause();
    }

    @Override
    public void handleResult(Result result) {

        try {
            ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            tg.startTone(ToneGenerator.TONE_PROP_BEEP);

            myResult = result.getText();
            Log.d("QRCodeScanner", result.getText());
            Log.d("QRCodeScanner", result.getBarcodeFormat().toString());

            progressBar = new ProgressBar(TwoFactorVerificationActivity.this, null, android.R.attr.progressBarStyleLarge);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            scannerView.addView(progressBar, params);
            progressBar.setVisibility(View.VISIBLE);

            twoFactorAuthViewModel.twoFactorAuth("Bearer "+ token ,myResult);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent main = new Intent(this,MainActivity.class);
        startActivity(main);
    }
}
package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AssetCheckInOutDetailsState;
import com.ami.abc.ami_app.datastates.AssetCheckState;
import com.ami.abc.ami_app.datastates.AssetCheckedOutPointState;
import com.ami.abc.ami_app.datastates.CustodianState;
import com.ami.abc.ami_app.datastates.GetAssetScannedDetailsState;
import com.ami.abc.ami_app.models.AssetCheckOutReport;
import com.ami.abc.ami_app.repos.AssetCheckInOutDetailsRepo;
import com.ami.abc.ami_app.repos.AssetCheckOutRepo;
import com.ami.abc.ami_app.repos.AssetCheckOutReportsRepo;
import com.ami.abc.ami_app.repos.AssetCheckRepo;
import com.ami.abc.ami_app.repos.CustodianRepo;
import com.ami.abc.ami_app.repos.GetAssetsScannedRepo;

public class AssetCheckOutViewModel extends AndroidViewModel {
    private MediatorLiveData<AssetCheckedOutPointState> assetCheckedOutPointStateMediatorLiveData;
    private AssetCheckOutRepo assetCheckOutRepo;

    private MediatorLiveData<CustodianState> custodianStateMediatorLiveData;
    private CustodianRepo custodianRepo;

    private MediatorLiveData<AssetCheckState> assetCheckStateMediatorLiveData;
    private AssetCheckRepo assetCheckRepo;

    private MediatorLiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateMediatorLiveData;
    private GetAssetsScannedRepo getAssetsScannedRepo;

    private AssetCheckOutReportsRepo assetCheckOutReportsRepo;

    private MediatorLiveData<AssetCheckInOutDetailsState> assetCheckInOutDetailsStateMediatorLiveData;
    private AssetCheckInOutDetailsRepo assetCheckInOutDetailsRepo;



    public AssetCheckOutViewModel(Application application){
        super(application);

        assetCheckStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckRepo = new AssetCheckRepo(application);

        assetCheckedOutPointStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckOutRepo = new AssetCheckOutRepo(application);

        custodianStateMediatorLiveData = new MediatorLiveData<>();
        custodianRepo = new CustodianRepo(application);


        getAssetScannedDetailsStateMediatorLiveData = new MediatorLiveData<>();
        getAssetsScannedRepo = new GetAssetsScannedRepo(application);

        assetCheckOutReportsRepo = new AssetCheckOutReportsRepo();

        assetCheckInOutDetailsStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckInOutDetailsRepo = new AssetCheckInOutDetailsRepo(application);


    }

    public LiveData<AssetCheckState> assetCheckResponse() {
        return assetCheckStateMediatorLiveData;
    }

    public void assetCheck(String accessToken) {
        LiveData<AssetCheckState> assetCheckStateLiveData = assetCheckRepo.assetCheck(accessToken);
        assetCheckStateMediatorLiveData.addSource(assetCheckStateLiveData,
                assetCheckStateMediatorLiveData -> {
                    if (this.assetCheckStateMediatorLiveData.hasActiveObservers()) {
                        this.assetCheckStateMediatorLiveData.removeSource(assetCheckStateLiveData);
                    }
                    this.assetCheckStateMediatorLiveData.setValue(assetCheckStateMediatorLiveData);
                });
    }


    public LiveData<AssetCheckedOutPointState> checkOutAssetResponse() {
        return assetCheckedOutPointStateMediatorLiveData;
    }

    public void checkOutAsset(String accessToken, String productCode, String custodian, String mileage, String fuel) {
        LiveData<AssetCheckedOutPointState> assetCheckedOutPointStateLiveData = assetCheckOutRepo.checkoutAsset(accessToken, productCode, custodian, mileage, fuel);
        assetCheckedOutPointStateMediatorLiveData.addSource(assetCheckedOutPointStateLiveData,
                assetCheckedOutPointStateMediatorLiveData -> {
                    if (this.assetCheckedOutPointStateMediatorLiveData.hasActiveObservers()) {
                        this.assetCheckedOutPointStateMediatorLiveData.removeSource(assetCheckedOutPointStateLiveData);
                    }
                    this.assetCheckedOutPointStateMediatorLiveData.setValue(assetCheckedOutPointStateMediatorLiveData);
                });
    }

    public LiveData<CustodianState> getAllCustodiansResponse() {
        return custodianStateMediatorLiveData;
    }

    public void getAllCustodians(String accessToken) {
        LiveData<CustodianState> custodianStateLiveData = custodianRepo.getAllCustodians(accessToken);
        custodianStateMediatorLiveData.addSource(custodianStateLiveData,
                custodianStateMediatorLiveData -> {
                    if (this.custodianStateMediatorLiveData.hasActiveObservers()) {
                        this.custodianStateMediatorLiveData.removeSource(custodianStateLiveData);
                    }
                    this.custodianStateMediatorLiveData.setValue(custodianStateMediatorLiveData);
                });
    }


    public LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsResponse() {
        return getAssetScannedDetailsStateMediatorLiveData;
    }

    public void getAssetScannedDetails(String code, String accessToken) {
        LiveData<GetAssetScannedDetailsState> getAssetScannedDetailsStateLiveData = getAssetsScannedRepo.getAssetScanned(code, accessToken);
        getAssetScannedDetailsStateMediatorLiveData.addSource(getAssetScannedDetailsStateLiveData,
                getAssetScannedDetailsStateMediatorLiveData -> {
                    if (this.getAssetScannedDetailsStateMediatorLiveData.hasActiveObservers()) {
                        this.getAssetScannedDetailsStateMediatorLiveData.removeSource(getAssetScannedDetailsStateLiveData);
                    }
                    this.getAssetScannedDetailsStateMediatorLiveData.setValue(getAssetScannedDetailsStateMediatorLiveData);
                });

    }

    public LiveData<AssetCheckInOutDetailsState> assetCheckInOutDetails() {
        return assetCheckInOutDetailsStateMediatorLiveData;
    }

    public void getAssetCheckInOutDetails(String accessToken,String code) {
        LiveData<AssetCheckInOutDetailsState> assetCheckInOutDetailsStateLiveData = assetCheckInOutDetailsRepo.assetCheckInOutDetails(accessToken, code);
        assetCheckInOutDetailsStateMediatorLiveData.addSource(assetCheckInOutDetailsStateLiveData, assetCheckInOutDetailsStateMediatorLiveData -> {
            if (this.assetCheckInOutDetailsStateMediatorLiveData.hasActiveObservers()) {
                this.assetCheckInOutDetailsStateMediatorLiveData.removeSource(assetCheckInOutDetailsStateLiveData);
            }
            this.assetCheckInOutDetailsStateMediatorLiveData.setValue(assetCheckInOutDetailsStateMediatorLiveData);
        });

    }

    //save individual assets properties scanned in room for reports
    public void saveAssetsReports(AssetCheckOutReport assetsReports) {
        assetCheckOutReportsRepo.saveAssets(assetsReports);
    }
}

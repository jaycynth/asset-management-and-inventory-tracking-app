package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.LoginError;

public class GetAssetScannedDetailsState {
    private GetAssetScannedDetails getAssetScannedDetails;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;


    public GetAssetScannedDetailsState(GetAssetScannedDetails getAssetScannedDetails) {
        this.getAssetScannedDetails = getAssetScannedDetails;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public GetAssetScannedDetailsState(String message) {
        this.message = message;
        this.getAssetScannedDetails = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public GetAssetScannedDetailsState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.getAssetScannedDetails = null;
        this.message = null;
        this.loginError = null;
        this.failError = null;
    }

    public GetAssetScannedDetailsState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.getAssetScannedDetails = null;
        this.failError = null;
    }

    public GetAssetScannedDetailsState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.errorThrowable = null;
        this.getAssetScannedDetails = null;
        this.loginError = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public GetAssetScannedDetails getGetAssetScannedDetails() {
        return getAssetScannedDetails;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

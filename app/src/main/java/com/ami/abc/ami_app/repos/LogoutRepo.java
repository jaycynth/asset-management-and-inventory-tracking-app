package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.LogoutState;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Logout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutRepo {

    private ApiClient mApiClient;

    //constructor
    public LogoutRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<LogoutState> logoutUser(String accessToken) {

        MutableLiveData<LogoutState> logoutStateMutableLiveData = new MutableLiveData<>();
        Call<Logout> call = mApiClient.amiService().logout(accessToken);
        call.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, Response<Logout> response) {
                if (response.code() == 200) {
                    logoutStateMutableLiveData.setValue(new LogoutState(response.body()));

                }else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    logoutStateMutableLiveData.setValue(new LogoutState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    logoutStateMutableLiveData.setValue(new LogoutState(error));
                }
                else {

                    logoutStateMutableLiveData.setValue(new LogoutState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                logoutStateMutableLiveData.setValue(new LogoutState(t));
            }
        });

        return logoutStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.StockTakeReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StockTakeReportAdapter extends RecyclerView.Adapter<StockTakeReportAdapter.MyViewHolder> {
    private List<StockTakeReport> stockTakeReportList;
    private Context context;

    public StockTakeReportAdapter(List<StockTakeReport> stockTakeReportList, Context context) {
        this.stockTakeReportList = stockTakeReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.inventory_code)
        TextView asset_code;


        @BindView(R.id.count)
        TextView count;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public StockTakeReportAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_reports_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StockTakeReportAdapter.MyViewHolder holder, int i) {

        StockTakeReport assetScanned = stockTakeReportList.get(i);
        holder.asset_code.setText(String.format("Inventory Barcode : %s", assetScanned.getBarcode()));
        holder.count.setText(String.format("Count : %s", String.valueOf(assetScanned.getCount())));


    }

    @Override
    public int getItemCount() {
        return stockTakeReportList.size();
    }


}


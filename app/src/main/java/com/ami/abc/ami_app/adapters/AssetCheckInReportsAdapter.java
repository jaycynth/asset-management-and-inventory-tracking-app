package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AssetCheckInReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetCheckInReportsAdapter extends RecyclerView.Adapter<AssetCheckInReportsAdapter.MyViewHolder> {
    private List<AssetCheckInReport> assetInReportList;
    private Context context;

    public AssetCheckInReportsAdapter(List<AssetCheckInReport> assetInReportList, Context context) {
        this.assetInReportList = assetInReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.status)
        TextView status;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public AssetCheckInReportsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.asset_check_reports_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetCheckInReportsAdapter.MyViewHolder holder, int i) {
        AssetCheckInReport assetScanned = assetInReportList.get(i);
        holder.asset_code.setText(assetScanned.getBarcode());
        holder.name.setText("Product Name : " + assetScanned.getName());
        holder.status.setText("Located at : "+ assetScanned.getLocation());


    }

    @Override
    public int getItemCount() {
        return assetInReportList.size();
    }


}

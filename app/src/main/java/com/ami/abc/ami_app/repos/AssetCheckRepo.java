package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetCheckState;
import com.ami.abc.ami_app.models.AssetCheckPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetCheckRepo {
    private ApiClient mApiClient;
    //constructor
    public AssetCheckRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<AssetCheckState> assetCheck(String accessToken) {

        MutableLiveData<AssetCheckState> assetCheckPointStateMutableLiveData = new MutableLiveData<>();

        Call<AssetCheckPoint> call = mApiClient.amiService().assetCheck(accessToken);
        call.enqueue(new Callback<AssetCheckPoint>() {
            @Override
            public void onResponse(Call<AssetCheckPoint> call, Response<AssetCheckPoint> response) {
                if (response.code() == 200) {
                    assetCheckPointStateMutableLiveData.setValue(new AssetCheckState(response.body()));

                } else if (response.code() == 401 || response.code() == 500 || response.code() == 422) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    assetCheckPointStateMutableLiveData.setValue(new AssetCheckState(loginError));
                }
                else {
                    assetCheckPointStateMutableLiveData.setValue(new AssetCheckState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AssetCheckPoint> call, Throwable t) {
                assetCheckPointStateMutableLiveData.setValue(new AssetCheckState(t));
            }
        });

        return assetCheckPointStateMutableLiveData;

    }
}

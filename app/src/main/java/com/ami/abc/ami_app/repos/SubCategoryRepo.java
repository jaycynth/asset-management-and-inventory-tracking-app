package com.ami.abc.ami_app.repos;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.SubCategoriesState;
import com.ami.abc.ami_app.models.GetSubCategory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryRepo {

    private ApiClient apiClient;

    public SubCategoryRepo(Application application) {
        apiClient = new ApiClient(application);
    }


    public LiveData<SubCategoriesState> getAllSubCategories(String accessToken){

        MutableLiveData<SubCategoriesState> subCategoriesStateMutableLiveData = new MutableLiveData<>();

        Call<GetSubCategory> call = apiClient.amiService().getSubCategories(accessToken);

        call.enqueue(new Callback<GetSubCategory>() {
            @Override
            public void onResponse(Call<GetSubCategory> call, Response<GetSubCategory> response) {
               if(response.code() == 200){
                   subCategoriesStateMutableLiveData.setValue(new SubCategoriesState(response.body()));
               }else{
                   subCategoriesStateMutableLiveData.setValue(new SubCategoriesState(response.message()));
               }
            }

            @Override
            public void onFailure(Call<GetSubCategory> call, Throwable t) {
               subCategoriesStateMutableLiveData.setValue(new SubCategoriesState(t));
            }
        });

        return subCategoriesStateMutableLiveData;
    }
}

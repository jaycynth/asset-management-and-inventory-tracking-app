package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.ResetPasswordState;
import com.ami.abc.ami_app.models.ResetPassword;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordRepo {
    private ApiClient mApiClient;

    //constructor
    public ResetPasswordRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<ResetPasswordState> resetPasswordUser(String accessToken, String email) {

        MutableLiveData<ResetPasswordState> resetPasswordStateMutableLiveData = new MutableLiveData<>();
        Call<ResetPassword> call = mApiClient.amiService().resetPassword(accessToken, email);
        call.enqueue(new Callback<ResetPassword>() {
            @Override
            public void onResponse(Call<ResetPassword> call, Response<ResetPassword> response) {
                if (response.code() == 200) {
                    resetPasswordStateMutableLiveData.setValue(new ResetPasswordState(response.body()));

                }else {

                    resetPasswordStateMutableLiveData.setValue(new ResetPasswordState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<ResetPassword> call, Throwable t) {
                resetPasswordStateMutableLiveData.setValue(new ResetPasswordState(t));
            }
        });

        return resetPasswordStateMutableLiveData;

    }
}

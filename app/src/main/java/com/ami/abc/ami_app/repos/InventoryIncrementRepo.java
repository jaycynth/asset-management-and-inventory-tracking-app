package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.IncrementInventoryState;
import com.ami.abc.ami_app.models.IncrementInventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryIncrementRepo {
    private ApiClient mApiClient;

    //constructor
    public InventoryIncrementRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<IncrementInventoryState> incrementInventory(String accessToken, int inventoryId, int quantity) {

        MutableLiveData<IncrementInventoryState> incrementInventoryStateMutableLiveData = new MutableLiveData<>();
        Call<IncrementInventory> call = mApiClient.amiService().incrementInventory(accessToken,inventoryId,quantity,Constants.INC_TYPE);
        call.enqueue(new Callback<IncrementInventory>() {
            @Override
            public void onResponse(Call<IncrementInventory> call, Response<IncrementInventory> response) {
                if (response.code() == 200) {
                    incrementInventoryStateMutableLiveData.setValue(new IncrementInventoryState(response.body()));

                }else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    incrementInventoryStateMutableLiveData.setValue(new IncrementInventoryState(loginError));
                } else {
                    incrementInventoryStateMutableLiveData.setValue(new IncrementInventoryState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<IncrementInventory> call, Throwable t) {
                incrementInventoryStateMutableLiveData.setValue(new IncrementInventoryState(t));
            }
        });

        return incrementInventoryStateMutableLiveData;

    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AddInventoryImage;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;

public class AddInventoryImageState {
    private AddInventoryImage addInventoryImage;
    private Throwable errorThrowable;
    private String message;
    private FailError failError;
    private LoginError loginError;

    public AddInventoryImageState(AddInventoryImage addInventoryImage) {
        this.addInventoryImage = addInventoryImage;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public AddInventoryImageState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.failError = null;
        this.loginError = null;
        this.addInventoryImage = null;
        this.message = null;
    }

    public AddInventoryImageState(String message) {
        this.message = message;
        this.failError = null;
        this.loginError = null;
        this.addInventoryImage = null;
        this.errorThrowable = null;

    }

    public AddInventoryImageState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.loginError = null;
        this.addInventoryImage = null;
        this.errorThrowable = null;
    }

    public AddInventoryImageState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.failError = null;
        this.addInventoryImage = null;
        this.errorThrowable = null;
    }

    public AddInventoryImage getAddInventoryImage() {
        return addInventoryImage;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public String getMessage() {
        return message;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

package com.ami.abc.ami_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomArrayAdapter extends ArrayAdapter<Room> {

    private List<Room> roomListFull;
    private List<Room> filteredRoomList = new ArrayList<>();


    public RoomArrayAdapter(@NonNull Context context, @NonNull List<Room> roomList) {
        super(context, 0, roomList);
        this.roomListFull = roomList;
    }

    @Override
    public int getCount() {
        return filteredRoomList.size();
    }

    @Override
    public Room getItem(int position) {
        return filteredRoomList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return roomFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_array_layout, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.value);

        Room room = getItem(position);

        if (room != null) {
            textViewName.setText(room.getName());
        }

        return convertView;
    }

    private Filter roomFilter = new Filter() {

        List<Room> suggestions = new ArrayList<>();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(roomListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Room item : roomListFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredRoomList.clear();
            if (results.count > 0) {
                filteredRoomList.addAll((List) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Room) resultValue).getName();
        }
    };
}

package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.DepartmentState;
import com.ami.abc.ami_app.datastates.DivisionState;
import com.ami.abc.ami_app.datastates.ShelvesInStoreState;
import com.ami.abc.ami_app.datastates.StoreState;
import com.ami.abc.ami_app.repos.DepartmentRepo;
import com.ami.abc.ami_app.repos.DivisionsRepo;
import com.ami.abc.ami_app.repos.ShelvesInStoreRepo;
import com.ami.abc.ami_app.repos.StoreRepo;

public class InventoryLocationViewModel extends AndroidViewModel {
    private MediatorLiveData<DepartmentState> departmentStateMediatorLiveData;
    private DepartmentRepo departmentRepo;

    private MediatorLiveData<StoreState> storeStateMediatorLiveData;
    private StoreRepo storeRepo;

    private MediatorLiveData<ShelvesInStoreState> shelvesInStoreStateMediatorLiveData;
    private ShelvesInStoreRepo shelvesInStoreRepo;

    private MediatorLiveData<DivisionState> divisionStateMediatorLiveData;
    private DivisionsRepo divisionsRepo;

    public InventoryLocationViewModel(Application application) {
        super(application);
        departmentStateMediatorLiveData = new MediatorLiveData<>();
        departmentRepo = new DepartmentRepo(application);

        storeStateMediatorLiveData = new MediatorLiveData<>();
        storeRepo = new StoreRepo(application);

        shelvesInStoreStateMediatorLiveData = new MediatorLiveData<>();
        shelvesInStoreRepo = new ShelvesInStoreRepo(application);

        divisionStateMediatorLiveData = new MediatorLiveData<>();
        divisionsRepo = new DivisionsRepo(application);
    }

    public LiveData<DivisionState> getDivisionResponse(){
        return divisionStateMediatorLiveData;
    }

    public void getDivisions(String accessToken){

        LiveData<DivisionState> divisionStateLiveData = divisionsRepo.getGetDivisions(accessToken);
        divisionStateMediatorLiveData.addSource(divisionStateLiveData,
                divisionStateMediatorLiveData -> {
                    if (this.divisionStateMediatorLiveData.hasActiveObservers()){
                        this.divisionStateMediatorLiveData.removeSource(divisionStateLiveData);
                    }
                    this.divisionStateMediatorLiveData.setValue(divisionStateMediatorLiveData);
                });

    }

    public LiveData<DepartmentState> getDepartmentResponse(){
        return departmentStateMediatorLiveData;
    }

    public void getDepartments(String accessToken){

        LiveData<DepartmentState> departmentStateLiveData = departmentRepo.getAllDepartments(accessToken);
        departmentStateMediatorLiveData.addSource(departmentStateLiveData,
                departmentStateMediatorLiveData -> {
                    if (this.departmentStateMediatorLiveData.hasActiveObservers()){
                        this.departmentStateMediatorLiveData.removeSource(departmentStateLiveData);
                    }
                    this.departmentStateMediatorLiveData.setValue(departmentStateMediatorLiveData);
                });

    }
    public LiveData<StoreState> getStoreResponse(){
        return storeStateMediatorLiveData;
    }

    public void getStores(String accessToken){

        LiveData<StoreState> storeStateLiveData = storeRepo.getAllStores(accessToken);
        storeStateMediatorLiveData.addSource(storeStateLiveData,
                storeStateMediatorLiveData -> {
                    if (this.storeStateMediatorLiveData.hasActiveObservers()){
                        this.storeStateMediatorLiveData.removeSource(storeStateLiveData);
                    }
                    this.storeStateMediatorLiveData.setValue(storeStateMediatorLiveData);
                });

    }
    public LiveData<ShelvesInStoreState> getShelvesInStoreResponse(){
        return shelvesInStoreStateMediatorLiveData;
    }

    public void getShelvesInStore(String accessToken, int storeId){

        LiveData<ShelvesInStoreState> shelvesInStoreStateLiveData = shelvesInStoreRepo.getShelvesIStore(accessToken, storeId);
        shelvesInStoreStateMediatorLiveData.addSource(shelvesInStoreStateLiveData,
                shelvesInStoreStateMediatorLiveData -> {
                    if (this.shelvesInStoreStateMediatorLiveData.hasActiveObservers()){
                        this.shelvesInStoreStateMediatorLiveData.removeSource(shelvesInStoreStateLiveData);
                    }
                    this.shelvesInStoreStateMediatorLiveData.setValue(shelvesInStoreStateMediatorLiveData);
                });

    }

}

package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;

import com.ami.abc.ami_app.models.Asset;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.DynamicFieldAdapter;
import com.ami.abc.ami_app.models.AssetCheck;
import com.ami.abc.ami_app.models.AssetCheckInOutDetails;
import com.ami.abc.ami_app.models.AssetCheckInReport;
import com.ami.abc.ami_app.models.AssetCheckPoint;
import com.ami.abc.ami_app.models.AssetCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.AssetCheckInViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetCheckInActivity extends AppCompatActivity {

    @BindView(R.id.asset_id)
    EditText asset_code;

    String building, room, depart, assetId;

    AssetCheckInViewModel assetCheckInViewModel;
    String accessToken;

    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.location)
    TextView location;


    @BindView(R.id.custodian)
    TextView custodian;
    @BindView(R.id.holder_layout)
    LinearLayout holder_layout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.check_in)
    Button checkIn;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    LinearLayout detailsLayout;

    @BindView(R.id.dynamic_fields_layout)
    LinearLayout dynamicFieldsLayout;

    @BindView(R.id.milage)
    TextInputEditText milage;

    @BindView(R.id.fuel)
    TextInputEditText fuel;


    static RecyclerView.LayoutManager layoutManager;
    DynamicFieldAdapter dynamicFieldAdapter;

    List<String> dynamicFieldValues = new ArrayList();

    TextView dynamicOne;


    String code;
    @BindView(R.id.dynamic_fields_title)
    TextView dynamicsTitle;
    List<String> dynamicfieldList = new ArrayList();

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_check_in);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Check In");


        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");

        assetCheckInViewModel = ViewModelProviders.of(this).get(AssetCheckInViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        asset_code.requestFocus();

        if (accessToken != null && assetId != null) {

            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);

            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);

            assetCheckInViewModel.assetCheck("Bearer " + accessToken);
           // assetCheckInViewModel.getAssetCheckInOutDetails("Bearer "+ accessToken, assetId);

        }

        confirm.setOnClickListener(v -> {
            code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                assetCheckInViewModel.assetCheck("Bearer " + accessToken);
            }
        });

        assetCheckInViewModel.assetCheckInOutDetails().observe(this, assetCheckInOutDetailsState -> {
            assert assetCheckInOutDetailsState != null;
            if (assetCheckInOutDetailsState.getAssetCheckInOutDetails() != null) {
                handleAssetCheckInOutDetails(assetCheckInOutDetailsState.getAssetCheckInOutDetails());
            }
            if (assetCheckInOutDetailsState.getMessage() != null) {
                handleNetworkResponse(assetCheckInOutDetailsState.getMessage());
            }
            if (assetCheckInOutDetailsState.getErrorThrowable() != null) {
                handleError(assetCheckInOutDetailsState.getErrorThrowable());
            }

        });
        assetCheckInViewModel.assetCheckResponse().observe(this, assetCheckState -> {
            assert assetCheckState != null;
            if (assetCheckState.getAssetCheckPoint() != null) {
                handleAssetCheck(assetCheckState.getAssetCheckPoint());
            }
            if (assetCheckState.getMessage() != null) {
                handleNetworkResponse(assetCheckState.getMessage());
            }
            if (assetCheckState.getErrorThrowable() != null) {
                handleError(assetCheckState.getErrorThrowable());
            }
            if (assetCheckState.getLoginError() != null) {
                handleUnAuthorized(assetCheckState.getLoginError());
            }
        });


        if (assetId != null) {
            asset_code.setText(assetId);
        } else {
            asset_code.setText(code);
        }

        assetCheckInViewModel.checkInAssetResponse().observe(this, assetCheckedInPointState -> {
            assert assetCheckedInPointState != null;
            if (assetCheckedInPointState.getAssetCheckedInPoint() != null) {
                handleAssetCheckIn(assetCheckedInPointState.getAssetCheckedInPoint());
            }
            if (assetCheckedInPointState.getMessage() != null) {
                handleNetworkResponse(assetCheckedInPointState.getMessage());
            }
            if (assetCheckedInPointState.getErrorThrowable() != null) {
                handleError(assetCheckedInPointState.getErrorThrowable());
            }
            if (assetCheckedInPointState.getLoginError() != null) {
                handleUnAuthorized(assetCheckedInPointState.getLoginError());
            }
        });


        checkIn.setOnClickListener(v -> {
            String itemCode = asset_code.getText().toString().trim();
            String mMileage = milage.getText().toString().trim();
            String mFuel = fuel.getText().toString().trim();

            if(TextUtils.isEmpty(itemCode)){
                Toast.makeText(this, "Code is empty", Toast.LENGTH_SHORT).show();
            }else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                assetCheckInViewModel.checkInAsset("Bearer " + accessToken, itemCode,mMileage,mFuel);
            }
        });





    }

    private void handleAssetCheckInOutDetails(AssetCheckInOutDetails assetCheckInOutDetails) {

        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = assetCheckInOutDetails.getStatus();
        if (status) {
            Asset asset = assetCheckInOutDetails.getAsset();

            dynamicfieldList = asset.getDynamicfields();
            if(asset.getCategory().getType().equalsIgnoreCase("motor vehicle") ||
                    asset.getCategory().getType().equalsIgnoreCase("motor vehicles") ||
                    asset.getCategory().getType().equalsIgnoreCase("motorvehicles")){
                        dynamicFieldsLayout.setVisibility(View.VISIBLE);


            }

        }

    }

    private void handleAssetCheckIn(AssetCheckedInPoint assetCheckedInPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = assetCheckedInPoint.getStatus();
        if (status) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(assetCheckedInPoint.getMessage());
            alert.setCancelable(false);
            alert.setMessage("Do You want to continue checking in ? ");

            alert.setNeutralButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNegativeButton("Scanner", (dialog, which) -> {
                Intent scanAsset = new Intent(this, AssetCheckInActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "J");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "J");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();

        }

    }

    private void handleAssetCheck(AssetCheckPoint assetCheckPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);



        boolean status = assetCheckPoint.getStatus();
        if (status) {
            List<AssetCheck> assetsChecked = assetCheckPoint.getAssetcheck();
            code = asset_code.getText().toString().trim();


            for (AssetCheck assetCheck : assetsChecked) {
                if (assetCheck.getAsset().getCode().equals(code) && assetCheck.getCheckedOut() == 1) {
                    custodian.setText(assetCheck.getAsset().getCust());
                    assetName.setText(assetCheck.getAsset().getName());
                    location.setText(String.format("%s, %s", assetCheck.getAsset().getBuilding(), assetCheck.getAsset().getRoom()));
                    assetCheckInViewModel.getAssetCheckInOutDetails("Bearer "+ accessToken, code);

                    AssetCheckInReport assetsReports = new AssetCheckInReport();
                    assetsReports.setId(assetCheck.getId());
                    assetsReports.setBarcode(assetId);
                    assetsReports.setName(assetCheck.getAsset().getName());
                    assetsReports.setLocation(building + ", " + room);
                    assetCheckInViewModel.saveAssets(assetsReports);

                    return;


                }
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("The asset you scanned was not checked out.. ");
            // prevent cancel of AlertDialog on click of back button and outside touch
            alert.setCancelable(false);
            alert.setMessage("Do you want to scan again ?");

            alert.setNeutralButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNegativeButton("Scanner", (dialog, which) -> {
                Intent scanAsset = new Intent(this, AssetCheckInActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "J");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "J");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();


        }
    }

    private void handleNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }


    }

    private void handleUnAuthorized(LoginError loginError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        Toast.makeText(this, loginError.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, AssetsOptions.class);
        startActivity(loc);
    }
}

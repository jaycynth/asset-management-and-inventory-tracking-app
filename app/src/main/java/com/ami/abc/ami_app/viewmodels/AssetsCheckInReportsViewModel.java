package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.AssetCheckInReport;
import com.ami.abc.ami_app.repos.AssetCheckInReportsRepo;

import java.util.List;

public class AssetsCheckInReportsViewModel extends AndroidViewModel {

    private AssetCheckInReportsRepo assetCheckInReportsRepo;

    private LiveData<List<AssetCheckInReport>> listLiveData;



    public AssetsCheckInReportsViewModel(Application application) {
        super(application);

        assetCheckInReportsRepo = new AssetCheckInReportsRepo();

        listLiveData = assetCheckInReportsRepo.getAssetsReports();
    }

    public LiveData<List<AssetCheckInReport>> getListLiveData() {
        return listLiveData;
    }



    //save individual assets scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(AssetCheckInReport assetInReport) {
        assetCheckInReportsRepo.saveAssets(assetInReport);
    }

}

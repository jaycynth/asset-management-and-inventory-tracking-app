package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.CurrentAccountType;
import com.ami.abc.ami_app.models.LoginError;

public class CurrentAccountTypeState {

    private CurrentAccountType currentAccountType;
    private String message;
    private Throwable throwable;
    private LoginError loginError;

    public CurrentAccountTypeState(CurrentAccountType currentAccountType) {
        this.currentAccountType = currentAccountType;
        this.message = null;
        this.throwable = null;
        this.loginError = null;
    }

    public CurrentAccountTypeState(String message) {
        this.message = message;
        this.currentAccountType = null;
        this.throwable = null;
        this.loginError = null;
    }

    public CurrentAccountTypeState(Throwable throwable) {
        this.throwable = throwable;
        this.message = null;
        this.currentAccountType = null;
        this.loginError = null;
    }

    public CurrentAccountTypeState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.currentAccountType = null;
        this.throwable = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public CurrentAccountType getCurrentAccountType() {
        return currentAccountType;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}

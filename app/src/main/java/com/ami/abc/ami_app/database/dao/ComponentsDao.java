package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.CacheComponents;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ComponentsDao {
    @Insert(onConflict = REPLACE)
    void saveComponent(CacheComponents cacheComponents);

    @Query("SELECT * FROM CacheComponents")
    LiveData<List<CacheComponents>> getComponents();

    @Query("SELECT * FROM CacheComponents")
    List<CacheComponents> getAll();

    @Query("DELETE FROM CacheComponents")
    void deleteAllComponents();
}

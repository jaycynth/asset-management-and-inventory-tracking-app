package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.AssetCheckOutReportDao;
import com.ami.abc.ami_app.models.AssetCheckOutReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AssetCheckOutReportsRepo {
    private AssetCheckOutReportDao assetsInReportDao;
    private Executor executor;

    public AssetCheckOutReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        assetsInReportDao = AppDatabase.getDatabase(AMI.context).assetCheckOutReportDao();

    }


    public void saveAssets(AssetCheckOutReport assetCheckReport) {
        executor.execute(() -> assetsInReportDao.saveAsset(assetCheckReport));
    }


    public LiveData<List<AssetCheckOutReport>> getAssetsReports() {
        return assetsInReportDao.getAssetsReports();
    }



    public void deleteAssets() {
        executor.execute(() -> assetsInReportDao.deleteAssets());
    }
}

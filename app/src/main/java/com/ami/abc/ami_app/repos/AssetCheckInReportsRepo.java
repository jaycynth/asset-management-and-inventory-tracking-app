package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.database.AppDatabase;
import com.ami.abc.ami_app.database.dao.AssetCheckInReportDao;
import com.ami.abc.ami_app.models.AssetCheckInReport;
import com.ami.abc.ami_app.utils.AMI;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AssetCheckInReportsRepo {
    private AssetCheckInReportDao assetCheckInReportDao;
    private Executor executor;

    public AssetCheckInReportsRepo() {
        executor = Executors.newSingleThreadExecutor();
        assetCheckInReportDao = AppDatabase.getDatabase(AMI.context).assetCheckInReportDao();

    }


    public void saveAssets(AssetCheckInReport assetCheckReport) {
        executor.execute(() -> assetCheckInReportDao.saveAsset(assetCheckReport));
    }


    public LiveData<List<AssetCheckInReport>> getAssetsReports() {
        return assetCheckInReportDao.getAssetsReports();
    }



    public void deleteAssets() {
        executor.execute(() -> assetCheckInReportDao.deleteAssets());
    }
}

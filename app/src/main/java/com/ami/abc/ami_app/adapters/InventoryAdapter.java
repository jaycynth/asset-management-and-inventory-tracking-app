package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Inventory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.MyViewHolder> {

    private List<Inventory> assetList;
    private Context context;

    public InventoryAdapter(List<Inventory> assetList, Context context) {
        this.assetList = assetList;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.location)
        TextView location;

        @BindView(R.id.count)
        TextView count;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }


    }

    @NonNull
    @Override
    public InventoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_search_layout, parent, false);


        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull InventoryAdapter.MyViewHolder holder, int i) {
        Inventory asset = assetList.get(i);
        holder.asset_code.setText(asset.getProductCode());
        holder.name.setText(String.format("Item Name : %s", asset.getProductName()));
        holder.location.setText(String.format("Location : %s, %s", asset.getStore(), asset.getShelf()));
        holder.count.setText(String.format("Quantity : %s", String.valueOf(asset.getQuantity())));


    }

    @Override
    public int getItemCount() {
        return assetList.size();
    }

    public void updateList(List<Inventory> newList) {
        //reinitialie the list
        assetList = new ArrayList<>();
        assetList.addAll(newList);
        notifyDataSetChanged();
    }

}

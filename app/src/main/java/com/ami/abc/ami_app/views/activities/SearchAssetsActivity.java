package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.AssetsAdapter;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.ui.ItemClickListener;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.AssetViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAssetsActivity extends AppCompatActivity {

    @BindView(R.id.search)
    SearchView searchView;

    @BindView(R.id.all_verified_assets)
    RecyclerView all_verified_assets;

    static RecyclerView.LayoutManager layoutManager;

    AssetsAdapter assetsAdapter;

    List<Asset> allVerifiedAssets = new ArrayList<>();

    AssetViewModel assetViewModel;

    String accessToken;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.no_results_layout)
    RelativeLayout noResultsLayout;

    @BindView(R.id.results_layout)
    RelativeLayout resultsLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_assets);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.search));


        assetViewModel = ViewModelProviders.of(this).get(AssetViewModel.class);
        assetViewModel.getAllAssetResponse().observe(this, assetState -> {
            assert assetState != null;
            if (assetState.getAllAssets() != null) {
                handleAssets(assetState.getAllAssets());
            }

            if (assetState.getMessage() != null) {
                handleNetworkResponse(assetState.getMessage());
            }

            if (assetState.getErrorThrowable() != null) {
                handleError(assetState.getErrorThrowable());
            }
            if (assetState.getLoginError() != null) {
                handleUnAuthorized(assetState.getLoginError());
            }
        });

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            all_verified_assets.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            assetViewModel.getAllAssets("Bearer " + accessToken);
        }

        searchView.setQueryHint(" Name or Barcode ");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String userInput = newText;
                List<Asset> newList = new ArrayList<>();
                if (!TextUtils.isEmpty(userInput)) {

                    resultsLayout.setVisibility(View.VISIBLE);
                    noResultsLayout.setVisibility(View.GONE);

                    if (!allVerifiedAssets.isEmpty()) {
                        for (Asset asset : allVerifiedAssets) {

                            if (asset.getName().contains(userInput) || asset.getCode().contains(userInput)) {
                                newList.add(asset);

                            }

                        }
                        if (newList.isEmpty()) {
                            resultsLayout.setVisibility(View.GONE);
                            noResultsLayout.setVisibility(View.VISIBLE);
                        } else {
                            initView(newList);
                        }
                    }

                } else {
                    all_verified_assets.setAdapter(null);


                }
                return true;
            }
        });

    }

    private void handleUnAuthorized(LoginError loginError) {
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }

    private void handleAssets(AllAssets allAssets) {
        progressBar.setVisibility(View.GONE);
        all_verified_assets.setVisibility(View.VISIBLE);

        boolean status = allAssets.getStatus();
        if (status) {
            allVerifiedAssets = allAssets.getData();
        }
    }

    private void initView(List<Asset> assetList) {
        assetsAdapter = new AssetsAdapter(assetList, this);
        layoutManager = new LinearLayoutManager(this);
        all_verified_assets.setLayoutManager(layoutManager);
        all_verified_assets.setItemAnimator(new DefaultItemAnimator());
        all_verified_assets.setAdapter(assetsAdapter);
        all_verified_assets.setNestedScrollingEnabled(false);


        all_verified_assets.addOnItemTouchListener(new ItemClickListener(this, all_verified_assets, new ItemClickListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent details = new Intent(SearchAssetsActivity.this, SearchAssetDetailsActivity.class);
                details.putExtra("assetCode", assetList.get(position).getCode());
                startActivity(details);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);
        all_verified_assets.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);
        all_verified_assets.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(this, AssetsOptions.class);
        startActivity(back);
    }
}

package com.ami.abc.ami_app.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AddAssetImageState;
import com.ami.abc.ami_app.datastates.AddAssetState;

import com.ami.abc.ami_app.datastates.AssetWithComponentsState;
import com.ami.abc.ami_app.datastates.CategoryState;
import com.ami.abc.ami_app.datastates.CurrentAccountTypeState;
import com.ami.abc.ami_app.datastates.CustodianState;
import com.ami.abc.ami_app.datastates.SubCategoriesState;
import com.ami.abc.ami_app.datastates.SupplierState;
import com.ami.abc.ami_app.models.AssetInReport;
import com.ami.abc.ami_app.models.CacheComponents;
import com.ami.abc.ami_app.repos.AddAssetImageRepo;
import com.ami.abc.ami_app.repos.AssetWithComponentRepo;
import com.ami.abc.ami_app.repos.AssetsInReportsRepo;
import com.ami.abc.ami_app.repos.CategoryRepo;
import com.ami.abc.ami_app.repos.ComponentsRepo;
import com.ami.abc.ami_app.repos.CurrentAccountTypeRepo;
import com.ami.abc.ami_app.repos.CustodianRepo;
import com.ami.abc.ami_app.repos.NewAssetRepo;
import com.ami.abc.ami_app.repos.SubCategoryRepo;
import com.ami.abc.ami_app.repos.SuppliersRepo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;


public class NewAssetViewModel extends AndroidViewModel {

    private MediatorLiveData<AddAssetState> addAssetStateMediatorLiveData;
    private NewAssetRepo newAssetRepo;

    private MediatorLiveData<SupplierState> supplierStateMediatorLiveData;
    private SuppliersRepo suppliersRepo;

    private MediatorLiveData<CategoryState> categoryStateMediatorLiveData;
    private CategoryRepo categoryRepo;

    private MediatorLiveData<CustodianState> custodianStateMediatorLiveData;
    private CustodianRepo custodianRepo;

    private MediatorLiveData<AddAssetImageState> addAssetImageStateMediatorLiveData;
    private AddAssetImageRepo addAssetImageRepo;

    private MediatorLiveData<SubCategoriesState> subCategoriesStateMediatorLiveData;
    private SubCategoryRepo subCategoryRepo;

    private MediatorLiveData<CurrentAccountTypeState> currentAccountTypeStateMediatorLiveData;
    private CurrentAccountTypeRepo currentAccountTypeRepo;

    private MediatorLiveData<AssetWithComponentsState> assetWithComponentsMediatorLiveData;
    private AssetWithComponentRepo assetWithComponentRepo;

    private AssetsInReportsRepo assetsInReportsRepo;

    private ComponentsRepo componentsRepo;

    private LiveData<List<CacheComponents>> listLiveData;

    private List<CacheComponents> cacheComponentsList = new ArrayList<>();

    public NewAssetViewModel(Application application) {
        super(application);
        addAssetStateMediatorLiveData = new MediatorLiveData<>();
        newAssetRepo = new NewAssetRepo(application);

        supplierStateMediatorLiveData = new MediatorLiveData<>();
        suppliersRepo = new SuppliersRepo(application);

        categoryStateMediatorLiveData = new MediatorLiveData<>();
        categoryRepo = new CategoryRepo(application);

        custodianStateMediatorLiveData = new MediatorLiveData<>();
        custodianRepo = new CustodianRepo(application);

        addAssetImageStateMediatorLiveData = new MediatorLiveData<>();
        addAssetImageRepo = new AddAssetImageRepo(application);

        subCategoriesStateMediatorLiveData = new MediatorLiveData<>();
        subCategoryRepo = new SubCategoryRepo(application);

        currentAccountTypeStateMediatorLiveData = new MediatorLiveData<>();
        currentAccountTypeRepo = new CurrentAccountTypeRepo(application);

        assetWithComponentsMediatorLiveData = new MediatorLiveData<>();
        assetWithComponentRepo = new AssetWithComponentRepo(application);

        assetsInReportsRepo = new AssetsInReportsRepo();

        componentsRepo = new ComponentsRepo();

        listLiveData = componentsRepo.getComponents();
    }

    public LiveData<AssetWithComponentsState> postAssetWithComponentsResponse() {
        return assetWithComponentsMediatorLiveData;
    }

    public void postAssetWithComponents(String accessToken, RequestBody requestBody) {

        LiveData<AssetWithComponentsState> addAssetStateLiveData = assetWithComponentRepo.postAssetWithComponents(accessToken, requestBody);
        assetWithComponentsMediatorLiveData.addSource(addAssetStateLiveData,
                assetWithComponentsMediatorLiveData -> {
                    if (this.assetWithComponentsMediatorLiveData.hasActiveObservers()) {
                        this.assetWithComponentsMediatorLiveData.removeSource(addAssetStateLiveData);
                    }
                    this.assetWithComponentsMediatorLiveData.setValue(assetWithComponentsMediatorLiveData);
                });

    }

    public LiveData<AddAssetState> getPostNewAssetResponse() {
        return addAssetStateMediatorLiveData;
    }

    public void postNewAsset(String accessToken,
                             String code,
                             String name,
                             String status,
                             String serial,
                             String model,
                             String building,
                             String room,
                             String division,
                             String supplier,
                             String date_of_purchase,
                             String purchase_cost,
                             String est_end_of_life_cost,
                             String depart,
                             String cust,
                             String asset_category,
                             int sub_category_id,
                             String fCost, String iDuties, String nRefundTax, String sCost, String dCost,
                             String breakdownPurchaseCost,
                             Boolean breakdown) {

        LiveData<AddAssetState> addAssetStateLiveData = newAssetRepo.postNewAsset(accessToken, code, name, status, serial, model, building, room, division, supplier, date_of_purchase, purchase_cost, est_end_of_life_cost,
                depart, cust, asset_category, sub_category_id, fCost, iDuties, nRefundTax, sCost, dCost, breakdownPurchaseCost, breakdown);
        addAssetStateMediatorLiveData.addSource(addAssetStateLiveData,
                addAssetStateMediatorLiveData -> {
                    if (this.addAssetStateMediatorLiveData.hasActiveObservers()) {
                        this.addAssetStateMediatorLiveData.removeSource(addAssetStateLiveData);
                    }
                    this.addAssetStateMediatorLiveData.setValue(addAssetStateMediatorLiveData);
                });

    }

    public LiveData<SupplierState> getAllSuppliersResponse() {
        return supplierStateMediatorLiveData;
    }

    public void getAllSupliers(String accessToken) {
        LiveData<SupplierState> supplierStateLiveData = suppliersRepo.getAllSuppliers(accessToken);
        supplierStateMediatorLiveData.addSource(supplierStateLiveData,
                supplierStateMediatorLiveData -> {
                    if (this.supplierStateMediatorLiveData.hasActiveObservers()) {
                        this.supplierStateMediatorLiveData.removeSource(supplierStateLiveData);
                    }
                    this.supplierStateMediatorLiveData.setValue(supplierStateMediatorLiveData);
                });
    }

    public LiveData<CurrentAccountTypeState> getAccountCurrentType() {
        return currentAccountTypeStateMediatorLiveData;
    }

    public void getCurrentAccountType(String accessToken) {
        LiveData<CurrentAccountTypeState> currentAccountTypeStateLiveData = currentAccountTypeRepo.getCurrentAccountType(accessToken);
        currentAccountTypeStateMediatorLiveData.addSource(currentAccountTypeStateLiveData,
                currentAccountTypeStateMediatorLiveData -> {
                    if (this.currentAccountTypeStateMediatorLiveData.hasActiveObservers()) {
                        this.currentAccountTypeStateMediatorLiveData.removeSource(currentAccountTypeStateLiveData);
                    }
                    this.currentAccountTypeStateMediatorLiveData.setValue(currentAccountTypeStateMediatorLiveData);
                });
    }


    public LiveData<CategoryState> getAllCategoryResponse() {
        return categoryStateMediatorLiveData;
    }

    public void getAllCategories(String accessToken) {
        LiveData<CategoryState> categoryStateLiveData = categoryRepo.getAllCategories(accessToken);
        categoryStateMediatorLiveData.addSource(categoryStateLiveData,
                categoryStateMediatorLiveData -> {
                    if (this.categoryStateMediatorLiveData.hasActiveObservers()) {
                        this.categoryStateMediatorLiveData.removeSource(categoryStateLiveData);
                    }
                    this.categoryStateMediatorLiveData.setValue(categoryStateMediatorLiveData);
                });
    }

    public LiveData<CustodianState> getAllCustodiansResponse() {
        return custodianStateMediatorLiveData;
    }

    public void getAllCustodians(String accessToken) {
        LiveData<CustodianState> custodianStateLiveData = custodianRepo.getAllCustodians(accessToken);
        custodianStateMediatorLiveData.addSource(custodianStateLiveData,
                custodianStateMediatorLiveData -> {
                    if (this.custodianStateMediatorLiveData.hasActiveObservers()) {
                        this.custodianStateMediatorLiveData.removeSource(custodianStateLiveData);
                    }
                    this.custodianStateMediatorLiveData.setValue(custodianStateMediatorLiveData);
                });
    }

    public LiveData<AddAssetImageState> addAssetImage() {
        return addAssetImageStateMediatorLiveData;
    }

    public void postAddAssetImage(String accessToken, File photo, int assetId) {
        LiveData<AddAssetImageState> addAssetImageStateLiveData = addAssetImageRepo.postAddAssetImage(accessToken, photo, assetId);
        addAssetImageStateMediatorLiveData.addSource(addAssetImageStateLiveData,
                addAssetImageStateMediatorLiveData -> {
                    if (this.addAssetImageStateMediatorLiveData.hasActiveObservers()) {
                        this.addAssetImageStateMediatorLiveData.removeSource(addAssetImageStateLiveData);
                    }
                    this.addAssetImageStateMediatorLiveData.setValue(addAssetImageStateMediatorLiveData);
                });

    }

    //for sub categories
    public LiveData<SubCategoriesState> subCategoriesResponse() {
        return subCategoriesStateMediatorLiveData;
    }

    public void getAllSubCategories(String accessToken) {
        LiveData<SubCategoriesState> subCategoriesStateLiveData = subCategoryRepo.getAllSubCategories(accessToken);

        subCategoriesStateMediatorLiveData.addSource(subCategoriesStateLiveData,
                subCategoriesStateMediatorLiveData -> {
                    if (this.subCategoriesStateMediatorLiveData.hasActiveObservers()) {
                        this.subCategoriesStateMediatorLiveData.removeSource(subCategoriesStateLiveData);
                    }
                    this.subCategoriesStateMediatorLiveData.setValue(subCategoriesStateMediatorLiveData);
                });
    }


    //save individual assets scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(AssetInReport assetInReport) {
        assetsInReportsRepo.saveAssets(assetInReport);
    }

    public void saveComponents(CacheComponents cacheComponents) {
        componentsRepo.saveComponents(cacheComponents);
    }

    public void deleteAllComponents() {
        componentsRepo.deleteAllComponents();
    }

    public LiveData<List<CacheComponents>> getComponentsListLiveData() {
        return listLiveData;
    }

    public List<CacheComponents> getAllComponents() {
        cacheComponentsList = componentsRepo.getAll();
        return cacheComponentsList;
    }

}

package com.ami.abc.ami_app.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Entity
public class CacheComponents {

    @NonNull
    @PrimaryKey
    private int id;

    @SerializedName("at_cost")
    @Expose
    private String atCost;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("date_of_purchase")
    @Expose
    private String dateOfPurchase;
    @SerializedName("lifespan")
    @Expose
    private String lifespan;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("scrap_value")
    @Expose
    private String scrapValue;
    @SerializedName("serial")
    @Expose
    private String serial;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("supplier")
    @Expose
    private String supplier;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAtCost() {
        return atCost;
    }

    public void setAtCost(String atCost) {
        this.atCost = atCost;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getLifespan() {
        return lifespan;
    }

    public void setLifespan(String lifespan) {
        this.lifespan = lifespan;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScrapValue() {
        return scrapValue;
    }

    public void setScrapValue(String scrapValue) {
        this.scrapValue = scrapValue;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }


}

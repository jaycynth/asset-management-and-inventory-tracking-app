package com.ami.abc.ami_app.api;

import android.content.Context;
import android.content.Intent;


import com.ami.abc.ami_app.BuildConfig;

import com.ami.abc.ami_app.views.activities.LoginActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ami.abc.ami_app.api.ApiEndPoints.BASE_URL;

public class ApiClient {
    private Context context;


    public ApiClient(Context context) {
        this.context = context;
    }


    //Configure OkHttpClient
    private OkHttpClient.Builder okHttpClient() {


        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

        okHttpClient.connectTimeout(2, TimeUnit.MINUTES);
        okHttpClient.readTimeout(7, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(2, TimeUnit.MINUTES);


        okHttpClient.addInterceptor(chain -> {

            Request request = chain.request();
            Response response = chain.proceed(request);

            //re-direct user to login
            if (response.code() == 401 || response.code() == 500) {
                Intent logoutIntent = new Intent(context, LoginActivity.class);
                logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(logoutIntent);

                return response;
            }
            return response;
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(logging);
        }


        return okHttpClient;
    }


    private Retrofit getClient() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BASE_URL);
        builder.addConverterFactory(GsonConverterFactory.create(gson));
        OkHttpClient.Builder okhttpBuilder = okHttpClient();
        builder.client(okhttpBuilder.build());

        return builder.build();
    }

    //login
    public ApiService loginApiService() {
        return getClient().create(ApiService.class);
    }

    //get all buildings
    public ApiService getAllBuildingsService() {
        return getClient().create(ApiService.class);
    }

    //get all rooms
    public ApiService getAllRoomsService() {
        return getClient().create(ApiService.class);
    }

    //get all departments
    public ApiService getAllDepartmentsService() {
        return getClient().create(ApiService.class);
    }

    //post asset
    public ApiService postAssetService() {
        return getClient().create(ApiService.class);
    }

    //get assets
    public ApiService getAllAssetService() {
        return getClient().create(ApiService.class);
    }

    public ApiService amiService() {
        return getClient().create(ApiService.class);
    }


}

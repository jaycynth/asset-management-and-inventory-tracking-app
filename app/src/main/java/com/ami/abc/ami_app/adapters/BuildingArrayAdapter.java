package com.ami.abc.ami_app.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Building;
import com.ami.abc.ami_app.models.Department;

import java.util.ArrayList;
import java.util.List;

public class BuildingArrayAdapter extends ArrayAdapter<Building> {
    private List<Building> buildingsListFull;
    private List<Building> filteredBuildingList = new ArrayList<>();

    public BuildingArrayAdapter(@NonNull Context context, @NonNull List<Building> buildingList) {
        super(context, 0, buildingList);
       this.buildingsListFull = buildingList;
    }

    @Override
    public Building getItem(int position) {
        return filteredBuildingList.get(position);
    }

    @Override
    public int getCount() {
        return filteredBuildingList.size();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return buildingFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_array_layout, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.value);

        Building buildings = getItem(position);

        if (buildings != null) {
            textViewName.setText(buildings.getName());
        }

        return convertView;
    }

    private Filter buildingFilter = new Filter() {

        List<Building> suggestions = new ArrayList<>();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(buildingsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Building item : buildingsListFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredBuildingList.clear();
            if (results.count > 0) {
                filteredBuildingList.addAll((List) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Building) resultValue).getName();
        }
    };
}

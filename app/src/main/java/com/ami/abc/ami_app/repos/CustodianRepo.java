package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.CustodianState;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustodianRepo {

    private ApiClient mApiClient;

    //constructor
    public CustodianRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<CustodianState> getAllCustodians(String accessToken) {

        MutableLiveData<CustodianState> custodianStateMutableLiveData = new MutableLiveData<>();
        Call<AllCustodians> call = mApiClient.amiService().getAllCustodians(accessToken);
        call.enqueue(new Callback<AllCustodians>() {
            @Override
            public void onResponse(Call<AllCustodians> call, Response<AllCustodians> response) {
                if (response.code() == 200) {
                    custodianStateMutableLiveData.setValue(new CustodianState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    custodianStateMutableLiveData.setValue(new CustodianState(loginError));
                }
                else {
                    custodianStateMutableLiveData.setValue(new CustodianState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<AllCustodians> call, Throwable t) {
                custodianStateMutableLiveData.setValue(new CustodianState(t));
            }
        });

        return custodianStateMutableLiveData;

    }
}

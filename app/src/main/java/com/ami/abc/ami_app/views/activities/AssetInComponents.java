package com.ami.abc.ami_app.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllCategories;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.AssetCategory;
import com.ami.abc.ami_app.models.CurrentAccountType;
import com.ami.abc.ami_app.models.Custodian;
import com.ami.abc.ami_app.models.GetSubCategory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.SubCategory;
import com.ami.abc.ami_app.models.Supplier;
import com.ami.abc.ami_app.utils.AssetStatusData;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.NewAssetViewModel;
import com.ami.abc.ami_app.views.fragments.BreakDownCostDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetInComponents extends AppCompatActivity {

    @BindView(R.id.asset_code_layout)
    TextInputLayout asset_code_layout;
    @BindView(R.id.asset_id)
    TextInputEditText asset_code;

    @BindView(R.id.asset_name)
    TextInputEditText asset_name;
    @BindView(R.id.asset_name_layout)
    TextInputLayout asset_name_layout;

    @BindView(R.id.status)
    AutoCompleteTextView nStatus;
    @BindView(R.id.status_layout)
    TextInputLayout status_layout;
    @BindView(R.id.status_drop_down)
    ImageView status_drop_down;

    @BindView(R.id.date)
    TextInputEditText mDate;
    @BindView(R.id.date_layout)
    TextInputLayout mDateLayout;

    @BindView(R.id.serial_number)
    TextInputEditText serial_number;
    @BindView(R.id.serial_number_layout)
    TextInputLayout serial_number_layout;

    @BindView(R.id.model)
    TextInputEditText nModel;
    @BindView(R.id.model_layout)
    TextInputLayout model_layout;


    @BindView(R.id.end_estimated_cost)
    TextInputEditText end_estimated_cost;
    @BindView(R.id.end_estimated_cost_layout)
    TextInputLayout end_estimated_cost_layout;

    @BindView(R.id.proceed_to_add_components)
    Button proceed_to_add_components;

    List<AssetCategory> categoryList = new ArrayList<>();
    private List<String> categoryName = new ArrayList<>();
    @BindView(R.id.asset_category)
    AutoCompleteTextView category;
    @BindView(R.id.category_layout)
    TextInputLayout category_layout;
    @BindView(R.id.category_drop_down)
    ImageView category_drop_down;

    List<SubCategory> subCategoryList = new ArrayList<>();
    List<String> subCategoryName = new ArrayList<>();
    @BindView(R.id.sub_asset_category)
    AutoCompleteTextView subCategory;
    @BindView(R.id.sub_category_layout)
    TextInputLayout sub_category_layout;
    @BindView(R.id.sub_category_drop_down)
    ImageView sub_category_drop_down;

    private List<String> custodianName = new ArrayList<>();
    @BindView(R.id.custodian)
    AutoCompleteTextView custodian;
    @BindView(R.id.custodian_layout)
    TextInputLayout custodian_layout;
    @BindView(R.id.custodian_drop_down)
    ImageView custodian_drop_down;


    private List<String> supplierName = new ArrayList<>();
    @BindView(R.id.supplier)
    AutoCompleteTextView nSupplier;
    @BindView(R.id.supplier_layout)
    TextInputLayout supplier_layout;
    @BindView(R.id.supplier_drop_down)
    ImageView supplier_drop_down;

    String building, room, depart, assetId, division;

    private NewAssetViewModel newAssetViewModel;
    private String accessToken;

    int categoryId, subCategoryId;

    //fields for the additional breakdown cost
    String freightCost, importDuties, nonRefundableTax, setUpCost, dismantlingCost, breakdownPurchaseCost;
    Boolean breakdown = false;

    //track if asset is already uploaded per transaction or not
    Boolean assetUpdateStatus = false;

    @BindView(R.id.break_down_switch)
    Switch breakDownSwitch;

    @BindView(R.id.at_cost_layout)
    TextInputLayout at_cost_layout;
    @BindView(R.id.at_cost)
    TextInputEditText at_cost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_in_components);

        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.asset_components_upload));

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        division = scanIntents.getStringExtra("division");
        assetId = scanIntents.getStringExtra("scanResult");

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            mDate.setText(sdf.format(myCalendar.getTime()));
        };

        mDate.setOnClickListener(v -> new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        newAssetViewModel = ViewModelProviders.of(this).get(NewAssetViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null) {
            newAssetViewModel.getCurrentAccountType("Bearer " + accessToken);
            newAssetViewModel.getAllSupliers("Bearer " + accessToken);
            newAssetViewModel.getAllCategories("Bearer " + accessToken);
            newAssetViewModel.getAllCustodians("Bearer " + accessToken);
        }

        newAssetViewModel.getAllSuppliersResponse().observe(this, supplierState -> {
            assert supplierState != null;
            if (supplierState.getAllSuppliers() != null) {
                handleSuppliers(supplierState.getAllSuppliers());
            }
            if (supplierState.getMessage() != null) {
                handleNetworkResponse(supplierState.getMessage());
            }
            if (supplierState.getErrorThrowable() != null) {
                handleError(supplierState.getErrorThrowable());
            }
            if (supplierState.getLoginError() != null) {
                handleUnAuthorized(supplierState.getLoginError());
            }
        });


        newAssetViewModel.getAllCategoryResponse().observe(this, categoryState -> {
            assert categoryState != null;
            if (categoryState.getAllCategories() != null) {
                handleCategories(categoryState.getAllCategories());
            }
            if (categoryState.getMessage() != null) {
                handleNetworkResponse(categoryState.getMessage());
            }
            if (categoryState.getErrorThrowable() != null) {
                handleError(categoryState.getErrorThrowable());
            }
            if (categoryState.getLoginError() != null) {
                handleUnAuthorized(categoryState.getLoginError());
            }
        });


        newAssetViewModel.subCategoriesResponse().observe(this, subCategoriesState -> {
            if (subCategoriesState.getGetSubCategory() != null) {
                handleSubCategories(subCategoriesState.getGetSubCategory());
            }
            if (subCategoriesState.getMessage() != null) {
                handleSubCategoriesNetworkResponse(subCategoriesState.getMessage());
            }
            if (subCategoriesState.getThrowable() != null) {
                handleSubCategoriesError(subCategoriesState.getThrowable());
            }
        });


        newAssetViewModel.getAllCustodiansResponse().observe(this, custodianState -> {
            assert custodianState != null;
            if (custodianState.getAllCustodians() != null) {
                handleCustodians(custodianState.getAllCustodians());
            }
            if (custodianState.getMessage() != null) {
                handleNetworkResponse(custodianState.getMessage());
            }
            if (custodianState.getErrorThrowable() != null) {
                handleError(custodianState.getErrorThrowable());
            }
            if (custodianState.getLoginError() != null) {
                handleUnAuthorized(custodianState.getLoginError());
            }
        });

        newAssetViewModel.getAccountCurrentType().observe(this, currentAccountTypeState -> {
            if (currentAccountTypeState.getCurrentAccountType() != null) {
                getCurrentAccountType(currentAccountTypeState.getCurrentAccountType());
            }
            if (currentAccountTypeState.getMessage() != null) {
                handleNetworkResponse(currentAccountTypeState.getMessage());
            }
            if (currentAccountTypeState.getThrowable() != null) {
                handleError(currentAccountTypeState.getThrowable());
            }
            if (currentAccountTypeState.getLoginError() != null) {
                handleUnAuthorized(currentAccountTypeState.getLoginError());
            }
        });

        purchaseCostBreakdown();

        //set up drop down for status;
        List<String> statusName = AssetStatusData.getAssetStatuses();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, statusName);
        nStatus.setAdapter(adapter1);

        status_drop_down.setOnClickListener(v -> nStatus.showDropDown());

        nStatus.setOnClickListener(v -> nStatus.showDropDown());

        //set up custodian drop down
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, custodianName);
        custodian.setAdapter(adapter2);

        custodian_drop_down.setOnClickListener(v -> custodian.showDropDown());
        custodian.setOnClickListener(v -> custodian.showDropDown());


        //set up category drop down
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, categoryName);
        category.setAdapter(adapter3);

        category_drop_down.setOnClickListener(v -> category.showDropDown());
        category.setOnClickListener(v -> category.showDropDown());

        category.setOnItemClickListener((parent, view, position, id) -> {
            subCategoryName.clear();
            String categoryName = (String) parent.getItemAtPosition(position);

            if (categoryList.get(position).getType().equalsIgnoreCase(categoryName)) {
                categoryId = categoryList.get(position).getId();
                if (categoryId == 0) {


                    subCategoryName.add("None");

                } else {

                    Toast.makeText(this, "Loading sub categories........", Toast.LENGTH_SHORT).show();
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    if (!subCategoryName.isEmpty()) {
                        subCategoryName.clear();
                    }

                    newAssetViewModel.getAllSubCategories("Bearer " + accessToken);
                }
            }
        });


        //set up sub category drop down linked to category drop down
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, subCategoryName);
        subCategory.setAdapter(adapter5);

        sub_category_drop_down.setOnClickListener(v -> subCategory.showDropDown());
        subCategory.setOnClickListener(v -> subCategory.showDropDown());

        subCategory.setOnItemClickListener((parent, view, position, id) -> subCategoryId = subCategoryList.get(position).getId());


        //set up supplier drop down
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, supplierName);
        nSupplier.setAdapter(adapter4);

        supplier_drop_down.setOnClickListener(v -> nSupplier.showDropDown());
        nSupplier.setOnClickListener(v -> nSupplier.showDropDown());


        proceed_to_add_components.setOnClickListener(v -> {
            String code = asset_code.getText().toString().trim();
            String name = asset_name.getText().toString().trim();
            String status = nStatus.getText().toString().trim();
            String date_of_purchase = mDate.getText().toString().trim();
            String serial = serial_number.getText().toString().trim();
            String model = nModel.getText().toString().trim();
            String est_end_of_life_cost = end_estimated_cost.getText().toString().trim();
            String asset_category = category.getText().toString().trim();
            String cust = custodian.getText().toString().trim();
            String supplier = nSupplier.getText().toString().trim();
            String atCost = at_cost.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                asset_name_layout.setError(getResources().getString(R.string.asset_name_error));
                asset_name.requestFocus();
                asset_name.findFocus();
            } else if (TextUtils.isEmpty(date_of_purchase)) {
                mDateLayout.setError(getResources().getString(R.string.date_error));
                mDate.requestFocus();
                mDate.findFocus();
            } else if (TextUtils.isEmpty(atCost)) {
                at_cost_layout.setError(getResources().getString(R.string.purchase_cost_error));
                at_cost.requestFocus();
                at_cost.findFocus();
            } else if (TextUtils.isEmpty(est_end_of_life_cost)) {
                end_estimated_cost_layout.setError(getResources().getString(R.string.end_cost_error));
                end_estimated_cost.requestFocus();
                end_estimated_cost.findFocus();
            } else if (TextUtils.isEmpty(asset_category)) {
                category_layout.setError(getResources().getString(R.string.category_error));
                category.requestFocus();
                category.findFocus();
            } else {
                Toast.makeText(this, "Asset details added successfully", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(AssetInComponents.this, ComponentsActivity.class);
                intent.putExtra("assetName", name);
                intent.putExtra("assetCode", code);
                intent.putExtra("assetCust", cust);
                intent.putExtra("assetScrapCost", est_end_of_life_cost);
                intent.putExtra("asseDatePurchase", date_of_purchase);
                intent.putExtra("assetCategory", asset_category);
                intent.putExtra("assetStatus", status);
                intent.putExtra("assetSerial", serial);
                intent.putExtra("assetModel", model);
                intent.putExtra("assetSupplier", supplier);
                intent.putExtra("assetAtCost", atCost);
                intent.putExtra("assetSubCategoryId", subCategoryId);
                intent.putExtra("building", building);
                intent.putExtra("room", room);
                intent.putExtra("division", division);
                intent.putExtra("depart", depart);
                intent.putExtra("assetFcost", freightCost);
                intent.putExtra("assetImportDuties", importDuties);
                intent.putExtra("assetDismantlingCost", dismantlingCost);
                intent.putExtra("assetNonRefundableTax", nonRefundableTax);
                intent.putExtra("assetSetUpCost", setUpCost);
                intent.putExtra("assetPurchaseCost", breakdownPurchaseCost);
                intent.putStringArrayListExtra("suppliers", (ArrayList<String>) supplierName);
                startActivity(intent);
            }
        });
    }

    private void purchaseCostBreakdown() {
        //listen for changes on the switch and click on the purchase cost field
        breakDownSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //hide keyboard and show breakdown dialog
                toggleKeyboard(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();

            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });

        at_cost.setOnClickListener(v -> {
            if (breakDownSwitch.isChecked()) {
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();

            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);

            }
        });
    }

    private void toggleKeyboard(int showImplicit, int i) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(showImplicit, i);
        }
    }

    /*opens a dialog that shows additional fields of breakdown of purchase cost*/
    private void breakDownCostDialog() {
        BreakDownCostDialogFragment breakDownCostDialogFragment = new BreakDownCostDialogFragment();
        breakDownCostDialogFragment.setListener((fCost, iDuties, nRefundTax, sCost, dCost, bPurchaseCost) -> {

            assetUpdateStatus = true;
            int totalPurchase = Integer.parseInt(fCost) + Integer.parseInt(iDuties) +
                    Integer.parseInt(nRefundTax) + Integer.parseInt(sCost) + Integer.parseInt(dCost) +
                    Integer.parseInt(bPurchaseCost);

            at_cost.setText(String.valueOf(totalPurchase));

            //enable breakdown when values are returned
            breakdown = true;

            freightCost = fCost;
            importDuties = iDuties;
            nonRefundableTax = nRefundTax;
            setUpCost = sCost;
            dismantlingCost = dCost;
            breakdownPurchaseCost = bPurchaseCost;
        });
        Bundle bundle = new Bundle();
        bundle.putBoolean("assetPostStatus", assetUpdateStatus);
        bundle.putString("freightCost", freightCost);
        bundle.putString("importDuties", importDuties);
        bundle.putString("nonRefundableTax", nonRefundableTax);
        bundle.putString("setupCost", setUpCost);
        bundle.putString("dismantlingCost", dismantlingCost);
        bundle.putString("breakdownPurchaseCost", breakdownPurchaseCost);
        breakDownCostDialogFragment.setArguments(bundle);
        breakDownCostDialogFragment.show(getSupportFragmentManager(), "dialog");
    }


    private void getCurrentAccountType(CurrentAccountType currentAccountType) {
        int status = currentAccountType.getAutomatic();

        if (status == 1) {
            asset_code.setFocusable(false);
            asset_code.setClickable(false);
            asset_code.setText(getString(R.string.automatic_text));
            asset_code.setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }


    private void handleCustodians(AllCustodians allCustodians) {
        boolean status = allCustodians.getStatus();
        if (status) {
            List<Custodian> custodianList = allCustodians.getCustodians();
            for (Custodian custodian : custodianList) {
                custodianName.add(custodian.getName());
            }
        }
    }

    private void handleCategories(AllCategories allCategories) {
        boolean status = allCategories.getStatus();
        if (status) {
            categoryList = allCategories.getAssetcategories();
            for (AssetCategory assetCategory : categoryList) {
                categoryName.add(assetCategory.getType());
            }
        }
    }

    private void handleError(Throwable errorThrowable) {
        throwableError(errorThrowable, getString(R.string.network_failure), getString(R.string.error_occured1), "conversion");
    }

    private void handleNetworkResponse(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    private void handleSuppliers(AllSuppliers allSuppliers) {
        boolean status = allSuppliers.getStatus();
        if (status) {
            List<Supplier> supplierList = allSuppliers.getSuppliers();
            for (Supplier supplier : supplierList) {
                supplierName.add(supplier.getName());
            }
        }
    }

    private void handleUnAuthorized(LoginError loginError) {
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
        Log.d("Unauthorized Error", loginError.getMessage());
    }


    //......method to handle sub categories
    private void handleSubCategories(GetSubCategory getSubCategory) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        subCategoryList = getSubCategory.getSubCategories();

        if (subCategoryList.isEmpty()) {
            subCategoryName.add("None");
        } else {
            for (SubCategory subCategory : subCategoryList) {
                if (subCategory.getItemtypeId() == categoryId) {

                    String mSubCategoryName = subCategory.getName();
                    subCategoryName.add(mSubCategoryName);
                } else {
                    subCategoryName.add("None");
                }
            }
        }
    }

    private void handleSubCategoriesError(Throwable throwable) {
        throwableError(throwable, "You are Currently Offline", "An Error Occured", "CONVERSION ERRORS");
    }

    private void handleSubCategoriesNetworkResponse(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    private void throwableError(Throwable throwable, String s, String s2, String s3) {
        if (throwable instanceof IOException) {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, s2, Toast.LENGTH_SHORT).show();
            Log.d(s3, Objects.requireNonNull(throwable.getMessage()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.InventoryCheckInReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface InventoryCheckInReportDao {
    @Insert(onConflict = REPLACE)
    void saveInventory(InventoryCheckInReport inventoryReports);
    //observe the list in the room database

    @Query("SELECT * FROM  InventoryCheckInReport")
    LiveData<List<InventoryCheckInReport>> getInventorysReports();

    @Query("SELECT * FROM InventoryCheckInReport")
    List<InventoryCheckInReport> getAll();

    @Delete
    void delete(InventoryCheckInReport inventoryReports);

    //delete all the inventory in the room database upon sending information to the inventory verified table
    @Query("DELETE FROM InventoryCheckInReport")
    void deleteInventorys();

}

package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddInventory {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("addedAsset")
    @Expose
    private AddedAsset addedAsset;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public AddedAsset getAddedAsset() {
        return addedAsset;
    }

    public void setAddedAsset(AddedAsset addedAsset) {
        this.addedAsset = addedAsset;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

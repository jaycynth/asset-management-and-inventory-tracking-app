package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AssetCheckOutReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetCheckOutReportsAdapter extends RecyclerView.Adapter<AssetCheckOutReportsAdapter.MyViewHolder> {
    private List<AssetCheckOutReport> assetInReportList;
    private Context context;

    public AssetCheckOutReportsAdapter(List<AssetCheckOutReport> assetInReportList, Context context) {
        this.assetInReportList = assetInReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.status)
        TextView status;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public AssetCheckOutReportsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.asset_check_reports_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetCheckOutReportsAdapter.MyViewHolder holder, int i) {
        AssetCheckOutReport assetScanned = assetInReportList.get(i);
        holder.asset_code.setText(assetScanned.getBarcode());
        holder.name.setText("Product Name : " + assetScanned.getName());
        holder.status.setText(assetScanned.getCustodian());


    }

    @Override
    public int getItemCount() {
        return assetInReportList.size();
    }


}

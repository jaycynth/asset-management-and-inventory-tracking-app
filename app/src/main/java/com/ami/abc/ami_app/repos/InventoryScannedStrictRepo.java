package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryScannedStrictState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryScannedStrict;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryScannedStrictRepo {
    private ApiClient mApiClient;



    //constructor
    public InventoryScannedStrictRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<InventoryScannedStrictState> getInventoryScannedStrict(String code, String store, String accessToken) {

        MutableLiveData<InventoryScannedStrictState> stockTakeStateMutableLiveData = new MutableLiveData<>();

        Call<InventoryScannedStrict> call = mApiClient.amiService().getInventoryScannedStrict(code,store, accessToken);
        call.enqueue(new Callback<InventoryScannedStrict>() {
            @Override
            public void onResponse(Call<InventoryScannedStrict> call, Response<InventoryScannedStrict> response) {
                if (response.code() == 200) {
                    stockTakeStateMutableLiveData.setValue(new InventoryScannedStrictState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    assert response.errorBody() != null;
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    stockTakeStateMutableLiveData.setValue(new InventoryScannedStrictState(loginError));
                }else if(response.code() == 404){

                    stockTakeStateMutableLiveData.setValue(new InventoryScannedStrictState(response.message()));
                }
                else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    assert response.errorBody() != null;
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    stockTakeStateMutableLiveData.setValue(new InventoryScannedStrictState(failError));
                }
            }

            @Override
            public void onFailure(Call<InventoryScannedStrict> call, Throwable t) {
                stockTakeStateMutableLiveData.setValue(new InventoryScannedStrictState(t));
            }
        });

        return stockTakeStateMutableLiveData;

    }
}

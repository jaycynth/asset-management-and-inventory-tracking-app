package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.DecrementInventory;
import com.ami.abc.ami_app.models.LoginError;

public class DecrementInventoryState {
    private DecrementInventory decrementInventory;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public DecrementInventoryState(DecrementInventory decrementInventory) {
        this.decrementInventory = decrementInventory;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public DecrementInventoryState(String message) {
        this.message = message;
        this.decrementInventory = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public DecrementInventoryState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.decrementInventory = null;
        this.message = null;
        this.loginError = null;
    }

    public DecrementInventoryState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.decrementInventory = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public DecrementInventory getDecrementInventory() {
        return decrementInventory;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

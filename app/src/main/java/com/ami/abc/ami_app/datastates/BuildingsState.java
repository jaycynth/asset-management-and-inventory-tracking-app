package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.LoginError;

public class BuildingsState {

    private AllBuildings allBuildings;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private Error error;


    public BuildingsState(AllBuildings allBuildings) {
        this.allBuildings = allBuildings;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;

    }

    public BuildingsState(String message) {
        this.message = message;
        this.allBuildings = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.error = null;

    }

    public BuildingsState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allBuildings = null;
        this.loginError = null;
        this.error = null;

    }

    public BuildingsState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
         this.allBuildings = null;
        this.error = null;
    }

    public BuildingsState(Error error) {
        this.error = error;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.allBuildings = null;
    }

    public Error getError() {
        return error;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllBuildings getAllBuildings() {
        return allBuildings;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

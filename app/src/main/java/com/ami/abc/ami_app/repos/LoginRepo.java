package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.LoginState;
import com.ami.abc.ami_app.models.Login;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.UniqueLoginError;
import com.ami.abc.ami_app.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepo {

    private ApiClient mApiClient;

    //constructor
    public LoginRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<LoginState> loginUser(String email, String password, Boolean withLogout) {

        MutableLiveData<LoginState> loginStateMutableLiveData = new MutableLiveData<>();
        Call<Login> call = mApiClient.loginApiService().loginUser(email, password, Constants.GRANT_TYPE,Constants.CLIENT_ID,Constants.CLIENT_SECRET, Constants.TYPE, withLogout);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.code() == 200) {
                    loginStateMutableLiveData.setValue(new LoginState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    loginStateMutableLiveData.setValue(new LoginState(loginError));
                }else if(response.code() == 422){
                    Gson gson = new Gson();
                    Type type = new TypeToken<UniqueLoginError>() {}.getType();
                    UniqueLoginError failError = gson.fromJson(response.errorBody().charStream(),type);
                    loginStateMutableLiveData.setValue(new LoginState(failError));
                }else{
                    loginStateMutableLiveData.setValue(new LoginState(response.message()));

                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                loginStateMutableLiveData.setValue(new LoginState(t));
                Log.d("LoginActivity", t.getMessage());
            }
        });

        return loginStateMutableLiveData;

    }
}

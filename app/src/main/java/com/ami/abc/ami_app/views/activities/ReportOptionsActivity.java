package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.MainActivity;
import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Role;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportOptionsActivity extends AppCompatActivity {

    @BindView(R.id.asset_in_card)
    CardView asset_in_card;

    @BindView(R.id.scan_inventory_card)
    CardView scan_inventory_card;

    MainViewModel mainViewModel;


    String accessToken;

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;
    @BindView(R.id.first_layout)
    LinearLayout firstLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_options);
        ButterKnife.bind(this);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reports");

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();


        if (accessToken != null) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            firstLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            mainViewModel.getRole("Bearer " + accessToken);

        }


        mainViewModel.getRoleResponse().observe(this, getRoleState -> {
            assert getRoleState != null;
            if (getRoleState.getGetRole() != null) {
                handleRoles(getRoleState.getGetRole());
            }
            if (getRoleState.getMessage() != null) {
                handleNetworkResponse(getRoleState.getMessage());
            }

            if (getRoleState.getErrorThrowable() != null) {
                handleError(getRoleState.getErrorThrowable());
            }

            if (getRoleState.getLoginError() != null) {
                handleUnauthorized(getRoleState.getLoginError());
            }
        });
        scan_inventory_card.setOnClickListener(v -> {
            Intent scanInventory = new Intent(ReportOptionsActivity.this, InventoryReportsActivity.class);
            startActivity(scanInventory);

        });
        asset_in_card.setOnClickListener(v -> {
            Intent scanAssets = new Intent(ReportOptionsActivity.this, AssetReportsActivity.class);
            startActivity(scanAssets);

        });

    }

    private void handleRoles(GetRole getRole) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        List<Role> roleList;
        roleList = getRole.getRoles();

        if (getRole.getType().equals("Admin")) {
            scan_inventory_card.setOnClickListener(v -> {
                Intent scanInventory = new Intent(ReportOptionsActivity.this, InventoryReportsActivity.class);
                startActivity(scanInventory);

            });
            asset_in_card.setOnClickListener(v -> {
                Intent scanAssets = new Intent(ReportOptionsActivity.this, AssetReportsActivity.class);
                startActivity(scanAssets);

            });
        } else {
            if(getRole.getManaging().equals("Asset")){
                scan_inventory_card.setOnClickListener(v -> {
                    Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();
                });
                asset_in_card.setOnClickListener(v -> {
                    Intent scanAssets = new Intent(ReportOptionsActivity.this, AssetReportsActivity.class);
                    startActivity(scanAssets);

                });

                return;

            }
            if(getRole.getManaging().equals("Inventory")){
                asset_in_card.setOnClickListener(v -> {
                    Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();
                });
                scan_inventory_card.setOnClickListener(v -> {
                    Intent scanAssets = new Intent(ReportOptionsActivity.this, InventoryReportsActivity.class);
                    startActivity(scanAssets);

                });

                return;

            }
            for (Role role : roleList) {
                if (role.getName().equals("Asset Mobile Report")) {

                    scan_inventory_card.setOnClickListener(v -> {
                        Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();
                    });
                    asset_in_card.setOnClickListener(v -> {
                        Intent scanAssets = new Intent(ReportOptionsActivity.this, AssetReportsActivity.class);
                        startActivity(scanAssets);

                    });

                    return;
                } else if (role.getName().equals("Inventory Mobile Report")) {
                    asset_in_card.setOnClickListener(v -> {
                        Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();
                    });
                    scan_inventory_card.setOnClickListener(v -> {
                        Intent scanInventory = new Intent(ReportOptionsActivity.this, InventoryReportsActivity.class);
                        startActivity(scanInventory);

                    });

                    return;
                } else if (role.getName().equals("Inventory Mobile Report") && role.getName().equals("Asset Mobile Report")) {
                    scan_inventory_card.setOnClickListener(v -> {
                        Intent scanInventory = new Intent(ReportOptionsActivity.this, InventoryReportsActivity.class);
                        startActivity(scanInventory);

                    });
                    asset_in_card.setOnClickListener(v -> {
                        Intent scanAssets = new Intent(ReportOptionsActivity.this, AssetReportsActivity.class);
                        startActivity(scanAssets);

                    });
                }

            }
        }


    }

    private void handleError(Throwable errorThrowable) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnauthorized(LoginError loginError) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent unauth = new Intent(this, LoginActivity.class);
//        startActivity(unauth);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(ReportOptionsActivity.this, MainActivity.class);
        startActivity(back);
    }
}

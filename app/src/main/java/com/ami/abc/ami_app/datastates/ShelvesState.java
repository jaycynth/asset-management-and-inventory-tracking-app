package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.LoginError;

public class ShelvesState {

    private AllShelves allShelves;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public ShelvesState(AllShelves allShelves) {
        this.allShelves = allShelves;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public ShelvesState(String message) {
        this.message = message;
        this.allShelves = null;
        this.loginError = null;
        this.errorThrowable = null;
    }

    public ShelvesState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allShelves = null;
        this.loginError = null;
    }

    public ShelvesState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.allShelves = null;
        this.errorThrowable = null;

    }

    public AllShelves getAllShelves() {
        return allShelves;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public LoginError getLoginError() {
        return loginError;
    }
    
}

package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AssetCheckReport;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetCheckReportsAdapter extends RecyclerView.Adapter<AssetCheckReportsAdapter.MyViewHolder> {
    private List<AssetCheckReport> assetInReportList;
    private Context context;

    public AssetCheckReportsAdapter(List<AssetCheckReport> assetInReportList, Context context) {
        this.assetInReportList = assetInReportList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.status)
        TextView status;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @NonNull
    @Override
    public AssetCheckReportsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.asset_check_reports_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetCheckReportsAdapter.MyViewHolder holder, int i) {
        AssetCheckReport assetScanned = assetInReportList.get(i);
        holder.asset_code.setText("Asset Barcode : " + assetScanned.getBarcode());
        holder.name.setText("Asset Name : " + assetScanned.getName());
        holder.status.setText("Status : "+ assetScanned.getStatus());


    }

    @Override
    public int getItemCount() {
        return assetInReportList.size();
    }

    public void setAssetCheckReportList(List<AssetCheckReport> assetInReportList){
        this.assetInReportList = assetInReportList;
        notifyDataSetChanged();
    }
}

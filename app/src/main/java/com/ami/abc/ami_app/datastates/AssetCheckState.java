package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AssetCheckPoint;
import com.ami.abc.ami_app.models.LoginError;

public class AssetCheckState {
    private AssetCheckPoint assetCheckPoint;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public AssetCheckState(AssetCheckPoint assetCheckPoint) {
        this.assetCheckPoint = assetCheckPoint;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
    }

    public AssetCheckState(String message) {
        this.message = message;
        this.assetCheckPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AssetCheckState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.assetCheckPoint =null;
        this.loginError = null;
    }

    public AssetCheckState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.assetCheckPoint = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AssetCheckPoint getAssetCheckPoint() {
        return assetCheckPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

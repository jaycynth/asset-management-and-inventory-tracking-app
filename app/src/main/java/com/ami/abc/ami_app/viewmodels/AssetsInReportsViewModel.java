package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.AssetInReport;
import com.ami.abc.ami_app.repos.AssetsInReportsRepo;

import java.util.List;

public class AssetsInReportsViewModel extends AndroidViewModel {

    private AssetsInReportsRepo assetsInReportsRepo;

    private LiveData<List<AssetInReport>> listLiveData;



    public AssetsInReportsViewModel(Application application) {
         super(application);
        assetsInReportsRepo = new AssetsInReportsRepo();

        listLiveData = assetsInReportsRepo.getAssetsReports();
    }

    public LiveData<List<AssetInReport>> getListLiveData() {
        return listLiveData;
    }

    public void saveScannedAsset(AssetInReport assetInReport) {
        assetsInReportsRepo.saveAssets(assetInReport);
    }





}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllRooms;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.LoginError;

public class RoomState {
    private AllRooms allRooms;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private Error error;


    public RoomState(AllRooms allRooms) {
        this.allRooms = allRooms;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
        this.error = null;
    }

    public RoomState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.allRooms = null;
        this.loginError = null;
        this.error = null;
    }

    public RoomState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allRooms = null;
        this.loginError = null;
        this.error = null;
    }

    public RoomState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allRooms = null;
        this.error = null;
    }

    public RoomState(Error error) {
        this.error = error;
        this.loginError = null;
        this.message = null;
        this.errorThrowable = null;
        this.allRooms = null;
    }

    public Error getError() {
        return error;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllRooms getAllRooms() {
        return allRooms;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

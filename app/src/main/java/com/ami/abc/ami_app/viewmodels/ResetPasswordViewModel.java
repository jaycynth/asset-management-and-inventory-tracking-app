package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.ResetPasswordState;
import com.ami.abc.ami_app.repos.ResetPasswordRepo;

public class ResetPasswordViewModel extends AndroidViewModel {
    private MediatorLiveData<ResetPasswordState> resetPasswordStateMediatorLiveData;
    private ResetPasswordRepo resetPasswordRepo;

    public ResetPasswordViewModel(Application application) {
        super(application);
        resetPasswordStateMediatorLiveData = new MediatorLiveData<>();
        resetPasswordRepo = new ResetPasswordRepo(application);
    }

    public LiveData<ResetPasswordState> resetPasswordResponse() {
        return resetPasswordStateMediatorLiveData;
    }

    public void resetPassword(String accessToken, String email) {

        LiveData<ResetPasswordState> resetPasswordStateLiveData = resetPasswordRepo.resetPasswordUser(accessToken, email);
        resetPasswordStateMediatorLiveData.addSource(resetPasswordStateLiveData,
                resetPasswordStateMediatorLiveData -> {
                    if (this.resetPasswordStateMediatorLiveData.hasActiveObservers()) {
                        this.resetPasswordStateMediatorLiveData.removeSource(resetPasswordStateLiveData);
                    }
                    this.resetPasswordStateMediatorLiveData.setValue(resetPasswordStateMediatorLiveData);
                });

    }

}

package com.ami.abc.ami_app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkCheck {

    private Context context;
    private static ConnectivityManager connectivityManager;
    private static NetworkInfo activeNetworkInfo;


    public NetworkCheck(Context context) {
        this.context = context;
    }


    private boolean isconnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}

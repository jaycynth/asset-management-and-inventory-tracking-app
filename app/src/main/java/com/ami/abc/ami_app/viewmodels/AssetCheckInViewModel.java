package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AssetCheckInOutDetailsState;
import com.ami.abc.ami_app.datastates.AssetCheckState;
import com.ami.abc.ami_app.datastates.AssetCheckedInPointState;
import com.ami.abc.ami_app.models.AssetCheckInReport;
import com.ami.abc.ami_app.repos.AssetCheckInOutDetailsRepo;
import com.ami.abc.ami_app.repos.AssetCheckInRepo;
import com.ami.abc.ami_app.repos.AssetCheckInReportsRepo;
import com.ami.abc.ami_app.repos.AssetCheckRepo;

public class AssetCheckInViewModel extends AndroidViewModel {
    private MediatorLiveData<AssetCheckState> assetCheckStateMediatorLiveData;
    private AssetCheckRepo assetCheckRepo;

    private MediatorLiveData<AssetCheckedInPointState> assetCheckedInPointStateMediatorLiveData;
    private AssetCheckInRepo assetCheckInRepo;

    private MediatorLiveData<AssetCheckInOutDetailsState> assetCheckInOutDetailsStateMediatorLiveData;
    private AssetCheckInOutDetailsRepo assetCheckInOutDetailsRepo;



    private AssetCheckInReportsRepo assetCheckInReportsRepo;

    public AssetCheckInViewModel(Application application) {
        super(application);
        assetCheckStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckRepo = new AssetCheckRepo(application);

        assetCheckedInPointStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckInRepo = new AssetCheckInRepo(application);


        assetCheckInReportsRepo = new AssetCheckInReportsRepo();

        assetCheckInOutDetailsStateMediatorLiveData = new MediatorLiveData<>();
        assetCheckInOutDetailsRepo = new AssetCheckInOutDetailsRepo(application);

    }

    public LiveData<AssetCheckState> assetCheckResponse(){
        return  assetCheckStateMediatorLiveData;
    }

    public void assetCheck(String accessToken){
        LiveData<AssetCheckState> assetCheckStateLiveData = assetCheckRepo.assetCheck(accessToken);
        assetCheckStateMediatorLiveData.addSource(assetCheckStateLiveData,
                assetCheckStateMediatorLiveData-> {
                    if (this.assetCheckStateMediatorLiveData.hasActiveObservers()){
                        this.assetCheckStateMediatorLiveData.removeSource(assetCheckStateLiveData);
                    }
                    this.assetCheckStateMediatorLiveData.setValue(assetCheckStateMediatorLiveData);
                });
    }

    public LiveData<AssetCheckedInPointState> checkInAssetResponse(){
        return  assetCheckedInPointStateMediatorLiveData;
    }

    public void checkInAsset(String accessToken, String productCode, String mileage, String fuel){
        LiveData<AssetCheckedInPointState> assetCheckedInPointStateLiveData = assetCheckInRepo.checkinAsset(accessToken, productCode, mileage,fuel);
        assetCheckedInPointStateMediatorLiveData.addSource(assetCheckedInPointStateLiveData,
                assetCheckedInPointStateMediatorLiveData-> {
                    if (this. assetCheckedInPointStateMediatorLiveData.hasActiveObservers()){
                        this. assetCheckedInPointStateMediatorLiveData.removeSource(assetCheckedInPointStateLiveData);
                    }
                    this. assetCheckedInPointStateMediatorLiveData.setValue( assetCheckedInPointStateMediatorLiveData);
                });
    }

    public LiveData<AssetCheckInOutDetailsState> assetCheckInOutDetails() {
        return assetCheckInOutDetailsStateMediatorLiveData;
    }

    public void getAssetCheckInOutDetails(String accessToken,String code) {
        LiveData<AssetCheckInOutDetailsState> assetCheckInOutDetailsStateLiveData = assetCheckInOutDetailsRepo.assetCheckInOutDetails(accessToken, code);
        assetCheckInOutDetailsStateMediatorLiveData.addSource(assetCheckInOutDetailsStateLiveData, assetCheckInOutDetailsStateMediatorLiveData -> {
            if (this.assetCheckInOutDetailsStateMediatorLiveData.hasActiveObservers()) {
                this.assetCheckInOutDetailsStateMediatorLiveData.removeSource(assetCheckInOutDetailsStateLiveData);
            }
            this.assetCheckInOutDetailsStateMediatorLiveData.setValue(assetCheckInOutDetailsStateMediatorLiveData);
        });

    }
    //save individual assets properties scanned in room for reports
    public void saveAssets(AssetCheckInReport assetsReports) {
        assetCheckInReportsRepo.saveAssets(assetsReports);
    }
}

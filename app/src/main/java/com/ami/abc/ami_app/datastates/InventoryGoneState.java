package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetInventoryGone;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryGoneState {
    private GetInventoryGone getInventoryGone;
    private String message;
    private Throwable errorThrowable;
    private FailError failError;
    private LoginError loginError;

    public InventoryGoneState(GetInventoryGone getInventoryGone) {
        this.getInventoryGone = getInventoryGone;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryGoneState(String message) {
        this.message = message;
        this.getInventoryGone = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryGoneState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.getInventoryGone = null;
        this.message = null;
        this.failError = null;
        this.loginError = null;
    }

    public InventoryGoneState(FailError failError) {
        this.failError = failError;
        this.getInventoryGone = null;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public InventoryGoneState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.getInventoryGone = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public FailError getFailError() {
        return failError;
    }

    public GetInventoryGone getGetInventoryGone() {
        return getInventoryGone;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

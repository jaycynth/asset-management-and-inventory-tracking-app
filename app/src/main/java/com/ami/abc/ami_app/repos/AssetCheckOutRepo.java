package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.AssetCheckedOutPointState;
import com.ami.abc.ami_app.models.AssetCheckedOutPoint;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetCheckOutRepo {
    private ApiClient mApiClient;

    public AssetCheckOutRepo(Application application) {
        mApiClient = new ApiClient(application);

    }

    public LiveData<AssetCheckedOutPointState> checkoutAsset(String accessToken, String code, String custodan, String milage, String fuel) {
        MutableLiveData<AssetCheckedOutPointState> assetCheckedOutPointStateMutableLiveData = new MutableLiveData<>();
        Call<AssetCheckedOutPoint> call = mApiClient.amiService().checkOutAsset(accessToken, code, custodan, milage, fuel);
        call.enqueue(new Callback<AssetCheckedOutPoint>() {
            @Override
            public void onResponse(Call<AssetCheckedOutPoint> call, Response<AssetCheckedOutPoint> response) {
                if (response.code() == 200) {
                    assetCheckedOutPointStateMutableLiveData.setValue(new AssetCheckedOutPointState(response.body()));

                }  else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    assetCheckedOutPointStateMutableLiveData.setValue(new AssetCheckedOutPointState(loginError));
                }
                else{
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError failError = gson.fromJson(response.errorBody().charStream(),type);
                    assetCheckedOutPointStateMutableLiveData.setValue(new AssetCheckedOutPointState(failError));
                }

//               else{
//                    assetCheckedOutPointStateMutableLiveData.setValue(new AssetCheckedOutPointState(response.message()));
//                }
            }

            @Override
            public void onFailure(Call<AssetCheckedOutPoint> call, Throwable t) {
                assetCheckedOutPointStateMutableLiveData.setValue(new AssetCheckedOutPointState(t));
            }
        });

        return assetCheckedOutPointStateMutableLiveData;


    }
}

package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.content.Context;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.GetRoleState;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRoleRepo {
    private ApiClient mApiClient;
    private Context context;

    //constructor
    public GetRoleRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<GetRoleState> getGetRole(String accessToken) {

        MutableLiveData<GetRoleState> getRoleStateMutableLiveData = new MutableLiveData<>();
        Call<GetRole> call = mApiClient.amiService().getRoles(accessToken);
        call.enqueue(new Callback<GetRole>() {
            @Override
            public void onResponse(Call<GetRole> call, Response<GetRole> response) {
                if (response.code() == 200) {
                    getRoleStateMutableLiveData.setValue(new GetRoleState(response.body()));

                }
                else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    getRoleStateMutableLiveData.setValue(new GetRoleState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    getRoleStateMutableLiveData.setValue(new GetRoleState(error));
                }
                else{
                    getRoleStateMutableLiveData.setValue(new GetRoleState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetRole> call, Throwable t) {
                getRoleStateMutableLiveData.setValue(new GetRoleState(t));
            }
        });

        return getRoleStateMutableLiveData;

    }
}

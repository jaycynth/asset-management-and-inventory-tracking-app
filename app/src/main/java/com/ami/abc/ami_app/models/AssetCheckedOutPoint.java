package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssetCheckedOutPoint {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("checked out")
    @Expose
    private AssetCheckedOut checkedOut;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public AssetCheckedOut getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(AssetCheckedOut  checkedOut) {
        this.checkedOut = checkedOut;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

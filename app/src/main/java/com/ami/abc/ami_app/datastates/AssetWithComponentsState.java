package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AddAsset;

public class AssetWithComponentsState {
    private AddAsset addAsset;
    private String message;
    private Throwable throwable;

    public AssetWithComponentsState(AddAsset addAsset) {
        this.addAsset = addAsset;
        this.message = null;
        this.throwable = null;
    }

    public AssetWithComponentsState(String message) {
        this.message = message;
        this.addAsset = null;
        this.throwable = null;
    }

    public AssetWithComponentsState(Throwable throwable) {
        this.throwable = throwable;
        this.addAsset = null;
        this.message = null;
    }

    public AddAsset getAddAsset() {
        return addAsset;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}

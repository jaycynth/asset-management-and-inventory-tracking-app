package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.LoginUnprocessableEntity;
import com.ami.abc.ami_app.models.UniqueLoginError;
import com.ami.abc.ami_app.models.Login;
import com.ami.abc.ami_app.models.LoginError;

public class LoginState {
    private Login allLogins;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private UniqueLoginError uniqueLoginError;
    private LoginUnprocessableEntity loginUnprocessableEntity;

    public LoginState(LoginUnprocessableEntity loginUnprocessableEntity) {
        this.loginUnprocessableEntity = loginUnprocessableEntity;
        this.allLogins= null;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.uniqueLoginError = null;

    }

    public LoginState(Login allLogins) {
        this.allLogins = allLogins;
        this.message =null;
        this.errorThrowable = null;
        this.loginError = null;
        this.uniqueLoginError = null;
        this.loginUnprocessableEntity = null;
    }

    public LoginState(String message) {
        this.message = message;
        this.allLogins = null;
        this.errorThrowable =null;
        this.loginError = null;
        this.uniqueLoginError = null;
        this.loginUnprocessableEntity = null;

    }

    public LoginState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.allLogins = null;
        this.message = null;
        this.loginError = null;
        this.uniqueLoginError = null;
        this.loginUnprocessableEntity = null;

    }

    public LoginState(LoginError loginError) {
        this.loginError = loginError;
        this.errorThrowable = null;
        this.allLogins = null;
        this.message = null;
        this.uniqueLoginError = null;
        this.loginUnprocessableEntity = null;

    }

    public LoginState(UniqueLoginError uniqueLoginError) {
        this.loginError = null;
        this.errorThrowable = null;
        this.allLogins = null;
        this.message = null;
        this.uniqueLoginError = uniqueLoginError;
        this.loginUnprocessableEntity = null;

    }

    public LoginUnprocessableEntity getLoginUnprocessableEntity() {
        return loginUnprocessableEntity;
    }

    public UniqueLoginError getUniqueLoginError() {
        return uniqueLoginError;
    }

    public Login getAllLogins() {
        return allLogins;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }

    public LoginError getLoginError() {
        return loginError;
    }
}

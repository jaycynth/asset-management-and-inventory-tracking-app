package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Role;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryOptions extends AppCompatActivity {

    @BindView(R.id.inventory_in_card)
    CardView inventory_in_card;

    @BindView(R.id.inventory_out_card)
    CardView inventory_out_card;

    @BindView(R.id.stock_take_card)
    CardView stock_take_card;

    @BindView(R.id.check_out_card)
    CardView check_out_card;

    @BindView(R.id.check_in_card)
    CardView check_in_card;

    @BindView(R.id.search_card)
    CardView search_card;

    @BindView(R.id.stock_return_card)
    CardView stock_return_card;

    MainViewModel mainViewModel;
    String accessToken;

    @BindView(R.id.first_layout)
    LinearLayout firstLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    List<String> roleName = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_options);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Inventory");
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        if (accessToken != null) {
            firstLayout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            mainViewModel.getRole("Bearer " + accessToken);

        }


        mainViewModel.getRoleResponse().observe(this, getRoleState -> {
            assert getRoleState != null;
            if (getRoleState.getGetRole() != null) {
                handleRoles(getRoleState.getGetRole());
            }
            if (getRoleState.getMessage() != null) {
                handleNetworkResponse(getRoleState.getMessage());
            }

            if (getRoleState.getErrorThrowable() != null) {
                handleError(getRoleState.getErrorThrowable());
            }

            if (getRoleState.getLoginError() != null) {
                handleUnauthorized(getRoleState.getLoginError());
            }
        });


        inventory_in_card.setOnClickListener(v -> {
            Intent inventoryIn = new Intent(InventoryOptions.this, InventoryLocation.class);
            inventoryIn.putExtra("TAG", "D");
            startActivity(inventoryIn);
        });

        inventory_out_card.setOnClickListener(v -> {
            Intent inventoryOut = new Intent(InventoryOptions.this, InventoryLocation.class);
            inventoryOut.putExtra("TAG", "E");
            startActivity(inventoryOut);
        });

        stock_take_card.setOnClickListener(v -> {
            Intent stockTake = new Intent(InventoryOptions.this, InventoryLocation.class);
            stockTake.putExtra("TAG", "F");
            startActivity(stockTake);
        });

        search_card.setOnClickListener(v -> {
            Intent stockTake = new Intent(InventoryOptions.this, SearchInventoryActivity.class);
            startActivity(stockTake);

        });
        check_out_card.setOnClickListener(v -> {
            Intent checkout = new Intent(InventoryOptions.this, InventoryLocation.class);
            checkout.putExtra("TAG", "G");
            startActivity(checkout);
        });

        check_in_card.setOnClickListener(v -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Scanner : ");
            alert.setCancelable(true);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                Intent checkin = new Intent(InventoryOptions.this, InventoryCheckIn.class);
                checkin.putExtra("TAG", "H");
                startActivity(checkin);


            }));

            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent checkin = new Intent(InventoryOptions.this, ScanActivity.class);
                checkin.putExtra("TAG", "H");
                startActivity(checkin);

                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();

        });

        stock_return_card.setOnClickListener(v -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Scanner : ");
            alert.setCancelable(true);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                Intent stockReturn = new Intent(InventoryOptions.this, StockReturnActivity.class);
                stockReturn.putExtra("TAG", "K");
                startActivity(stockReturn);


            }));

            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent stockReturn = new Intent(InventoryOptions.this, ScanActivity.class);
                stockReturn.putExtra("TAG", "K");
                startActivity(stockReturn);

                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();


        });
    }

    private void handleRoles(GetRole getRole) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);

        List<Role> roleList;
        roleList = getRole.getRoles();

        for (Role role : roleList) {
            roleName.add(role.getName());
        }
        final View.OnClickListener onClickListener = v -> {
            Toast.makeText(this, getString(R.string.no_access_rights), Toast.LENGTH_SHORT).show();
        };
        if (roleName.contains("Inventory Check In") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Upload")
                && roleName.contains("Inventory Out") && roleName.contains("Stock Take")) {

            stock_return_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Check In") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Upload")
                && roleName.contains("Inventory Out") && roleName.contains("Stock Return")) {

            stock_take_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Upload")
                && roleName.contains("Inventory Out") && roleName.contains("Stock Return")) {

            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check In") && roleName.contains("Inventory Upload")
                && roleName.contains("Inventory Out") && roleName.contains("Stock Return")) {

            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Inventory Out") && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Upload")
                && roleName.contains("Inventory Check In") && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);

        }
        else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Take")) {

            inventory_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Stock Take") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Stock Take") && roleName.contains("Inventory Check Out")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Take")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Stock Take")
                && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Inventory Out")) {

            stock_take_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            stock_take_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Out")
                && roleName.contains("Stock Return")) {

            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out") && roleName.contains("Inventory Out")
                && roleName.contains("Stock Take")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check In") && roleName.contains("Inventory Out")
                && roleName.contains("Stock Take")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Return") && roleName.contains("Inventory Out")
                && roleName.contains("Stock Take")) {

            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        }
        else if (roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Check Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Take")) {

            inventory_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Check Out") && roleName.contains("Stock Take")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Inventory Check Out")) {

            inventory_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check Out")
                && roleName.contains("Stock Return")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Take")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check Out")
                && roleName.contains("Stock Take")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Out") && roleName.contains("Stock Return")
                && roleName.contains("Stock Take")) {

            inventory_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check In")
                && roleName.contains("Inventory Check Out")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check In")
                && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out")
                && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check In")
                && roleName.contains("Inventory Out")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);


        } else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take")
                && roleName.contains("Inventory Out")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);


        }
        else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take")
                && roleName.contains("Inventory Check In")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        }
        else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take")
                && roleName.contains("Inventory Check Out")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        }
        else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take")
                && roleName.contains("Stock Return")) {

            check_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        }
        //TODO left at inventory in check out check in, end of three's

        else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Out")) {

            check_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Take")) {

            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Stock Return")) {

            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check In")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload") && roleName.contains("Inventory Check Out")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            inventory_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Stock Take")) {

            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Stock Return")) {

            stock_take_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check In")) {

            stock_take_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out") && roleName.contains("Inventory Check Out")) {

            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check In")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take") && roleName.contains("Inventory Check Out")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Return") && roleName.contains("Inventory Check In")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Return") && roleName.contains("Inventory Check Out")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Check In") && roleName.contains("Inventory Check Out")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Upload")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Take")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Stock Return")) {

            inventory_out_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Check In")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Check Out")) {

            inventory_out_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            inventory_in_card.setOnClickListener(onClickListener);

        } else if (roleName.contains("Inventory Out")) {

            inventory_in_card.setOnClickListener(onClickListener);
            stock_return_card.setOnClickListener(onClickListener);
            stock_take_card.setOnClickListener(onClickListener);
            check_in_card.setOnClickListener(onClickListener);
            check_out_card.setOnClickListener(onClickListener);

        }
    }


    private void handleError(Throwable errorThrowable) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnauthorized(LoginError loginError) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        firstLayout.setAlpha(1);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent unauth = new Intent(this, LoginActivity.class);
//        startActivity(unauth);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(InventoryOptions.this, ScanOptionsActivity.class);
        startActivity(back);
    }
}

package com.ami.abc.ami_app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllCategories {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("assetcategories")
    @Expose
    private List<AssetCategory> assetcategories = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AssetCategory> getAssetcategories() {
        return assetcategories;
    }

    public void setAssetcategories(List<AssetCategory> assetcategories) {
        this.assetcategories = assetcategories;
    }
}

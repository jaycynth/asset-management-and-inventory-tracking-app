package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.InventoryGoneState;
import com.ami.abc.ami_app.datastates.StockReturnState;
import com.ami.abc.ami_app.datastates.StockTakeState;
import com.ami.abc.ami_app.repos.GetInventoryGoneRepo;
import com.ami.abc.ami_app.repos.StockReturnRepo;
import com.ami.abc.ami_app.repos.StockTakeRepo;

public class StockReturnViewModel extends AndroidViewModel {

    private MediatorLiveData<StockTakeState> stockTakeStateMediatorLiveData;
    private StockTakeRepo stockTakeRepo;

    private MediatorLiveData<InventoryGoneState> inventoryGoneStateMediatorLiveData;
    private GetInventoryGoneRepo getInventoryGoneRepo;

    private MediatorLiveData<StockReturnState> stockReturnStateMediatorLiveData;
    private StockReturnRepo stockReturnRepo;

    public StockReturnViewModel(Application application) {
        super(application);
        stockTakeStateMediatorLiveData = new MediatorLiveData<>();
        stockTakeRepo = new StockTakeRepo(application);

        inventoryGoneStateMediatorLiveData = new MediatorLiveData<>();
        getInventoryGoneRepo = new GetInventoryGoneRepo(application);

        stockReturnStateMediatorLiveData = new MediatorLiveData<>();
        stockReturnRepo = new StockReturnRepo(application);
    }

    public LiveData<StockTakeState> stockTakeResponse(){
        return stockTakeStateMediatorLiveData;
    }

    public void stockTake(String code,String accessToken){

        LiveData<StockTakeState> stockTakeStateLiveData = stockTakeRepo.getStockTake(code, accessToken);
        stockTakeStateMediatorLiveData.addSource(stockTakeStateLiveData,
                stockTakeStateMediatorLiveData-> {
                    if (this.stockTakeStateMediatorLiveData.hasActiveObservers()){
                        this.stockTakeStateMediatorLiveData.removeSource(stockTakeStateLiveData);
                    }
                    this.stockTakeStateMediatorLiveData.setValue(stockTakeStateMediatorLiveData);
                });

    }


    public LiveData<InventoryGoneState> inventoryGoneResponse(){
        return inventoryGoneStateMediatorLiveData;
    }

    public void inventoryGone( String accessToken , String Code, String invoiceNumber){

        LiveData<InventoryGoneState> inventoryGoneStateLiveData = getInventoryGoneRepo.getInventoryGone(accessToken, Code, invoiceNumber);
        inventoryGoneStateMediatorLiveData.addSource(inventoryGoneStateLiveData,
                inventoryGoneStateMediatorLiveData-> {
                    if (this.inventoryGoneStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryGoneStateMediatorLiveData.removeSource(inventoryGoneStateLiveData);
                    }
                    this.inventoryGoneStateMediatorLiveData.setValue(inventoryGoneStateMediatorLiveData);
                });

    }


    public LiveData<StockReturnState> stockReturnResponse(){
        return stockReturnStateMediatorLiveData;
    }

    public void stockReturn( String accessToken , int inventoryId, String invoiceNumber, int quantity){

        LiveData<StockReturnState> stockReturnStateLiveData = stockReturnRepo.getStockReturn(accessToken, inventoryId, invoiceNumber, quantity);
        stockReturnStateMediatorLiveData.addSource(stockReturnStateLiveData,
                stockReturnStateMediatorLiveData-> {
                    if (this. stockReturnStateMediatorLiveData.hasActiveObservers()){
                        this. stockReturnStateMediatorLiveData.removeSource(stockReturnStateLiveData);
                    }
                    this. stockReturnStateMediatorLiveData.setValue(stockReturnStateMediatorLiveData);
                });

    }
}

package com.ami.abc.ami_app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Department;

import java.util.ArrayList;
import java.util.List;

public class DepartmentArrayAdapter extends ArrayAdapter<Department> {

    private List<Department> departmentListFull;
    private List<Department> filteredDepartmentList = new ArrayList<>();


    public DepartmentArrayAdapter(@NonNull Context context, @NonNull List<Department> departmentList) {
        super(context, 0, departmentList);
        this.departmentListFull = departmentList;
    }

    @Override
    public int getCount() {
        return filteredDepartmentList.size();
    }

    @Override
    public Department getItem(int position) {
        return filteredDepartmentList.get(position);
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return departmentFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_array_layout, parent, false
            );
        }

        TextView textViewName = convertView.findViewById(R.id.value);

        Department department = getItem(position);

        if (department != null) {
            textViewName.setText(department.getName());
        }

        return convertView;
    }

    private Filter departmentFilter = new Filter() {
        List<Department> suggestions = new ArrayList<>();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            Log.d("Departments", constraint.toString());
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(departmentListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Department item : departmentListFull) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredDepartmentList.clear();
            if (results.count > 0) {
                filteredDepartmentList.addAll((List) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Department) resultValue).getName();
        }
    };
}

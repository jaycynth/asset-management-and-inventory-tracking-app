package com.ami.abc.ami_app.repos;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.CurrentAccountTypeState;
import com.ami.abc.ami_app.models.CurrentAccountType;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentAccountTypeRepo {

    private ApiClient mApiClient;
    //constructor
    public CurrentAccountTypeRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<CurrentAccountTypeState> getCurrentAccountType(String accessToken) {

        MutableLiveData<CurrentAccountTypeState> currentAccountTypeStateMutableLiveData = new MutableLiveData<>();
        Call<CurrentAccountType> call = mApiClient.amiService().getCurrentAccountType(accessToken);
        call.enqueue(new Callback<CurrentAccountType>() {
            @Override
            public void onResponse(Call<CurrentAccountType> call, Response<CurrentAccountType> response) {
                if (response.code() == 200) {
                    currentAccountTypeStateMutableLiveData.setValue(new CurrentAccountTypeState(response.body()));

                } else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    currentAccountTypeStateMutableLiveData.setValue(new CurrentAccountTypeState(loginError));
                }
                else {
                    currentAccountTypeStateMutableLiveData.setValue(new CurrentAccountTypeState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<CurrentAccountType> call, Throwable t) {
                currentAccountTypeStateMutableLiveData.setValue(new CurrentAccountTypeState(t));
            }
        });

        return currentAccountTypeStateMutableLiveData;

    }
}

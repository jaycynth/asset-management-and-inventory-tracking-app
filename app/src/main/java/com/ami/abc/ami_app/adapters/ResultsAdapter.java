package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.CountedItems;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.MyViewHolder> {

    private List<CountedItems> inventoryList;
    private Context context;
    private CountedItems mRecentlyDeletedItem;
    private int mRecentlyDeletedItemPosition;

    public ResultsAdapter(List<CountedItems> inventoryList, Context context) {
        this.inventoryList = inventoryList;
        this.context = context;

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.product_code)
        TextView productCode;


        @BindView(R.id.product_count)
        TextView productCount;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventory_scanned_list, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
        CountedItems inventory = inventoryList.get(pos);
        holder.productCode.setText(String.format("Item code : %s", inventory.getProduct_code()));
        holder.productCount.setText(String.format("Count : %s", String.valueOf(inventory.getQuantity())));


    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }


    public void deleteItem(int position) {
        mRecentlyDeletedItem = inventoryList.get(position);
        mRecentlyDeletedItemPosition = position;
        inventoryList.remove(position);
        notifyItemRemoved(position);

    }

    public CountedItems getCountedItemAtPosition(int position) {
        return inventoryList.get(position);
    }



}

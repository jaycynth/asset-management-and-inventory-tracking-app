package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.AllInventory;
import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.AllStores;
import com.ami.abc.ami_app.models.AllVerifiedStatus;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.Inventory;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.SummaryViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreSummary extends AppCompatActivity {

    @BindView(R.id.available_assets_count)
    TextView availableAssetsCount;

    @BindView(R.id.verified_assets_count)
    TextView verifiedAssetsCount;

    @BindView(R.id.inventory_count)
    TextView inventoryCount;

    SummaryViewModel summaryViewModel;

    @BindView(R.id.holder_layout)
    LinearLayout holderLayout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.total_shelves)
    TextView totalShelves;
    @BindView(R.id.shelf_count)
    ProgressBar shelfCount;

    @BindView(R.id.total_stores)
    TextView totalStores;
    @BindView(R.id.store_count)
    ProgressBar storeCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_summary);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.summary));


        summaryViewModel = ViewModelProviders.of(this).get(SummaryViewModel.class);

        String accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            progressBar.setVisibility(View.VISIBLE);
            holderLayout.setVisibility(View.GONE);
            summaryViewModel.getAllAssets("Bearer " + accessToken);
            summaryViewModel.getAllInventory("Bearer " + accessToken);
            summaryViewModel.getAllVerifiedAssets("Bearer " + accessToken);
            summaryViewModel.getStores("Bearer " + accessToken);
            summaryViewModel.getShelves("Bearer " + accessToken);
        }

        summaryViewModel.getShelvesResponse().observe(this, shelvesState -> {
            assert shelvesState != null;
            if (shelvesState.getAllShelves() != null) {
                handleAllShelves(shelvesState.getAllShelves());
            }
            if (shelvesState.getMessage() != null) {
                handleNetworkResponse(shelvesState.getMessage());
            }
            if (shelvesState.getErrorThrowable() != null) {
                handleError(shelvesState.getErrorThrowable());
            }
            if (shelvesState.getLoginError() != null) {
                handleUnAuthorized(shelvesState.getLoginError());
            }
        });

        summaryViewModel.getAllAssetResponse().observe(this, assetState -> {
            assert assetState != null;
            if (assetState.getAllAssets() != null) {
                handleAllAssets(assetState.getAllAssets());
            }
            if (assetState.getMessage() != null) {
                handleNetworkResponse(assetState.getMessage());
            }
            if (assetState.getErrorThrowable() != null) {
                handleError(assetState.getErrorThrowable());
            }
            if (assetState.getLoginError() != null) {
                handleUnAuthorized(assetState.getLoginError());
            }
        });

        summaryViewModel.getAllInventoryResponse().observe(this, inventoryState -> {
            assert inventoryState != null;
            if (inventoryState.getAllInventory() != null) {
                handleInventory(inventoryState.getAllInventory());
            }
            if (inventoryState.getMessage() != null) {
                handleNetworkResponse(inventoryState.getMessage());
            }
            if (inventoryState.getErrorThrowable() != null) {
                handleError(inventoryState.getErrorThrowable());
            }
            if (inventoryState.getLoginError() != null) {
                handleUnAuthorized(inventoryState.getLoginError());
            }
        });

        summaryViewModel.getAllVerifiedAssetsResponse().observe(this, allVerifiedStatusState -> {
            assert allVerifiedStatusState != null;
            if (allVerifiedStatusState.getAllVerifiedStatus() != null) {
                handleVerifiedAssets(allVerifiedStatusState.getAllVerifiedStatus());
            }
            if (allVerifiedStatusState.getMessage() != null) {
                handleNetworkResponse(allVerifiedStatusState.getMessage());
            }
            if (allVerifiedStatusState.getErrorThrowable() != null) {
                handleError(allVerifiedStatusState.getErrorThrowable());
            }
            if (allVerifiedStatusState.getLoginError() != null) {
                handleUnAuthorized(allVerifiedStatusState.getLoginError());
            }
        });

        summaryViewModel.getStoreResponse().observe(this, storeState -> {
            assert storeState != null;
            if (storeState.getAllStores() != null) {
                handleAllStores(storeState.getAllStores());
            }
            if (storeState.getMessage() != null) {
                handleNetworkResponse(storeState.getMessage());
            }
            if (storeState.getErrorThrowable() != null) {
                handleError(storeState.getErrorThrowable());
            }
            if (storeState.getLoginError() != null) {
                handleUnAuthorized(storeState.getLoginError());
            }
        });

    }

    private void handleAllShelves(AllShelves allShelves) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        boolean status = allShelves.getStatus();
        if (status) {
            totalShelves.setText(String.format("%s shelves ", String.valueOf(allShelves.getShelves().size())));
            shelfCount.setProgress(allShelves.getShelves().size());
        }
    }

    private void handleAllStores(AllStores allStores) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        boolean status = allStores.getStatus();
        if (status) {
            totalStores.setText(String.format("%s stores ", String.valueOf(allStores.getStores().size())));
            storeCount.setProgress(allStores.getStores().size());
        }
    }

    private void handleInventory(AllInventory allInventory) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        boolean status = allInventory.getStatus();
        if (status) {
            List<Inventory> inventories = allInventory.getInventory();
            inventoryCount.setText(String.valueOf(inventories.size()));
        }

    }

    private void handleVerifiedAssets(AllVerifiedStatus allVerifiedStatus) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        boolean status = allVerifiedStatus.getStatus();
        if (status) {
            List<Asset> assets = allVerifiedStatus.getAssets();
            verifiedAssetsCount.setText(String.valueOf(assets.size()));
        }
    }

    private void handleAllAssets(AllAssets allAssets) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);

        boolean status = allAssets.getStatus();
        if (status) {
            List<Asset> assetList = allAssets.getData();
            availableAssetsCount.setText(String.valueOf(assetList.size()));
        }
    }

    private void handleError(Throwable errorThrowable) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleNetworkResponse(String message) {
        progressBar.setVisibility(View.GONE);
        holderLayout.setVisibility(View.VISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {

        Toast.makeText(this, loginError.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}

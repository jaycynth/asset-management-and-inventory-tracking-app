package com.ami.abc.ami_app.views.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ami.abc.ami_app.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class BreakDownCostDialogFragment extends DialogFragment {

    @BindView(R.id.freight_cost_layout)
    TextInputLayout freightCostLayout;
    @BindView(R.id.freight_cost)
    TextInputEditText freightCost;

    @BindView(R.id.import_duties_layout)
    TextInputLayout importDutiesLayout;
    @BindView(R.id.import_duties)
    TextInputEditText importDuties;

    @BindView(R.id.non_refundable_tax_layout)
    TextInputLayout nonRefundableTaxLayout;
    @BindView(R.id.non_refundable_tax)
    TextInputEditText nonRefundableTax;

    @BindView(R.id.setup_cost_layout)
    TextInputLayout setupCostLayout;
    @BindView(R.id.setup_cost)
    TextInputEditText setupCost;

    @BindView(R.id.dismantling_cost_layout)
    TextInputLayout dismantlingCostLayout;
    @BindView(R.id.dismantling_cost)
    TextInputEditText dismantlingCost;

    @BindView(R.id.purchase_cost_layout)
    TextInputLayout purchaseCostLayout;
    @BindView(R.id.purchase_cost)
    TextInputEditText purchaseCost;

    @BindView(R.id.done)
    Button done;

    private Unbinder unbinder;

    private ReturnData returnData;
    private boolean assetPostStatus;
    private String nBreakdownPurchaseCost, nFreightCost, nDismantlingCost, nImportDuties, nNonRefundableCost, nSetupCost;

    private String mFreightCost, mImportDuties, mNonRefundTax, mSetupCost, mDismantlingCost, breakdownPurchaseCost;

    public void setListener(ReturnData returnData) {
        this.returnData = returnData;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setOnShowListener(dialog -> Objects.requireNonNull(getDialog().getWindow()).setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_down_cost_dialog, container, false);

        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            assetPostStatus = bundle.getBoolean("assetPostStatus");
            nBreakdownPurchaseCost = bundle.getString("breakdownPurchaseCost");
            nFreightCost = bundle.getString("freightCost");
            nDismantlingCost = bundle.getString("dismantlingCost");
            nImportDuties = bundle.getString("importDuties");
            nNonRefundableCost = bundle.getString("nonRefundableTax");
            nSetupCost = bundle.getString("setupCost");
        }

        if (assetPostStatus && nBreakdownPurchaseCost != null && nFreightCost != null && nDismantlingCost != null &&
                nImportDuties != null && nNonRefundableCost != null && nSetupCost != null) {
            purchaseCost.setText(nBreakdownPurchaseCost);
            freightCost.setText(nFreightCost);
            dismantlingCost.setText(nDismantlingCost);
            importDuties.setText(nImportDuties);
            nonRefundableTax.setText(nNonRefundableCost);
            setupCost.setText(nSetupCost);
        }


        done.setOnClickListener(v -> {
            mFreightCost = freightCost.getText().toString().trim();
            mImportDuties = importDuties.getText().toString().trim();
            mNonRefundTax = nonRefundableTax.getText().toString().trim();
            mSetupCost = setupCost.getText().toString().trim();
            mDismantlingCost = dismantlingCost.getText().toString().trim();
            breakdownPurchaseCost = purchaseCost.getText().toString().trim();


            if (TextUtils.isEmpty(mFreightCost)) {
                freightCostLayout.setError("Enter Freight cost");
                freightCost.requestFocus();
                freightCost.findFocus();
            } else if (TextUtils.isEmpty(mImportDuties)) {
                importDutiesLayout.setError("Enter Import Duties");
                importDuties.requestFocus();
                importDuties.findFocus();
            } else if (TextUtils.isEmpty(mNonRefundTax)) {
                nonRefundableTaxLayout.setError("Enter Non Refundable Tax");
                nonRefundableTax.requestFocus();
                nonRefundableTax.findFocus();
            } else if (TextUtils.isEmpty(mSetupCost)) {
                setupCostLayout.setError("Enter Setup cost ");
                setupCost.requestFocus();
                setupCost.findFocus();
            } else if (TextUtils.isEmpty(mDismantlingCost)) {
                dismantlingCostLayout.setError("Enter Dismantling cost");
                dismantlingCost.requestFocus();
                dismantlingCost.findFocus();
            } else if (TextUtils.isEmpty(breakdownPurchaseCost)) {
                purchaseCostLayout.setError("Enter Purchase Cost");
                purchaseCost.requestFocus();
                purchaseCost.findFocus();
            } else {
                returnData.returnData(mFreightCost, mImportDuties, mNonRefundTax, mSetupCost, mDismantlingCost, breakdownPurchaseCost);
                getDialog().dismiss();
            }
        });

        return view;
    }


    public interface ReturnData {
        void returnData(String fCost, String iDuties, String nRefundTax, String sCost, String dCost, String bPurchaseCost);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

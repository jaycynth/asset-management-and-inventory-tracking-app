package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ami.abc.ami_app.models.AssetScanned;


import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AssetsDao {

    @Insert(onConflict = REPLACE)
    void saveAssetScanned(AssetScanned assets);

    @Update(onConflict = REPLACE)
    void updateAssetScanned(AssetScanned assetScanned);

    //observe the list in the room database
    @Query("SELECT * FROM AssetScanned")
    LiveData<List<AssetScanned>> getAssetsScanned();

    @Delete
    void delete(AssetScanned assets);

    //delete all the assets in the room database upon sending information to the assets verified table
    @Query("DELETE FROM AssetScanned")
    void deleteAssets();


}

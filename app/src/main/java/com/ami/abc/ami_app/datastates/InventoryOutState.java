package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryOut;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryOutState {
    private InventoryOut inventoryOut;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;
    private FailError failError;

    public InventoryOutState(InventoryOut inventoryOut) {
        this.inventoryOut = inventoryOut;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;
    }

    public InventoryOutState(String message) {
        this.message = message;
        this.inventoryOut = null;
        this.errorThrowable = null;
        this.loginError = null;
        this.failError = null;

    }

    public InventoryOutState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.inventoryOut = null;
        this.loginError = null;
        this.failError = null;

    }

    public InventoryOutState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.inventoryOut = null;
        this.failError = null;

    }

    public InventoryOutState(FailError failError) {
        this.failError = failError;
        this.message = null;
        this.errorThrowable = null;
        this.inventoryOut = null;
        this.loginError = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public InventoryOut getInventoryOut() {
        return inventoryOut;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.DynamicFieldAdapter;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.AssetCheckInOutDetails;
import com.ami.abc.ami_app.models.AssetCheckOutReport;
import com.ami.abc.ami_app.models.AssetCheckPoint;
import com.ami.abc.ami_app.models.AssetCheckedOut;
import com.ami.abc.ami_app.models.AssetCheckedOutPoint;
import com.ami.abc.ami_app.models.Custodian;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.AssetCheckOutViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetCheckOutActivity extends AppCompatActivity {


    @BindView(R.id.asset_id)
    EditText asset_code;

    String building, room, depart, assetId;

    @BindView(R.id.custodian)
    AutoCompleteTextView custodian;

    AssetCheckOutViewModel assetCheckedOutViewModel;
    String accessToken;

    @BindView(R.id.holder_layout)
    LinearLayout holder_layout;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    @BindView(R.id.check_out)
    Button checkOut;

    @BindView(R.id.asset_name)
    TextInputEditText assetName;
    @BindView(R.id.serial_number)
    TextInputEditText serialNumber;
    @BindView(R.id.model)
    TextInputEditText model;

    String iModel, iSerial, name, barCode, custodianTag, tag;

    @BindView(R.id.confirm)
    Button confirm;
    @BindView(R.id.details_layout)
    LinearLayout detailsLayout;

    @BindView(R.id.custodian_title)
    TextView enterCustodian;

    @BindView(R.id.input_custodian_layout)
    RelativeLayout inputCustodianLayout;


    private List<String> custodianName = new ArrayList<>();

//    @BindView(R.id.dynamic_one)
//    TextView dynamicOne;
//    @BindView(R.id.dynamic_one_value)
//    EditText dynamicOneValue;

//    @BindView(R.id.dynamic_field_rv)
//    RecyclerView card;

    static RecyclerView.LayoutManager layoutManager;
    DynamicFieldAdapter dynamicFieldAdapter;

    List<String> dynamicfieldList = new ArrayList();


    @BindView(R.id.dynamic_fields_title)
    TextView dynamicsTitle;

    List<String> dynamicFieldValues = new ArrayList();

    @BindView(R.id.dynamic_fields_layout)
    LinearLayout dynamicFieldsLayout;

    @BindView(R.id.milage)
    TextInputEditText milage;

    @BindView(R.id.fuel)
    TextInputEditText fuel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_check_out);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.check_out_title));

        Intent scanIntents = getIntent();
        building = scanIntents.getStringExtra("building");
        room = scanIntents.getStringExtra("room");
        depart = scanIntents.getStringExtra("mDepartment");
        assetId = scanIntents.getStringExtra("scanResult");


        //for custodian tag scan
        name = scanIntents.getStringExtra("assetName");
        barCode = scanIntents.getStringExtra("barCode");
        iSerial = scanIntents.getStringExtra("serial");
        iModel = scanIntents.getStringExtra("model");
        custodianTag = scanIntents.getStringExtra("custodianTag");
        tag = scanIntents.getStringExtra("TAG");

        asset_code.requestFocus();


        custodian.setText(custodianTag);

        if (assetId != null) {
            asset_code.setText(assetId);
        } else if (barCode != null) {
            asset_code.setText(barCode);
        } else {
            asset_code.setText(" ");
        }
        if (name != null) {
            assetName.setText(name);
        }
        if (iSerial != null) {
            serialNumber.setText(iSerial);
        }
        if (iModel != null) {
            model.setText(iModel);
        }

        assetCheckedOutViewModel = ViewModelProviders.of(this).get(AssetCheckOutViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();

        /* the second check is for when scanning the custodian tag such that when the user scan the custodian tag and comes back to
        the activity, it does not execute the get details for the asset scanned since we already have the details
        and the call requires the asset id as a parameter
      */
        if (accessToken != null && tag.equals("I") && assetId != null) {
            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);

            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);

            assetCheckedOutViewModel.assetCheck("Bearer " + accessToken);
            // assetCheckedOutViewModel.getAssetCheckInOutDetails("Bearer " + accessToken,assetId);


        }

        if (tag.equals("Y")) {

            confirm.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);

        }

        confirm.setOnClickListener(v -> {
            String code = asset_code.getText().toString().trim();
            if (TextUtils.isEmpty(code)) {
                Toast.makeText(this, "Enter Item Code", Toast.LENGTH_SHORT).show();
            } else {
                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                assetCheckedOutViewModel.assetCheck("Bearer " + accessToken);
                //assetCheckedOutViewModel.getAssetCheckInOutDetails("Bearer " + accessToken,assetId);

            }
        });

        enterCustodian.setOnClickListener(v -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Choose Action : ");
            alert.setCancelable(false);
            alert.setNegativeButton("Scanner", ((dialog, which) -> {
                inputCustodianLayout.setVisibility(View.VISIBLE);
                inputCustodianLayout.requestFocus();

            }));

            alert.setPositiveButton("Camera", (dialog, which) -> {

                inputCustodianLayout.setVisibility(View.VISIBLE);


                String code = asset_code.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(this, "Enter Your Code First", Toast.LENGTH_SHORT).show();
                } else {
                    Intent scanTag = new Intent(this, ScanActivity.class);
                    scanTag.putExtra("TAG", "Y");
                    scanTag.putExtra("barCode", code);
                    scanTag.putExtra("assetName", name);
                    scanTag.putExtra("model", iModel);
                    scanTag.putExtra("serial", iSerial);
                    startActivity(scanTag);

                }
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();

        });

        //set up custodian drop down
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, custodianName);
        custodian.setAdapter(adapter2);

        assetCheckedOutViewModel.getAllCustodiansResponse().observe(this, custodianState -> {
            assert custodianState != null;
            if (custodianState.getAllCustodians() != null) {
                handleCustodians(custodianState.getAllCustodians());
            }
            if (custodianState.getMessage() != null) {
                handleCustodianNetworkResponse(custodianState.getMessage());
            }
            if (custodianState.getErrorThrowable() != null) {
                handleCustodianError(custodianState.getErrorThrowable());
            }
            if (custodianState.getLoginError() != null) {
                handleUnAuthorized(custodianState.getLoginError());
            }
        });
        assetCheckedOutViewModel.assetCheckResponse().observe(this, assetCheckState -> {
            assert assetCheckState != null;
            if (assetCheckState.getAssetCheckPoint() != null) {
                handleAssetCheck(assetCheckState.getAssetCheckPoint());
            }
            if (assetCheckState.getMessage() != null) {
                handleNetworkResponse(assetCheckState.getMessage());
            }
            if (assetCheckState.getErrorThrowable() != null) {
                handleError(assetCheckState.getErrorThrowable());
            }
            if (assetCheckState.getLoginError() != null) {
                handleUnAuthorized(assetCheckState.getLoginError());
            }
        });

        assetCheckedOutViewModel.assetCheckInOutDetails().observe(this, assetCheckInOutDetailsState -> {
            assert assetCheckInOutDetailsState != null;
            if (assetCheckInOutDetailsState.getAssetCheckInOutDetails() != null) {
                handleAssetCheckInOutDetails(assetCheckInOutDetailsState.getAssetCheckInOutDetails());
            }
            if (assetCheckInOutDetailsState.getMessage() != null) {
                handleNetworkResponse(assetCheckInOutDetailsState.getMessage());
            }
            if (assetCheckInOutDetailsState.getErrorThrowable() != null) {
                handleError(assetCheckInOutDetailsState.getErrorThrowable());
            }

        });

        assetCheckedOutViewModel.checkOutAssetResponse().observe(this, assetCheckedOutPointState -> {
            assert assetCheckedOutPointState != null;
            if (assetCheckedOutPointState.getAssetCheckedOutPoint() != null) {
                handleCheckOutAsset(assetCheckedOutPointState.getAssetCheckedOutPoint());
            }
            if (assetCheckedOutPointState.getMessage() != null) {
                handleCheckOutNetworkResponse(assetCheckedOutPointState.getMessage());
            }
            if (assetCheckedOutPointState.getErrorThrowable() != null) {
                handleError(assetCheckedOutPointState.getErrorThrowable());
            }
            if (assetCheckedOutPointState.getLoginError() != null) {
                handleUnAuthorized(assetCheckedOutPointState.getLoginError());
            }
            if (assetCheckedOutPointState.getFailError() != null) {
                handleFail(assetCheckedOutPointState.getFailError());
            }
        });

        assetCheckedOutViewModel.getAssetScannedDetailsResponse().observe(this, getAssetScannedDetailsState -> {
            assert getAssetScannedDetailsState != null;
            if (getAssetScannedDetailsState.getGetAssetScannedDetails() != null) {
                handleDetails(getAssetScannedDetailsState.getGetAssetScannedDetails());
            }
            if (getAssetScannedDetailsState.getMessage() != null) {
                handleNetworkResponse(getAssetScannedDetailsState.getMessage());
            }
            if (getAssetScannedDetailsState.getErrorThrowable() != null) {
                handleError(getAssetScannedDetailsState.getErrorThrowable());
            }
            if (getAssetScannedDetailsState.getLoginError() != null) {
                handleUnAuthorized(getAssetScannedDetailsState.getLoginError());
            }
            if (getAssetScannedDetailsState.getFailError() != null) {
                handleFailError(getAssetScannedDetailsState.getFailError());
            }
        });


        checkOut.setOnClickListener(v -> {
            String productCode = asset_code.getText().toString().trim();
            String cust = custodian.getText().toString().trim();
            String mil = milage.getText().toString().trim();
            String fu = fuel.getText().toString().trim();


            if (TextUtils.isEmpty(cust)) {
                Toast.makeText(this, "Please enter the custodian", Toast.LENGTH_SHORT).show();

            } else {

                holder_layout.setAlpha(0.4f);
                progressBar.setVisibility(View.VISIBLE);
                assetCheckedOutViewModel.checkOutAsset("Bearer " + accessToken, productCode, cust, mil, fu);
            }
        });
    }


    private void handleAssetCheckInOutDetails(AssetCheckInOutDetails assetCheckInOutDetails) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = assetCheckInOutDetails.getStatus();
        if (status) {
            Asset asset = assetCheckInOutDetails.getAsset();

            name = asset.getName();
            iSerial = asset.getSerial();
            iModel = asset.getModel();

            assetName.setText(asset.getName());
            serialNumber.setText(asset.getSerial());
            model.setText(asset.getModel());
            dynamicfieldList = asset.getDynamicfields();
            if (asset.getCategory().getType().equals("vehicle")) {
                dynamicFieldsLayout.setVisibility(View.VISIBLE);

            }

        }

    }

    private void handleFail(FailError failError) {
        progressBar.setVisibility(View.GONE);
        holder_layout.setAlpha(1);

        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(failError.getMessage());
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue checking out?");

        alert.setNeutralButton("Edit Details", (dialog, which) -> dialog.dismiss());
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, AssetCheckOutActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "I");
                    startActivity(scanAsset);
                    dialog.dismiss();
                });
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "I");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();


    }


    private void handleCustodians(AllCustodians allCustodians) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        boolean status = allCustodians.getStatus();
        if (status) {
            List<Custodian> custodianList = allCustodians.getCustodians();
            for (Custodian custodian : custodianList) {
                custodianName.add(custodian.getName());
            }
        }
    }

    private void handleCustodianNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Custodian" + message, Toast.LENGTH_SHORT).show();
    }

    private void handleCustodianError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }
    }

    private void handleAssetCheck(AssetCheckPoint assetCheckPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);

        confirm.setVisibility(View.GONE);
        detailsLayout.setVisibility(View.VISIBLE);

        boolean status = assetCheckPoint.getStatus();
        if (status) {
            List<com.ami.abc.ami_app.models.AssetCheck> assetsChecked = assetCheckPoint.getAssetcheck();

            String code = asset_code.getText().toString().trim();

            holder_layout.setAlpha(0.4f);
            progressBar.setVisibility(View.VISIBLE);
            assetCheckedOutViewModel.getAssetCheckInOutDetails("Bearer " + accessToken, code);
            assetCheckedOutViewModel.getAssetScannedDetails(code, "Bearer " + accessToken);

        }
    }


    private void handleDetails(GetAssetScannedDetails getAssetScannedDetails) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = getAssetScannedDetails.getStatus();
        if (status) {
            name = getAssetScannedDetails.getAsset().getName();
            iSerial = getAssetScannedDetails.getAsset().getSerial();
            iModel = getAssetScannedDetails.getAsset().getModel();


            assetName.setText(getAssetScannedDetails.getAsset().getName());
            serialNumber.setText(getAssetScannedDetails.getAsset().getSerial());
            model.setText(getAssetScannedDetails.getAsset().getModel());

            dynamicfieldList = getAssetScannedDetails.getAsset().getDynamicfields();
            if (getAssetScannedDetails.getAsset().getCategory().getType().equalsIgnoreCase("motor vehicle") ||
                    getAssetScannedDetails.getAsset().getCategory().getType().equalsIgnoreCase("motor vehicles") ||
                    getAssetScannedDetails.getAsset().getCategory().getType().equalsIgnoreCase("motorvehicles")) {
                dynamicFieldsLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void handleFailError(FailError failError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(failError.getMessage());
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue checking out?");

        alert.setNeutralButton("Exit", (dialog, which) -> {
                    onBackPressed();
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, AssetCheckOutActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "I");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "I");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void handleCheckOutAsset(AssetCheckedOutPoint assetCheckedOutPoint) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = assetCheckedOutPoint.getStatus();
        if (status) {
            AssetCheckedOut assetCheckedOut = assetCheckedOutPoint.getCheckedOut();
            AssetCheckOutReport assetsReports = new AssetCheckOutReport();
            assetsReports.setId(assetCheckedOut.getId());
            assetsReports.setBarcode(assetId);
            assetsReports.setName(assetName.getText().toString());
            assetsReports.setCustodian(custodian.getText().toString());
            assetCheckedOutViewModel.saveAssetsReports(assetsReports);

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(assetCheckedOutPoint.getMessage());
            alert.setCancelable(false);
            alert.setMessage("Do you want to continue checking out?");

            alert.setNeutralButton("Exit", (dialog, which) -> {
                        onBackPressed();
                        dialog.dismiss();
                    }
            );
            alert.setNegativeButton("Scanner", (dialog, which) -> {
                        Intent scanAsset = new Intent(this, AssetCheckOutActivity.class);
                        scanAsset.putExtra("building", building);
                        scanAsset.putExtra("room", room);
                        scanAsset.putExtra("mDepartment", depart);
                        scanAsset.putExtra("TAG", "I");
                        startActivity(scanAsset);
                        dialog.dismiss();
                    }
            );
            alert.setPositiveButton("Camera", (dialog, which) -> {
                Intent scanAsset = new Intent(this, ScanActivity.class);
                scanAsset.putExtra("building", building);
                scanAsset.putExtra("room", room);
                scanAsset.putExtra("mDepartment", depart);
                scanAsset.putExtra("TAG", "I");
                startActivity(scanAsset);
                dialog.dismiss();
            });
            AlertDialog dialog = alert.create();
            dialog.show();
        }

    }

    private void handleCheckOutNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(message);
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue checking out?");

        alert.setNeutralButton("Exit", (dialog, which) -> {
                    onBackPressed();
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, AssetCheckOutActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "I");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "I");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void handleNetworkResponse(String message) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(message);
        alert.setCancelable(false);
        alert.setMessage("Do you want to continue checking out?");

        alert.setNeutralButton("Exit", (dialog, which) -> {
                    onBackPressed();
                    dialog.dismiss();
                }
        );
        alert.setNegativeButton("Scanner", (dialog, which) -> {
                    Intent scanAsset = new Intent(this, AssetCheckOutActivity.class);
                    scanAsset.putExtra("building", building);
                    scanAsset.putExtra("room", room);
                    scanAsset.putExtra("mDepartment", depart);
                    scanAsset.putExtra("TAG", "I");
                    startActivity(scanAsset);
                    dialog.dismiss();
                }
        );
        alert.setPositiveButton("Camera", (dialog, which) -> {
            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("TAG", "I");
            startActivity(scanAsset);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private void handleError(Throwable errorThrowable) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
            Log.d("conversion", errorThrowable.getMessage());
        }

    }

    private void handleUnAuthorized(LoginError loginError) {
        holder_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, AssetsOptions.class);
        startActivity(loc);
    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.LoginError;

public class SupplierState {
    private AllSuppliers allSuppliers;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public SupplierState(AllSuppliers allSuppliers) {
        this.allSuppliers = allSuppliers;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
    }

    public SupplierState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.allSuppliers = null;
        this.loginError = null;
    }

    public SupplierState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.allSuppliers = null;
        this.loginError = null;
    }

    public SupplierState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.allSuppliers = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AllSuppliers getAllSuppliers() {
        return allSuppliers;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

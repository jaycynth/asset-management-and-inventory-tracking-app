package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.DivisionState;
import com.ami.abc.ami_app.models.Error;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DivisionsRepo {
    private ApiClient mApiClient;
    //constructor
    public DivisionsRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<DivisionState> getGetDivisions(String accessToken) {

        MutableLiveData<DivisionState> divisionStateMutableLiveData = new MutableLiveData<>();
        Call<GetDivisions> call = mApiClient.amiService().getDivisions(accessToken);
        call.enqueue(new Callback<GetDivisions>() {
            @Override
            public void onResponse(Call<GetDivisions> call, Response<GetDivisions> response) {
                if (response.code() == 200) {
                    divisionStateMutableLiveData.setValue(new DivisionState(response.body()));

                } else if (response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    divisionStateMutableLiveData.setValue(new DivisionState(loginError));
                }
                else if (response.code() == 401){
                    Gson gson = new Gson();
                    Type type = new TypeToken<Error>() {
                    }.getType();
                    Error error = gson.fromJson(response.errorBody().charStream(), type);
                    divisionStateMutableLiveData.setValue(new DivisionState(error));
                }
                else {
                    divisionStateMutableLiveData.setValue(new DivisionState(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GetDivisions> call, Throwable t) {
                divisionStateMutableLiveData.setValue(new DivisionState(t));
            }
        });

        return divisionStateMutableLiveData;

    }
}

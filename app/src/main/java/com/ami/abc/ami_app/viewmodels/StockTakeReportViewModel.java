package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.StockTakeReport;
import com.ami.abc.ami_app.repos.StockTakeReportRepo;


import java.util.List;

public class StockTakeReportViewModel extends AndroidViewModel {
    private StockTakeReportRepo stockTakeReportRepo;

    private LiveData<List<StockTakeReport>> listLiveData;



    public StockTakeReportViewModel(Application application) {
        super(application);

        stockTakeReportRepo = new StockTakeReportRepo();

        listLiveData = stockTakeReportRepo.getInventorysReports();
    }

    public LiveData<List<StockTakeReport>> getListLiveData() {
        return listLiveData;
    }



    public void saveScannedInventory(StockTakeReport stockTakeReport) {
        stockTakeReportRepo.saveInventorys(stockTakeReport);
    }

}

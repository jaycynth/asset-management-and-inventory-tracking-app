package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.InventoryCheckPoint;
import com.ami.abc.ami_app.models.LoginError;

public class InventoryCheckPointState {
    private InventoryCheckPoint inventoryCheckPoint;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public InventoryCheckPointState(InventoryCheckPoint inventoryCheckPoint) {
        this.inventoryCheckPoint = inventoryCheckPoint;
        this.errorThrowable = null;
        this.message = null;
        this.loginError = null;
    }

    public InventoryCheckPointState(String message) {
        this.message = message;
        this.inventoryCheckPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public InventoryCheckPointState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.inventoryCheckPoint = null;
        this.loginError = null;
    }

    public InventoryCheckPointState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.inventoryCheckPoint = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public InventoryCheckPoint getInventoryCheckPoint() {
        return inventoryCheckPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorTrowable() {
        return errorThrowable;
    }
}

package com.ami.abc.ami_app.views.activities;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.AllStores;
import com.ami.abc.ami_app.models.LoginError;

import com.ami.abc.ami_app.models.Shelf;
import com.ami.abc.ami_app.models.Store;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.InventoryLocationViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InventoryLocation extends AppCompatActivity {


    @BindView(R.id.store)
    AutoCompleteTextView store;
    @BindView(R.id.shelf)
    AutoCompleteTextView shelf;

    @BindView(R.id.choose)
    AutoCompleteTextView choose;


    @BindView(R.id.drop_down_store)
    ImageView drop_down_store;

    @BindView(R.id.drop_down_shelf)
    ImageView drop_down_shelf;
    @BindView(R.id.drop_down_choice)
    ImageView drop_down_choice;


    private List<Store> storeList = new ArrayList<>();
    private List<String> storeName = new ArrayList<>();

    List<Shelf> shelfList = new ArrayList<>();
    private List<String> shelfName = new ArrayList<>();

    private List<String> choiceList = new ArrayList<>();


    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.spin_kit)
    ProgressBar progressBar;

    String Tag;

    @BindView(R.id.main_spin_kit)
    ProgressBar main_spin_kit;

    @BindView(R.id.layout_location_details)
    LinearLayout layout_location_details;


    String accessToken;
    InventoryLocationViewModel inventoryLocationViewModel;

    int storeId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_location);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.location_title));


        Intent getTag = getIntent();
        Tag = getTag.getStringExtra("TAG");

        inventoryLocationViewModel = ViewModelProviders.of(this).get(InventoryLocationViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            main_spin_kit.setVisibility(View.VISIBLE);
            layout_location_details.setAlpha(0.3f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            inventoryLocationViewModel.getStores("Bearer " + accessToken);
        }


        inventoryLocationViewModel.getStoreResponse().observe(this, storeState -> {
            assert storeState != null;
            if (storeState.getAllStores() != null) {
                handleGetAllStores(storeState.getAllStores());
            }

            if (storeState.getMessage() != null) {
                handleNetworkResponse(storeState.getMessage());
            }

            if (storeState.getErrorThrowable() != null) {
                handleError(storeState.getErrorThrowable());
            }
            if (storeState.getLoginError() != null) {
                handleUnAuthorized(storeState.getLoginError());
            }
        });

        inventoryLocationViewModel.getShelvesInStoreResponse().observe(this, shelvesInStoreState -> {
            assert shelvesInStoreState != null;
            if (shelvesInStoreState.getAllShelves() != null) {
                handleShelvesInStore(shelvesInStoreState.getAllShelves());
            }
            if (shelvesInStoreState.getMessage() != null) {
                handleNetworkResponse(shelvesInStoreState.getMessage());
            }

            if (shelvesInStoreState.getErrorThrowable() != null) {
                handleError(shelvesInStoreState.getErrorThrowable());
            }
            if (shelvesInStoreState.getLoginError() != null) {
                handleUnAuthorized(shelvesInStoreState.getLoginError());
            }
        });


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, storeName);
        store.setAdapter(adapter);

        drop_down_store.setOnClickListener(v -> store.showDropDown());
        store.setOnClickListener(v -> store.showDropDown());

        store.setOnItemClickListener((parent, view, position, id) -> {
            String mStoreName = (String) parent.getItemAtPosition(position);
            if (storeList.get(position).getName().equalsIgnoreCase(mStoreName)) {
                storeId = storeList.get(position).getId();

                if (storeId == 0) {
                    shelfName.add("None");
                } else {
                    main_spin_kit.setVisibility(View.VISIBLE);
                    layout_location_details.setAlpha(0.3f);

                    if (!shelfName.isEmpty()) {
                        shelfName.clear();
                    }

                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    inventoryLocationViewModel.getShelvesInStore("Bearer " + accessToken, storeId);
                }

            }
        });

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, shelfName);
        shelf.setAdapter(adapter1);

        drop_down_shelf.setOnClickListener(v -> shelf.showDropDown());

        shelf.setOnClickListener(v -> shelf.showDropDown());

        choiceList.add("Camera");
        choiceList.add("Scanner");
        ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, choiceList);
        choose.setAdapter(adapter5);

        choose.setOnClickListener(v -> choose.showDropDown());
        drop_down_choice.setOnClickListener(v -> choose.showDropDown());


        submit.setOnClickListener(v -> {
            //Ensuring user has filled all the details before proceeding
            String mStore = store.getText().toString().trim();
            String mShelf = shelf.getText().toString().trim();
            String mChoice = choose.getText().toString().trim();


            if (TextUtils.isEmpty(mStore)) {
                Toast.makeText(this, "Please input the store name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mShelf)) {
                Toast.makeText(this, "Please input the shelf name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mChoice)) {
                Toast.makeText(this, "Please select the action you want to use", Toast.LENGTH_SHORT).show();
            } else {
                submit.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(this, "Please wait as we confirm your location", Toast.LENGTH_SHORT).show();
                //send a get request to get the details of your location
                //returns the name of the location then you can start updating data
                if (mChoice.equals("Camera")) {

                    new Handler().postDelayed(() -> {
                        switch (Tag) {
                            case "D": {
                                Intent scan = new Intent(InventoryLocation.this, ScanActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "E": {
                                Intent scan = new Intent(InventoryLocation.this, ScanActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "F": {
                                Intent scan = new Intent(InventoryLocation.this, ScanActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                            case "G": {
                                Intent scan = new Intent(InventoryLocation.this, ScanActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "H": {
                                Intent scan = new Intent(InventoryLocation.this, ScanActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                        }

                    }, 2000);

                } else {

                    new Handler().postDelayed(() -> {
                        switch (Tag) {
                            case "D": {
                                Intent scan = new Intent(InventoryLocation.this, InventoryIn.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "E": {
                                Intent scan = new Intent(InventoryLocation.this, InventoryOut.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "F": {
                                Intent scan = new Intent(InventoryLocation.this, StockTakeActivity.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                            case "G": {
                                Intent scan = new Intent(InventoryLocation.this, InventoryCheckOut.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "H": {
                                Intent scan = new Intent(InventoryLocation.this, InventoryCheckIn.class);
                                scan.putExtra("building", mStore);
                                scan.putExtra("room", mShelf);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                        }

                    }, 2000);

                }

            }
        });
    }

    private void handleShelvesInStore(AllShelves allShelves) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        shelfList = allShelves.getShelves();
        if(shelfList.isEmpty()){
            shelfName.add("None");
        }else {
            for (Shelf shelf : shelfList) {
                String sName = shelf.getDescription();
                shelfName.add(sName);
            }
        }


    }

    private void handleGetAllStores(AllStores allStores) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        storeList = allStores.getStores();
        Store none = new Store(0, "None");
        storeList.add(none);
        for (Store store : storeList) {
            String sName = store.getName();
            storeName.add(sName);
        }


    }



    private void handleError(Throwable errorThrowable) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent loc = new Intent(this, InventoryOptions.class);
        startActivity(loc);
    }
}

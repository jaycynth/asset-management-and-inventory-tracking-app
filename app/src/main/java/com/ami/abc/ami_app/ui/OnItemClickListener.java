package com.ami.abc.ami_app.ui;

public interface OnItemClickListener {
    void onItemClick(int position);
}

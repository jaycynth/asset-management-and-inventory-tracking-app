package com.ami.abc.ami_app.api;

import com.ami.abc.ami_app.models.AddAsset;
import com.ami.abc.ami_app.models.AddAssetImage;
import com.ami.abc.ami_app.models.AddInventory;
import com.ami.abc.ami_app.models.AddInventoryImage;
import com.ami.abc.ami_app.models.AllAssets;
import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.AllCategories;
import com.ami.abc.ami_app.models.AllCustodians;
import com.ami.abc.ami_app.models.AllDepartments;
import com.ami.abc.ami_app.models.AllInventory;
import com.ami.abc.ami_app.models.AllRooms;
import com.ami.abc.ami_app.models.AllShelves;
import com.ami.abc.ami_app.models.AllStores;
import com.ami.abc.ami_app.models.AllSuppliers;
import com.ami.abc.ami_app.models.AllVerifiedStatus;
import com.ami.abc.ami_app.models.AssetCheckInOutDetails;
import com.ami.abc.ami_app.models.AssetCheckPoint;
import com.ami.abc.ami_app.models.AssetCheckedInPoint;
import com.ami.abc.ami_app.models.AssetCheckedOutPoint;
import com.ami.abc.ami_app.models.BulkStockTakeEntry;
import com.ami.abc.ami_app.models.CurrentAccountType;
import com.ami.abc.ami_app.models.GetBuildingsInDivision;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.models.GetInventoryGone;
import com.ami.abc.ami_app.models.GetRole;
import com.ami.abc.ami_app.models.GetSubCategory;
import com.ami.abc.ami_app.models.InventoryCheckedInPoint;
import com.ami.abc.ami_app.models.InventoryCheckedOutPoint;
import com.ami.abc.ami_app.models.DecrementInventory;
import com.ami.abc.ami_app.models.GetAssetScannedDetails;
import com.ami.abc.ami_app.models.GetRoomInBuilding;
import com.ami.abc.ami_app.models.IncrementInventory;
import com.ami.abc.ami_app.models.InventoryCheckPoint;
import com.ami.abc.ami_app.models.InventoryOut;
import com.ami.abc.ami_app.models.InventoryScannedStrict;
import com.ami.abc.ami_app.models.Login;
import com.ami.abc.ami_app.models.Logout;
import com.ami.abc.ami_app.models.MoveAsset;
import com.ami.abc.ami_app.models.ResetPassword;
import com.ami.abc.ami_app.models.StockReturn;
import com.ami.abc.ami_app.models.StockTake;
import com.ami.abc.ami_app.models.TwoFactorAuth;
import com.ami.abc.ami_app.models.VerifyAsset;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("login")
    Call<Login> loginUser(
            @Field("username") String email,
            @Field("password") String password,
            @Field("grant_type") String grant_type,
            @Field("client_id") int client_id,
            @Field("client_secret") String client_secret,
            @Field("type") String type,
            @Field("withlogout") Boolean withLogout
    );

    @Headers({"Accept: application/json"})
    @GET("getbuildings")
    Call<AllBuildings> getBuildings(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getrooms")
    Call<AllRooms> getRooms(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getdepartments")
    Call<AllDepartments> getDepartments(
            @Header("Authorization") String accessToken

    );

    @Headers({"Accept: application/json"})
    @GET("getroomsinbuilding")
    Call<GetRoomInBuilding> getRoomsInBuilding(
            @Query("building_id") int buildingId,
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getstores")
    Call<AllStores> getStores(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getshelfsinstore")
    Call<AllShelves> getShelvesInStore(
            @Header("Authorization") String accessToken,
            @Query("store_id") int storeId
    );

    @Headers({"Accept: application/json"})
    @GET("getshelves")
    Call<AllShelves> getAllShelves(
            @Header("Authorization") String accessToken
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("postasset")
    Call<AddAsset> postAsset(
            @Header("Authorization") String accessToken,
            @Field("code") String code,
            @Field("name") String name,
            @Field("status") String status,
            @Field("serial") String serial,
            @Field("model") String model,
            @Field("building") String building,
            @Field("room") String room,
            @Field("division") String division,
            @Field("supplier") String supplier,
            @Field("date_of_purchase") String date_of_purchase,
            @Field("purchase_cost") String purchase_cost,
            @Field("est_end_of_life_cost") String est_end_of_life_cost,
            @Field("depart") String depart,
            @Field("cust") String cust,
            @Field("asset_category") String asset_category,
            @Field("sub_category_id") int sub_category_id,
            @Field("freight_cost") String freight_cost,
            @Field("import_duties") String import_duties,
            @Field("non_refundable_taxes") String non_refundable_tax,
            @Field("set_up_cost") String setup_cost,
            @Field("dismantling_costs") String dismantling_cost,
            @Field("breakdown_purchase_cost") String breakdown_purchase_cost,
            @Field("breakdown") Boolean breakdown);


    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("postAssetWithComponents")
    Call<AddAsset> postAssetWithComponents(
            @Header("Authorization") String accessToken,
            @Body RequestBody requestBody
//            @Field("asset_category") String asset_category,
//            @Field("breakdown_purchase_cost") String breakdown_purchase_cost,
//            @Field("building") String building,
//            @Field("code") String code,
//            @Field("cust") String cust,
//            @Field("date_of_purchase") String date_of_purchase,
//            @Field("depart") String depart,
//            @Field("dismantling_costs") String dismantling_cost,
//            @Field("division") String division,
//            @Field("est_end_of_life_cost") String est_end_of_life_cost,
//            @Field("freight_cost") String freight_cost,
//            @Field("import_duties") String import_duties,
//            @Field("model") String model,
//            @Field("name") String name,
//            @Field("non_refundable_taxes") String non_refundable_tax,
//            @Field("purchase_cost") String purchase_cost,
//            @Field("room") String room,
//            @Field("serial") String serial,
//            @Field("set_up_cost") String setup_cost,
//            @Field("status") String status,
//            @Field("sub_category_id") int sub_category_id,
//            @Field("supplier") String supplier,
//            @Field("cacheComponents") String components
    );


    @Headers({"Accept: application/json"})
    @GET("getassets")
    Call<AllAssets> getAssets(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @POST("verifyassets")
    Call<VerifyAsset> verifyAssets(
            @Header("Authorization") String accessToken,
            @Body RequestBody requestBody

    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("moveasset/{asset_id}")
    Call<MoveAsset> moveAsset(
            @Header("Authorization") String accessToken,
            @Path("asset_id") int assetId,
            @Field("building") String building,
            @Field("room") String room,
            @Field("department") String department,
            @Field("division") String division,
            @Field("cust") String cust

    );


    @Headers({"Accept: application/json"})
    @GET("getassetsverified")
    Call<AllVerifiedStatus> getAssetsVerified(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getpurchasing")
    Call<AllSuppliers> getAllSuppliers(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("gettypes")
    Call<AllCategories> getAllCategories(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getcustodians")
    Call<AllCustodians> getAllCustodians(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getinventory")
    Call<AllInventory> getAllInventory(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("inventory/{id}")
    Call<IncrementInventory> incrementInventory(
            @Header("Authorization") String accessToken,
            @Path("id") int inventoryId,
            @Field("quantity") int quantity,
            @Field("type") String type
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("inventory/{id}")
    Call<DecrementInventory> decrementInventory(
            @Header("Authorization") String accessToken,
            @Path("id") int inventoryId,
            @Field("quantity") int quantity,
            @Field("type") String type
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("postinventory")
    Call<AddInventory> addInventory(
            @Header("Authorization") String accessToken,
            @Field("product_name") String productName,
            @Field("product_code") String productCode,
            @Field("store") String building,
            @Field("shelf") String room,
            @Field("supplier") String supplier,
            @Field("quantity") int quantity,
            @Field("unit_price") int unitPrice,
            @Field("reorder_level") int reorderLevel);


    @Headers({"Accept: application/json"})
    @GET("getinventoryscanned")
    Call<StockTake> getInventoryScanned(
            @Query("code") String code,
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("getinventoryscannedstrict")
    Call<InventoryScannedStrict> getInventoryScannedStrict(
            @Query("code") String code,
            @Query("store") String store,
            @Header("Authorization") String accessToken

    );


    @Headers({"Accept: application/json"})
    @GET("getassetscanned")
    Call<GetAssetScannedDetails> getAssetsScanned(
            @Query("code") String code,
            @Header("Authorization") String accessToken
    );

//    @FormUrlEncoded
//    @POST("stocktake")
//    Call<PostStockTake> postStockTake(
//            @Header("Authorization") String accessToken,
//            @Field("product_code") String productCode,
//            @Field("quantity") int quantity
//
//    );

    @Headers({"Accept: application/json"})
    @POST("stocktake")
    Call<BulkStockTakeEntry> bulkStockEntry(
            @Header("Authorization") String accessToken,
            @Body RequestBody requestBody

    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("checkoutinventory")
    Call<InventoryCheckedOutPoint> checkOutInventory(
            @Header("Authorization") String accessToken,
            @Field("product_code") String productCode,
            @Field("custodian") String custodian,
            @Field("quantity") int quantity

    );

    @Headers({"Accept: application/json"})
    @GET("checkinoutinv")
    Call<InventoryCheckPoint> inventoryCheck(
            @Header("Authorization") String accessToken
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("checkininventory")
    Call<InventoryCheckedInPoint> checkInInventory(
            @Header("Authorization") String accessToken,
            @Field("product_code") String productCode,
            @Field("quantity") int quantity

    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("checkoutasset")
    Call<AssetCheckedOutPoint> checkOutAsset(
            @Header("Authorization") String accessToken,
            @Field("code") String code,
            @Field("custodian") String custodian,
            @Field("mileage") String mileage,
            @Field("fuel") String fuel
    );

    @Headers({"Accept: application/json"})
    @GET("checkinoutast")
    Call<AssetCheckPoint> assetCheck(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("checkinasset")
    Call<AssetCheckedInPoint> checkInAsset(
            @Header("Authorization") String accessToken,
            @Field("code") String code,
            @Field("mileage") String mileage,
            @Field("fuel") String fuel
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("inventoryout/{id}")
    Call<InventoryOut> outInventory(
            @Path("id") int id,
            @Header("Authorization") String accessToken,
            @Field("customer") String customer,
            @Field("invoice_number") String invoiceNumber,
            @Field("invoice_value") int invoiceValue,
            @Field("quantity") int quantity
    );


    @Headers({"Accept: application/json"})
    @GET("getinventoryout")
    Call<GetInventoryGone> inventoryGone(
            @Header("Authorization") String accessToken,
            @Query("code") String code,
            @Query("invoice_number") String invoiceNumber
    );


    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @PATCH("stockreturn")
    Call<StockReturn> stockReturn(
            @Header("Authorization") String accessToken,
            @Field("inventory_id") int inventoryId,
            @Field("invoice_number") String invoiceNumber,
            @Field("quantity") int quantity
    );

    @Headers({"Accept: application/json"})
    @POST("logout")
    Call<Logout> logout(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("password/create")
    Call<ResetPassword> resetPassword(
            @Header("Authorization") String accessToken,
            @Field("email") String email

    );

    @Headers({"Accept: application/json"})
    @Multipart
    @POST("updateAssetImg/{asset_id}")
    Call<AddAssetImage> addAssetImage(
            @Header("Authorization") String accessToken,
            @Part("type") RequestBody mobile,
            @Part MultipartBody.Part file, @Part("photo") RequestBody photo,
            @Path("asset_id") int assetId
    );

    @Headers({"Accept: application/json"})
    @Multipart
    @POST("updateInventoryImg/{inventory_id}")
    Call<AddInventoryImage> addInventoryImage(
            @Header("Authorization") String accessToken,
            @Part("type") RequestBody mobile,
            @Part MultipartBody.Part file, @Part("photo") RequestBody photo,
            @Path("inventory_id") int inventoryId
    );


    @Headers({"Accept: application/json"})
    @GET("user")
    Call<GetRole> getRoles(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("createCode")
    Call<TwoFactorAuth> twoFactorAuth(
            @Header("Authorization") String accessToken,
            @Field("2fa") String code
    );

    @Headers({"Accept: application/json"})
    @GET("astcheckinoutdetails")
    Call<AssetCheckInOutDetails> getAssetCheckInOutDetails(
            @Header("Authorization") String accessToken,
            @Query("code") String code
    );


    @Headers({"Accept: application/json"})
    @GET("getDivisions")
    Call<GetDivisions> getDivisions(
            @Header("Authorization") String accessToken

    );

    @Headers({"Accept: application/json"})
    @GET("getbuildingsindivision")
    Call<GetBuildingsInDivision> getBuildingsInDivision(
            @Query("division_id") int divisionId,
            @Header("Authorization") String accessToken
    );


    @Headers({"Accept: application/json"})
    @GET("get_sub_categories")
    Call<GetSubCategory> getSubCategories(
            @Header("Authorization") String accessToken
    );

    @Headers({"Accept: application/json"})
    @GET("accountcurrent")
    Call<CurrentAccountType> getCurrentAccountType(
            @Header("Authorization") String accessToken
    );

}

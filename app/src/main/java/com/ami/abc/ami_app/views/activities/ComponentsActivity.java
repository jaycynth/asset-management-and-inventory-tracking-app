package com.ami.abc.ami_app.views.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.AddAsset;
import com.ami.abc.ami_app.models.AddAssetImage;
import com.ami.abc.ami_app.models.Asset;
import com.ami.abc.ami_app.models.CacheComponents;
import com.ami.abc.ami_app.utils.AssetStatusData;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.NewAssetViewModel;
import com.ami.abc.ami_app.views.fragments.BreakDownCostDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.ami.abc.ami_app.utils.Constants.REQUEST_PICTURE_CAPTURE;

public class ComponentsActivity extends AppCompatActivity {

    @BindView(R.id.component_code_layout)
    TextInputLayout component_layout;
    @BindView(R.id.component_id)
    TextInputEditText component_code;

    @BindView(R.id.component_name)
    TextInputEditText component_name;

    @BindView(R.id.serial_number)
    TextInputEditText serial_number;

    @BindView(R.id.model)
    TextInputEditText nModel;

    @BindView(R.id.date)
    TextInputEditText mDate;
    @BindView(R.id.date_layout)
    TextInputLayout mDateLayout;


    @BindView(R.id.supplier)
    AutoCompleteTextView nSupplier;
    @BindView(R.id.supplier_layout)
    TextInputLayout supplier_layout;
    @BindView(R.id.supplier_drop_down)
    ImageView supplier_drop_down;

    @BindView(R.id.status)
    AutoCompleteTextView nStatus;
    @BindView(R.id.status_layout)
    TextInputLayout status_layout;
    @BindView(R.id.status_drop_down)
    ImageView status_drop_down;

    //fields for the additional breakdown cost
    String freightCost, importDuties, nonRefundableTax, setUpCost, dismantlingCost, breakdownPurchaseCost;
    Boolean breakdown = false;

    //track if asset is already uploaded per transaction or not
    Boolean assetUpdateStatus = false;

    @BindView(R.id.break_down_switch)
    Switch breakDownSwitch;

    @BindView(R.id.at_cost_layout)
    TextInputLayout at_cost_layout;
    @BindView(R.id.at_cost)
    TextInputEditText at_cost;

    @BindView(R.id.lifespan_layout)
    TextInputLayout lifespan_layout;
    @BindView(R.id.lifespan)
    TextInputEditText lifespan;


    @BindView(R.id.end_estimated_cost)
    TextInputEditText scrap_cost;

    @BindView(R.id.add_component)
    Button add_component;

    @BindView(R.id.components_container_layout)
    LinearLayout components_layout;
    @BindView(R.id.components_spin_kit)
    ProgressBar progressBar;


    private String assetName, assetCode, assetCust, assetScrapCost, assetDatePurchase, assetCategory,
            assetStatus, assetSerial, assetModel, assetSupplier, building, room, division, depart,
            assetFreightCost, assetDismantlingCost, assetNonRefundTax, assetSetUpCost, assetImportDuties, assetPurchaseCost, assetAtCost;

    private int subCategoryId;

    private NewAssetViewModel newAssetViewModel;
    private String accessToken;

    List<CacheComponents> cacheComponentsList = new ArrayList<>();

    String pictureFilePath;
    File imgFile;
    int id;
    int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_components);

        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_component));

        Intent intent = getIntent();
        assetName = intent.getStringExtra("assetName");
        assetCode = intent.getStringExtra("assetCode");
        assetCust = intent.getStringExtra("assetCust");
        assetScrapCost = intent.getStringExtra("assetScrapCost");
        assetDatePurchase = intent.getStringExtra("asseDatePurchase");
        assetCategory = intent.getStringExtra("assetCategory");
        assetStatus = intent.getStringExtra("assetStatus");
        assetSerial = intent.getStringExtra("assetSerial");
        assetModel = intent.getStringExtra("assetModel");
        assetSupplier = intent.getStringExtra("assetSupplier");
        building = intent.getStringExtra("building");
        room = intent.getStringExtra("room");
        division = intent.getStringExtra("division");
        subCategoryId = intent.getIntExtra("assetSubCategoryId", 0);
        depart = intent.getStringExtra("depart");
        assetFreightCost = intent.getStringExtra("assetFcost");
        assetImportDuties = intent.getStringExtra("assetImportDuties");
        assetDismantlingCost = intent.getStringExtra("assetDismantlingCost");
        assetNonRefundTax = intent.getStringExtra("assetNonRefundableTax");
        assetSetUpCost = intent.getStringExtra("assetSetUpCost");
        assetPurchaseCost = intent.getStringExtra("assetPurchaseCost");
        assetAtCost = intent.getStringExtra("assetAtCost");
        List<String> supplierName = intent.getStringArrayListExtra("suppliers");

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
            mDate.setText(sdf.format(myCalendar.getTime()));
        };
        mDate.setOnClickListener(v -> new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        newAssetViewModel = ViewModelProviders.of(this).get(NewAssetViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();


        newAssetViewModel.postAssetWithComponentsResponse().observe(this, assetWithComponentsState -> {
            if (assetWithComponentsState.getAddAsset() != null) {
                handlePost(assetWithComponentsState.getAddAsset());
            }
            if (assetWithComponentsState.getMessage() != null) {
                handleNetworkResponse(assetWithComponentsState.getMessage());
            }
            if (assetWithComponentsState.getThrowable() != null) {
                handleError(assetWithComponentsState.getThrowable());
            }
        });

        newAssetViewModel.addAssetImage().observe(this, addAssetImageState -> {
            assert addAssetImageState != null;
            if (addAssetImageState.getAddAssetImage() != null) {
                handleAddAssetImage(addAssetImageState.getAddAssetImage());
            }
            if (addAssetImageState.getMessage() != null) {
                handleNetworkResponse(addAssetImageState.getMessage());
            }
            if (addAssetImageState.getErrorThrowable() != null) {
                handleError(addAssetImageState.getErrorThrowable());
            }

        });

        purchaseCostBreakdown();

        //set up drop down for status;
        List<String> statusName = AssetStatusData.getAssetStatuses();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, statusName);
        nStatus.setAdapter(adapter1);

        status_drop_down.setOnClickListener(v -> nStatus.showDropDown());

        nStatus.setOnClickListener(v -> nStatus.showDropDown());

        //set up supplier drop down
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, Objects.requireNonNull(supplierName));
        nSupplier.setAdapter(adapter4);

        supplier_drop_down.setOnClickListener(v -> nSupplier.showDropDown());
        nSupplier.setOnClickListener(v -> nSupplier.showDropDown());


        add_component.setOnClickListener(v -> {
            String code = Objects.requireNonNull(component_code.getText()).toString().trim();
            String name = Objects.requireNonNull(component_name.getText()).toString().trim();
            String status = nStatus.getText().toString().trim();
            String date_of_purchase = Objects.requireNonNull(mDate.getText()).toString().trim();
            String serial = Objects.requireNonNull(serial_number.getText()).toString().trim();
            String model = Objects.requireNonNull(nModel.getText()).toString().trim();
            String nAtCost = Objects.requireNonNull(at_cost.getText()).toString().trim();
            String est_end_of_life_cost = Objects.requireNonNull(scrap_cost.getText()).toString().trim();
            String supplier = nSupplier.getText().toString().trim();
            String lSpan = Objects.requireNonNull(lifespan.getText()).toString().trim();

            if (TextUtils.isEmpty(name)) {
                component_layout.setError(getResources().getString(R.string.asset_name_error));
                component_name.requestFocus();
                component_name.findFocus();
            } else if (TextUtils.isEmpty(date_of_purchase)) {
                mDateLayout.setError(getResources().getString(R.string.date_error));
                mDate.requestFocus();
                mDate.findFocus();
            } else if (TextUtils.isEmpty(lSpan)) {
                lifespan_layout.setError(getResources().getString(R.string.date_error));
                lifespan.requestFocus();
                lifespan.findFocus();
            } else if (TextUtils.isEmpty(nAtCost)) {
                at_cost_layout.setError(getResources().getString(R.string.purchase_cost_error));
                this.at_cost.requestFocus();
                this.at_cost.findFocus();
            } else if (TextUtils.isEmpty(est_end_of_life_cost)) {
                at_cost_layout.setError(getResources().getString(R.string.end_cost_error));
                scrap_cost.requestFocus();
                scrap_cost.findFocus();
            } else {
                CacheComponents c = new CacheComponents();
                c.setId(count++);
                c.setName(name);
                c.setCode(code);
                c.setStatus(status);
                c.setDateOfPurchase(date_of_purchase);
                c.setLifespan(lSpan);
                c.setSupplier(supplier);
                c.setAtCost(nAtCost);
                c.setScrapValue(est_end_of_life_cost);
                c.setSerial(serial);
                c.setModel(model);
                newAssetViewModel.saveComponents(c);

                Toast.makeText(this, "Component added", Toast.LENGTH_SHORT).show();
                resetFields();
            }
        });
    }


    private void resetFields() {
        component_code.setText(" ");
        component_name.setText(" ");
        serial_number.setText(" ");
        nModel.setText(" ");
        at_cost.setText(" ");
        scrap_cost.setText(" ");
        nSupplier.setText(" ");
        lifespan.setText(" ");
        mDate.setText(" ");
        nStatus.setText(" ");
    }

    private void handleAddAssetImage(AddAssetImage addAssetImage) {
        components_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        boolean status = addAssetImage.getStatus();
        if (status) {
            continueScanningDialog(addAssetImage.getMessage() + " Do you want to continue adding items ? ");

        }
    }

    private void handlePost(AddAsset addAsset) {
        components_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, addAsset.getMessage(), Toast.LENGTH_SHORT).show();
        id = addAsset.getAddedAsset().getId();
        takePhotoDialog(addAsset.getMessage());

    }

    private void takePhotoDialog(String addAsset) {
        AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
        alert1.setTitle(addAsset);
        alert1.setCancelable(false);
        alert1.setMessage("Do you want to add image photo");

        alert1.setPositiveButton("Yes", (dialog1, which1) -> {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    takePhoto();
                } else {
                    Toast.makeText(this, "You have no permissions set", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_PICTURE_CAPTURE);
                }

            } else {

                takePhoto();
            }

        });

        alert1.setNegativeButton("No", (dialog1, which1) -> {
            continueScanningDialog("Continue Scanning");

            dialog1.dismiss();
        });
        AlertDialog dialog = alert1.create();
        dialog.show();
    }

    private void continueScanningDialog(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage(message);
        alert.setPositiveButton("Camera", (dialog, which) -> {
            dialog.dismiss();

            Intent scanAsset = new Intent(this, ScanActivity.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("division", division);
            scanAsset.putExtra("TAG", "A");
            startActivity(scanAsset);

        });

        alert.setNeutralButton("Scanner", ((dialog, which) -> {
            Intent scanAsset = new Intent(this, AssetInComponents.class);
            scanAsset.putExtra("building", building);
            scanAsset.putExtra("room", room);
            scanAsset.putExtra("mDepartment", depart);
            scanAsset.putExtra("division", division);
            scanAsset.putExtra("TAG", "A");
            startActivity(scanAsset);
            dialog.dismiss();

        }));

        alert.setNegativeButton("Exit", (dialog, which) -> {
            dialog.dismiss();
            onBackPressed();

        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }


    private void takePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File pictureFile = getPictureFile();
            Uri photoURI;
            if ((Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)) {
                photoURI = FileProvider.getUriForFile(this, getPackageName() + ".provider", pictureFile);
            } else {
                photoURI = Uri.fromFile(pictureFile);
            }
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);

        }
    }

    private File getPictureFile() {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "AMI_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(pictureFile, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert image != null;
        pictureFilePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {

            if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
                imgFile = new File(pictureFilePath);
                if (imgFile.exists()) {
                    Toast.makeText(this, "Image taken successfully", Toast.LENGTH_SHORT).show();
                    component_layout.setAlpha(0.3f);
                    progressBar.setVisibility(View.VISIBLE);
                    newAssetViewModel.postAddAssetImage("Bearer " + accessToken, imgFile, id);
                }
            }
        }

    }

    private void handleNetworkResponse(String message) {
        components_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    private void handleError(Throwable errorThrowable) {
        components_layout.setAlpha(1);
        progressBar.setVisibility(View.GONE);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, "Network Failure", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Conversion Error", Toast.LENGTH_SHORT).show();
            Log.d("conversion", Objects.requireNonNull(errorThrowable.getMessage()));
        }
    }


    private void purchaseCostBreakdown() {
        //listen for changes on the switch and click on the purchase cost field

        breakDownSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //hide keyboard and show breakdown dialog
                toggleKeyboard(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();

            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });

        at_cost.setOnClickListener(v -> {
            if (breakDownSwitch.isChecked()) {
                at_cost.setFocusable(false);
                at_cost.setClickable(false);
                breakDownCostDialog();
            } else {
                at_cost.setFocusableInTouchMode(true);
                at_cost.setFocusable(true);
                toggleKeyboard(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });
    }


    private void toggleKeyboard(int showImplicit, int i) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(showImplicit, i);
        }
    }

    /*opens a dialog that shows additional fields of breakdown of purchase cost*/
    private void breakDownCostDialog() {
        BreakDownCostDialogFragment breakDownCostDialogFragment = new BreakDownCostDialogFragment();
        breakDownCostDialogFragment.setListener((fCost, iDuties, nRefundTax, sCost, dCost, bPurchaseCost) -> {

            assetUpdateStatus = true;
            int totalPurchase = Integer.parseInt(fCost) + Integer.parseInt(iDuties) +
                    Integer.parseInt(nRefundTax) + Integer.parseInt(sCost) + Integer.parseInt(dCost) +
                    Integer.parseInt(bPurchaseCost);

            at_cost.setText(String.valueOf(totalPurchase));

            //enable breakdown when values are returned
            breakdown = true;

            freightCost = fCost;
            importDuties = iDuties;
            nonRefundableTax = nRefundTax;
            setUpCost = sCost;
            dismantlingCost = dCost;
            breakdownPurchaseCost = bPurchaseCost;
        });

        Bundle bundle = new Bundle();
        bundle.putBoolean("assetPostStatus", assetUpdateStatus);
        bundle.putString("freightCost", freightCost);
        bundle.putString("importDuties", importDuties);
        bundle.putString("nonRefundableTax", nonRefundableTax);
        bundle.putString("setupCost", setUpCost);
        bundle.putString("dismantlingCost", dismantlingCost);
        bundle.putString("breakdownPurchaseCost", breakdownPurchaseCost);
        breakDownCostDialogFragment.setArguments(bundle);
        breakDownCostDialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.added_components_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.view_added_components:
                Intent intent = new Intent(this, AddedComponentsView.class);
                startActivity(intent);
                break;
            case R.id.post_asset:

                newAssetViewModel.getComponentsListLiveData().observe(this, components -> {
                    if (components != null && !components.isEmpty()) {
                        String componentsLists = serialize(components);

                        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), componentsLists);

                        components_layout.setAlpha(0.3f);
                        progressBar.setVisibility(View.VISIBLE);
                        newAssetViewModel.postAssetWithComponents("Bearer " + accessToken, requestBody);
                    }
                });


                newAssetViewModel.deleteAllComponents();

                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private String serialize(List<CacheComponents> cacheComponentsList) {
        Asset a = new Asset();
        a.setAssetCategory(assetCategory);
        a.setBreakdownPurchaseCost(assetPurchaseCost);
        a.setBuilding(building);
        a.setCode(assetCode);
        a.setCust(assetCust);
        a.setDateOfPurchase(assetDatePurchase);
        a.setDepart(depart);
        a.setDismantlingCosts(assetDismantlingCost);
        a.setDivision(division);
        a.setEstEndOfLifeCost(assetScrapCost);
        a.setFreightCost(assetFreightCost);
        a.setImportDuties(assetImportDuties);
        a.setModel(assetModel);
        a.setName(assetName);
        a.setNonRefundableTaxes(assetNonRefundTax);
        a.setPurchaseCost(assetAtCost);
        a.setRoom(room);
        a.setSerial(assetSerial);
        a.setSetUpCost(assetSetUpCost);
        a.setStatus(assetStatus);
        a.setSubCategoryId(subCategoryId);
        a.setSupplier(assetSupplier);

        Gson gson = new Gson();
        JsonElement je = gson.toJsonTree(a);
        JsonObject jo = new JsonObject();
        jo.add("asset", je);
        JsonElement je2 = gson.toJsonTree(cacheComponentsList);

        jo.add("cacheComponents", je2);

        return jo.toString();
    }

}
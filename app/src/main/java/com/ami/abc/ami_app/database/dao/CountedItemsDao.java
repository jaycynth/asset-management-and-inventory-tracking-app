package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.ami.abc.ami_app.models.CountedItems;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CountedItemsDao {

    @Insert(onConflict = REPLACE)
    void saveCountedItems(CountedItems countedItems);

    @Update(onConflict = REPLACE)
    void updateCountedItems(CountedItems countedItems);

    @Query("DELETE FROM CountedItems WHERE id=:itemId")
    void deleteItemById(long itemId);

    @Query("SELECT * FROM CountedItems")
    LiveData<List<CountedItems>> getCountedItems();



    @Delete
    void delete(CountedItems countedItems);

    @Query("DELETE FROM CountedItems")
    void deleteCountedItems();
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.GetSubCategory;

public class SubCategoriesState {

    private GetSubCategory  getSubCategory;
    private Throwable throwable;
    private String message;

    public SubCategoriesState(GetSubCategory getSubCategory) {
        this.getSubCategory = getSubCategory;
        this.throwable = null;
        this.message = null;
    }

    public SubCategoriesState(Throwable throwable) {
        this.throwable = throwable;
        this.getSubCategory = null;
        this.message = null;
    }

    public SubCategoriesState(String message) {
        this.message = message;
        this.throwable = null;
        this.getSubCategory = null;
    }

    public GetSubCategory getGetSubCategory() {
        return getSubCategory;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getMessage() {
        return message;
    }
}

package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.BulkStockTakeEntry;
import com.ami.abc.ami_app.models.FailError;

public class BulkStockState {
    private BulkStockTakeEntry bulkStockTakeEntry;
    private String message;
    private Throwable errorThrowable;
    private FailError failError;

    public BulkStockState(BulkStockTakeEntry bulkStockTakeEntry) {
        this.bulkStockTakeEntry = bulkStockTakeEntry;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
    }

    public BulkStockState(String message) {
        this.message = message;
        this.bulkStockTakeEntry = null;
        this.errorThrowable = null;
        this.failError = null;
    }

    public BulkStockState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.bulkStockTakeEntry = null;
        this.message = null;
        this.failError = null;
    }

    public BulkStockState(FailError failError) {
        this.failError = failError;
        this.errorThrowable = null;
        this.bulkStockTakeEntry = null;
        this.message = null;
        this.failError = null;
    }

    public FailError getFailError() {
        return failError;
    }

    public BulkStockTakeEntry getBulkStockTakeEntry() {
        return bulkStockTakeEntry;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

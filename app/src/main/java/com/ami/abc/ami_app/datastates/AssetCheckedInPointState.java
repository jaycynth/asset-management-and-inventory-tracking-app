package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.AssetCheckedInPoint;
import com.ami.abc.ami_app.models.LoginError;

public class AssetCheckedInPointState {
    private AssetCheckedInPoint assetCheckedInPoint;
    private String message;
    private Throwable errorThrowable;
    private LoginError loginError;

    public AssetCheckedInPointState(AssetCheckedInPoint assetCheckedInPoint) {
        this.assetCheckedInPoint = assetCheckedInPoint;
        this.message = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AssetCheckedInPointState(String message) {
        this.message = message;
        this.assetCheckedInPoint = null;
        this.errorThrowable = null;
        this.loginError = null;
    }

    public AssetCheckedInPointState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.message = null;
        this.assetCheckedInPoint = null;
        this.loginError = null;
    }

    public AssetCheckedInPointState(LoginError loginError) {
        this.loginError = loginError;
        this.assetCheckedInPoint = null;
        this.message = null;
        this.errorThrowable = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public AssetCheckedInPoint getAssetCheckedInPoint() {
        return assetCheckedInPoint;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

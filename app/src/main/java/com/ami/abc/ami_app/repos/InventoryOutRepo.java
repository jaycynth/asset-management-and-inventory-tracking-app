package com.ami.abc.ami_app.repos;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.ami.abc.ami_app.api.ApiClient;
import com.ami.abc.ami_app.datastates.InventoryOutState;
import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.InventoryOut;
import com.ami.abc.ami_app.models.LoginError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryOutRepo {

    private ApiClient mApiClient;
    //constructor
    public InventoryOutRepo(Application application) {
        mApiClient = new ApiClient(application);
    }

    public LiveData<InventoryOutState> inventoryOut(int id,String accessToken, String customer, String invoiceNumber,int invoiceValue, int quantity) {

        MutableLiveData<InventoryOutState> inventoryOutStateMutableLiveData = new MutableLiveData<>();
        Call<InventoryOut> call = mApiClient.amiService().outInventory(id, accessToken, customer, invoiceNumber, invoiceValue, quantity);
        call.enqueue(new Callback<InventoryOut>() {
            @Override
            public void onResponse(Call<InventoryOut> call, Response<InventoryOut> response) {
                if (response.code() == 200) {
                    inventoryOutStateMutableLiveData.setValue(new InventoryOutState(response.body()));

                }
                else if (response.code() == 401 || response.code() == 500) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginError>() {}.getType();
                    LoginError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryOutStateMutableLiveData.setValue(new InventoryOutState(loginError));
                }else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<FailError>() {}.getType();
                    FailError loginError = gson.fromJson(response.errorBody().charStream(),type);
                    inventoryOutStateMutableLiveData.setValue(new InventoryOutState(loginError));
                }
            }

            @Override
            public void onFailure(Call<InventoryOut> call, Throwable t) {
                inventoryOutStateMutableLiveData.setValue(new InventoryOutState(t));
            }
        });

        return inventoryOutStateMutableLiveData;

    }
}

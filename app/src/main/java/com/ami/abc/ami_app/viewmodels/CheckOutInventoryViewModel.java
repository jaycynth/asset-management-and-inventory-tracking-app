package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.CheckOutInventoryState;
import com.ami.abc.ami_app.datastates.CustodianState;
import com.ami.abc.ami_app.models.InventoryCheckOutReport;
import com.ami.abc.ami_app.repos.InventoryCheckOutRepo;
import com.ami.abc.ami_app.repos.CustodianRepo;
import com.ami.abc.ami_app.repos.InventoryCheckOutReportsRepo;

public class CheckOutInventoryViewModel extends AndroidViewModel {
    private MediatorLiveData<CheckOutInventoryState> checkOutInventoryStateMediatorLiveData;
    private InventoryCheckOutRepo inventoryCheckOutRepo;

    private MediatorLiveData<CustodianState> custodianStateMediatorLiveData;
    private CustodianRepo custodianRepo;

    private InventoryCheckOutReportsRepo inventoryCheckOutReportsRepo;

    public CheckOutInventoryViewModel(Application application) {
            super(application);
        checkOutInventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryCheckOutRepo = new InventoryCheckOutRepo(application);

        custodianStateMediatorLiveData = new MediatorLiveData<>();
        custodianRepo = new CustodianRepo(application);

        inventoryCheckOutReportsRepo = new InventoryCheckOutReportsRepo();

    }

    public LiveData<CheckOutInventoryState> checkOutInventoryResponse(){
        return checkOutInventoryStateMediatorLiveData;
    }

    public void checkOutInventory(String accessToken, String productCode, String custodian, int quantity){
        LiveData<CheckOutInventoryState> checkOutInventoryStateLiveData = inventoryCheckOutRepo.checkOutInventory(accessToken, productCode, custodian, quantity);
        checkOutInventoryStateMediatorLiveData.addSource(checkOutInventoryStateLiveData,
                checkOutInventoryStateMediatorLiveData -> {
                    if (this.checkOutInventoryStateMediatorLiveData.hasActiveObservers()){
                        this.checkOutInventoryStateMediatorLiveData.removeSource(checkOutInventoryStateLiveData);
                    }
                    this.checkOutInventoryStateMediatorLiveData.setValue(checkOutInventoryStateMediatorLiveData);
                });
    }

    public LiveData<CustodianState> getAllCustodiansResponse(){
        return custodianStateMediatorLiveData;
    }

    public void getAllCustodians(String accessToken){
        LiveData<CustodianState> custodianStateLiveData = custodianRepo.getAllCustodians(accessToken);
        custodianStateMediatorLiveData.addSource(custodianStateLiveData,
                custodianStateMediatorLiveData -> {
                    if (this.custodianStateMediatorLiveData.hasActiveObservers()){
                        this.custodianStateMediatorLiveData.removeSource(custodianStateLiveData);
                    }
                    this.custodianStateMediatorLiveData.setValue(custodianStateMediatorLiveData);
                });
    }


    //save individual inventory scanned in room ,,and send them as a bunch at the end o the scanning
    public void saveScannedAsset(InventoryCheckOutReport assetScanned) {
        inventoryCheckOutReportsRepo.saveInventorys(assetScanned);
    }


}

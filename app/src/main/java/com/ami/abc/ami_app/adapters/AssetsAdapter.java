package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.models.Asset;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssetsAdapter extends RecyclerView.Adapter<AssetsAdapter.MyViewHolder> {

    private List<Asset> assetList;
    private Context context;

    public AssetsAdapter(List<Asset> assetList, Context context) {
        this.assetList = assetList;
        this.context = context;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.asset_code)
        TextView asset_code;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.location)
        TextView location;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    @NonNull
    @Override
    public AssetsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.verified_asset_layout, parent, false);


        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull AssetsAdapter.MyViewHolder holder, int i) {
        Asset asset = assetList.get(i);
        holder.asset_code.setText(asset.getCode());
        holder.name.setText("Item Name : " + asset.getName());

        holder.location.setText("Located at : " + asset.getBuilding() + ", " + asset.getRoom());


    }

    @Override
    public int getItemCount() {
        return assetList.size();
    }


}

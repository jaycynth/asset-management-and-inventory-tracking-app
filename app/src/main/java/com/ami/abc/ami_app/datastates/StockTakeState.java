package com.ami.abc.ami_app.datastates;

import com.ami.abc.ami_app.models.FailError;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.StockTake;

public class StockTakeState {

    private StockTake stockTake;
    private String message;
    private Throwable errorThrowable;
    private FailError failError;
    private LoginError loginError;

    public StockTakeState(StockTake stockTake) {
        this.stockTake = stockTake;
        this.message = null;
        this.errorThrowable = null;
        this.failError = null;
        this.loginError = null;
    }

    public StockTakeState(String message) {
        this.message = message;
        this.errorThrowable = null;
        this.stockTake = null;
        this.failError = null;
        this.loginError = null;

    }

    public StockTakeState(Throwable errorThrowable) {
        this.errorThrowable = errorThrowable;
        this.stockTake = null;
        this.message = null;
        this.failError = null;
        this.loginError = null;
    }

    public StockTakeState(FailError failError) {
        this.failError = failError;
        this.errorThrowable = null;
        this.message = null;
        this.stockTake = null;
        this.loginError = null;
    }

    public StockTakeState(LoginError loginError) {
        this.loginError = loginError;
        this.message = null;
        this.errorThrowable = null;
        this.stockTake = null;
        this.failError = null;
    }

    public LoginError getLoginError() {
        return loginError;
    }

    public FailError getFailError() {
        return failError;
    }

    public StockTake getStockTake() {
        return stockTake;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getErrorThrowable() {
        return errorThrowable;
    }
}

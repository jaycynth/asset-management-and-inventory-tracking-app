package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.RoomState;
import com.ami.abc.ami_app.repos.RoomRepo;

public class RoomViewModel extends AndroidViewModel {

    private MediatorLiveData<RoomState> roomStateMediatorLiveData;
    private RoomRepo roomRepo;

    public RoomViewModel(Application application){
        super(application);
        roomStateMediatorLiveData = new MediatorLiveData<>();
        roomRepo = new RoomRepo(application);
    }

    public LiveData<RoomState> getRoomResponse(){
        return roomStateMediatorLiveData;
    }

    public void getRooms(String accessToken){

        LiveData<RoomState> roomStateLiveData = roomRepo.getAllRooms(accessToken);
        roomStateMediatorLiveData.addSource(roomStateLiveData,
                roomStateMediatorLiveData -> {
                    if (this.roomStateMediatorLiveData.hasActiveObservers()){
                        this.roomStateMediatorLiveData.removeSource(roomStateLiveData);
                    }
                    this.roomStateMediatorLiveData.setValue(roomStateMediatorLiveData);
                });

    }
}

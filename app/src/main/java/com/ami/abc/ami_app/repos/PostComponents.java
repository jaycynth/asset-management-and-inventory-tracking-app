package com.ami.abc.ami_app.repos;

import androidx.lifecycle.LiveData;

import com.ami.abc.ami_app.models.CacheComponents;

import java.util.List;

public interface PostComponents {
    LiveData<List<CacheComponents>> getAllComponents();
    void toggleList();
}

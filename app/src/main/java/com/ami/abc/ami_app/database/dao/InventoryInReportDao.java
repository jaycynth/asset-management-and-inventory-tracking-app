package com.ami.abc.ami_app.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ami.abc.ami_app.models.InventoryInReport;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface InventoryInReportDao {
    @Insert(onConflict = REPLACE)
    void saveInventory(InventoryInReport inventoryReports);
    //observe the list in the room database

    @Query("SELECT * FROM  InventoryInReport")
    LiveData<List<InventoryInReport>> getInventorysReports();

    @Query("SELECT * FROM InventoryInReport")
    List<InventoryInReport> getAll();

    @Delete
    void delete(InventoryInReport inventoryReports);

    //delete all the inventory in the room database upon sending information to the inventory verified table
    @Query("DELETE FROM InventoryInReport")
    void deleteInventorys();


}

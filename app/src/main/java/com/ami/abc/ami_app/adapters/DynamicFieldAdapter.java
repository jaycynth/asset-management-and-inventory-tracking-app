package com.ami.abc.ami_app.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.ui.OnEditTextChanged;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DynamicFieldAdapter extends RecyclerView.Adapter<DynamicFieldAdapter.MyViewHolder> {

    private List<String> assetList;
    private Context context;
    OnEditTextChanged onEditTextChanged;

    public DynamicFieldAdapter(List<String> assetList, Context context, OnEditTextChanged onEditTextChanged) {
        this.assetList = assetList;
        this.context = context;
        this.onEditTextChanged = onEditTextChanged;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dynamic_one)
        TextView asset_code;

        @BindView(R.id.dynamic_one_value)
        EditText value;




        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

    }

    @NonNull
    @Override
    public DynamicFieldAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dynamic_fields, parent, false);


        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull DynamicFieldAdapter.MyViewHolder holder, int pos) {
        holder.asset_code.setText(assetList.get(pos));

        holder.value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                  onEditTextChanged.onTextChanged(pos,charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return assetList.size();
    }


}


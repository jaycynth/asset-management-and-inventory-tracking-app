package com.ami.abc.ami_app.viewmodels;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.ami.abc.ami_app.datastates.AllVerifiedStatusState;
import com.ami.abc.ami_app.datastates.AssetState;
import com.ami.abc.ami_app.datastates.InventoryState;
import com.ami.abc.ami_app.datastates.ShelvesState;
import com.ami.abc.ami_app.datastates.StoreState;
import com.ami.abc.ami_app.repos.AllVerifiedAssetsRepo;
import com.ami.abc.ami_app.repos.AssetRepo;
import com.ami.abc.ami_app.repos.InventoryRepo;
import com.ami.abc.ami_app.repos.ShelvesRepo;
import com.ami.abc.ami_app.repos.StoreRepo;

public class SummaryViewModel extends AndroidViewModel {
    //for all assets
    private MediatorLiveData<AssetState> assetStateMediatorLiveData;
    private AssetRepo assetRepo;

    //for all inventory
    private MediatorLiveData<InventoryState> inventoryStateMediatorLiveData;
    private InventoryRepo inventoryRepo;


    //for verified assets
    private MediatorLiveData<AllVerifiedStatusState> allVerifiedStatusStateMediatorLiveData;
    private AllVerifiedAssetsRepo allVerifiedAssetsRepo;

    private MediatorLiveData<StoreState> storeStateMediatorLiveData;
    private StoreRepo storeRepo;

    private MediatorLiveData<ShelvesState> shelvesStateMediatorLiveData;
    private ShelvesRepo shelvesRepo;


    public SummaryViewModel(Application application) {
        super(application);
        storeStateMediatorLiveData = new MediatorLiveData<>();
        storeRepo = new StoreRepo(application);

        assetStateMediatorLiveData = new MediatorLiveData<>();
        assetRepo = new AssetRepo(application);

        allVerifiedStatusStateMediatorLiveData = new MediatorLiveData<>();
        allVerifiedAssetsRepo = new AllVerifiedAssetsRepo(application);

        inventoryStateMediatorLiveData = new MediatorLiveData<>();
        inventoryRepo = new InventoryRepo(application);

        shelvesStateMediatorLiveData = new MediatorLiveData<>();
        shelvesRepo = new ShelvesRepo(application);


    }

    public LiveData<StoreState> getStoreResponse(){
        return storeStateMediatorLiveData;
    }

    public void getStores(String accessToken){

        LiveData<StoreState> storeStateLiveData = storeRepo.getAllStores(accessToken);
        storeStateMediatorLiveData.addSource(storeStateLiveData,
                storeStateMediatorLiveData -> {
                    if (this.storeStateMediatorLiveData.hasActiveObservers()){
                        this.storeStateMediatorLiveData.removeSource(storeStateLiveData);
                    }
                    this.storeStateMediatorLiveData.setValue(storeStateMediatorLiveData);
                });

    }

    public LiveData<ShelvesState> getShelvesResponse(){
        return shelvesStateMediatorLiveData;
    }

    public void getShelves(String accessToken){

        LiveData<ShelvesState> shelvesStateLiveData = shelvesRepo.getAllShelvess(accessToken);
        shelvesStateMediatorLiveData.addSource(shelvesStateLiveData,
                shelvesStateMediatorLiveData -> {
                    if (this.shelvesStateMediatorLiveData.hasActiveObservers()){
                        this.shelvesStateMediatorLiveData.removeSource(shelvesStateLiveData);
                    }
                    this.shelvesStateMediatorLiveData.setValue(shelvesStateMediatorLiveData);
                });

    }

    public LiveData<AssetState> getAllAssetResponse() {
        return assetStateMediatorLiveData;
    }

    public void getAllAssets(String accessToken) {
        LiveData<AssetState> assetStateLiveData = assetRepo.getAllAssets(accessToken);
        assetStateMediatorLiveData.addSource(assetStateLiveData,
                assetStateMediatorLiveData -> {
                    if (this.assetStateMediatorLiveData.hasActiveObservers()) {
                        this.assetStateMediatorLiveData.removeSource(assetStateLiveData);
                    }
                    this.assetStateMediatorLiveData.setValue(assetStateMediatorLiveData);
                });
    }

    public LiveData<AllVerifiedStatusState> getAllVerifiedAssetsResponse(){
        return allVerifiedStatusStateMediatorLiveData;
    }

    public void getAllVerifiedAssets(String accessToken){

        LiveData<AllVerifiedStatusState> allVerifiedStatusStateLiveData = allVerifiedAssetsRepo.getVerifiedAssets(accessToken);
        allVerifiedStatusStateMediatorLiveData.addSource(allVerifiedStatusStateLiveData,
                allVerifiedStatusStateMediatorLiveData -> {
                    if (this.allVerifiedStatusStateMediatorLiveData.hasActiveObservers()){
                        this.allVerifiedStatusStateMediatorLiveData.removeSource(allVerifiedStatusStateLiveData);
                    }
                    this.allVerifiedStatusStateMediatorLiveData.setValue(allVerifiedStatusStateMediatorLiveData);
                });

    }

    public LiveData<InventoryState> getAllInventoryResponse(){
        return inventoryStateMediatorLiveData;
    }

    public void getAllInventory(String accessToken){
        LiveData<InventoryState> inventoryStateLiveData = inventoryRepo.getAllInventory(accessToken);
        inventoryStateMediatorLiveData.addSource(inventoryStateLiveData,
                inventoryStateMediatorLiveData -> {
                    if (this.inventoryStateMediatorLiveData.hasActiveObservers()){
                        this.inventoryStateMediatorLiveData.removeSource(inventoryStateLiveData);
                    }
                    this.inventoryStateMediatorLiveData.setValue(inventoryStateMediatorLiveData);
                });
    }
}

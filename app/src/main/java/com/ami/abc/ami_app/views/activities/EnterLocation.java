package com.ami.abc.ami_app.views.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.ami.abc.ami_app.R;
import com.ami.abc.ami_app.adapters.BuildingArrayAdapter;
import com.ami.abc.ami_app.adapters.CustomArrayAdapter;
import com.ami.abc.ami_app.adapters.DepartmentArrayAdapter;
import com.ami.abc.ami_app.adapters.RoomArrayAdapter;
import com.ami.abc.ami_app.models.AllBuildings;
import com.ami.abc.ami_app.models.AllDepartments;
import com.ami.abc.ami_app.models.Building;
import com.ami.abc.ami_app.models.Department;
import com.ami.abc.ami_app.models.Divisions;
import com.ami.abc.ami_app.models.GetBuildingsInDivision;
import com.ami.abc.ami_app.models.GetDivisions;
import com.ami.abc.ami_app.models.GetRoomInBuilding;
import com.ami.abc.ami_app.models.LoginError;
import com.ami.abc.ami_app.models.Room;
import com.ami.abc.ami_app.utils.SharedPreferenceManager;
import com.ami.abc.ami_app.viewmodels.MainViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterLocation extends AppCompatActivity {

    @BindView(R.id.spin_kit)
    ProgressBar progressBar;
    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.division)
    AutoCompleteTextView division;
    @BindView(R.id.building)
    AutoCompleteTextView building;
    @BindView(R.id.room)
    AutoCompleteTextView room;
    @BindView(R.id.department)
    AutoCompleteTextView department;
    @BindView(R.id.choose)
    AutoCompleteTextView choose;

    String Tag;

    private List<Building> buildingList = new ArrayList<>();
    private List<String> buildingName = new ArrayList<>();

    List<Room> roomList = new ArrayList<>();
    private List<String> roomName = new ArrayList<>();

    private List<String> departmentName = new ArrayList<>();
    private List<String> divisionName = new ArrayList<>();

    private List<String> choiceList = new ArrayList<>();

    @BindView(R.id.drop_down_division)
    ImageView drop_down_division;
    @BindView(R.id.drop_down_building)
    ImageView drop_down_building;
    @BindView(R.id.drop_down_room)
    ImageView drop_down_room;
    @BindView(R.id.drop_down_department)
    ImageView drop_down_department;
    @BindView(R.id.drop_down_choice)
    ImageView drop_down_choice;

    List<Divisions> divisionsList = new ArrayList<>();


    MainViewModel mainViewModel;
    String accessToken;

    @BindView(R.id.main_spin_kit)
    ProgressBar main_spin_kit;

    @BindView(R.id.layout_location_details)
    LinearLayout layout_location_details;

    @BindView(R.id.scan_room)
    Button scan_room;

    int buildingId, divisionId;

    String roomResult, build, div;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_location);
        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.location_title));

        Intent getTag = getIntent();
        Tag = getTag.getStringExtra("TAG");
        roomResult = getTag.getStringExtra("scanResult");
        if (roomResult != null) {
            room.setText(roomResult);
        }
        build = getTag.getStringExtra("building");
        if (build != null) {
            building.setText(build);
        }

        div = getTag.getStringExtra("division");
        if (div != null) {
            division.setText(div);
        }


        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        accessToken = SharedPreferenceManager.getInstance(this).getToken();
        if (accessToken != null) {
            main_spin_kit.setVisibility(View.VISIBLE);
            layout_location_details.setAlpha(0.3f);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            mainViewModel.getDivisions("Bearer " + accessToken);
            //mainViewModel.getAllBuildings("Bearer " + accessToken);
            mainViewModel.getDepartments("Bearer " + accessToken);

        }

        mainViewModel.getDivisionResponse().observe(this, divisionState -> {
            assert divisionState != null;
            if (divisionState.getGetDivisions() != null) {
                handleGetAllDivisions(divisionState.getGetDivisions());
            }

            if (divisionState.getMessage() != null) {
                handleNetworkResponse(divisionState.getMessage());
            }

            if (divisionState.getErrorThrowable() != null) {
                handleError(divisionState.getErrorThrowable());
            }
            if (divisionState.getLoginError() != null) {
                handleUnAuthorized(divisionState.getLoginError());
            }
        });

        mainViewModel.getBuildingsResponse().observe(this, buildingsState -> {
            assert buildingsState != null;
            if (buildingsState.getAllBuildings() != null) {
                handleGetAllBuildings(buildingsState.getAllBuildings());
            }

            if (buildingsState.getMessage() != null) {
                handleNetworkResponse(buildingsState.getMessage());
            }

            if (buildingsState.getErrorThrowable() != null) {
                handleError(buildingsState.getErrorThrowable());
            }
            if (buildingsState.getLoginError() != null) {
                handleUnAuthorized(buildingsState.getLoginError());
            }
        });

        mainViewModel.getBuildingsInDivisionResponse().observe(this, buildingsInDivisionState -> {
            if (buildingsInDivisionState.getBuildingInDivision() != null) {
                handleGetBuildingsInDivision(buildingsInDivisionState.getBuildingInDivision());
            }

            if (buildingsInDivisionState.getMessage() != null) {
                handleNetworkResponse(buildingsInDivisionState.getMessage());
            }

            if (buildingsInDivisionState.getErrorThrowable() != null) {
                handleError(buildingsInDivisionState.getErrorThrowable());
            }
            if (buildingsInDivisionState.getLoginError() != null) {
                handleUnAuthorized(buildingsInDivisionState.getLoginError());
            }
        });

        mainViewModel.getRoomInBuildingResponse().observe(this, getRoomInBuildingState -> {
            assert getRoomInBuildingState != null;
            if (getRoomInBuildingState.getGetRoomInBuilding() != null) {
                handleGetAllRoomsInBuilding(getRoomInBuildingState.getGetRoomInBuilding());
            }

            if (getRoomInBuildingState.getMessage() != null) {
                handleNetworkResponse(getRoomInBuildingState.getMessage());
            }

            if (getRoomInBuildingState.getErrorThrowable() != null) {
                handleError(getRoomInBuildingState.getErrorThrowable());
            }

            if (getRoomInBuildingState.getLoginError() != null) {
                handleUnAuthorized(getRoomInBuildingState.getLoginError());
            }
        });

        mainViewModel.getDepartmentResponse().observe(this, departmentState -> {
            assert departmentState != null;
            if (departmentState.getAllDepartments() != null) {
                handleGetAllDepartments(departmentState.getAllDepartments());
            }

            if (departmentState.getMessage() != null) {
                handleNetworkResponse(departmentState.getMessage());
            }

            if (departmentState.getErrorThrowable() != null) {
                handleError(departmentState.getErrorThrowable());
            }
            if (departmentState.getLoginError() != null) {
                handleUnAuthorized(departmentState.getLoginError());
            }
        });


        choiceList.add("Camera");
        choiceList.add("Scanner");
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, choiceList);
        choose.setAdapter(adapter5);

        choose.setOnClickListener(v -> choose.showDropDown());
        drop_down_choice.setOnClickListener(v -> choose.showDropDown());

        scan_room.setOnClickListener(v -> {
            Intent scan = new Intent(EnterLocation.this, ScanActivity.class);
            scan.putExtra("TAG", Tag);
            scan.putExtra("building", building.getText().toString().trim());
            scan.putExtra("division", division.getText().toString().trim());
            scan.putExtra("from", "SCAN_ROOM");
            startActivity(scan);

        });

        submit.setOnClickListener(v -> {
            //Ensuring user has filled all the details before proceeding
            String mBuilding = building.getText().toString().trim();
            String mRoom = room.getText().toString().trim();
            String mDepartment = department.getText().toString().trim();
            String mDivision = division.getText().toString().trim();
            String mChoice = choose.getText().toString().trim();

            if (TextUtils.isEmpty(mBuilding)) {
                Toast.makeText(this, "Please input the Building name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mRoom)) {
                Toast.makeText(this, "Please input the Room name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mDepartment)) {
                Toast.makeText(this, "Please input Department name", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mChoice)) {
                Toast.makeText(this, "Please select the action you want to use", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(mDivision)) {
                Toast.makeText(this, "Please select Division ", Toast.LENGTH_SHORT).show();
            } else {
                submit.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(this, "Please wait as we confirm your location", Toast.LENGTH_SHORT).show();
                //send a get request to get the details of your location
                //returns the name of the location then you can start updating data

                if (mChoice.equals("Camera")) {

                    new Handler().postDelayed(() -> {
                        progressBar.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);

                        switch (Tag) {
                            case "A":
                            case "B":
                            case "C":
                            case "I":
                            case "J": {
                                Intent scan = new Intent(EnterLocation.this, ScanActivity.class);
                                scan.putExtra("building", mBuilding);
                                scan.putExtra("room", mRoom);
                                scan.putExtra("mDepartment", mDepartment);
                                scan.putExtra("division", mDivision);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                            default: {
                                break;
                            }
                        }

                    }, 2000);

                } else {
                    new Handler().postDelayed(() -> {
                        progressBar.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);

                        switch (Tag) {
                            case "A": {
                                uploadAssetTypeDialog(mBuilding, mRoom, mDepartment, mDivision, Tag);
                                break;
                            }
                            case "B": {

                                Intent scan = new Intent(EnterLocation.this, VerifyAsset.class);
                                scan.putExtra("building", mBuilding);
                                scan.putExtra("room", mRoom);
                                scan.putExtra("mDepartment", mDepartment);
                                scan.putExtra("division", mDivision);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }
                            case "C": {
                                Intent scan = new Intent(EnterLocation.this, AssetMove.class);
                                scan.putExtra("building", mBuilding);
                                scan.putExtra("room", mRoom);
                                scan.putExtra("mDepartment", mDepartment);
                                scan.putExtra("division", mDivision);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }


                            case "I": {
                                Intent scan = new Intent(EnterLocation.this, AssetCheckOutActivity.class);
                                scan.putExtra("building", mBuilding);
                                scan.putExtra("room", mRoom);
                                scan.putExtra("mDepartment", mDepartment);
                                scan.putExtra("division", mDivision);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                            case "J": {
                                Intent scan = new Intent(EnterLocation.this, AssetCheckInActivity.class);
                                scan.putExtra("building", mBuilding);
                                scan.putExtra("room", mRoom);
                                scan.putExtra("mDepartment", mDepartment);
                                scan.putExtra("division", mDivision);
                                scan.putExtra("TAG", Tag);
                                startActivity(scan);
                                break;
                            }

                            default: {
                                break;
                            }
                        }

                    }, 2000);


                }
            }
        });
    }

    private void uploadAssetTypeDialog(String mBuilding, String mRoom, String mDepartment, String mDivision, String tag) {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.choose_upload_type);
        alert.setCancelable(false);
        alert.setPositiveButton("Asset Upload", (dialog, which1) -> {

                    Intent scan = new Intent(EnterLocation.this, AssetIn.class);
                    scan.putExtra("building", mBuilding);
                    scan.putExtra("room", mRoom);
                    scan.putExtra("mDepartment", mDepartment);
                    scan.putExtra("division", mDivision);
                    scan.putExtra("TAG", Tag);
                    startActivity(scan);

                    dialog.dismiss();

                }
        );

        alert.setNegativeButton("Asset Upload With Components", (dialog, which1) -> {
            Intent scan = new Intent(EnterLocation.this, AssetInComponents.class);
            scan.putExtra("building", mBuilding);
            scan.putExtra("room", mRoom);
            scan.putExtra("mDepartment", mDepartment);
            scan.putExtra("division", mDivision);
            scan.putExtra("TAG", Tag);
            startActivity(scan);
            dialog.dismiss();
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void handleGetAllDivisions(GetDivisions getDivisions) {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        CustomArrayAdapter adapter6 = new CustomArrayAdapter(this, (ArrayList<Divisions>) getDivisions.getData());
        division.setAdapter(adapter6);

        drop_down_division.setOnClickListener(v -> division.showDropDown());
        division.setOnClickListener(v -> division.showDropDown());

        Log.d("Divisions", getDivisions.getData().toString());

        division.setOnItemClickListener((adapterView, view, i, l) -> {
            Divisions mDivisionName = (Divisions) adapterView.getItemAtPosition(i);
            if (divisionsList.get(i).getName().equalsIgnoreCase(mDivisionName.getName())) {
                divisionId = divisionsList.get(i).getId();
                if (divisionId == 0) {


                    Building none = new Building(0, "None");
                    buildingList.add(none);

                    BuildingArrayAdapter adapter = new BuildingArrayAdapter(this, buildingList);
                    building.setAdapter(adapter);

                    drop_down_building.setOnClickListener(v -> building.showDropDown());
                    building.setOnClickListener(v -> building.showDropDown());

                } else {

                    main_spin_kit.setVisibility(View.VISIBLE);
                    layout_location_details.setAlpha(0.3f);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (!buildingName.isEmpty()) {

                        buildingName.clear();
                    }

                    mainViewModel.getBuildingsInDivision(divisionId, "Bearer " + accessToken);
                }

            }
        });
        divisionsList = getDivisions.getData();
        Divisions none = new Divisions(0, "None");
        divisionsList.add(none);
        for (Divisions department : divisionsList) {
            String dName = department.getName();
            divisionName.add(dName);
        }

    }

    private void handleGetBuildingsInDivision(GetBuildingsInDivision buildingInDivision) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        buildingList = buildingInDivision.getBuildings();

        Building none = new Building(0, "None");
        buildingList.add(none);

        BuildingArrayAdapter adapter = new BuildingArrayAdapter(this, buildingList);
        building.setAdapter(adapter);

        drop_down_building.setOnClickListener(v -> building.showDropDown());
        building.setOnClickListener(v -> building.showDropDown());

        building.setOnItemClickListener((parent, view, position, id) -> {
            Building mBuildingName = (Building) parent.getItemAtPosition(position);

            if (mBuildingName.getName().equalsIgnoreCase("None")) {
                buildingId = 0;

                Room emptyRoom = new Room(0, "None");
                roomList.add(emptyRoom);

                displayRoomDropDown();

//                roomName.add("None");

            } else if (buildingList.get(position).getName().equalsIgnoreCase(mBuildingName.getName())) {
                buildingId = buildingList.get(position).getId();

                main_spin_kit.setVisibility(View.VISIBLE);
                layout_location_details.setAlpha(0.3f);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if (!roomName.isEmpty()) {
                    roomName.clear();
                }
                mainViewModel.getRoomsInBuilding(buildingId, "Bearer " + accessToken);
            }

        });

        if (buildingList.isEmpty()) {
            buildingName.add("None");
        } else {
            for (Building building : buildingList) {

                String bName = building.getName();
                buildingName.add(bName);
            }
        }

    }

    private void displayRoomDropDown() {
        RoomArrayAdapter roomArrayAdapter = new RoomArrayAdapter(this, roomList);
        room.setAdapter(roomArrayAdapter);

        drop_down_room.setOnClickListener(v -> room.showDropDown());
        room.setOnClickListener(v -> room.showDropDown());
    }

    private void handleGetAllRoomsInBuilding(GetRoomInBuilding getRoomInBuilding) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        roomList = getRoomInBuilding.getRooms();

        Log.d("ROOMS", String.valueOf(getRoomInBuilding.getRooms().size()));
        if (getRoomInBuilding.getRooms().size() == 0) {

            Room emptyRoom = new Room(0, "None");
            roomList.add(emptyRoom);
//            roomName.add("None");

        }
        displayRoomDropDown();


    }

    private void handleGetAllBuildings(AllBuildings allBuildings) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        buildingList = allBuildings.getBuildings();
        Building none = new Building(0, "None");

        buildingList.add(none);
        for (Building building : buildingList) {
            String bName = building.getName();

            buildingName.add(bName);
        }


    }

    private void handleGetAllDepartments(AllDepartments allDepartments) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);

        List<Department> departmentList = allDepartments.getData();

        Department none = new Department(0, "None");
        departmentList.add(none);

        DepartmentArrayAdapter adapter = new DepartmentArrayAdapter(this, departmentList);
        department.setAdapter(adapter);

        drop_down_department.setOnClickListener(v -> department.showDropDown());
        department.setOnClickListener(v -> department.showDropDown());

        for (Department department : departmentList) {
            String dName = department.getName();
            departmentName.add(dName);
        }

    }

    private void handleError(Throwable errorThrowable) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        if (errorThrowable instanceof IOException) {
            Toast.makeText(this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.error_occured1), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleNetworkResponse(String message) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private void handleUnAuthorized(LoginError loginError) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        main_spin_kit.setVisibility(View.GONE);
        layout_location_details.setAlpha(1);
        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show();
//        Intent auth = new Intent(this, LoginActivity.class);
//        startActivity(auth);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent scanOptions = new Intent(EnterLocation.this, AssetsOptions.class);
        startActivity(scanOptions);
    }
}
